/*
 *  I2W_Twitgoo.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 26/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "I2W_Twitgoo.h"
I2W_Twitgoo::I2W_Twitgoo(){
	this->username = this->password = "";
	this->requiredParams.push_back("username");
	this->requiredParams.push_back("password");
}

I2W_Twitgoo::~I2W_Twitgoo(){
}

int I2W_Twitgoo::addParams(ofxHttpForm& apiForm, string filename, map<string,string> params){
	int count = 0;
	stringstream messageBuilder;
	
	if(this->username == ""){
		messageBuilder << "I2W_Twitgoo: error adding parameters: no username set." << endl;
		ofLog(OF_LOG_ERROR, messageBuilder.str());
		messageBuilder.str("");
	}
	else
		count++;
	
	if(this->password == ""){
		messageBuilder << "I2W_Twitgoo: error adding parameters: no password set." << endl;
		ofLog(OF_LOG_ERROR, messageBuilder.str());
		messageBuilder.str("");
	}
	else
		count++;
	
	if(filename == ""){
		messageBuilder << "I2W_Twitgoo: error adding parameters: no filename set." << endl;
		ofLog(OF_LOG_ERROR, messageBuilder.str());
		messageBuilder.str("");
	}
	else
		count++;
	
	if(count == 3){
		apiForm.addFormField("username", this->username);
		apiForm.addFormField("password", this->password);
		apiForm.addFormField("media", filename, true);
		if(params.find("message") != params.end() && params["message"] != "")
			apiForm.addFormField("message", params["message"]);
			
		this->formReady = true;
	}
	return count;
}

string I2W_Twitgoo::getFormAction(){
	return TWITGOO_ACTION_UPLOAD_AND_POST;
}

string I2W_Twitgoo::getFormName(){
	return TWITGOO_FORM_NAME;
}

bool I2W_Twitgoo::setParameters(map<string,string> params){
	bool result = true;
	stringstream messageBuilder;
	
	if(this->checkParams(params)){
		messageBuilder << "I2W_Twitgoo:all required params found!";
		ofLog(OF_LOG_NOTICE, messageBuilder.str());
		messageBuilder.str("");
		// because we passed the checkParams, we assume we have everything we need
		this->username = params["username"];
		this->password = params["password"];
	}
	else
		result = false;
	
	return result;
}
