/*
 *  ofxImage2WebServiceIndex.h
 *  openFrameworks
 *
 *  Created by Pat Long on 26/02/10.
 *  Copyright 2010 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_SERVICE_INDEX
#define _OFX_IMAGE2WEB_SERVICE_INDEX

#include "Image2WebService.h"
#include "Image2WebHttpService.h"

#include "I2W_Email.h"
#include "I2W_Twitgoo.h"

#endif
