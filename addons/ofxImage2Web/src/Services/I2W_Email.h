/*
 *  I2W_Email.h
 *  openFrameworks
 *
 *  Created by Pat Long on 03/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_SERVICE_EMAIL
#define _OFX_IMAGE2WEB_SERVICE_EMAIL

#include "Image2WebService.h"
#include "ofxMailUtils.h"
#include "ofxTI_Utils.h"

#define I2W_EMAIL_DEFAULT_TIMEOUT -1

extern int currentLogLevel;

class I2W_Email : public Image2WebService{
protected:
	ofxSmtpClientUtils* smtp;
	ofxMailMessage* message;
	int timeout;
	
	bool validEmail(string emailAddress);
	
	virtual bool checkParams(map<string,string> params);
	
public:
	I2W_Email();
	~I2W_Email();
	
	void setTimeout(int timeout=I2W_EMAIL_DEFAULT_TIMEOUT);
	
	bool connectSMTP(string smtp_server, int smtpPort, string smtp_username, string smtp_password);
	
	virtual void update();
	virtual bool postImage(ofImage image, map<string,string> params);
};

#endif
