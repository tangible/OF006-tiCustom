/*
 *  I2W_Twitgoo.h
 *  openFrameworks
 *
 *  Created by Pat Long on 26/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_SERVICE_TWITGOO
#define _OFX_IMAGE2WEB_SERVICE_TWITGOO

#include "Image2WebHttpService.h"

#define TWITGOO_FORM_NAME	"twitgooApiForm"

#define TWITGOO_ACTION_UPLOAD			"http://twitgoo.com/api/upload"
#define TWITGOO_ACTION_UPLOAD_AND_POST	"http://twitgoo.com/api/uploadAndPost"

//#define TWITGOO_ACTION_UPLOAD			"http://localhost/formTest1.php"
//#define TWITGOO_ACTION_UPLOAD_AND_POST	"http://localhost/formTest1.php"

class I2W_Twitgoo : public Image2WebHttpService{
	protected:
		string username, password;
	
		virtual int addParams(ofxHttpForm& apiForm, string filename, map<string,string> params);
		virtual bool setParameters(map<string,string> params);
		virtual string getFormAction();
		virtual string getFormName();
	
	public:
		I2W_Twitgoo();
		~I2W_Twitgoo();
};

#endif
