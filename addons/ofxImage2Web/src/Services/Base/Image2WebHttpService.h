/*
 *  Image2WebHttpService.h
 *  openFrameworks
 *
 *  Created by Pat Long on 26/02/10.
 *  Copyright 2010 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_HTTP_SERVICE
#define _OFX_IMAGE2WEB_HTTP_SERVICE

#include "ofxHttpEvents.h"
#include "ofxHttpUtils.h"

#include "Image2WebService.h"

extern ofxHttpUtils ofxHttpUtil;
extern ofxHttpEventManager ofxHttpEvents;

class Image2WebHttpService : public Image2WebService, public ofxHttpEventListener{
	protected:
		bool formReady;

		virtual int addParams(ofxHttpForm& apiForm, string filename, map<string,string> params)=0;
		virtual bool setParameters(map<string,string> params)=0;
		virtual bool isFormReady();
		virtual string getFormName()=0;
		virtual string getFormAction()=0;
	
	public:
		Image2WebHttpService();
		~Image2WebHttpService();
	
		virtual bool postImage(ofImage image, map<string,string> params);
		virtual int postImageForm(ofxHttpForm& apiForm, bool multipart=true, int method=OFX_HTTP_POST);

	// ofxHttpEventListener implementation
		virtual void newResponse( ofxHttpResponse & response );
		virtual void newError(string & error);
};

#endif
