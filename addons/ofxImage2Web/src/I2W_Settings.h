/*
 *  I2W_Settings.h
 *  openFrameworks
 *
 *  Created by Pat Long on 11/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_SETTINGS
#define _OFX_IMAGE2WEB_SETTINGS

#include "ofxXmlSettings.h"

#define DEFAULT_I2W_XML_FILENAME "i2w_settings.xml"
#define DEFAULT_I2W_SETTINGS_IMAGE_NAME "i2w_temp_image.png"

class I2W_Settings : public ofxXmlSettings{
	protected:
		string dataRoot;
		string imageName;
	
		bool twitterEnabled;
		string twitterDefaultMessage;
		string twitterMessageAppend;
	
		bool emailEnabled;
		string emailSubject;
		bool emailMessageGUIEnabled;
		string emailDefaultMessage;
		string emailMessageAppend;
	
		bool emailSmtpEnabled;
		string emailSmtpServer;
		int emailSmtpPort;
		string emailSmtpUsername;
		string emailSmtpPassword;
	
		bool emailHubEnabled;
	
		int hubInstallationID;
		string hubSavePath;
		
	public:
		I2W_Settings();
		~I2W_Settings();
	
		bool loadSettings(string filename=DEFAULT_I2W_XML_FILENAME);
	
		string getDataRoot();
		string getImageName();
	
		// twitter settings
		bool isTwitterEnabled();
		string getTwitterDefaultMessage();
		string getTwitterMessageAppend();
		
		// email settings
		bool isEmailEnabled();
		string getEmailSubject();
		bool isEmailMessageGUIEnabled();
		string getEmailDefaultMessage();
		string getEmailMessageAppend();
		
		// email smtp settings
		bool isEmailSmtpEnabled();
		string getEmailSmtpServer();
		int getEmailSmtpPort();
		string getEmailSmtpUsername();
		string getEmailSmtpPassword();
	
		bool isEmailHubEnabled();
	
		// Hub settings
		int getHubInstallationID();
		string getHubSavePath();
};

#endif
