/*
 *  I2W_Settings.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 11/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "I2W_Settings.h"

I2W_Settings::I2W_Settings(){
	dataRoot = "";
	imageName = DEFAULT_I2W_SETTINGS_IMAGE_NAME;
	
	twitterEnabled = true;
	twitterDefaultMessage = "";
	twitterMessageAppend = "";
	
	emailEnabled = true;
	emailSubject = "";
	emailMessageGUIEnabled = true;
	emailDefaultMessage = "";
	emailMessageAppend = "";
	
	emailSmtpEnabled = true;
	emailSmtpServer = "";
	emailSmtpPort = 26;
	emailSmtpUsername = "";
	emailSmtpPassword = "";
	
	emailHubEnabled = false;
	
	hubInstallationID = 0;
	hubSavePath = "";
}

I2W_Settings::~I2W_Settings(){
}

bool I2W_Settings::loadSettings(string filename){
	if(filename != "" && ofxXmlSettings::loadFile(ofToDataPath(filename))){
		if(this->tagExists("Image2Web")){
			this->pushTag("Image2Web");
			
			dataRoot = this->getValue("data_root", "");
			imageName = this->getValue("image_name", DEFAULT_I2W_SETTINGS_IMAGE_NAME);
			
			if(this->tagExists("twitter")){
				this->pushTag("twitter");
			
				twitterEnabled = (this->getValue("enabled", "true") == "true");
				
				twitterDefaultMessage = this->getValue("default_message", "posted from Image2Web add-on for #openFrameworks");
				if(twitterDefaultMessage.length() > 1){
					if(twitterDefaultMessage.at(0) == '"' && twitterDefaultMessage.at(twitterDefaultMessage.length()-1) == '"')
						twitterDefaultMessage = twitterDefaultMessage.substr(1, twitterDefaultMessage.length()-2);
				}
				
				twitterMessageAppend = this->getValue("message_append", "");
				if(twitterMessageAppend.length() > 1){
					if(twitterMessageAppend.at(0) == '"' && twitterMessageAppend.at(twitterMessageAppend.length()-1) == '"')
						twitterMessageAppend = twitterMessageAppend.substr(1, twitterMessageAppend.length()-2);
				}
				
				this->popTag(); // /twitter
			}
			
			if(this->tagExists("email")){
				this->pushTag("email");
				
				emailEnabled = (this->getValue("enabled", "true") == "true");
				
				emailSubject = this->getValue("subject", "Sent from Image2Web");
				if(emailSubject.length() > 1){
					if(emailSubject.at(0) == '"' && emailSubject.at(emailSubject.length()-1) == '"')
						emailSubject = emailSubject.substr(1, emailSubject.length()-2);
				}
				
				
				emailMessageGUIEnabled = (this->getValue("message_gui_enabled", "true") == "true");
				
				emailDefaultMessage = this->getValue("default_message", "");
				if(emailDefaultMessage.length() > 1){
					if(emailDefaultMessage.at(0) == '"' && emailDefaultMessage.at(emailDefaultMessage.length()-1) == '"')
						emailDefaultMessage = emailDefaultMessage.substr(1, emailDefaultMessage.length()-2);
				}
				
				emailMessageAppend = this->getValue("message_append", "");
				if(emailMessageAppend.length() > 1){
					if(emailMessageAppend.at(0) == '"' && emailMessageAppend.at(emailMessageAppend.length()-1) == '"')
						emailMessageAppend = emailMessageAppend.substr(1, emailMessageAppend.length()-2);
				}
				
				if(this->tagExists("smtp")){
					this->pushTag("smtp");
				
					emailSmtpEnabled = (this->getValue("enabled", "true") == "true");
					emailSmtpServer = this->getValue("server", "");
					emailSmtpPort = this->getValue("port", 26);
					emailSmtpUsername = this->getValue("username", "");
					emailSmtpPassword = this->getValue("password", "");
					
					this->popTag(); // /smtp
				}
				
				
				emailHubEnabled = (this->getValue("hub_enabled", "false") == "true");
				
				this->popTag(); // /email
			}
			
			if(this->tagExists("hub")){
				this->pushTag("hub");
				
				hubInstallationID = this->getValue("installation_id", 0);
				hubSavePath = this->getValue("save_path", "");
				
				this->popTag(); // hub
			}
			
			this->popTag(); // /Image2Web
			return true;
		}
	}
	return false;
}

string I2W_Settings::getDataRoot(){
	return this->dataRoot;
}

string I2W_Settings::getImageName(){
	return this->imageName;
}

// twitter settings
bool I2W_Settings::isTwitterEnabled(){
	return this->twitterEnabled;
}

string I2W_Settings::getTwitterDefaultMessage(){
	return this->twitterDefaultMessage;
}

string I2W_Settings::getTwitterMessageAppend(){
	return this->twitterMessageAppend;
}

// email settings
bool I2W_Settings::isEmailEnabled(){
	return this->emailEnabled;
}

string I2W_Settings::getEmailSubject(){
	return this->emailSubject;
}

bool I2W_Settings::isEmailMessageGUIEnabled(){
	return this->emailMessageGUIEnabled;
}

string I2W_Settings::getEmailDefaultMessage(){
	return this->emailDefaultMessage;
}

string I2W_Settings::getEmailMessageAppend(){
	return this->emailMessageAppend;
}

// email smtp settings
bool I2W_Settings::isEmailSmtpEnabled(){
	return this->emailSmtpEnabled;
}

string I2W_Settings::getEmailSmtpServer(){
	return this->emailSmtpServer;
}

int I2W_Settings::getEmailSmtpPort(){
	return this->emailSmtpPort;
}

string I2W_Settings::getEmailSmtpUsername(){
	return this->emailSmtpUsername;
}

string I2W_Settings::getEmailSmtpPassword(){
	return this->emailSmtpPassword;
}

bool I2W_Settings::isEmailHubEnabled(){
	return this->emailHubEnabled;
}

// hub settings
int I2W_Settings::getHubInstallationID(){
	return this->hubInstallationID;
}

string I2W_Settings::getHubSavePath(){
	return this->hubSavePath;
}
