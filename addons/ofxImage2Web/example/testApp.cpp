#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofSetLogLevel(OF_LOG_VERBOSE);
	ofSetFrameRate(60);
//	ofBackground(0, 0, 0);
	ofBackground(255, 255, 255);
	
	i2wSettings.loadSettings("i2w_settings.xml");
	
	videoGrabber.setDeviceID(0);
	videoGrabber.initGrabber(320, 240);
	
	testCount = 1;
}

//--------------------------------------------------------------
void testApp::update(){
	videoGrabber.update();
	if(videoGrabber.isFrameNew())
		image.setFromPixels(videoGrabber.getPixels(), videoGrabber.getWidth(), videoGrabber.getHeight(), OF_IMAGE_COLOR);

}

//--------------------------------------------------------------
void testApp::draw(){
	image.draw(50, 50);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	stringstream message;
	map<string,string> params;
	int result = -1;
	
	switch(key){
		case 'e':
			if(i2wSettings.isEmailEnabled()){
				message << i2wSettings.getEmailDefaultMessage() << i2wSettings.getEmailMessageAppend() << endl;
//				message << "testing Image2Web - email API from #openFrameworks (test #" << this->testCount++ << ")" << endl;
				cout << "sending email with message \"" << message.str() << "\"" << endl;
				image.saveImage(TEMP_IMAGE_NAME);
				
				params["smtp_server"] = i2wSettings.getEmailSmtpServer();
				params["smtp_port"] = ofToString(i2wSettings.getEmailSmtpPort(), 0);
				params["smtp_username"] = i2wSettings.getEmailSmtpUsername();
				params["smtp_password"] = i2wSettings.getEmailSmtpPassword();

				params["sender"] = "plong00@gmail.com";
				params["recipients"] = "plong00@gmail.com";
				params["subject"] = i2wSettings.getEmailSubject();
				params["body"] = message.str();
				
				if(email.postImage(image, params))
					result = email.getResponseCode();
				
				cout << "response: " << result << ":" << endl;
				message.str("");
			}
			
			break;
			
		case 't':
			if(i2wSettings.isTwitterEnabled()){
//				message << "testing Image2Web - twitgoo API from #openFrameworks (test #" << this->testCount++ << ")" << endl;
				message << i2wSettings.getTwitterDefaultMessage() << i2wSettings.getTwitterMessageAppend() << endl;
				cout << "uploading to twitgoo with message \"" << message.str() << "\"" << endl;
				image.saveImage(TEMP_IMAGE_NAME);			

				params["username"] = "yourusername";
				params["password"] = "yourpassword";
				params["message"] = message.str();
				if(twitgoo.postImage(image, params))
					result = twitgoo.getResponseCode();
				
				cout << "response: " << result << ":" << endl;
				message.str("");
			}

			break;
	}
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}
