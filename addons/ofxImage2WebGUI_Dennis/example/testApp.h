#ifndef _TEST_APP
#define _TEST_APP

#include "ofMain.h"
#include "ofxImage2Web.h"
#include "ofxImage2WebGUI.h"

#define TEMP_IMAGE_NAME "image2web_upload_temp.jpg"

class testApp : public ofBaseApp{
	protected:
		ofVideoGrabber videoGrabber;
		ofImage image;
		int testCount;

		ofxImage2WebGUI i2w_gui;

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

};

#endif
