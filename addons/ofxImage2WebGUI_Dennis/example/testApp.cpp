#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofSetLogLevel(OF_LOG_VERBOSE);
	ofSetFrameRate(60);
	ofBackground(0, 0, 0);
//	ofBackground(255, 255, 255);
	ofEnableAlphaBlending();
	
	videoGrabber.setDeviceID(0);
	videoGrabber.initGrabber(320, 240);
	
	i2w_gui.loadSettings("i2w_settings.xml");
	i2w_gui.init(50, 620, 120+6*8, 30);
	i2w_gui.setHorizontalSpacing(8);
	i2w_gui.setTabAnimationMode(TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOM, 10);
	
	ShishaTabContainer* newTab;
/**	newTab = new I2WGUI_Flickr();
	newTab->init(50, 345);
	i2w_gui.addContainer(newTab);*/
	
	newTab = new I2WGUI_Twitter();
	newTab->init(50, 345);
	i2w_gui.addContainer(newTab);
	
/**	newTab = new I2WGUI_Facebook();
	newTab->init(50, 345);
	i2w_gui.addContainer(newTab);*/
	
	newTab = new I2WGUI_Email();
	newTab->init(50, 345);
	i2w_gui.addContainer(newTab);
//	((I2WGUI_Email*)newTab)->connectSMTP();
	
	i2w_gui.setImage(&image);
	
	testCount = 1;
}

//--------------------------------------------------------------
void testApp::update(){
	videoGrabber.update();
	if(videoGrabber.isFrameNew()){
		image.setFromPixels(videoGrabber.getPixels(), videoGrabber.getWidth(), videoGrabber.getHeight(), OF_IMAGE_COLOR);
		image.mirror(true, false);
	}

	i2w_gui.update();
}

//--------------------------------------------------------------
void testApp::draw(){
	image.draw(50, 50);
	i2w_gui.draw();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
	stringstream message;
	int result = -1;
	
	i2w_gui.checkKeyPressed(key);
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	i2w_gui.checkKeyReleased(key);
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
	i2w_gui.checkCursorHover(x, y, MOUSE_ID, 1.0, 1.0);
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
	i2w_gui.checkCursorDrag(x, y, button, MOUSE_ID, 1.0, 1.0);
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
	i2w_gui.checkCursorPress(x, y, button, MOUSE_ID, 1.0, 1.0);
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	i2w_gui.checkCursorRelease(x, y, button, MOUSE_ID, 1.0, 1.0);
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}
