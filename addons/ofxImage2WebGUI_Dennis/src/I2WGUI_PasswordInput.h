/*
 *  I2WGUI_PasswordInput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 05/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _I2WGUI_PASSWORD_INPUT_H
#define _I2WGUI_PASSWORD_INPUT_H

#include "I2WGUI_TextInput.h"

class I2WGUI_PasswordInput : public I2WGUI_TextInput{
protected:
	virtual string getOutputValue();
	virtual string getOutputLine(int i);
	
public:
	I2WGUI_PasswordInput();
	~I2WGUI_PasswordInput();
};

#endif
