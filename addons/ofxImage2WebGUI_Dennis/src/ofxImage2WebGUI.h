/*
 *  ofxImage2WebGUI.h
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI
#define _OFX_IMAGE2WEB_GUI

#include "ofxImage2Web.h"
#include "ofxGUIShisha.h"

#include "ofxImage2WebGUIServiceIndex.h"
#include "I2WGUI_Theme.h"

class ofxImage2WebGUI : public ShishaTabManager{
protected:
	I2W_Settings settings;
	
	virtual void drawChildren();
	
public:
	ofxImage2WebGUI();
	~ofxImage2WebGUI();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
	virtual void addContainer(ShishaTabContainer* container, bool inheritAnimation=true);
	void loadSettings(string settingFileName=DEFAULT_I2W_XML_FILENAME);
	I2W_Settings* getSettings();
	void setImage(ofImage* image);
	
	//---------------------- %DENNIS% changes:
	ofImage backGroundPic; //image that goes behind the shisha map - eg window frame, with disclaimer, etc.
	//---

	
};

#endif
