/*
 *  I2WGUI_Theme.h
 *  openFrameworks
 *
 *  Created by Pat Long on 04/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _I2WGUI_THEME_H
#define _I2WGUI_THEME_H

#include "ofxGUIShisha.h"

#define I2WGUI_FONT_ROOT			"i2w_gui/fonts/"
#define I2WGUI_DEFAULT_FONT			I2WGUI_FONT_ROOT"Geogtq-Md.otf"
#define I2WGUI_DEFAULT_FONT_SIZE	9

class I2WGUI_Theme : public TI_Theme{
protected:
	string basePath;
	
	virtual void initTheme();
	
public:
	I2WGUI_Theme();
	~I2WGUI_Theme();
	
	void setBasePath(string basePath="");
	
};

#endif
