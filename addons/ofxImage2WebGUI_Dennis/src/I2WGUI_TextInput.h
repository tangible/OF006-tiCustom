/*
 *  I2WGUI_TextInput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 05/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _I2WGUI_TEXT_INPUT_H
#define _I2WGUI_TEXT_INPUT_H

#include "ofxGUIShisha.h"

class I2WGUI_TextInput : public TextInput{
	protected:
		bool firstClick;
		string defaultValue;
	
		ofImage defaultValueImage;
		bool hasDefaultValueImage;
	
		virtual void onPress(string cursorID);
	
	public:
		I2WGUI_TextInput();
		~I2WGUI_TextInput();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
		virtual void draw(float x, float y, float w, float h, bool borders);
	
		virtual void reset();
		virtual void setDefaultValue(string defaultValue="");
		virtual void setDefaultValueImage(string filename="");
		virtual string getString(string selector=DEFAULT_SELECTOR);
		virtual void setString(string value="");
};

#endif
