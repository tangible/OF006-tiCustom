/*
 *  I2WGUI_Facebook.h
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI_SERVICE_FACEBOOK
#define _OFX_IMAGE2WEB_GUI_SERVICE_FACEBOOK

#include "I2WGUI_TabContainer.h"

class I2WGUI_Facebook : public I2WGUI_TabContainer{
protected:
	
	virtual void sliceInterface();

	virtual void doCancel();
	virtual void doSubmit();

public:
	I2WGUI_Facebook();
	~I2WGUI_Facebook();
	
	virtual string getServiceName(){ return "Facebook"; };
	
	virtual void setDefaultFormValues();
};

#endif
