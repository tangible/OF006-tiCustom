/*
 *  I2WGUI_HubXml.h
 *  graffitiwall 7.3
 *
 *  Created by Eli Smiles on 11-04-18.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI_SERVICE_HUBXML
#define _OFX_IMAGE2WEB_GUI_SERVICE_HUBXML

#include "I2WGUI_TabContainer.h"
#include "ofxTI_HubXml.h"

class I2WGUI_HubXml : public I2WGUI_TabContainer{
	protected:
		ofxTI_HubXml hub;
		
		void setHubSettings(int installationID, string savePath);
			
	public:
		I2WGUI_HubXml();
		~I2WGUI_HubXml();
	
		virtual void setSettings(I2W_Settings* settings);
};

#endif
