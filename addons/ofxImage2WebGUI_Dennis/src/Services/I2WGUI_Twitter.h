/*
 *  I2WGUI_Twitter.h
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI_SERVICE_TWITTER
#define _OFX_IMAGE2WEB_GUI_SERVICE_TWITTER

#include "I2WGUI_TabContainer.h"
#include "I2W_Twitgoo.h"

class I2WGUI_Twitter : public I2WGUI_TabContainer{
protected:
	I2W_Twitgoo* twitgoo;
	
	virtual void sliceInterface();

	virtual void doCancel();
	virtual void doSubmit();
	
public:
	I2WGUI_Twitter();
	~I2WGUI_Twitter();
	
	virtual string getServiceName(){ return "Twitter"; };
	virtual void setDefaultFormValues();
};

#endif
