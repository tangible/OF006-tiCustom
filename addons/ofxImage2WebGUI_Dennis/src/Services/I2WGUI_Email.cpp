/*
 *  I2WGUI_Email.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 04/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "I2WGUI_Email.h"

I2WGUI_Email::I2WGUI_Email(){
	this->email = new I2W_Email();
	this->setSmtpParams();
	this->form = NULL;
	this->submitWait = -1;
	formLoaded = false;
};

I2WGUI_Email::~I2WGUI_Email(){
	delete this->email;
}

void I2WGUI_Email::setSettings(I2W_Settings* settings){
	I2WGUI_TabContainer::setSettings(settings);
	if(settings != NULL){
		this->setSmtpParams(settings->getEmailSmtpServer(), settings->getEmailSmtpPort(), settings->getEmailSmtpUsername(), settings->getEmailSmtpPassword());
	}
}

void I2WGUI_Email::setSmtpParams(string smtp_server, int smtpPort, string smtp_username, string smtp_password){
	this->smtp_server = smtp_server;
	this->smtp_port = smtpPort;
	this->smtp_username = smtp_username;
	this->smtp_password = smtp_password;
}

void I2WGUI_Email::sliceInterface(){
	if(this->formLoaded && this->form != NULL){
		
		string dataRoot = "";
		if(this->settings != NULL)
			dataRoot = this->settings->getDataRoot();
		
		string baseDefaultImage = "";
		stringstream fileBuilder;
		fileBuilder << dataRoot << I2WGUI_FORM_ROOT << "form_" << this->getServiceName() << "_default_";
		baseDefaultImage = fileBuilder.str();
		fileBuilder.str("");
		
		int xOffset = 30;
		int yOffset = 10;
		
		BasicButton* newButton;
		newButton = new BasicButton();
		newButton->init(xOffset + 349,yOffset + 58, 92, 33);
		newButton->setName("Submit");
		this->form->mapElement(newButton, xOffset +349, yOffset +58, 92, 33);
		
		newButton = new BasicButton();
		newButton->init(xOffset +448, yOffset +59, 92, 33);
		newButton->setName("Cancel");
		this->form->mapElement(newButton, xOffset +448, yOffset +59, 92, 33);
		
		newButton = new BasicButton();
		newButton->init(xOffset +511, yOffset +18, 29, 29);
		newButton->setName("Close");
		this->form->mapElement(newButton, xOffset + 511, yOffset +18, 29, 29);
		
		I2WGUI_TextInput* newTextInput;
		newTextInput = new I2WGUI_TextInput();
//		newTextInput->init(1, 41, 165, 31);
		newTextInput->init(xOffset +1, yOffset +19, 340, 33);
		newTextInput->setName("email_from");
		newTextInput->setDefaultValue("Your Email");		
		fileBuilder << baseDefaultImage << "email_from.png";
		newTextInput->setDefaultValueImage(fileBuilder.str());
		fileBuilder.str("");
		newTextInput->setPadding(6, 3, -3);
//		this->form->mapElement(newTextInput, 1, 41, 165, 31, true, true);
		this->form->mapElement(newTextInput, xOffset +1, yOffset +19, 340, 33, true, true);

		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("email_from");
		
		newTextInput = new I2WGUI_TextInput();
//		newTextInput->init(170, 41, 165, 33);
		newTextInput->init(xOffset +1, yOffset +58, 340, 34);
		newTextInput->setName("email_to");
		newTextInput->setDefaultValue("Friend's Email");
		fileBuilder << baseDefaultImage << "email_to.png";
		newTextInput->setDefaultValueImage(fileBuilder.str());
		fileBuilder.str("");
		newTextInput->setPadding(6, 3, -3);
//		this->form->mapElement(newTextInput, 170, 41, 165, 31, true, true);
		this->form->mapElement(newTextInput, xOffset +1, yOffset +57, 340, 34, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("email_to");
		
		if(settings == NULL || (settings != NULL && settings->isEmailMessageGUIEnabled())){
			newTextInput = new I2WGUI_TextInput();
			newTextInput->init(xOffset +1, yOffset +97, 540, 34);
			newTextInput->setName("message");
			newTextInput->setDefaultValue("Message");
			fileBuilder << baseDefaultImage << "email_message.png";
			newTextInput->setDefaultValueImage(fileBuilder.str());
			fileBuilder.str("");
			newTextInput->setPadding(6, 3, -3);
			this->form->mapElement(newTextInput, xOffset +1, yOffset + 97, 540, 34, true, true);
			newTextInput->setMaxLineCount(1);
			newTextInput->setMaxTextLength(-1);
			formInputNames.push_back("message");
		}
		this->setDefaultFormValues();
	}
}

void I2WGUI_Email::setDefaultFormValues(){
	if(this->formLoaded && this->form != NULL){
		this->form->setStringValue("email_from", "Your Email");
		this->form->setStringValue("email_to", "Friend's Email");
		if(settings->isEmailMessageGUIEnabled())
			this->form->setStringValue("message", "Message");
	}
}

void I2WGUI_Email::update(){
	I2WGUI_TabContainer::update();
	
	if(this->submitWait != -1 && ofGetFrameNum() >= this->submitWait){
		this->submitWait = -1;
		this->doSubmit1();
	}
}

void I2WGUI_Email::doCancel(){
	this->setDefaultFormValues();
	if(this->statusImage != NULL)
		this->statusImage->setActiveImage("");
}

void I2WGUI_Email::doSubmit(){
	if(this->submitWait == -1 && !this->statusUpdater.isThreadRunning() && this->formLoaded && this->form != NULL && this->image != NULL){
		this->setStatusImage("processing");
		this->submitWait = ofGetFrameNum()+1;
	}
}

void I2WGUI_Email::doSubmit1(){
	if(!this->statusUpdater.isThreadRunning() && this->formLoaded && this->form != NULL && this->image != NULL && !this->requestCapture){		
		//	if(this->formLoaded && this->form != NULL && this->image != NULL){
		map<string,string> params;
		int result = -1;
		
		string imageName = this->image->getFileName();

		if(imageName == "")
			imageName = settings->getDataRoot()+settings->getImageName();
		
		imageName = imageName.substr(0 , imageName.length() - 3)  + "jpg";
//		cout << "image name: " << imageName << endl;		
//		this->image->saveImage(imageName);
		this->image->saveJpg(imageName, 92);
		
		params["smtp_server"] = this->smtp_server;
		params["smtp_port"] = ofToString(this->smtp_port, 0);
		params["smtp_username"] = this->smtp_username;
		params["smtp_password"] = this->smtp_password;
		
		params["sender"] = this->form->getString("email_from");
		params["recipients"] = this->form->getString("email_to");
		params["subject"] = (settings != NULL)?settings->getEmailSubject():"Sent from I2WGUI";
		
		if ( this->form->getString("email_to").size() < 3 ||  this->form->getString("email_to") == "Friend's Email" )
		{
			params["recipients"] =  params["sender"];
			
		}
		
		log_sender =  this->form->getString("email_from");
		log_recipient = this->form->getString("email_to");
		
		stringstream messageBuilder;
		string formMsg = "";
		if(settings->isEmailMessageGUIEnabled())
			this->form->getString("message");
		if(settings != NULL){
			if(formMsg == "") formMsg = settings->getEmailDefaultMessage();
			messageBuilder << formMsg << settings->getEmailMessageAppend() << endl;

		}
		else
			messageBuilder << formMsg << endl;
		params["body"] = messageBuilder.str();
		messageBuilder.str("");
		
		if(email->postImage(*(this->image), params)){
			this->setStatusImage("processing");
			statusUpdater.start(email);
		}
		else
			this->setStatusImage("error");
	}
}



//-----%DENNIS%----------------
void I2WGUI_Email::logData(){
	
	//------------------output email here:
	string 	message = "loading xml data";
	cout << message << endl;
	
	string path = this->settings->getDataRoot() + "/../";
	cout << path << endl;
	
	
	//we load our settings file
	if( mailList.loadFile(path+ "emails.xml") ){
		message = "emails.xml loaded!";
	}else{
		message = "unable to load emails.xml check data/ folder";
		
		mailList.addTag("data");
	}
	cout << message << endl;
	
	mailList.pushTag("data", 0);
	
	int target = mailList.addTag("email");
	mailList.setValue("email:sender",log_sender, target);
	mailList.setValue("email:recipient",log_recipient, target);
	cout << log_sender << endl;
	cout << log_recipient << endl;
	
	mailList.popTag();
	
	mailList.saveFile(path + "emails.xml");
	cout << endl << "saved XML FILE - mailList.xml" << endl;
	//----------------------------
}

//------------------------------