/*
 *  I2WGUI_Facebook.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 04/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "I2WGUI_Facebook.h"

I2WGUI_Facebook::I2WGUI_Facebook(){
};

I2WGUI_Facebook::~I2WGUI_Facebook(){
}

void I2WGUI_Facebook::sliceInterface(){
	if(this->formLoaded && this->form != NULL){
		BasicButton* newButton;
		newButton = new BasicButton();
		newButton->init(349, 41, 92, 31);
		newButton->setName("Submit");
		this->form->mapElement(newButton, 349, 41, 92, 31);
		
		newButton = new BasicButton();
		newButton->init(448, 41, 92, 31);
		newButton->setName("Cancel");
		this->form->mapElement(newButton, 448, 41, 92, 31);
		
		newButton = new BasicButton();
		newButton->init(511, 2, 29, 29);
		newButton->setName("Close");
		this->form->mapElement(newButton, 511, 2, 29, 29);
		
		I2WGUI_TextInput* newTextInput;
		newTextInput = new I2WGUI_TextInput();
		newTextInput->init(1, 41, 165, 31);
		newTextInput->setName("username");
		newTextInput->setDefaultValue("User");
		newTextInput->setPadding(3, 3, -3);
		this->form->mapElement(newTextInput, 1, 41, 165, 31, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("username");
		
		newTextInput = new I2WGUI_PasswordInput();
		newTextInput->init(170, 41, 165, 31);
		newTextInput->setName("password");
		newTextInput->setDefaultValue("Password");
		newTextInput->setPadding(3, 3, -3);
		this->form->mapElement(newTextInput, 170, 41, 165, 31, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("password");
		
		newTextInput = new I2WGUI_TextInput();
		newTextInput->init(1, 81, 540, 31);
		newTextInput->setName("comment");
		newTextInput->setDefaultValue("Comment");
		newTextInput->setPadding(3, 3, -3);
		this->form->mapElement(newTextInput, 1, 81, 540, 31, true, true);
		newTextInput->setMaxLineCount(1);
		newTextInput->setMaxTextLength(-1);
		formInputNames.push_back("comment");
		this->setDefaultFormValues();
	}
}

void I2WGUI_Facebook::setDefaultFormValues(){
	if(this->formLoaded && this->form != NULL){
		this->form->setStringValue("username", "User");
		this->form->setStringValue("password", "Password");
		this->form->setStringValue("comment", "Comment");
	}
}

void I2WGUI_Facebook::doCancel(){
	this->setDefaultFormValues();
	if(this->statusImage != NULL)
		this->statusImage->setActiveImage("");
}

void I2WGUI_Facebook::doSubmit(){
	cout << "facebook submit!" << endl;
	if(this->formLoaded && this->form != NULL){
		cout << "\tusername:" << this->form->getString("username") << ":" << endl;
		cout << "\tpassword:" << this->form->getString("password") << ":" << endl;
		cout << "\tcomment:" << this->form->getString("comment") << ":" << endl;
		//	image->saveImage(DEFAULT_I2W_TEMP_IMAGE_NAME);
	}
	this->setDefaultFormValues();
}