/*
 *  I2WGUI_HubEmail.h
 *  graffitiwall 7.3
 *
 *  Created by Eli Smiles on 11-04-18.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */
#ifndef _OFX_IMAGE2WEB_GUI_SERVICE_HUBEMAIL
#define _OFX_IMAGE2WEB_GUI_SERVICE_HUBEMAIL

#include "I2WGUI_HubXml.h"

class I2WGUI_HubEmail : public I2WGUI_HubXml{
	protected:
		int submitWait;
	
		virtual void sliceInterface();
		virtual void doCancel();
		virtual void doSubmit();
		bool doSubmit1();
	
		bool validEmail(string emailAddress);
	
		//---------------%DENNIS%-------
		ofxXmlSettings mailList;
		string	log_sender;
		string  log_recipient;
	
		void logData();
		//--------------------------
		
	public:
		I2WGUI_HubEmail();
		~I2WGUI_HubEmail();
	
		virtual void update();
	
		virtual string getServiceName(){ return "HubEmail"; };
	
		virtual void setDefaultFormValues();
};

#endif
