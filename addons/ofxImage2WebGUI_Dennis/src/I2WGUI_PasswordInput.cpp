/*
 *  I2WGUI_PasswordInput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 05/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "I2WGUI_PasswordInput.h"

I2WGUI_PasswordInput::I2WGUI_PasswordInput(){
}

I2WGUI_PasswordInput::~I2WGUI_PasswordInput(){
}

string I2WGUI_PasswordInput::getOutputValue(){
	if(this->textValue != this->defaultValue){
		stringstream output;
		if(this->getLabel() != "")
			output << this->getLabel();
		for(int i=0; i < this->textValue.length(); i++)
			output << "*";
		return output.str();
	}
	return I2WGUI_TextInput::getOutputValue();
}

string I2WGUI_PasswordInput::getOutputLine(int i){
	string theVal = I2WGUI_TextInput::getOutputLine(i);
	if(theVal != "" && theVal != this->defaultValue){
		stringstream output;
		for(int i=0; i < theVal.length(); i++)
			output << "*";
		theVal = output.str();
		output.str("");
	}
	return theVal;
}
