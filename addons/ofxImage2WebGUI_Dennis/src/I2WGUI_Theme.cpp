/*
 *  I2WGUI_Theme.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 04/03/10.
 *  Copyright 2010 Tangible Interaction Inc. All rights reserved.
 *
 */

#include "I2WGUI_Theme.h"

I2WGUI_Theme::I2WGUI_Theme(){
	this->basePath = "";
}

I2WGUI_Theme::~I2WGUI_Theme(){
}

void I2WGUI_Theme::initTheme(){
	this->themeName = "Image2WebGUI Theme";
	this->themeColours[SHISHA_COLOUR_DEFAULT] =		new ColourRGBA(255, 255, 255, 255); // should never change this one
	this->themeColours[SHISHA_COLOUR_BACKGROUND] =	new ColourRGBA(51, 51, 51, 0);
	this->themeColours[SHISHA_COLOUR_BORDER] =		new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEOVER] = new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEDOWN] = new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEDRAG] = new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_FOREGROUND_1] =	new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_FOREGROUND_2] =	new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_CURSORS] =		new ColourRGBA(255, 255, 255);
	
	this->font.loadFont(this->basePath+I2WGUI_DEFAULT_FONT, I2WGUI_DEFAULT_FONT_SIZE);
	
	this->containerBorders = false;
	this->panelBorders = false;
	this->buttonBorders = false;
	this->imageBorders = false;
	this->textOutputBorders = false;
	this->textInputBorders = false;
	this->initted = true;
}

void I2WGUI_Theme::setBasePath(string basePath){
	this->basePath = basePath;
}
