/*
 *  I2WGUI_TabContainer.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 02/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "I2WGUI_TabContainer.h"

void I2WGUI_TabContainer::I2WGUI_Tab::drawAsRectangle(float x, float y, float w, float h, bool borders){
	if(this->container != NULL){
		ofImage* icon = ((I2WGUI_TabContainer*)this->container)->getIcon();
		if(icon != NULL){
			icon->draw(x, y, w, h);
			if(borders){
				ofNoFill();
				ofRect(x, y, w, h);
				ofFill();
			}
		}
		else
			ShishaTabContainer::ShishaTab::drawAsRectangle(x, y, w, h, borders);
	}
}

I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::I2WGUI_TabContainerStatusUpdater(){
	this->service = NULL;
	this->container = NULL;
}

I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::~I2WGUI_TabContainerStatusUpdater(){
	stop();
}

void I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::start(Image2WebService* service){
	if(service != NULL){
		this->setService(service);
		this->startThread(true, false);
	}
}

void I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::stop(){
	this->stopThread();
}

void I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::threadedFunction(){
	while(this->isThreadRunning()){
		if(this->service != NULL){
			this->service->update();
			int result = this->service->getResponseCode();
			if(result != I2W_RESPONSE_WAITING){
				if(this->container != NULL){
					if(result == I2W_RESPONSE_SUCCESS)
					{
						this->container->setStatusImage("success");
					//-----------------------
					}
					else if(result == I2W_RESPONSE_SEND_ERROR)
						this->container->setStatusImage("error");
					else if(result == I2W_RESPONSE_RESP_ERROR)
						this->container->setStatusImage("error");
					this->container->setDefaultFormValues();
				}
				stop();
			}
		}
		else{
			stop();
		}
	}
}

void I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::setContainer(I2WGUI_TabContainer* container){
	this->container = container;
}

void I2WGUI_TabContainer::I2WGUI_TabContainerStatusUpdater::setService(Image2WebService* service){
	this->service = service;
}

I2WGUI_TabContainer::I2WGUI_TabContainer(){
	this->settings = NULL;
	this->iconLoaded = false;
	this->form = NULL;
	this->statusImage = NULL;
	this->formLoaded = false;
	this->submitPressed = false;
	this->cancelPressed = false;
	this->closePressed = false;
	
	this->mtKeyboard = NULL;
	
	this->image = NULL;
	this->statusUpdater.setContainer(this);
	
	
}

I2WGUI_TabContainer::~I2WGUI_TabContainer(){
}

void I2WGUI_TabContainer::setSettings(I2W_Settings* settings){
	this->settings = settings;
}

void I2WGUI_TabContainer::setImage(ofImage* image){
	this->image = image;
}

void I2WGUI_TabContainer::setStatusImage(string imageName){
	this->statusImage->setActiveImage(imageName);

	if(imageName != "processing")
		submitRequested = false;
	
	//---------------------- %DENNIS% changes:
	if ( imageName == "success" )
	{
		autoClose = true;
		closeCounterNow = ofGetElapsedTimeMillis();
		closeCounterLast = closeCounterNow;		
	}
	//----------------------
}

ofImage* I2WGUI_TabContainer::getImage(){
	return this->image;
}

void I2WGUI_TabContainer::initContainerTab(){
	this->containerTab = new I2WGUI_TabContainer::I2WGUI_Tab(this);
	
	float tabWidth = DEFAULT_TABCONTAINER_TAB_WIDTH;
	float tabHeight = DEFAULT_TABCONTAINER_TAB_HEIGHT;
	
	this->containerTab->init(this->x+this->width/2.0-tabWidth/2.0, this->y-tabHeight, tabWidth, tabHeight);
	this->containerTab->setLabel("");
	this->containerTab->setName(this->getName()+":slideTab");
}

void I2WGUI_TabContainer::init(float x, float y, float width, float height, string name, int elementID){
	if(name == "")
		name = "i2w_"+this->getServiceName()+"_tabContainer";
	ShishaTabContainer::init(x, y, width, height, name, elementID);
	this->elementType = SHISHA_TYPE_I2WGUI_TAB_CONTAINER;
	if(this->containerTab != NULL)
		this->containerTab->setName(this->getName()+":slideTab");
	this->initInterface();
	this->sliceInterface();
	
	this->submitRequested = false;
	this->requestCapture = false;
	
	//---------------------- %DENNIS% changes:
	autoClose = false;
	closeCounterNow = ofGetElapsedTimeMillis();
	closeCounterLast = closeCounterNow;
	closeCounterPeriod = 2200;
	//-----------------------------
	
	
}

void I2WGUI_TabContainer::initInterface(){
	this->setHorizontalSpacing(0);
	this->setVerticalSpacing(0);
	this->setFlowMode(SHISHA_FLOW_MODE_HORIZONTAL);
	
	string dataRoot = "";
	if(this->settings != NULL)
		dataRoot = this->settings->getDataRoot();
	
	// load the icon
	stringstream fileBuilder;
	fileBuilder << dataRoot << I2WGUI_ICON_ROOT << "icon_" << this->getServiceName() << ".png";
	cout << "load:" << ofToDataPath(fileBuilder.str()) << ":" << endl;
	this->iconLoaded = this->icon.loadImage(fileBuilder.str());
	fileBuilder.str("");
	
	if(this->iconLoaded)
		this->containerTab->setDimensions(this->icon.getWidth(), this->icon.getHeight());
	
	// load the form image map
	int formOffsetX =  13;
	fileBuilder << dataRoot << I2WGUI_FORM_ROOT << "form_" << this->getServiceName() << ".png";
	cout << "filebuilder: " << fileBuilder.str() << endl;
	this->form = new ShishaImageMap();
	this->formLoaded = this->form->loadImageMap(fileBuilder.str());
	this->form->init(formOffsetX, 0, this->form->getWidth(), this->form->getHeight()-268 );
	fileBuilder.str("");
	
	if(this->formLoaded){
		this->form->setName("form_"+this->getServiceName());
		this->setDimensions(formOffsetX+this->form->getWidth(), this->form->getHeight());
		this->addElement(this->form, true);
		this->form->setPosition(formOffsetX +this->form->getX(), this->form->getY());
	}
	
	// load the status messages
	
	this->statusImage = new ImageList();
	this->statusImage->init(this->getX()+45, this->getY() + 10, 495, 15);
	this->statusImage->setName("Status");
	fileBuilder << dataRoot << I2WGUI_STATUS_ROOT << "status_error.png";
	this->statusImage->addImage(fileBuilder.str(), "error");
	fileBuilder.str("");
	fileBuilder << dataRoot << I2WGUI_STATUS_ROOT << "status_processing.png";
	this->statusImage->addImage(fileBuilder.str(), "processing");
	fileBuilder.str("");
	fileBuilder << dataRoot << I2WGUI_STATUS_ROOT << "status_success.png";
	this->statusImage->addImage(fileBuilder.str(), "success");
	fileBuilder.str("");
	
	//	this->statusImage->loadDirectory(fileBuilder.str());
	this->addElement(this->statusImage, false);
	
	this->statusFader.addTransition(new ColourRGBA(255, 255, 255, 255), new ColourRGBA(255, 255, 255, 128), 100);
	this->statusFader.addTransition(new ColourRGBA(255, 255, 255, 128), new ColourRGBA(255, 255, 255, 255), 100);
	
	// load the multi-touch keyboard
	this->initMTKeyboard();
	this->setDrawBackground(false);
}

void I2WGUI_TabContainer::initMTKeyboard(){
	string dataRoot = "";
	if(this->settings != NULL)
		dataRoot = this->settings->getDataRoot();
	this->mtKeyboard = new ShishaKeyboard();
	this->mtKeyboard->setImageBase(dataRoot+"i2w_gui/interface/keyboard/");
	//this->mtKeyboard->init(0, 200);
	
	this->mtKeyboard->init(20, this->form->getHeight());
	this->setDimensions(this->mtKeyboard->getWidth()+100, this->getHeight()+this->mtKeyboard->getHeight() + 100);
	this->addElement(this->mtKeyboard, true);
	this->mtKeyboard->setPosition(this->mtKeyboard->getX() +30, this->mtKeyboard->getY() );
	this->mtKeyboard->setDrawBackground(false);
	
}

void I2WGUI_TabContainer::animateHide(){
	ShishaTabContainer::animateHide();
	this->doCancel();
	if(this->formLoaded && this->form != NULL){
		this->closePressed = false;
		this->form->setBoolValue("Close", false);
	}
	
}

void I2WGUI_TabContainer::drawChildren(){
	
	
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		
		bool colourChanged = false;
		if(((ShishaElement*)(*it).second)->getName() == "Status" && ((ImageList*)(*it).second)->getActiveImageName() == "processing"){
			this->statusFader.getNextColour()->activateColour();
			colourChanged = true;
		}
		((ShishaElement*)(*it).second)->draw();
		if(colourChanged)
			this->getTheme()->setColour();
		

	}
	//cout << endl << endl;
}

void I2WGUI_TabContainer::updateMTKeyboard(){
	if(this->formLoaded && this->form != NULL && this->mtKeyboard != NULL){
		ShishaElement* element;
		bool hasElement = false;
		for(int i=0; i < this->formInputNames.size(); i++){
			element = this->form->selectElement(this->formInputNames[i]);
			if(element != NULL && element->getBool()){
				this->mtKeyboard->setElement(element);
				hasElement = true;
			}
		}
		if(!hasElement)
			this->mtKeyboard->setElement(NULL);
	}
}

void I2WGUI_TabContainer::update(){
	ShishaTabContainer::update();
	this->updateMTKeyboard();
	
/**	if(this->getBool("ShishaKeyboard.any")){
		int theKey = this->getInt("ShishaKeyboard.popChar");
		if(theKey != SELECTOR_UNIDENTIFIED_INT)
			this->form->checkKeyPressed(theKey);
	}*/
	
	if(this->formLoaded && this->form != NULL){
		bool cSubmitState = this->form->getBool("Submit");
		
		if(!this->submitPressed && cSubmitState){
			this->submitPressed = true;
			
			if(!this->submitRequested){
				this->submitRequested = true;
				this->requestCapture = true;
				this->doSubmit();
			}
		}
		else if(this->submitPressed && !cSubmitState){
			this->submitPressed = false;
		}
		
		bool cCancelState = this->form->getBool("Cancel");
		if(!this->cancelPressed && cCancelState){
			this->cancelPressed = true;
			this->submitRequested = false;
			this->requestCapture = false;
			this->doCancel();
		}
		else if(this->cancelPressed && !cCancelState){
			this->cancelPressed = false;
		}
		
		if(this->isActive()){
			bool cCloseState = this->form->getBool("Close");
			if(!this->closePressed && cCloseState){
				this->closePressed = true;
				this->doHide();
			}
			else if(this->closePressed && !cCloseState){
				this->closePressed = false;
			}
		}
	}
 //---------------------- %DENNIS% changes:
	
	if (autoClose) {
		
		closeCounterNow = ofGetElapsedTimeMillis();	
		if (closeCounterNow - closeCounterLast > closeCounterPeriod ) 
		{
		   this->logData();	
			
		   autoClose = false;
		   this->closePressed = true;
		   this->doHide();
		}
	}
	
	//-------------
}

ofImage* I2WGUI_TabContainer::getIcon(){
	return &(this->icon);
}

ShishaKeyboard* I2WGUI_TabContainer::getKeyboard(){
	return this->mtKeyboard;
}

/**
bool I2WGUI_TabContainer::checkCursorHover(int x, int y, string cursorID, float w, float h){
	if(!mtKeyboard->checkCursorHover(x, y, cursorID, w, h))
		this->form->checkCursorHover(x, y, cursorID, w, h);
}

bool I2WGUI_TabContainer::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	if(!mtKeyboard->checkCursorDrag(x, y, button, cursorID, w, h))
		this->form->checkCursorDrag(x, y, button, cursorID, w, h);
}

bool I2WGUI_TabContainer::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
	if(!mtKeyboard->checkCursorPress(x, y, button, cursorID, w, h))
		this->form->checkCursorPress(x, y, button, cursorID, w, h);
}

bool I2WGUI_TabContainer::checkCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	if(!mtKeyboard->checkCursorRelease(x, y, button, cursorID, w, h))
		this->form->checkCursorRelease(x, y, button, cursorID, w, h);
}*/

bool I2WGUI_TabContainer::checkKeyPressed(int key){
	if(this->mtKeyboard != NULL)
		this->mtKeyboard->checkKeyPressed(key);
}

bool I2WGUI_TabContainer::checkKeyReleased(int key){
	if(this->mtKeyboard != NULL)
		this->mtKeyboard->checkKeyReleased(key);
}


bool I2WGUI_TabContainer::getBool(string selector){
	if(selector == "doCapture"){
		bool result = this->requestCapture;
		this->requestCapture = false;
		return result;
	}
	else
		return ShishaTabContainer::getBool(selector);
}


//-----%DENNIS%----------------
void I2WGUI_TabContainer::logData(){
	//if you need to log email addresses, etc. 
	//call this method
}

//------------------------------