/*
 *  I2WGUI_TextInput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 05/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "I2WGUI_TextInput.h"

I2WGUI_TextInput::I2WGUI_TextInput(){
}

I2WGUI_TextInput::~I2WGUI_TextInput(){
}

void I2WGUI_TextInput::init(float x, float y, float width, float height, int elementID){
	TextInput::init(x, y, width, height, elementID);
	this->setLabel("");
	this->setPadding(3, 3, 2);
	this->setUserCursorHeight(21, 4);
	this->maxLineCount = 1;
	this->maxTextLength = -1;
	this->firstClick = true;
	this->defaultValue = "";
	this->hasDefaultValueImage = false;
}

void I2WGUI_TextInput::draw(float x, float y, float w, float h, bool borders){
	TextInput::draw(x, y, w, h, borders);
	if(this->hasDefaultValueImage && this->defaultValue != "" && this->textValue == this->defaultValue){
		this->defaultValueImage.draw(x+this->getPaddingL(), y+(this->getHeight()-this->defaultValueImage.getHeight())/2.0);
	}
}

void I2WGUI_TextInput::onPress(string cursorID){
	TextInput::onPress(cursorID);
	if(this->firstClick){
		this->setString("");
		this->setCursorIndex(0);
		this->firstClick = false;
	}
}

void I2WGUI_TextInput::reset(){
	this->firstClick = true;
}

void I2WGUI_TextInput::setDefaultValue(string defaultValue){
	this->defaultValue = defaultValue;
	this->setString(this->defaultValue);
}

void I2WGUI_TextInput::setDefaultValueImage(string filename){
	this->hasDefaultValueImage = false;
	if(filename != "")
		this->hasDefaultValueImage = this->defaultValueImage.loadImage(filename);
}

string I2WGUI_TextInput::getString(string selector){
	if(this->selectSelf(selector)){
		if(this->textValue == this->defaultValue)
			return "";
		return this->textValue;
	}
	return ShishaElement::getString(selector);
}

void I2WGUI_TextInput::setString(string value){
	TextInput::setString(value);
	if(value == this->defaultValue)
		this->reset();
}
