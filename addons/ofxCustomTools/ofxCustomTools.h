#ifndef OFXCUSTOMTOOLS_H_INCLUDED
#define OFXCUSTOMTOOLS_H_INCLUDED

#include "ofMain.h"

    void copyPixelsRGB(unsigned char * source, int selectionX,int selectionY, int selectionWidth, int selectionHeight, int sourceWidth, int SourceHeight, unsigned char * destination, int dx,int dy, int destWidth, int destHeight);

    int getPixelR(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight);
    int getPixelG(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight);
    int getPixelB(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight);
    int getPixelA(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight);

    void alphaMaskImage(unsigned char * source, int sourceWidth, int SourceHeight, string maskFile );

    void drawTiledImage( ofImage img, int x, int y, int width, int height);



#endif // OFXCUSTOMTOOLS_H_INCLUDED
