
/*
 *  ofxCustomTools.cpp
 *  openFrameworks
 *
 *  Created by Dennis Rosenfeld Jan 06,2008.
 *
 */

#include "ofxCustomTools.h"



//----------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------PIXEL-TOOLS--
void copyPixelsRGB(unsigned char * source, int selectionX,int selectionY, int selectionWidth, int selectionHeight, int sourceWidth, int SourceHeight, unsigned char * destination, int dx,int dy, int destWidth, int destHeight)
{
    //fast copying of pixels.

    //source: image to be copied from.
    //selectionX,selectionY: the x + y point of the selection.
    //selectionWidth, selectionHeight: the width and height of selection.
    //sourceWidth, sourceWidth: the width and Height of 1st image. (it would be nice if i didnt have to pass this in....)

    //destination: image to be copied to. (can it be the same??)
    //dx,dy: the beginning x + y points of the target image.
    //destWidth, destHeight -the width and height of the 2nd image.

    int targetPixel = (dx + dy*destWidth)*3;
    int count = -999;

    for (int i = selectionY; i <selectionY + selectionHeight; i++)
    {
        count = 0;
        for (int j = selectionX; j <selectionX + selectionWidth; j++)
            {
              destination[count + targetPixel]= getPixelR(source,j,i,sourceWidth, SourceHeight);
              count++;

              destination[count + targetPixel] = getPixelG(source,j,i,sourceWidth, SourceHeight);
              count++;

              destination[count + targetPixel] = getPixelB(source,j,i,sourceWidth, SourceHeight);
              count++;
            }
        targetPixel += destWidth*3;
    }
}
//--------------------
int getPixelR(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight)
{
   // int targetPoint = (y* sourceWidth*3)+(x*3);
   // int val = source[targetPoint];
    return source[ (y* sourceWidth*3)+(x*3) ];
}
//--------------------
int getPixelG(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight)
{
   // int targetPoint = (y* sourceWidth*3)+(x*3)+1;
   // int val = source[targetPoint];
    return source[ (y* sourceWidth*3)+(x*3)+1 ];
}
//--------------------
int getPixelB(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight)
{
   // int targetPoint = (y* sourceWidth*3)+(x*3)+2;
   // int val = source[targetPoint];
    return source[ (y* sourceWidth*3)+(x*3)+2 ];
}

int getPixelA(unsigned char * source, int x,int y, int sourceWidth, int SourceHeight)
{
   // int targetPoint = (y* sourceWidth*3)+(x*3)+2;
   // int val = source[targetPoint];
    return source[ (y* sourceWidth*4)+(x*4)+3 ];
}



void alphaMaskImage(unsigned char * source, int sourceWidth, int SourceHeight, string maskFile )
{
    ofImage  tempImage;
    tempImage.loadImage(maskFile);
    tempImage.resize( sourceWidth, SourceHeight);

    tempImage.setImageType(OF_IMAGE_COLOR_ALPHA);
    unsigned char * maskPixels = tempImage.getPixels();

    int targetPixel = 0;
    int count = -999;

    for (int i = 0; i < SourceHeight; i++)
    {
        count = 0;
        for (int j = 0; j < sourceWidth; j++)
            {
              count+=3;
              source[count + targetPixel] = getPixelA(maskPixels,j,i,sourceWidth, SourceHeight);
              count++;

            }
        targetPixel += sourceWidth*4;
    }
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void drawTiledImage( ofImage img, int x, int y, int width, int height)
{
    unsigned char * tempPixels =  img.getPixels();

    int horzTiles =  width / img.width;
    int extraPixelsW =  width%img.width;

    int vertTiles =  height / img.height;
    int extraPixelsH =  height%img.height ;

    //rightmost section
    unsigned char * croppedPixel1 = new unsigned char[extraPixelsW  * img.height *3];
    copyPixelsRGB(tempPixels,0,0,extraPixelsW,img.height,img.width, img.height, croppedPixel1,0,0,extraPixelsW, img.height);
    ofTexture crop1;
    crop1.allocate(extraPixelsW, img.height, GL_RGB);
    crop1.loadData(croppedPixel1, extraPixelsW,img.height, GL_RGB);

    //bottom section
    unsigned char * croppedPixel2 = new unsigned char[img.width  * extraPixelsH *3];
    copyPixelsRGB(tempPixels,0,0,img.width,extraPixelsH,img.width, img.height, croppedPixel2,0,0,img.width, extraPixelsH);
    ofTexture crop2;
    crop2.allocate(img.width, extraPixelsH, GL_RGB);
    crop2.loadData(croppedPixel2, img.width,extraPixelsH, GL_RGB);

    //bottom right corner.
    unsigned char * croppedPixel3 = new unsigned char[extraPixelsW  *extraPixelsH *3];
    copyPixelsRGB(tempPixels,0,0,extraPixelsW,extraPixelsH,img.width, img.height, croppedPixel3,0,0,extraPixelsW, extraPixelsH);
    ofTexture crop3;
    crop3.allocate(extraPixelsW, extraPixelsH, GL_RGB);
    crop3.loadData(croppedPixel3, extraPixelsW,extraPixelsH, GL_RGB);


    //-----------------

    for (int i = 0; i <= vertTiles; i++)
    {
        for (int j = 0; j <= horzTiles; j++)
            {
                 if (j < horzTiles && i < vertTiles )
                 {
                     img.draw(x + j*img.width, y + i*img.height  );
                 }

                 else if (j == horzTiles && i < vertTiles )
                 {
                    crop1.draw(x + j*img.width, y + i*img.height  );
                 }

                 else if (j < horzTiles && i == vertTiles )
                 {
                    crop2.draw(x + j*img.width, y + i*img.height  );
                 }

                 else if (j == horzTiles && i == vertTiles )
                 {
                   crop3.draw(x + j*img.width, y + i*img.height  );
                 }

            }
    }
}


