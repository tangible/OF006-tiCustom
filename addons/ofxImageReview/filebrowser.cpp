/*---------------------------------------------------
Open Frameworks 0.05
FileBrowser - Dennis Rosenfeld - 2008

last edit -nov 21,2008
*///--------------------------------


#include "filebrowser.h"

FileBrowser::FileBrowser()//constructor
{
    initialize();
}


FileBrowser::~FileBrowser()//constructor
{

}


void    FileBrowser::initialize()
{
    //imgDIR.setVerbose(false);
    //imgDIR.ofSetLogLevel(OF_WARNING);
    nImages = imgDIR.listDir("../pictures");
    currentImage = 0;
    lastImage = 999;

    timeNow = 0;
    timeLast = 0;

    myFont.loadFont("frabk.ttf",16, true, false);

    /*          //load mulitple files at once possibility.
    for(int i = 0; i < nImages; i++){

            //loaderImg.loadImage(imgDIR.getPath(i));

            images[i].loadImage(imgDIR.getPath(i));
            float aspect = images[i].height /float(images[i].width );

            int newW = 800;
            int newH = int(newW * aspect);
            images[i].resize(newW, newH);
    }
    */

    //buttons and gui stuff:
    /*
    gui		= ofxGui::Instance(this);
	hideGUI = false;
	ofxGuiPanel* panel1 = gui->addPanel(kParameter_Panel1, "", 10, 10, OFXGUI_PANEL_BORDER, OFXGUI_PANEL_SPACING, 300);
	panel1->addButton(kParameter_Save, "SAVE", 50, 25, 0, kofxGui_Button_Trigger,99);
    */
    tx1 = 50;  //triangle x 1
    ty1 = ofGetScreenHeight()-60;  //triangle y1
    tx2 = ofGetScreenWidth() - tx1;
    ty2 = ty1;  //triangle y1
    isPressed1 = false;
    isPressed2 = false;
}

void    FileBrowser::update()  //load the current image
{
    timeNow = ofGetElapsedTimeMillis();

    //if ( currentImage != lastImage && timeNow > (timeLast+200)  )
    if (nImages > 0)
    {
        if ( currentImage != lastImage)
        {
                image.loadImage(imgDIR.getPath(currentImage));
                float aspect = image.height /float(image.width );
                int newW = 800;
                int newH = int(newW * aspect);
                //image.resize(newW, newH);

                lastImage = currentImage ;
                timeLast = timeNow;
        }
    }
}



void    FileBrowser::draw(int x, int y) //draw the image + some data.
{
    if (nImages > 0)
    {
        //ofSetColor(0,0,0);
        ofBackground(30,30,30);
        ofSetColor(255,255,255);
//        image.draw(30+x,30+y);

        float aspect = image.height /float(image.width );

        int newH = ofGetScreenHeight()-200;
        int newW = newH / aspect;
        int spacer = ( ofGetScreenWidth()- newW )/ 2;
        image.draw( spacer +x,100+y,newW ,newH);
        ofSetColor(255,255,255);
        string pathInfo = imgDIR.getPath(currentImage);

        pathInfo.erase(0,17);
        //ofDrawBitmapString(pathInfo, spacer+x,newH + 180+y);
        myFont.drawString(pathInfo, spacer+x,newH + 180+y);


        ofSetColor(255,255,255);
    }
         myFont.drawString("current image: " + ofToString(currentImage+1) + "/" + ofToString(nImages), 10+x, 30+y);
         myFont.drawString("Press 'R' to return", ofGetScreenWidth()-300,50 );


         //draw buttons
         ofFill();
         ofSetColor(75,75,75);
         if( isPressed1 == true)
         {
            ofSetColor(125,125,125);
            isPressed1 = false;
         }

         ofTriangle(tx1,ty1,tx1-30,ty1+15,tx1,ty1+30); //triangle1
         ofSetColor(75,75,75);

         if( isPressed2 == true)
         {
            ofSetColor(125,125,125);
            isPressed2 = false;
         }
         ofTriangle(tx2,ty2,tx2+30,ty2+15,tx2,ty2+30);//triangle2
         ofNoFill();
         ofSetColor(255,255,255);
         ofEnableSmoothing();
         ofTriangle(tx1,ty1,tx1-30,ty1+15,tx1,ty1+30);//triangle1
         ofTriangle(tx2,ty2,tx2+30,ty2+15,tx2,ty2+30);//triangle2

}

void    FileBrowser::keyPressed(int key)
{
//(key == '[' || key == '{')
                    if (key == '+'|| key == '=' ||key == OF_KEY_RIGHT){
                        currentImage++;
                        isPressed2 = true;
                        if (currentImage >= nImages ){
                            currentImage = nImages-1;
                        }
                    }


                    if (key == '-'|| key == '_' ||key == OF_KEY_LEFT){
                        currentImage--;
                        isPressed1 = true;
                        if (currentImage < 0){
                            currentImage = 0;
                        }
                    }
}


void    FileBrowser::mousePressed(int x, int y, int button)
{

            if ( x < tx1 && x > tx1 - 30 && y < ty1 + 30 && y > ty1)
            {
                currentImage--;
                isPressed1 = true;
                if (currentImage < 0){
                    currentImage = 0;
                }
            }

            else if ( x < tx2 +30 && x > tx2  && y < ty2 + 30 && y > ty2)
            {
                currentImage++;
                isPressed2 = true;
                if (currentImage >= nImages ){
                    currentImage = nImages-1;
                }
            }

}
