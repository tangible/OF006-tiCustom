#ifndef FILEBROWSER_H_INCLUDED
#define FILEBROWSER_H_INCLUDED
//--------------------------------------------------------------
#include "ofMain.h"


#define OF_ADDON_USING_OFXDIRLIST
#define OF_ADDON_USING_OFXVECTORMATH
#define OF_ADDON_USING_OFXXMLSETTINGS

#include "ofAddons.h"

//last edit -nov 20,2008


class   FileBrowser
{

    //A filebrowser object provides a modular way to browse through and review the images in your applications pictures folder.
    private:


    //VARIABLES:--------------------------------------------------
    //ofImage	* 	images;//Images Array;

    ofImage     image;
    ofxDirList  imgDIR;
    ofTrueTypeFont	    myFont;
//    ofxGui*		        gui;
    int         currentImage;
    int         lastImage;
    int 		nImages;

	int         timeNow;
    int         timeLast;

    int tx1;
    int ty1;
    int tx2;
    int ty2;
    bool isPressed1;
    bool isPressed2;


    public:

    FileBrowser();       //constructor
    ~FileBrowser();      //destructor
    void    initialize();       //initialize objects.

    //METHODS:--------------------------------------------------------
    void    update();
    void    draw(int x, int y);

    void    keyPressed(int key );
    void    mousePressed(int x, int y, int button);


};

#endif // FILEBROWSER_H_INCLUDED
