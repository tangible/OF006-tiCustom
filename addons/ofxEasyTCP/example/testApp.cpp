#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofSetFrameRate(60);
	ofSetLogLevel(OF_LOG_VERBOSE);
	
	string host = DEFAULT_HOST;
	int sendPort = DEFAULT_TCP_SEND_PORT;
	int listenPort =  DEFAULT_TCP_LISTEN_PORT;
	
	// to test locally, make a version that swaps the send and listen ports
	
	this->initClient(host, listenPort);
	this->initServer(sendPort);
	
}

void testApp::initClient(string host, int port){
	// client connets to the remote server
	this->client.enableEvents();
	this->client.connect(host, port);
	ofAddListener(this->client.serverSentMessage,this,&testApp::serverSentMessage);
	ofAddListener(this->client.serverConnected,this,&testApp::serverConnected);
	ofAddListener(this->client.serverDisconnected,this,&testApp::serverDisconnected);
}

void testApp::initServer(int port){
	// setup the server to listen
	this->server.enableEvents();
	this->server.startServer(port);
	ofAddListener(this->server.clientSentMessage,this,&testApp::clientSentMessage);
	ofAddListener(this->server.clientConnected,this,&testApp::clientConnected);
	ofAddListener(this->server.clientDisconnected,this,&testApp::clientDisconnected);
}

//--------------------------------------------------------------
void testApp::exit(){
//	this->client.shutdown();
	this->server.shutdown();
}

//--------------------------------------------------------------
void testApp::update(){
	this->server.update();
	this->client.update();
	if(this->client.hasReceivedMessages()){
		string msg;
		while((msg = this->client.getNextMessage()) != ""){
			cout << "testApp:client got:" << msg << ":" << endl;
		}
	}
	
	if(ofGetFrameNum() % 120 == 0)
		this->server.sendToAll("server says hello!");
	else if(ofGetFrameNum() % 60 == 0)
		this->client.send("client says what's up?!");
}

//--------------------------------------------------------------
void testApp::draw(){

}

// server events
void testApp::clientSentMessage(ofxEasyTCPEvent& event){
	cout << "clientSentMessage:" << event.getMessage() << ":" <<  endl;
	cout << "\tfrom:" << event.getSource() << ":" << event.getClientID() << ":" << event.getHost() << ":" << event.getPort() << ":" << endl;
}

void testApp::clientConnected(ofxEasyTCPEvent& event){
	cout << "clientConnected:" << event.getMessage() << ":" <<  endl;
	cout << "\tfrom:" << event.getSource() << ":" << event.getClientID() << ":" << event.getHost() << ":" << event.getPort() << ":" << endl;
}

void testApp::clientDisconnected(ofxEasyTCPEvent& event){
	cout << "clientDisconnected:" << event.getMessage() << ":" <<  endl;
	cout << "\tfrom:" << event.getSource() << ":" << event.getClientID() << ":" << event.getHost() << ":" << event.getPort() << ":" << endl;
}

// client events
void testApp::serverSentMessage(ofxEasyTCPEvent& event){
	cout << "serverSentMessage:" << event.getMessage() << ":" <<  endl;
	cout << "\tfrom:" << event.getSource() << ":" << event.getClientID() << ":" << event.getHost() << ":" << event.getPort() << ":" << endl;
}

void testApp::serverConnected(ofxEasyTCPEvent& event){
	cout << "serverConnected:" << event.getMessage() << ":" <<  endl;
	cout << "\tfrom:" << event.getSource() << ":" << event.getClientID() << ":" << event.getHost() << ":" << event.getPort() << ":" << endl;
}

void testApp::serverDisconnected(ofxEasyTCPEvent& event){
	cout << "serverDisconnected:" << event.getMessage() << ":" <<  endl;
	cout << "\tfrom:" << event.getSource() << ":" << event.getClientID() << ":" << event.getHost() << ":" << event.getPort() << ":" << endl;
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

