#ifndef _TEST_APP
#define _TEST_APP

#include "ofMain.h"
//#include "ChatClient.h"
//#include "ChatServer.h"
#include "ofxEasyTCP.h"

#define DEFAULT_HOST "127.0.0.1"

class testApp : public ofBaseApp{
	private:
		string host;
		int sendPort;
		int listenPort;
	
		ofxEasyTCPClient client;
		ofxEasyTCPServer server;
	
		void initClient(string host=DEFAULT_HOST, int port=DEFAULT_TCP_SEND_PORT);
		void initServer(int port=DEFAULT_TCP_LISTEN_PORT);

	public:
		void setup();
		void exit();
		void update();
		void draw();
	
	// server events
		void clientSentMessage(ofxEasyTCPEvent& event);
		void clientConnected(ofxEasyTCPEvent& event);
		void clientDisconnected(ofxEasyTCPEvent& event);	
	
	// client events
		void serverSentMessage(ofxEasyTCPEvent& event);
		void serverConnected(ofxEasyTCPEvent& event);
		void serverDisconnected(ofxEasyTCPEvent& event);

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

};

#endif
