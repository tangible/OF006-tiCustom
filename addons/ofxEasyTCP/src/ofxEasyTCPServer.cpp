/*
 *  ofxEasyTCPServer.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 06/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxEasyTCPServer.h"

ofxEasyTCPServer::ofxEasyTCPServer(){
	this->init();
}

ofxEasyTCPServer::~ofxEasyTCPServer(){
}

void ofxEasyTCPServer::init(int port){
	this->isRunning = false;
	this->port = port;
	this->eventsEnabled = false;
	this->oldNumClients = 0;
	this->startServer();
}

void ofxEasyTCPServer::startServer(int port){
	if(port > 0)
		this->port = port;
	if(this->port > 0){
		stringstream logBuilder;
		logBuilder << this->getName() << ": starting server on port " << port << "...";
		ofLog(OF_LOG_VERBOSE, logBuilder.str());
		logBuilder.str("");
		this->setup(this->port);
		this->isRunning = true;
		this->oldNumClients = 0;
	}
}

void ofxEasyTCPServer::shutdown(){
	ofLog(OF_LOG_VERBOSE, this->getName() + ": shutdown...");
	if(!this->isRunning)
		return;
	if(this->getNumClients() > 0){
		int connectCount = 0;
		for(int i = 0; i < this->getNumClients(); i++){
			if(this->isClientConnected(i)){
				this->send(i, "disconnect" );
				this->disconnectClient(i);
				connectCount++;
			}
		}
		if(connectCount > 0)
			ofSleepMillis(1000); // give it a second to send out the disconnect message
	}
	this->close();
	this->isRunning = false;
}

void ofxEasyTCPServer::update(){
	if(this->isRunning && this->getNumClients() > 0){
		if(this->oldNumClients < this->getNumClients()){
			this->handleNewClients(this->getNumClients()-this->oldNumClients);
		}
		this->oldNumClients = this->getNumClients();
		for(int i = 0; i < this->getNumClients(); i++){
			string str = this->receive(i);
			if(str != ""){
				if(!this->handleMessage(i, str))
					this->rQueueClient[i].push_back(str);
			}
		}
	}
}

int ofxEasyTCPServer::getNumIdentifiedClients(){
	int identifiedClientCount = 0;
	for(map< int, string>::iterator cIDit = clientIDs.begin(); cIDit != clientIDs.end(); cIDit++){
		if((*cIDit).second != "-1")
			identifiedClientCount++;
	}
	return identifiedClientCount;
}

string ofxEasyTCPServer::getClientID(int clientID){
	string clientAlias = "-1";
	
	if(this->clientIDs.find(clientID) != this->clientIDs.end())
		clientAlias = this->clientIDs[clientID];

	if(clientAlias == "-1"){
		stringstream aliasBuilder;
		aliasBuilder << clientID;
		clientAlias = aliasBuilder.str();
		aliasBuilder.str("");
	}
	return clientAlias;
}

int ofxEasyTCPServer::getClientID(string clientID){
	int clientAlias = -1;
	for(int i=0; i < this->clientIDs.size(); i++){
		if(this->clientIDs[i] == clientID){
			clientAlias = i;
			break;
		}
	}
	return clientAlias;
}

bool ofxEasyTCPServer::handleMessage(int clientID, string message, bool withEvent){
	stringstream logBuilder;
	bool eventSent = false;
	if(message == "disconnect"){
		logBuilder << this->getName() << ": client " << this->getClientID(clientID) << " disconnected from port " << this->getClientPort(clientID);
		ofLog(OF_LOG_NOTICE, logBuilder.str());
		logBuilder.str("");
		
		if(this->eventsEnabled && !eventSent){
			ofxEasyTCPEvent eventObject;
			logBuilder << "client " << clientID << " disconnected";
			eventObject.setMessage(logBuilder.str(), this->getName(), this->getClientIP(clientID), this->getClientPort(clientID), this->getClientID(clientID));
			logBuilder.str("");
			ofNotifyEvent(clientDisconnected, eventObject, this);
			eventSent = true;
		}
		this->unidentifyClient(clientID);
		this->disconnectClient(clientID);
		
		map<int, string>::iterator cIDit = this->clientIDs.find(clientID);
		if(cIDit != this->clientIDs.end())
			this->clientIDs.erase(cIDit);
		return true;
	}
	else if(message.find("identify") == 0){
		vector<string> msgSplit = split(message, ' ');
		if(msgSplit.size() > 1){
			string clientAlias = msgSplit[1];
			this->identifyClient(clientID, clientAlias);
			this->clientIDs[clientID] = clientAlias;
		}
	}
	if(this->eventsEnabled && !eventSent){
		ofxEasyTCPEvent eventObject;
		eventObject.setMessage(message, this->getName(), this->getClientIP(clientID), this->getClientPort(clientID), this->getClientID(clientID));
		ofNotifyEvent(clientSentMessage, eventObject, this);
		eventSent = true;
		return true;
	}
	return false;
}

int ofxEasyTCPServer::handleNewClients(int count){
	stringstream logBuilder;
	for(int i=this->getNumClients()-count; i < this->getNumClients(); i++){
		logBuilder << this->getName() << ": client " << i << " connected on port " << this->getClientPort(i);
		ofLog(OF_LOG_NOTICE, logBuilder.str());
		logBuilder.str("");
		
		this->clientIDs[i] = "-1";
		
		if(this->eventsEnabled){
			ofxEasyTCPEvent eventObject;
			logBuilder << "client " << i << " connected";
			eventObject.setMessage(logBuilder.str(), this->getName(), this->getClientIP(i), this->getClientPort(i), this->getClientID(i));
			logBuilder.str("");
			ofNotifyEvent(clientConnected, eventObject, this);
		}
	}
}

void ofxEasyTCPServer::identifyClient(int clientID, string clientAlias){
	if(this->getClientID(clientID) != "-1"){
		this->unidentifyClient(clientID);
	}
	
	this->clientIDs[clientID] = clientAlias;
	if(clientAlias != "" && this->eventsEnabled){
		stringstream logBuilder;
		ofxEasyTCPEvent eventObject;
		logBuilder << "client " << clientID << " identified as " << clientAlias;
		eventObject.setMessage(logBuilder.str(), this->getName(), this->getClientIP(clientID), this->getClientPort(clientID), this->getClientID(clientID));
		logBuilder.str("");
		ofNotifyEvent(clientIdentified, eventObject, this);
	}	
}

void ofxEasyTCPServer::unidentifyClient(int clientID){
	string oldAlias = this->getClientID(clientID);
	this->clientIDs[clientID] = "-1";
	if(oldAlias != "" && oldAlias != "-1" && this->eventsEnabled){
		stringstream logBuilder;
		ofxEasyTCPEvent eventObject;
		logBuilder << "client " << clientID << " unidentified";
		eventObject.setMessage(logBuilder.str(), this->getName(), this->getClientIP(clientID), this->getClientPort(clientID), oldAlias);
		logBuilder.str("");
		ofNotifyEvent(clientUnidentified, eventObject, this);
	}
}

bool ofxEasyTCPServer::sendToAlias(string clientID, string message){
	bool result = false;
	int clientAlias = this->getClientID(clientID);
	if(clientAlias != -1)
		result = this->send(clientAlias, message);
	return result;
}

bool ofxEasyTCPServer::hasClients(){
	return (this->getNumClients() > 0);
}

bool ofxEasyTCPServer::hasReceivedMessages(int clientID){
	return (this->rQueueClient[clientID].size() > 0);
}

string ofxEasyTCPServer::getNextMessage(int clientID){
	string result = "";
	if(this->rQueueClient[clientID].size() > 0){
		result = this->rQueueClient[clientID].front();
		this->rQueueClient[clientID].erase(this->rQueueClient[clientID].begin());
	}
	return result;
}

string ofxEasyTCPServer::getName(){
	return "easyTCP:Server";
}

void ofxEasyTCPServer::setPort(int port){
	this->port = port;
}

void ofxEasyTCPServer::enableEvents(){
	this->eventsEnabled = true;
}

void ofxEasyTCPServer::disableEvents(){
	this->eventsEnabled = false;
}
