/*
 *  ofxEasyTCPClient.h
 *  openFrameworks
 *
 *  Created by Pat Long on 06/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_EASYTCP_CLIENT_H
#define _OFX_EASYTCP_CLIENT_H

#include "ofMain.h"
#include "ofxNetwork.h"
#include "ofxEasyTCPEvent.h"

#define DEFAULT_TCP_SEND_PORT	11998
#define DEFAULT_RETRY_TIME		5000

class ofxEasyTCPClient : public ofxTCPClient{
protected:
	string host;
	int port;
	int clientID;
	vector<string> rQueue;
	int connectTime, deltaTime, retryTime;
	bool retryConnection;
	bool eventsEnabled;
	
	virtual void init(string host="", int port=0, int clientID=-1);
	
	virtual bool tryConnection();
	virtual void receiveQueue();
	virtual void handleConnection();
	virtual bool handleMessage(string message, bool withEvent=true);
	
	virtual void sendIdentifier();
	
public:
	ofxEasyTCPClient();
	~ofxEasyTCPClient();
	
	virtual bool connect(string host, int port, int clientID=-1);
	virtual void shutdown();
	virtual void update();
	
	bool hasReceivedMessages();
	string getNextMessage();
	string getName();
	string getHost();
	
	void setRetryConnection(bool retryConnection, int retryTime=-1);
	void setRetryTime(int retryTime);
	
	void enableEvents();
	void disableEvents();
	
	ofEvent<ofxEasyTCPEvent> serverSentMessage;
	ofEvent<ofxEasyTCPEvent> serverConnected;
	ofEvent<ofxEasyTCPEvent> serverDisconnected;
};

#endif
