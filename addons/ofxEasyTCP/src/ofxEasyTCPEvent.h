/*
 *  ofxEasyTCPEvent.h
 *  openFrameworks
 *
 *  Created by Pat Long on 06/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_EASYTCP_EVENT
#define _OFX_EASYTCP_EVENT

class ofxEasyTCPEvent{
protected:
	string message, source, host, clientID;
	int port;
	
public:
	ofxEasyTCPEvent(){
		this->setMessage("");
	};
	
	~ofxEasyTCPEvent(){
	};
	
	string getMessage(){
		return this->message;
	};
	
	string getSource(){
		return this->source;
	};
	
	string getHost(){
		return this->host;
	};
	
	int getPort(){
		return this->port;
	};
	
	string getClientID(){
		return this->clientID;
	}
	
	virtual void setMessage(string message, string source="", string host="", int port=0, string clientID="0"){
		this->message = message;
		this->source = source;
		this->host = host;
		this->port = port;
		this->clientID = clientID;
	};
	
	virtual string getOutput(){
		stringstream outputBuilder;
		outputBuilder << getPort() << ":" << getClientID() << ":" << getSource() << ":" << getMessage();
		return outputBuilder.str();
	};
};

#endif
