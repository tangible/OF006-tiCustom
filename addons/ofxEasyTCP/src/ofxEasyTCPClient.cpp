/*
 *  ofxEasyTCPClient.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 06/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxEasyTCPClient.h"

ofxEasyTCPClient::ofxEasyTCPClient(){
	this->init();
}

ofxEasyTCPClient::~ofxEasyTCPClient(){
	if(this->isConnected())
		this->shutdown();
}

void ofxEasyTCPClient::init(string host, int port, int clientID){
	this->connectTime = 0;
	this->deltaTime = 0;
	this->retryTime = DEFAULT_RETRY_TIME;
	if(this->retryTime > 0)
		this->retryConnection = true;
	
	this->host = host;
	this->port = port;
	this->clientID = clientID;
	this->eventsEnabled = false;
//	this->tryConnection();
}

bool ofxEasyTCPClient::tryConnection(){
	if(this->isConnected())
		return true;
	
	stringstream logBuilder;
	if(this->host != "" && this->port > 0){
		logBuilder << this->getName() << ": attempting connection to " << host << " on port " << port << "...";
		ofLog(OF_LOG_VERBOSE, logBuilder.str());
		logBuilder.str("");
		this->setup(host, port);
		if(this->isConnected())
			this->handleConnection();
		this->connectTime = ofGetElapsedTimeMillis();
	}
	return this->isConnected();
}

bool ofxEasyTCPClient::connect(string host, int port, int clientID){
	this->host = host;
	this->port = port;
	this->clientID = clientID;
	this->tryConnection();
}

void ofxEasyTCPClient::shutdown(){
	ofLog(OF_LOG_VERBOSE, this->getName()+": shutdown...");
	this->send("disconnect");
}

void ofxEasyTCPClient::setRetryConnection(bool retryConnection, int retryTime){
	this->retryConnection = retryConnection;
	if(retryTime != -1)
		this->setRetryTime(retryTime);
}

void ofxEasyTCPClient::setRetryTime(int retryTime){
	this->retryTime = retryTime;
}

void ofxEasyTCPClient::update(){
	if(this->isConnected()){
		this->receiveQueue();
	}
	else if(this->retryConnection && this->retryTime > 0){
		//if we are not connected lets try and reconnect every 5 seconds
		this->deltaTime = ofGetElapsedTimeMillis() - this->connectTime;
		if(this->deltaTime > this->retryTime)
			this->tryConnection();
	}
}

void ofxEasyTCPClient::receiveQueue(){
	string str = this->receive();
	if( str.length() > 0 ){
		if(!this->handleMessage(str))
			this->rQueue.push_back(str);
	}
}

void ofxEasyTCPClient::sendIdentifier(){
	if(this->clientID != -1 && this->isConnected()){
		stringstream msgBuilder;
		msgBuilder << "identify " << this->clientID;
		this->send(msgBuilder.str());
		msgBuilder.str("");
	}
}

void ofxEasyTCPClient::handleConnection(){
	stringstream logBuilder;
	logBuilder << this->getName() << ": connected on port " << this->getPort();
	ofLog(OF_LOG_NOTICE, logBuilder.str());
	logBuilder.str("");
	
	this->sendIdentifier();
	
	if(this->eventsEnabled){
		ofxEasyTCPEvent eventObject;
		eventObject.setMessage("connection established", this->getName(), this->getHost(), this->port);
		ofNotifyEvent(serverConnected, eventObject, this);
	}
}

bool ofxEasyTCPClient::handleMessage(string message, bool withEvent){
	stringstream logBuilder;
	bool eventSent = false;
	if(message == "disconnect"){
		this->retryConnection = true;
		this->connectTime = ofGetElapsedTimeMillis();
		
		logBuilder << this->getName() << ": server disconnected";
		ofLog(OF_LOG_NOTICE, logBuilder.str());
		logBuilder.str("");
		
		if(this->eventsEnabled && !eventSent){
			ofxEasyTCPEvent eventObject;
			eventObject.setMessage("server disconnected", this->getName(), this->getHost(), this->port);
			ofNotifyEvent(serverDisconnected, eventObject, this);
			eventSent = true;
		}
		return true;
	}
	if(withEvent && this->eventsEnabled && !eventSent){
		ofxEasyTCPEvent eventObject;
		eventObject.setMessage(message, this->getName(), this->getHost(), this->port);
		ofNotifyEvent(serverSentMessage, eventObject, this);
		eventSent = true;
		return true;
	}
	return false;
}

bool ofxEasyTCPClient::hasReceivedMessages(){
	return (this->rQueue.size() > 0);
}

string ofxEasyTCPClient::getNextMessage(){
	string result = "";
	if(this->rQueue.size() > 0){
		result = this->rQueue.front();
		this->rQueue.erase(this->rQueue.begin());
	}
	return result;
}

string ofxEasyTCPClient::getName(){
	return "easyTCP:Client";
}

string ofxEasyTCPClient::getHost(){
	return this->host;
}

void ofxEasyTCPClient::enableEvents(){
	this->eventsEnabled = true;
}

void ofxEasyTCPClient::disableEvents(){
	this->eventsEnabled = false;
}
