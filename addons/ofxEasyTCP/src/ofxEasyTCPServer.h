/*
 *  ofxEasyTCPServer.h
 *  openFrameworks
 *
 *  Created by Pat Long on 06/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_EASYTCP_SERVER_H
#define _OFX_EASYTCP_SERVER_H

#include "ofMain.h"
#include "ofxNetwork.h"
#include "ofxEasyTCPEvent.h"
#include "ofxTI_Utils.h"

#define DEFAULT_TCP_LISTEN_PORT		11999

class ofxEasyTCPServer : public ofxTCPServer{
protected:
	int port;
	bool isRunning;
	map< int, vector<string> > rQueueClient;
	bool eventsEnabled;
	int oldNumClients;
	map< int, string> clientIDs;
	
	virtual void init(int port=0);
	virtual bool handleMessage(int clientID, string message, bool withEvent=true);
	virtual int handleNewClients(int count);
	virtual void identifyClient(int clientID, string clientAlias);
	virtual void unidentifyClient(int clientID);
	
	string getClientID(int clientID);
	int getClientID(string clientID);
	
public:
	ofxEasyTCPServer();
	~ofxEasyTCPServer();
	
	virtual void startServer(int port=0);
	virtual void shutdown();
	virtual void update();
	
	int getNumIdentifiedClients();
	
	bool sendToAlias(string clientID, string message);
	
	bool hasClients();
	bool hasReceivedMessages(int clientID);
	string getNextMessage(int clientID);
	string getName();
	
	void setPort(int port);
	
	void enableEvents();
	void disableEvents();
	
	ofEvent<ofxEasyTCPEvent> clientSentMessage;
	ofEvent<ofxEasyTCPEvent> clientConnected;
	ofEvent<ofxEasyTCPEvent> clientDisconnected;
	ofEvent<ofxEasyTCPEvent> clientIdentified;
	ofEvent<ofxEasyTCPEvent> clientUnidentified;
};


#endif
