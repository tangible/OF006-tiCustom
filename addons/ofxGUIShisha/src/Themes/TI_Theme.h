/*
 *  TI_Theme.h
 *  openFrameworks
 *
 *  Created by Pat Long on 12/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef TI_THEME_H
#define TI_THEME_H

#include "ShishaTheme.h"

class TI_Theme : public ShishaTheme{
protected:
	ofTrueTypeFont font;
	
	virtual void initTheme();
	
public:
	TI_Theme();
	~TI_Theme();
	
	virtual void drawText(string text, float x, float y, bool center=true);
	virtual float getTextWidth(string text);
	virtual float getTextHeight(string text);

};

#endif
