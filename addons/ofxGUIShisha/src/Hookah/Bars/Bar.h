/*
 *  Bar.h
 *  openFrameworks
 *
 *  Created by Pat Long on 13/09/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_BAR
#define _OFX_GUISHISHA_BAR

#include "ShishaElement.h"

#define DEFAULT_MAX_VALUE 10

#define SELECTOR_PERCENTAGE "percent"

class Bar : public ShishaElement{
protected:
	int value, maxValue;
	int fillColour;
	
	virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
	virtual void drawAsCircle(float x, float y, float w, float h, bool borders);
	
public:
	Bar();
	~Bar();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);

	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	virtual float getPercentage();
	
	virtual void setInt(int value=0);
	virtual void setMaxValue(int maxValue=DEFAULT_MAX_VALUE);
	virtual void setFillColour(int fillColour=SHISHA_COLOUR_FOREGROUND_2);
	
};

#endif
