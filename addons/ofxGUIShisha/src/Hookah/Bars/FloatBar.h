/*
 *  FloatBar.h
 *  openFrameworks
 *
 *  Created by Pat Long on 13/09/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_FLOAT_BAR
#define _OFX_GUISHISHA_FLOAT_BAR

#include "Bar.h"

#define DEFAULT_MAX_FLOAT_VALUE 10.0

class FloatBar : public Bar{
protected:
	float floatValue, maxFloatValue;
	
public:
	FloatBar();
	~FloatBar();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	virtual float getPercentage();
	
	virtual void setInt(int value=0);
	virtual void setFloat(float value=0.0);
	virtual void setMaxValue(int maxValue=DEFAULT_MAX_VALUE);
	virtual void setMaxFloatValue(float maxFloatValue=DEFAULT_MAX_FLOAT_VALUE);
};

#endif
