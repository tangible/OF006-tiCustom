/*
 *  FloatBar.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 13/09/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "FloatBar.h"

FloatBar::FloatBar(){
}

FloatBar::~FloatBar(){
}

void FloatBar::init(float x, float y, float width, float height, int elementID){
	Bar::init(x, y, width, height, elementID);
	this->setName("FloatBar");
	this->setLabel("Float Bar");
	this->setMaxFloatValue();
	this->setFloat();
}

int FloatBar::getInt(string selector){
	if(this->selectSelf(selector))
		return this->value;
	return SELECTOR_UNIDENTIFIED_INT;
}

float FloatBar::getFloat(string selector){
	if(this->selectSelf(selector))
		return this->floatValue;
	else if(selector == SELECTOR_PERCENTAGE);
		return this->getPercentage();
	return SELECTOR_UNIDENTIFIED_FLOAT;
}

float FloatBar::getPercentage(){
	return (this->floatValue / this->maxFloatValue);
}

void FloatBar::setInt(int value){
	Bar::setInt(value);
	this->setFloat((float)value);
}

void FloatBar::setFloat(float value){
	this->floatValue = value;
}

void FloatBar::setMaxValue(int maxValue){
	Bar::setMaxValue(maxValue);
	this->setMaxFloatValue((float)maxValue);
}

void FloatBar::setMaxFloatValue(float maxFloatValue){
	this->maxFloatValue = maxFloatValue;
}
