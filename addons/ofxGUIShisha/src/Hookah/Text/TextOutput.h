/*
 *  TextOutput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 26/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_TEXT_OUTPUT
#define _OFX_GUISHISHA_TEXT_OUTPUT

#include "ShishaElement.h"

class TextOutput : public ShishaElement{
	protected:
		ofRectangle padding;
		string textValue;
	
		virtual string getOutputValue();
		
	public:
		TextOutput();
		~TextOutput();

		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
		virtual void draw(float x, float y, float w, float h);
		virtual void draw(float x, float y, float w, float h, bool borders);

		float getHPadding();
		float getVPadding();
		float getPaddingL();
		float getPaddingT();
		float getPaddingR();
		float getPaddingB();
		virtual void setPadding(float l=0.0, float t=0.0, float r=0.0, float b=0.0);
		virtual string getString(string selector=DEFAULT_SELECTOR);
		virtual void setString(string value="");
};

#endif
