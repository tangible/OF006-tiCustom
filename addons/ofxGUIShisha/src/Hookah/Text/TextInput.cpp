/*
 *  TextInput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 06/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "TextInput.h"

TextInput::TextInput(){
}

TextInput::~TextInput(){
}

void TextInput::init(float x, float y, float width, float height, int elementID){
	this->cursorIndex = -1;
	this->cursorOffsetX = this->cursorOffsetY = 0;
	this->maxTextLength = -1;
	this->cursorHeight = 0;
	this->setUserCursorHeight();
	
	TextOutput::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_TEXT_INPUT;
	this->setName("Input");
	this->setLabel("Input");
	this->setStringValue("Click to change");
	this->setCursorIndex();
	this->cursorBlinkSpeed = DEFAULT_CURSOR_BLINK_SPEED;
}

void TextInput::setDimensions(float width, float height){
	TextOutput::setDimensions(width, height);
	this->calculateMaxTextLength();
}

void TextInput::setPadding(float l, float t, float r, float b){
	TextOutput::setPadding(l, t, r, b);
	this->calculateMaxTextLength();
}

void TextInput::setLabel(string label){
	TextOutput::setLabel(label);
	
	this->setCursorIndex(this->cursorIndex);
	this->cursorOffsetY = (this->height+this->cursorHeight)/4.0-5;
}

void TextInput::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	TextOutput::setTheme(theme, doNotDelete, themeInherited);
	this->cursorHeight = this->getTheme()->getTextHeight("|")*1.5;
	this->cursorOffsetY = (this->height+this->cursorHeight)/4.0-5;
}

void TextInput::setUserCursorHeight(int userCursorHeight, int userCursorOffsetY){
	this->userCursorHeight  = userCursorHeight;
	this->userCursorOffsetY = userCursorOffsetY;
}

void TextInput::update(){
	if(this->cursorBlinkSpeed > 0 && ofGetFrameNum() % this->cursorBlinkSpeed == 0){
		this->cursorBlinkState = !this->cursorBlinkState;
	}
}

int TextInput::getGuiDisplayState(){
	if(this->cursorIndex == -1)
		return (this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_INACTIVE);
	else
		return (this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_ACTIVE);
}

void TextInput::draw(float x, float y, float w, float h){
	this->draw(x, y, w, h, this->getTheme()->drawTextInputBorders());
}

void TextInput::draw(float x, float y, float w, float h, bool borders){
	if(borders){
		float labelOffSet = this->getTheme()->getTextWidth(this->getLabel()); // - 4;
		
		// only draw the background if we're also drawing borders
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x+labelOffSet, y, w-labelOffSet, h);
		
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofRect(x+labelOffSet, y, w-labelOffSet, h);
		
		ofSetColor(0, 255, 0);
		ofRect(x+labelOffSet+this->getPaddingL(), y+this->getPaddingT(), this->getWidth()-labelOffSet-this->getHPadding(), this->getHeight()-this->getVPadding());
		ofSetColor(255, 255, 255);
		ofFill();
	}
	
	TextOutput::draw(x, y, w, h, false);
	if(this->cursorBlinkState && this->cursorIndex != -1 && this->cursorOffsetX != -1 && this->cursorOffsetY != -1){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		float x = this->x+this->getPaddingL()+this->cursorOffsetX;
		float y = this->y+this->getPaddingT()+this->cursorOffsetY+this->userCursorOffsetY;
		float h = ((this->userCursorHeight == 0.0)?this->cursorHeight:this->userCursorHeight);
		ofLine(x, y, x, y+h);
		this->getTheme()->setColour();
	}
}

void TextInput::nextCursorIndex(){
	this->setCursorIndex(this->cursorIndex+1);
}

void TextInput::previousCursorIndex(){
	if(this->cursorIndex > 0){
		this->setCursorIndex(this->cursorIndex-1);
	}
}

void TextInput::setCursorIndex(int index){
	if(this->textValue.length() == 0 && index > 0){
		index = 0;
	}
	else if(this->textValue.length() > 0 && index > (int)this->textValue.length()){
		index = this->textValue.length();
	}
	this->cursorIndex = index;
	if(index == 0){
		this->cursorOffsetX = this->getTheme()->getTextWidth(this->getLabel())+1;
		this->cursorBlinkState = true;
	}
	else if(index > 0){
		string output = this->getOutputValue();
		stringstream offsetBuilder;
		offsetBuilder << this->getLabel();
		if(index > 0)
			offsetBuilder << output.substr(this->getLabel().length(), index);
		this->cursorOffsetX = this->getTheme()->getTextWidth(offsetBuilder.str());
		offsetBuilder.str("");
		this->cursorBlinkState = true;
	}
}

int TextInput::calculateMaxTextLength(){
	this->maxTextLength = (this->getMaxTextWidth()-this->getTheme()->getTextWidth(this->getLabel())) / (this->getTheme()->getTextWidth(" ")*1.1);
}

int TextInput::getClosestLetterIndex(float x, float y){
	int result = -2;
	string fullStr = this->getOutputValue();
	stringstream checkStr;
	int i;
	checkStr << this->getLabel();
	for(i=this->getLabel().length(); i < fullStr.length(); i++){
		checkStr << fullStr.at(i);
		if(this->getX()+this->getTheme()->getTextWidth(checkStr.str()) >= x){
			result = i;
			break;
		}
	}
	if(i == this->textValue.length())
		result = i;
	result -= this->getLabel().length();
	if(result < 0)
		result = -2;
	return result;
}

void TextInput::setCursorLocation(float x, float y){
	if(x != -1){
		this->setCursorIndex(this->getClosestLetterIndex(x, y));
	}
	if(y != -1){
		this->cursorOffsetY = y;
	}
}

void TextInput::onRelease(string cursorID){
	if(this->cursors.isClickedOn(cursorID)){
		float labelOffSet = this->getTheme()->getTextWidth(this->getLabel()); // - 4;
		float cX = this->cursors.getX(cursorID);
		float cY = this->cursors.getY(cursorID);
		
		if(cX >= this->getX()+labelOffSet && cX <= this->getX()+labelOffSet+this->getWidth()
		   && cY >= this->getY() && cY <= this->getY()+this->getHeight()){
			this->setCursorLocation(this->cursors.getX(cursorID), -1);
		}
		else{
			this->setCursorIndex(this->textValue.length());
		}
	}
	ShishaElement::onRelease(cursorID);
}

bool TextInput::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool result = TextOutput::checkCursorPress(x, y, button, cursorID, w, h);
	if(!result){
		this->setCursorIndex();
	}
	return result;
}

bool TextInput::checkCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	if(!this->cursors.isClickedOn(cursorID)){
		this->setCursorIndex();
	}
	return TextOutput::checkCursorRelease(x, y, button, cursorID, w, h);
}

bool TextInput::checkKeyPressed(int key){
	if(this->cursorIndex != -1){
		if(key == OF_KEY_BACKSPACE){
			if(this->cursorIndex > 0){
				string::iterator delIt = this->textValue.begin()+this->cursorIndex-1;
				this->textValue.erase(delIt);
				this->previousCursorIndex();
			}
		}
		else if(key == OF_KEY_LEFT){
			this->previousCursorIndex();
		}
		else if(key == OF_KEY_RIGHT){
			this->nextCursorIndex();
		}
		else if(key == OF_KEY_RETURN){
			this->setCursorIndex();
		}
		else if(key == OF_KEY_HOME){
			this->setCursorIndex(0);
		}
		else if(key == OF_KEY_END){
			this->setCursorIndex(this->textValue.length());
		}
		else if(key == OF_KEY_ESC || key == OF_KEY_DEL 
				|| key == OF_KEY_F1 || key == OF_KEY_F2 || key == OF_KEY_F3 || key == OF_KEY_F4 || key == OF_KEY_F5 || key == OF_KEY_F6 || key == OF_KEY_F7 || key == OF_KEY_F8 || key == OF_KEY_F9 || key == OF_KEY_F10 || key == OF_KEY_F11 || key == OF_KEY_F12
			 || key == OF_KEY_UP || key == OF_KEY_DOWN || key == OF_KEY_PAGE_UP || key == OF_KEY_PAGE_DOWN || key == OF_KEY_INSERT){
		}
		else if(key == '\t'){
			// how to handle a tab key
		}
		else if(this->canInsert((char)key)){
			string::iterator inIt = this->textValue.begin()+this->cursorIndex;
			this->textValue.insert(inIt, key);
			this->nextCursorIndex();
		}
	}
	return true;
}

float TextInput::getMaxTextWidth(){
	return (this->getWidth()-this->getHPadding()); // - 4);
}

bool TextInput::canInsert(char newCheck){
	stringstream checker;
	checker << newCheck;
	return this->canInsert(checker.str());
}

bool TextInput::canInsert(string newCheck){
	//this->textValue.length() < this->maxTextLength-1 && 
	return (this->getTheme()->getTextWidth(this->getOutputValue()+newCheck) < this->getMaxTextWidth());
}
