/*
 *  TextOutput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 26/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "TextOutput.h"

TextOutput::TextOutput(){
}

TextOutput::~TextOutput(){
}

void TextOutput::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_TEXT_OUTPUT;
	this->setName("Text");
	this->setLabel("Text");
	this->setStringValue();
	this->setPadding();
}

void TextOutput::draw(float x, float y, float w, float h){
	this->draw(x, y, w, h, this->getTheme()->drawTextOutputBorders());
}

void TextOutput::draw(float x, float y, float w, float h, bool borders){
	if(borders){
		// only draw the background if we're also drawing borders
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x, y, w, h);
		
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
	
	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	string output = this->getOutputValue();
	this->getTheme()->drawText(output, this->padding.x+x, this->padding.y+y+(this->height+this->getTheme()->getTextHeight(output))/2.0-1, false);
}

string TextOutput::getOutputValue(){
	stringstream output;
	if(this->getLabel() != "")
		output << this->getLabel();
	output << this->textValue;
	return output.str();
}

float TextOutput::getHPadding(){
	return (this->getPaddingL()+this->getPaddingR());
}

float TextOutput::getVPadding(){
	return (this->getPaddingT()+this->getPaddingB());
}

float TextOutput::getPaddingL(){
	return this->padding.x;
}

float TextOutput::getPaddingT(){
	return this->padding.y;
}

float TextOutput::getPaddingR(){
	return this->padding.width;
}

float TextOutput::getPaddingB(){
	return this->padding.height;
}

void TextOutput::setPadding(float l, float t, float r, float b){
	this->padding.x = l;
	this->padding.y = t;
	this->padding.width = r;
	this->padding.height = b;
}

string TextOutput::getString(string selector){
	if(this->selectSelf(selector))
		return this->textValue;
	return ShishaElement::getString(selector);
}

void TextOutput::setString(string value){
	this->textValue = value;
}
