/*
 *  TextInput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 06/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_TEXT_INPUT
#define _OFX_GUISHISHA_TEXT_INPUT

#include "TextOutput.h"

#define DEFAULT_CURSOR_BLINK_SPEED	30

class TextInput : public TextOutput{
	protected:
		int cursorIndex, cursorOffsetX, cursorOffsetY, cursorHeight, cursorBlinkSpeed;
		int userCursorHeight, userCursorOffsetY;
		bool cursorBlinkState;
		int maxTextLength;
	
		int calculateMaxTextLength();
		bool canInsert(char newCheck=' ');
		bool canInsert(string newCheck="");
		float getMaxTextWidth();
	
		int getClosestLetterIndex(float x, float y);
		virtual void setCursorLocation(float x=-1, float y=-1);
	
		virtual void onRelease(string cursorID);
	
	public:
		TextInput();
		~TextInput();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
		virtual void update();
	
		virtual int getGuiDisplayState();
	
		virtual void nextCursorIndex();
		virtual void previousCursorIndex();
		virtual void setCursorIndex(int index=-1);
		virtual void setDimensions(float width, float height);
		virtual void setPadding(float l=0.0, float t=0.0, float r=0.0, float b=0.0);
		virtual void setLabel(string label);
		virtual void setTheme(ShishaTheme* theme=NULL, ShishaTheme* doNotDelete=NULL, bool themeInherited=false);
		virtual void setUserCursorHeight(int userCursorHeight=0, int userCursorOffsetY=0);
	
		virtual void draw(float x, float y, float w, float h);
		virtual void draw(float x, float y, float w, float h, bool borders);
	
		virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkKeyPressed(int key);
};

#endif
