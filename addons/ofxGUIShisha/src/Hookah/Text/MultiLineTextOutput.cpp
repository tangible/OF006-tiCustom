/*
 *  MultiLineTextOutput.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 29/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "MultiLineTextOutput.h"

MultiLineTextOutput::MultiLineTextOutput(){
	this->maxLineCount = -1;
}

MultiLineTextOutput::~MultiLineTextOutput(){
	this->textLines.clear();
}

void MultiLineTextOutput::drawAsRectangle(float x, float y, float w, float h, bool borders){
	bool drawStartSpaces = false;
	ofNoFill();

	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
		ofRect(x, y, w, h);
	}
	
	int maxLines = this->maxLineCount;
	if(maxLines <= -1)
		maxLines = this->textLines.size();

	this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_1, this);
	for(int i=0; i < this->textLines.size() && i < maxLines; i++){
		string cLine = this->textLines[i];
		if(!drawStartSpaces && cLine.length() > 0 && cLine.at(0) == ' ')
			cLine = cLine.substr(1);
		this->getTheme()->drawText(cLine, x, y+this->getTheme()->getTextHeight(".")*(i+1), false);
	}
/**	if(this->inputEnabled && this->cursorBlinkState && this->cursorXIndex != -1 && this->cursorOffsetX != -1 && this->cursorOffsetY != -1){
		ofLine(x+this->cursorOffsetX, y+this->cursorOffsetY, x+this->cursorOffsetX, y+this->cursorOffsetY+this->cursorHeight);
	}*/
	ofFill();
	this->getTheme()->setColour();
}

void MultiLineTextOutput::startNewLine(string lineText){
	if(lineText == "" || this->getTheme()->getTextWidth(lineText) <= this->getMaxLineWidth())
		this->textLines.push_back(lineText);
	else{
		int cSpace = lineText.find(' ');
		// we have a space and at least the first word will fit on a line...
		if(cSpace != string::npos && this->getTheme()->getTextWidth(lineText.substr(0, cSpace)) <= this->getMaxLineWidth()){
			// we found a space... check word by word
			string cWord = lineText.substr(0, cSpace);
			int goodSpace = -1;
			while(cSpace != string::npos && this->getTheme()->getTextWidth(cWord) <= this->getMaxLineWidth()){
				goodSpace = cSpace;
				cSpace = lineText.find(' ', cSpace+1);
				if(cSpace != string::npos)
					cWord = lineText.substr(0, cSpace);
			}
			if(goodSpace != -1){
				this->textLines.push_back(lineText.substr(0, goodSpace+1));
				this->startNewLine(lineText.substr(goodSpace+1));
			}
		}
		else{
			// no space... must break the word, so check character by character
			int cIndex = 0;
			string cWord = "";
			bool appendPrev = false;
			if(this->textLines.size() > 0){
				// might we fit some of our text on the previous line?
				string prevLine = this->textLines[this->textLines.size()-1];
				if(this->getTheme()->getTextWidth(prevLine) < this->getMaxLineWidth()){
					cWord = prevLine;
					appendPrev = true;
				}
			}
			// build it up character by character until we've exceeded the line size
			while(this->getTheme()->getTextWidth(cWord) <= this->getMaxLineWidth())
				cWord += lineText.at(cIndex++);
			
			if(cIndex > 0){
				cIndex--; // whatever the last character we tried to add made us exceed line size, so go back one
				if(appendPrev)
					this->textLines[this->textLines.size()-1] += lineText.substr(0, cIndex);
				else
					this->textLines.push_back(lineText.substr(0, cIndex));
				
				// put the rest onto a new line
				this->startNewLine(lineText.substr(cIndex));
			}
		}
	}
}

void MultiLineTextOutput::wrapLines(){
	this->textLines.clear();
	
	if(this->textValue.find('\n') != string::npos){
		vector<string> splits = split(this->textValue, '\n');
		string cLine = "";
		for(int i = 0; i < splits.size(); i++){
			cLine = splits[i];
			if(i < splits.size()-1 || this->textValue.at(this->textValue.length()-1) == '\n')
				cLine += '\n';
			
			this->startNewLine(cLine);
		}
	}
	else
		this->startNewLine(this->textValue);
	
	/**	cout << "textValue:" << this->textValue.length() << ":" << this->textValue << ":" << endl;
	 cout << "wrapped:" << this->textLines.size() << ":" << endl;
	 for(int i=0; i < this->textLines.size(); i++)
	 cout << "\t[" << i << "]:" << this->textLines[i].length() << ":" << this->textLines[i] << ":" << endl;
	 */
}

int MultiLineTextOutput::getMaxLineWidth(){
	return (this->getWidth()-5);
}

string MultiLineTextOutput::getTextValue(){
	return this->textValue;
/**	string result = "";
	for(int i=0; i < this->textLines.size(); i++)
		result += this->textLines[i];
	return result;*/
}

void MultiLineTextOutput::setTextValue(string textValue){
//	if(this->maxTextLength != -1 && textValue.length() > this->maxTextLength)
//		textValue = textValue.substr(0, this->maxTextLength);
	this->textValue = textValue;
	this->wrapLines();
//	this->setCursorIndex(this->textValue.length());
}

string MultiLineTextOutput::getString(string selector){
	if(this->selectSelf(selector))
		return this->getTextValue();
	return SELECTOR_UNIDENTIFIED_STRING;
}

void MultiLineTextOutput::setString(string value){
	this->setTextValue(value);
}
