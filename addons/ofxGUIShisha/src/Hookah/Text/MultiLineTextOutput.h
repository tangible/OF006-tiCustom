/*
 *  MultiLineTextOutput.h
 *  openFrameworks
 *
 *  Created by Pat Long on 29/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_MULTILINE_TEXT_OUTPUT
#define _OFX_GUISHISHA_MULTILINE_TEXT_OUTPUT

#include "ShishaElement.h"

class MultiLineTextOutput : public ShishaElement{
	protected:
		string textValue;
		vector<string> textLines;
		int maxLineCount;
	
		void startNewLine(string lineText="");
		virtual void wrapLines();
	
		int getMaxLineWidth();
//		int getTextWidth(string text);
	
		virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
		
	public:
		MultiLineTextOutput();
		~MultiLineTextOutput();
	
		string getTextValue();
		void setTextValue(string textValue="");
	
		virtual string getString(string selector=DEFAULT_SELECTOR);
		virtual void setString(string value="");
};

#endif
