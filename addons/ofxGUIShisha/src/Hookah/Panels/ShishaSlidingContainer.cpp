/*
 *  ShishaSlidingContainer.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 04/06/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ShishaSlidingContainer.h"

ShishaSlidingContainer::ShishaSlidingContainer(){
}

ShishaSlidingContainer::~ShishaSlidingContainer(){
}

void ShishaSlidingContainer::init(float x, float y, float width, float height, int slideDirection){
	ShishaContainer::init(x, y, width, height);
	this->setName("ShishaSlidingContainer");
	this->x = x;
	this->y = y;
	this->cSlideX = x;
	this->cSlideY = y;
	this->setAskBeforeSlide(false); // by, default we slide when we want
	this->waitingToSlide = false;
	this->setSlideDirection(slideDirection);
	this->setSlideTime();
	this->setSlideAwayTimeout();
	this->setSlidOut(true);
	this->doneSlide = true;
	this->slidOutTime = 0;
	this->setSlideMovement();
	this->setSlideDestination();
	
	this->initSlideTab();
}

void ShishaSlidingContainer::initSlideTab(){
	this->slideTab = new ShishaSlidingContainer::SlideTab(this);
	
	float tabWidth = 50;
	float tabHeight = 25;
	
	if(this->slideDirection == SLIDE_DIRECTION_UP)
		this->slideTab->init(this->x+this->width/2.0-tabWidth/2.0, this->y-tabHeight, tabWidth, tabHeight);
	else if(this->slideDirection == SLIDE_DIRECTION_DOWN)
		this->slideTab->init(this->x+this->width/2.0-tabWidth/2.0, this->y+this->height, tabWidth, tabHeight);
	else if(this->slideDirection == SLIDE_DIRECTION_RIGHT)
		this->slideTab->init(this->x+this->width, this->y+this->height/2.0-tabWidth/2.0, tabHeight, tabWidth);
	else // if(this->slideDirection == SLIDE_DIRECTION_LEFT)
		this->slideTab->init(this->x-tabHeight, this->y+this->height/2.0-tabWidth/2.0, tabHeight, tabWidth);
	
	this->slideTab->setLabel("");
	this->slideTab->setName(this->getName()+":slideTab");
	
	this->addElement(this->slideTab, false, false);
}

bool ShishaSlidingContainer::shouldSlideAway(){
//	cout << "shouldSlideAway:" << this->getName() << ":" << (this->isSlidOut()?"slidOut":"notOut") << ":" << (this->isSliding()?"isSliding":"notSliding") << ":" << this->slideAwayTimeout << ":" << this->slidOutTime << ":" << endl;
	if(this->isSlidOut() && this->isReadyToSlide() && this->slideAwayTimeout != -1 && this->slidOutTime != -1){
		if((ofGetElapsedTimeMillis() - this->slidOutTime) >= this->slideAwayTimeout)
			return true;
	}
	return false;
}

void ShishaSlidingContainer::setSlideMovement(float slideMoveX, float slideMoveY){
	this->slideMoveX = slideMoveX;
	this->slideMoveY = slideMoveY;
}

void ShishaSlidingContainer::setSlideDestination(float slideDestX, float slideDestY){
	if(slideDestX == -1.0)
		slideDestX = this->x;
	if(slideDestY == -1.0)
		slideDestY = this->y;
	this->slideDestX = slideDestX;
	this->slideDestY = slideDestY;
}

void ShishaSlidingContainer::setSlidOut(bool slidOut){
	this->slidOut = slidOut;
}

void ShishaSlidingContainer::startSlide(){
	this->prepareForMotion();
	this->doneSlide = false;
}

void ShishaSlidingContainer::endSlide(){
	this->slideMoveX = 0.0;
	this->slideMoveY = 0.0;
	this->doneSlide = true;
	this->slidOut = !this->slidOut;
	this->slidOutTime = (this->slidOut)?ofGetElapsedTimeMillis():-1;
//	if(!this->slidOut)
//		this->clearCursors();
}

bool ShishaSlidingContainer::isDoneSliding(){
	return ((this->slideMoveX == 0.0 || (this->slideMoveX > 0.0 && this->cSlideX >= this->slideDestX) || (this->slideMoveX < 0.0 && this->cSlideX <= this->slideDestX)) 
	&& (this->slideMoveY == 0.0 || (this->slideMoveY > 0.0 && this->cSlideY >= this->slideDestY) || (this->slideMoveY < 0.0 && this->cSlideY <= this->slideDestY)));	
}

void ShishaSlidingContainer::slideTo(float x, float y, bool force){
	if(this->isSliding())
		return;
	int slideTime = this->slideTime;
	float slideX = (x - this->cSlideX) / slideTime;
	float slideY = (y - this->cSlideY) / slideTime;
	this->setSlideMovement(slideX, slideY);
	this->setSlideDestination(x, y);
	if(force || !this->askBeforeSlide)
		this->startSlide();
	else
		this->waitingToSlide = true;
}

bool ShishaSlidingContainer::isSlidOut(){
	return this->slidOut;
}

bool ShishaSlidingContainer::isSliding(){
	return !this->doneSlide;
}

bool ShishaSlidingContainer::isReadyToSlide(){
	return (!this->isSliding() && !this->isWaitingToSlide());
}

bool ShishaSlidingContainer::isWaitingToSlide(){
	return this->waitingToSlide;
}

void ShishaSlidingContainer::doSlide(){
	if(this->isWaitingToSlide() && this->doneSlide){
		this->waitingToSlide = false;
		this->startSlide();
	}
}

ShishaElement* ShishaSlidingContainer::getElement(string selector, string& subSelector){
	if(selector == "SlideTab")
		return this->slideTab;
	return ShishaContainer::getElement(selector, subSelector);
}

void ShishaSlidingContainer::tabMoved(float xMove, float yMove){
	this->x += xMove;
	this->y += yMove;
	this->cropRect.x += xMove;
	this->cropRect.y += yMove;
	this->setPosition(this->cSlideX+xMove, this->cSlideY+yMove);
}

void ShishaSlidingContainer::setGUIState(int guiState){
	if(guiState != this->guiState){
		if(guiState == GUI_ELEMENT_STATE_MOUSEOUT)
			this->slidOutTime = ofGetElapsedTimeMillis();
		else
			this->slidOutTime = -1;
	}
	ShishaContainer::setGUIState(guiState);
}

void ShishaSlidingContainer::setAskBeforeSlide(bool askBeforeSlide){
	this->askBeforeSlide = askBeforeSlide;
}

void ShishaSlidingContainer::setPosition(float x, float y){
//	cout << this->getName() << ":setPosition:" << x << "," << y << ":" << endl;
	float xOffset = x - this->cSlideX;
	float yOffset = y - this->cSlideY;
	this->cSlideX = x;
	this->cSlideY = y;
	for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
		float newX = ((ShishaElement*)(*it).second)->getX() + xOffset;
		float newY = ((ShishaElement*)(*it).second)->getY() + yOffset;
		((*it).second)->setPosition(newX, newY);
	}
}

int ShishaSlidingContainer::getSlideDirection(){
	return this->slideDirection;
}

float ShishaSlidingContainer::getSlideX(){
	return this->cSlideX;
}

float ShishaSlidingContainer::getSlideY(){
	return this->cSlideY;
}

void ShishaSlidingContainer::setSlideDirection(int slideDirection){
	this->slideDirection = slideDirection;
}

void ShishaSlidingContainer::setSlideTime(int slideTime){
	this->slideTime = slideTime;
}

void ShishaSlidingContainer::setSlideAwayTimeout(int slideAwayTimeout){
	this->slideAwayTimeout = slideAwayTimeout;
}

bool ShishaSlidingContainer::slideIn(bool force){
	if(this->getSlideDirection() == SLIDE_DIRECTION_UP)
		this->slideTo(this->x, this->y+this->height, force);
	else if(this->getSlideDirection() == SLIDE_DIRECTION_DOWN)
		this->slideTo(this->x, this->y-this->height, force);
	else if(this->getSlideDirection() == SLIDE_DIRECTION_RIGHT)
		this->slideTo(this->x-this->width, this->y, force);
	else // if(this->slideDirection == SLIDE_DIRECTION_LEFT)
		this->slideTo(this->x+this->width, this->y, force);
}

bool ShishaSlidingContainer::slideOut(bool force){
	this->slideTo(this->x, this->y, force);
}

bool ShishaSlidingContainer::slideToggle(bool force){
	if(this->slidOut)
		this->slideIn(force);
	else
		this->slideOut(force);
}

void ShishaSlidingContainer::draw(){
	ShishaContainer::draw();
}

void ShishaSlidingContainer::draw(float x, float y, float w, float h, bool borders){
	ShishaContainer::draw(this->cSlideX, this->cSlideY, w, h, borders);
}

void ShishaSlidingContainer::update(){
	ShishaContainer::update();
	
	if(this->shouldSlideAway())
		this->slideIn(true);
	
	if(!this->isWaitingToSlide()){
		if(this->slideMoveX != 0.0 || this->slideMoveY != 0.0){
			this->setPosition(this->cSlideX + this->slideMoveX, this->cSlideY + this->slideMoveY);
			if(this->isDoneSliding())
				this->endSlide();
		}
	}
}

bool ShishaSlidingContainer::checkCursorHover(int x, int y, string cursorID, float w, float h){
	bool check = (!this->cropBounds || this->isSlidOut() || this->slideTab->checkCursorHover(x, y, cursorID, w, h));
	if(check)
		check = ShishaContainer::checkCursorHover(x, y, cursorID, w, h);
	return check;
}

void ShishaSlidingContainer::SlideTab::onRelease(string cursorID){
	bool clicked = true; //(this->cursors.isReleasedOn(cursorID) && (this->cursors.isClickedOn(cursorID) || this->cursors.isDraggedOn(cursorID)));
	BasicButton::onRelease(cursorID);
	if(clicked && this->container != NULL && this->container->isReadyToSlide()){
		this->container->slideToggle();
	}
}

bool ShishaSlidingContainer::SlideTab::isActive(){
	return (BasicButton::isActive() || this->container->isSlidOut() || (!this->container->isSlidOut() && (this->container->isSliding() || this->container->isWaitingToSlide())));
}

void ShishaSlidingContainer::SlideTab::draw(float x, float y, float w, float h){
	BasicButton::draw(x, y, w, h);
	if(this->container != NULL){
		if(this->container->getSlideDirection() == SLIDE_DIRECTION_UP)
			this->drawArrowUp(x, y, w, h);
		else if(this->container->getSlideDirection() == SLIDE_DIRECTION_DOWN)
			this->drawArrowDown(x, y, w, h);
		else if(this->container->getSlideDirection() == SLIDE_DIRECTION_RIGHT)
			this->drawArrowRight(x, y, w, h);
		else // if(this->slideDirection == SLIDE_DIRECTION_LEFT)
			this->drawArrowLeft(x, y, w, h);
	}
	else
		this->drawDefaultSymbol(x, y, w, h);
}

void ShishaSlidingContainer::SlideTab::drawArrowLeft(float x, float y, float w, float h){
	float x1 = x+w-5; float y1 = y+5;
	float x2 = x+5; float y2 = y+h/2.0;
	float x3 = x+w-5; float y3 = y+h-5;
	
	ofLine(x1, y1, x2, y2);
	ofLine(x2, y2, x3, y3);
	ofLine(x3, y3, x1, y1);
}

void ShishaSlidingContainer::SlideTab::drawArrowRight(float x, float y, float w, float h){
	float x1 = x+5; float y1 = y+5;
	float x2 = x+w-5; float y2 = y+h/2.0;
	float x3 = x+5; float y3 = y+h-5;
	
	ofLine(x1, y1, x2, y2);
	ofLine(x2, y2, x3, y3);
	ofLine(x3, y3, x1, y1);
}

void ShishaSlidingContainer::SlideTab::drawArrowUp(float x, float y, float w, float h){
	float x1 = x+5; float y1 = y+h-5;
	float x2 = x+w/2.0; float y2 = y+5;
	float x3 = x+w-5; float y3 = y+h-5;
	
	ofLine(x1, y1, x2, y2);
	ofLine(x2, y2, x3, y3);
	ofLine(x3, y3, x1, y1);
}

void ShishaSlidingContainer::SlideTab::drawArrowDown(float x, float y, float w, float h){
	float x1 = x+5; float y1 = y+5;
	float x2 = x+w/2.0; float y2 = y+h-5;
	float x3 = x+w-5; float y3 = y+5;
	
	ofLine(x1, y1, x2, y2);
	ofLine(x2, y2, x3, y3);
	ofLine(x3, y3, x1, y1);
}

void ShishaSlidingContainer::SlideTab::drawDefaultSymbol(float x, float y, float w, float h){
	float radius = ((w < h)?w:h)/4.0;
	ofCircle(x+w/2.0, y+h/2.0, radius);
}

