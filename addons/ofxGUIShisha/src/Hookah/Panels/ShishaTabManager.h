/*
 *  ShishaTabManager.h
 *  openFrameworks
 *
 *  Created by Pat Long on 07/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHATABMANAGER
#define _OFX_GUISHISHA_SHISHATABMANAGER

#include "ShishaContainer.h"
#include "ShishaTabContainer.h"

class ShishaTabManager : public ShishaContainer{
	protected:
		vector<ShishaTabContainer*> containers;
		ShishaTabContainer* activeContainer;
		bool allowEmptyTab;
		int tabAnimationMode, tabAnimationSpeed;
	
		virtual void drawActiveContainer();
		virtual void drawEmptyContainer();
	
		virtual void updateContainers();
		virtual bool checkContainersCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkContainersCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkContainersCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkContainersCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		
		virtual bool checkContainersKeyPressed(int key);
		virtual bool checkContainersKeyReleased(int key);
	
	
	public:
		ShishaTabManager();
		~ShishaTabManager();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
	
		virtual void draw();
		virtual void update();
	
		virtual void addContainer(ShishaTabContainer* container, bool inheritAnimation=true);
		virtual bool canToggle(ShishaTabContainer* container);
	
		void setAllowEmptyTab(bool allowEmptyTab);
		void setTabAnimationMode(int tabAnimationMode=DEFAULT_TABCONTAINER_ANIMATE_MODE, int tabAnimationSpeed=DEFAULT_TABCONTAINER_ANIMATE_SPEED);
	
		virtual bool getBool(string selector=DEFAULT_SELECTOR);
	
		virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		
		virtual bool checkKeyPressed(int key);
		virtual bool checkKeyReleased(int key);
};

#endif
