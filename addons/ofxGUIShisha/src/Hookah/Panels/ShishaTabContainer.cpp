/*
 *  ShishaTabContainer.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 07/12/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ShishaTabContainer.h"

ShishaTabContainer::ShishaTabContainer(){
}

ShishaTabContainer::~ShishaTabContainer(){
}

void ShishaTabContainer::init(float x, float y, float width, float height){
	this->containerTab = NULL;
	this->tabManager = NULL;
	this->active = NULL;
	this->animatingHide = false;
	this->animatingShow = false;
	this->waitingToShow = false;
	this->doneAnimating = true;
	
	ShishaContainer::init(x, y, width, height);
	this->setName("ShishaTabContainer");
	this->setAnimationMode();
	this->initContainerTab();
}

void ShishaTabContainer::initContainerTab(){
	this->containerTab = new ShishaTabContainer::ShishaTab(this);
	
	float tabWidth = DEFAULT_TABCONTAINER_TAB_WIDTH;
	float tabHeight = DEFAULT_TABCONTAINER_TAB_HEIGHT;
	
	this->containerTab->init(this->x+this->width/2.0-tabWidth/2.0, this->y-tabHeight, tabWidth, tabHeight);
	this->containerTab->setLabel("");
	this->containerTab->setName(this->getName()+":slideTab");
	
	//this->addElement(this->containerTab, false, false);
}

void ShishaTabContainer::update(){
	ShishaContainer::update();
	if(this->isAnimating()){
		if(this->checkAnimationFinished()){
			this->finishAnimating();
		}
	}
}

void ShishaTabContainer::animateHide(){
	this->animatingHide = true;
	this->doneAnimating = false;
	if(this->animationMode != TABCONTAINER_ANIMATE_MODE_NONE){
		this->setLocation(this->hiddenDimensions.x, this->hiddenDimensions.y, this->hiddenDimensions.width, this->hiddenDimensions.height, ((this->moveFrameCurrent > 0)?this->moveFrameCurrent:this->animationSpeed));
	}
}

void ShishaTabContainer::animateShow(){
	if(this->waitingToShow)
		this->waitingToShow = false;
	this->animatingShow = true;
	this->doneAnimating = false;
	if(this->animationMode != TABCONTAINER_ANIMATE_MODE_NONE){
		this->setLocation(this->visibleDimensions.x, this->visibleDimensions.y, this->visibleDimensions.width, this->visibleDimensions.height, ((this->moveFrameCurrent > 0)?this->moveFrameCurrent:this->animationSpeed));
	}
}

bool ShishaTabContainer::checkAnimationFinished(){
	if(this->animationMode == TABCONTAINER_ANIMATE_MODE_NONE){
		return true;
	}
	else{
		return (this->dx == 0 && this->dy == 0 && this->dw == 0 && this->dh == 0 && this->moveFrameCurrent == 0 && this->moveFrameTotal == 0);
	}
	return true;
}

void ShishaTabContainer::finishAnimating(){
	if(this->isAnimatingHide()){
		this->animatingHide = false;
		if(this->active)
			this->active = false;
	}
	else if(this->isAnimatingShow()){
		this->animatingShow = false;
	}
	this->doneAnimating = true;
}

void ShishaTabContainer::onPosition(float x, float y, float oldX, float oldY){
	ShishaContainer::onPosition(x, y, oldX, oldY);
	if(!this->isAnimating()){
		this->visibleDimensions.x = x;
		this->visibleDimensions.y = y;
	}
	
	if(this->animationMode != TABCONTAINER_ANIMATE_MODE_NONE){
		switch(this->animationMode){
			case TABCONTAINER_ANIMATE_MODE_SLIDE_TOP:
				this->hiddenDimensions.x = this->getX();
				this->hiddenDimensions.y = this->getY();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOM:
				this->hiddenDimensions.x = this->getX();
				this->hiddenDimensions.y = this->getY()+this->getHeight();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_LEFT:
				this->hiddenDimensions.x = this->getX();
				this->hiddenDimensions.y = this->getY();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_RIGHT:
				this->hiddenDimensions.x = this->getX()+this->getWidth();
				this->hiddenDimensions.y = this->getY();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_TOPLEFT:
				this->hiddenDimensions.x = this->getX();
				this->hiddenDimensions.y = this->getY();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_TOPRIGHT:
				this->hiddenDimensions.x = this->getX()+this->getWidth();
				this->hiddenDimensions.y = this->getY();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMLEFT:
				this->hiddenDimensions.x = this->getX();
				this->hiddenDimensions.y = this->getY()+this->getHeight();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMRIGHT:
				this->hiddenDimensions.x = this->getX()+this->getWidth();
				this->hiddenDimensions.y = this->getY()+this->getHeight();
				break;
		}
	}
}

void ShishaTabContainer::onResize(float width, float height){
	ShishaContainer::onResize(width, height);
	this->visibleDimensions.width = width;
	this->visibleDimensions.height = height;
	
	if(this->animationMode != TABCONTAINER_ANIMATE_MODE_NONE){
		switch(this->animationMode){
			case TABCONTAINER_ANIMATE_MODE_SLIDE_TOP:
				this->hiddenDimensions.width = this->getWidth();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOM:
				this->hiddenDimensions.width = this->getHeight();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_LEFT:
				this->hiddenDimensions.height = this->getHeight();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_RIGHT:
				this->hiddenDimensions.height = this->getHeight();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_TOPLEFT:
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_TOPRIGHT:
				this->hiddenDimensions.x = this->getX()+this->getWidth();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMLEFT:
				this->hiddenDimensions.y = this->getY()+this->getHeight();
				break;
			case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMRIGHT:
				this->hiddenDimensions.x = this->getX()+this->getWidth();
				this->hiddenDimensions.y = this->getY()+this->getHeight();
				break;
		}
	}
}

bool ShishaTabContainer::isActive(){
	return this->active;
}

bool ShishaTabContainer::isAnimating(){
	return (this->isAnimatingHide() || this->isAnimatingShow());
}

bool ShishaTabContainer::isAnimatingHide(){
	return this->animatingHide;
}

bool ShishaTabContainer::isAnimatingShow(){
	return this->animatingShow;
}

bool ShishaTabContainer::isDoneAnimating(){
	return this->doneAnimating;
}

bool ShishaTabContainer::isWaitingToShow(){
	return this->waitingToShow;
}

void ShishaTabContainer::doShow(bool force){
	if(force){
		this->animateShow();
		if(!this->active)
			this->active = true;
	}
	else{
		this->waitingToShow = true;
	}
}

void ShishaTabContainer::doHide(bool immediate){
	int myAnimationSpeed = this->animationSpeed;
	if(immediate) this->animationSpeed = 1;
	this->animateHide();
	if(immediate) this->animationSpeed = myAnimationSpeed;
}

bool ShishaTabContainer::toggleActive(){
	if(this->tabManager != NULL && this->tabManager->canToggle(this)){
		if(this->isActive()){
			this->doHide();
		}
		else{
			this->doShow();
		}
	}
}

ShishaElement* ShishaTabContainer::getContainerTab(){
	return this->containerTab;
}

ShishaElement* ShishaTabContainer::getElement(string selector, string& subSelector){
	if(selector == "ContainerTab")
		return this->containerTab;
	return ShishaContainer::getElement(selector, subSelector);
}

void ShishaTabContainer::setLabel(string label){
	ShishaContainer::setLabel(label);
	this->containerTab->setLabel(label);
}

void ShishaTabContainer::setTabManager(ShishaTabManager* tabManager){
	this->tabManager = tabManager;
}

void ShishaTabContainer::setAnimationMode(int animationMode, int animationSpeed){
	this->animationMode = animationMode;
	this->animationSpeed = animationSpeed;
	if(this->animationMode == TABCONTAINER_ANIMATE_MODE_NONE){
		this->animationSpeed = 0;
		this->cropBounds = false;
	}
	else{
		if(this->animationSpeed <= 0){
			this->setAnimationMode(TABCONTAINER_ANIMATE_MODE_NONE, 0);
		}
		else{
			this->cropBounds = true;
			switch(this->animationMode){
				case TABCONTAINER_ANIMATE_MODE_SLIDE_TOP:
					this->setHiddenDimensions(this->getX(), this->getY(), this->getWidth(), 1.0);
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOM:
					this->setHiddenDimensions(this->getX(), this->getY()+this->getHeight(), this->getWidth(), 1.0);
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_LEFT:
					this->setHiddenDimensions(this->getX(), this->getY(), 1.0, this->getHeight());
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_RIGHT:
					this->setHiddenDimensions(this->getX()+this->getWidth(), this->getY(), 1.0, this->getHeight());
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_TOPLEFT:
					this->setHiddenDimensions(this->getX(), this->getY(), 1.0, 1.0);
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_TOPRIGHT:
					this->setHiddenDimensions(this->getX()+this->getWidth(), this->getY(), 1.0, 1.0);
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMLEFT:
					this->setHiddenDimensions(this->getX(), this->getY()+this->getHeight(), 1.0, 1.0);
					break;
				case TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMRIGHT:
					this->setHiddenDimensions(this->getX()+this->getWidth(), this->getY()+this->getHeight(), 1.0, 1.0);
					break;
				default:
					this->setAnimationMode(TABCONTAINER_ANIMATE_MODE_NONE, 0);
					break;
			}
		}
	}
}

void ShishaTabContainer::setHiddenDimensions(float x, float y, float w, float h){
	this->hiddenDimensions.x = x;
	this->hiddenDimensions.y = y;
	this->hiddenDimensions.width = w;
	this->hiddenDimensions.height = h;
}

void ShishaTabContainer::ShishaTab::onRelease(string cursorID){
	BasicButton::onRelease(cursorID);
	bool clicked = (this->cursors.isReleasedOn(cursorID) && (this->cursors.isClickedOn(cursorID) || this->cursors.isDraggedOn(cursorID)));
	if(clicked){
		this->container->toggleActive();
	}
}

bool ShishaTabContainer::ShishaTab::isActive(){
	return (BasicButton::isActive() || this->container->isActive());
}
