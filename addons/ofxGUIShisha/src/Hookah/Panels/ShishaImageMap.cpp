/*
 *  ShishaImageMap.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 18/06/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ShishaImageMap.h"

ShishaImageMap::ShishaImageMap(){
}

ShishaImageMap::~ShishaImageMap(){
	for(int i=0; i < this->mapElements.size(); i++)
		delete this->mapElements[i];
}

void ShishaImageMap::init(float x, float y, float width, float height){
	ShishaContainer::init(x, y, width, height);
	this->setName("ShishaImageMap");
	this->setHorizontalSpacing(0.0);
	this->setVerticalSpacing(0.0);
	this->shouldClearMapPixels = false;
	this->cOffsetX = 0.0;
	this->cOffsetY = 0.0;
	this->cBiggestOffset = 0.0;
}

ShishaImageMap::ImageMapElement* ShishaImageMap::getMapElement(ShishaElement* element){
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->element == element)
			return (*it);
	}
	return NULL;
}

bool ShishaImageMap::mapHasPixel(int x, int y){
	if(!this->shouldClearMapPixels)
		return true;
	
	bool check = true;
	
	// localize co-ordinates
	x -= this->x;
	y -= this->y;
	
	//retrieve the pixel
	unsigned char* pixel = this->mapInactive.getPixels(NULL, x, y, 1, 1);
	if(pixel[3] == 0)
		check = false;
	delete pixel;
	return check;
}

bool ShishaImageMap::loadImageMap(string filename, int stateCount){
	if(stateCount <= 0)
		return false;
	bool result = this->imageMap.loadImage(filename);
	this->imageMap.setImageType(OF_IMAGE_COLOR_ALPHA);
	
	float stateWidth = this->imageMap.width;
	float stateHeight = this->imageMap.height / stateCount;
	
	this->setDimensions(stateWidth, stateHeight);

	int cState = 0;
	unsigned char* pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapInactive.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	cState++;

	pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapInactiveHover.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	cState++;
	
	pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapActiveHover.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	cState++;
	
	pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapActive.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	cState++;
	
	return result;
}

void ShishaImageMap::clearArea(float x, float y, float w, float h){
	ColourRGBA clear;
	clear.setRGBA(255, 0, 255, 0); // we will clear them pixels like we mean it
	this->mapInactive.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
	this->mapInactiveHover.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
	this->mapActiveHover.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
	this->mapActive.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
}

ShishaImageMap::ImageMapElement* ShishaImageMap::mapElement(ShishaElement* element, float x, float y, float w, float h, bool containerAdd, bool drawElement){
	ImageMapElement* newMapElement;
	vector<ofImage*> mapImages; // = new vector<ofImage*>(); // using a pointer because we don't want it to go out of scope
	ColourRGBA clear;
	clear.setRGBA(56, 56, 56, 0); // we will seriously cut out the pixels that get sliced already - destructive eh
	bool shouldClearPixels = this->shouldClearMapPixels;

	ofImage* mapImage;
	unsigned char* pixels;
	
	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapInactive.getPixels(pixels, (int)x, (int)y, (int)w, (int)h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapInactive.clearPixels((int)x, (int)y, (int)w, (int)h, clear.r, clear.g, clear.b, clear.a);

	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapInactiveHover.getPixels(pixels, (int)x, (int)y, (int)w, (int)h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapInactiveHover.clearPixels((int)x, (int)y, (int)w, (int)h, clear.r, clear.g, clear.b, clear.a);

	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapActiveHover.getPixels(pixels, x, y, w, h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapActiveHover.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);

	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapActive.getPixels(pixels, x, y, w, h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapActive.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);

	newMapElement = new ImageMapElement(element, mapImages, drawElement);
	newMapElement->setMapLocation(x, y, w, h);
	
	this->mapElements.push_back(newMapElement);
	if(containerAdd)
		ShishaContainer::addElement(element, false, false);
	element->setLocation(this->x+x, this->y+y, w, h);

	return newMapElement;
}

bool ShishaImageMap::toggleElementVisibility(ShishaElement* element){
	ImageMapElement* mapElement = this->getMapElement(element);
	if(mapElement != NULL){
		return mapElement->toggleVisibility();
	}
	return false;
}

void ShishaImageMap::setShouldClearMapPixels(bool shouldClearMapPixels){
	this->shouldClearMapPixels = shouldClearMapPixels;
}

void ShishaImageMap::draw(){
	ShishaContainer::draw();
}

void ShishaImageMap::draw(float x, float y){
	ShishaContainer::draw(x, y);
}

void ShishaImageMap::draw(float x, float y, float w, float h, bool borders){
	this->mapInactive.draw(x, y);

	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++)
		(*it)->draw();
	
//	this->mapInactiveHover.draw(x, y);
//	this->mapActiveHover.draw(x, y);
//	this->mapActive.draw(x, y);
}

bool ShishaImageMap::checkCursorHover(int x, int y, string cursorID, float w, float h){
	bool check = ShishaContainer::checkCursorHover(x, y, cursorID, w, h);
	return ((this->mapHasPixel(x, y) && this->isHovered(cursorID, x, y))?true:check);
}

bool ShishaImageMap::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool check = ShishaContainer::checkCursorDrag(x, y, button, cursorID, w, h);
	return ((this->mapHasPixel(x, y) && this->isHovered(cursorID, x, y))?true:check);
}

bool ShishaImageMap::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool check = ShishaContainer::checkCursorPress(x, y, button, cursorID, w, h);
	return ((this->mapHasPixel(x, y) && this->isHovered(cursorID, x, y))?true:check);
}

bool ShishaImageMap::checkChildrenCursorHover(int x, int y, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorHover(x, y, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaImageMap::checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorDrag(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaImageMap::checkChildrenCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorPress(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaImageMap::checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorRelease(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}


ShishaImageMap::ImageMapElement::ImageMapElement(ShishaElement* element, vector<ofImage*> stateImages, bool drawElement){
	this->element = element;
	this->stateImages = stateImages;
	this->setMapLocation(element->getX(), element->getY(), element->getWidth(), element->getHeight());
	this->visible = true;
	this->drawElement = drawElement;
}

ShishaImageMap::ImageMapElement::~ImageMapElement(){
	for(int i=0; i < this->stateImages.size(); i++){
		delete this->stateImages[i];
		this->stateImages[i] = NULL;
	}
	this->stateImages.clear();
}

void ShishaImageMap::ImageMapElement::setMapLocation(float x, float y, float w, float h){
	this->mapLocation.x = x;
	this->mapLocation.y = y;
	this->mapLocation.width = w;
	this->mapLocation.height = h;
}

bool ShishaImageMap::ImageMapElement::toggleVisibility(){
	this->visible = !this->visible;
	return this->visible;
}

int ShishaImageMap::ImageMapElement::getGuiState(){
	return this->element->getGuiDisplayState();
}

void ShishaImageMap::ImageMapElement::draw(){
	if(this->element != NULL && this->visible){
		int guiState = this->getGuiState();
		if(guiState < this->stateImages.size()){
//			cout << "drawing:" << guiState << ":" << this->element->getX() << "," << this->element->getY() << ":" << endl;
			this->stateImages[guiState]->draw(this->element->getX(), this->element->getY(), this->element->getWidth(), this->element->getHeight());
			if(this->drawElement)
				this->element->draw(this->element->getX(), this->element->getY(), this->element->getWidth(), this->element->getHeight());
		}
	}
}

