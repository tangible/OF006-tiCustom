/*
 *  ShishaPanel.h
 *  openFrameworks
 *
 *  Created by Pat Long on 23/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHAPANEL
#define _OFX_GUISHISHA_SHISHAPANEL

#include "ShishaContainer.h"
#include "BarsIndex.h"
#include "ButtonIndex.h"
#include "TextIndex.h"

#define DEFAULT_SHISHA_PANEL_LABEL	"Shisha Panel"
#define DEFAULT_SHISHA_PANEL_NAME	"Panel"

class ShishaPanel : public ShishaContainer{
	protected:
		class TitleBar : public ShishaElement{
			private:
				ShishaPanel* panel;
			
			public:
				TitleBar(ShishaPanel* panel);
				~TitleBar();
			
				virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
			
				virtual void draw();
				virtual void draw(float x, float y);
				virtual void draw(float x, float y, float w, float h);
		};
		TitleBar* titleBar;
		string panelName;
		void initTitleBar();
	
		virtual void resetVerticalOffsets();
	
	public:
		ShishaPanel();
		~ShishaPanel();
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, string panelLabel=DEFAULT_SHISHA_PANEL_LABEL, string panelName=DEFAULT_SHISHA_PANEL_NAME);
	
		string getName();
	
		virtual void setPosition(float x, float y);
		virtual void setTheme(ShishaTheme* theme=NULL, ShishaTheme* doNotDelete=NULL, bool themeInherited=false);
		virtual void setWidth(float width);
	
/**		virtual void draw();
		virtual void draw(float x, float y);
		virtual void draw(float x, float y, float w, float h);*/
	
		virtual ShishaElement* addElement(ShishaElement* element, bool updateLocation=true, bool inheritTheme=true);
	
		virtual ShishaElement* addElement(float w, float h);
		virtual BasicButton* addButton(float w, float h);
		virtual ToggleButton* addToggleButton(float w, float h);
		virtual RadioSet* addRadioSet(float w, float h);
		virtual Bar* addBar(float w, float h);
		virtual FloatBar* addFloatBar(float w, float h);
		virtual SliderBar* addSliderBar(float w, float h);
		virtual TextInput* addTextInput(float w, float h);
		virtual TextOutput* addTextOutput(float w, float h);
};

#endif
