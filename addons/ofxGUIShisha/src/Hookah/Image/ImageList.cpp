/*
 *  ImageList.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 10/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "ImageList.h"

ImageList::ImageList(){
	this->activeImage = NULL;
	this->activeImageName = "";
}

ImageList::~ImageList(){
	for(map<string,ofImage*>::iterator it=this->images.begin(); it != this->images.end(); it++){
		delete (*it).second;
		this->images[(*it).first] = NULL;
	}
	this->images.clear();
}

void ImageList::init(float x, float y, float width, float height, int elementID){
	ShishaElement::init(x, y, width, height, elementID);
	this->setName("ImageList");
}

void ImageList::draw(float x, float y, float w, float h, bool borders){
	if(this->activeImage != NULL){
		this->activeImage->draw(x, y);
		if(borders){
			this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);
			ofNoFill();
			ofRect(x, y, this->activeImage->getWidth(), this->activeImage->getHeight());
			ofFill();
			this->getTheme()->setColour();
		}
	}
}

void ImageList::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	ShishaElement::setTheme(theme, doNotDelete, themeInherited);
	ShishaTheme* cTheme = this->getTheme();
	if(cTheme != NULL)
		this->drawBorders = cTheme->drawImageBorders();
}

int ImageList::addImage(string imageFileName, string imageName){
	ofImage* newImage = new ofImage();
	if(newImage->loadImage(imageFileName))
		return this->addImage(newImage, imageName);
	else
		delete newImage;
	return -1;
}

int ImageList::addImage(ofImage* image, string imageName){
	if(image == NULL)
		return -1;
	if(imageName == ""){
		if(image->getFileName() != "")
			imageName = image->getFileName();
		else
			imageName = ofToString(this->images.size(), 0);
	}
	this->images[imageName] = image;
	return this->images.size();
}

int ImageList::loadDirectory(string imageDir){
	if(imageDir != ""){
		ofxDirList dirList;
		int listCount;
		dirList.allowExt("jpg");
		dirList.allowExt("png");
		dirList.allowExt("gif");
		listCount = dirList.listDir(imageDir);
		for(int i=0; i < listCount; i++)
			this->addImage(dirList.getPath(i), dirList.getName(i));
		return listCount;
	}
	return 0;
}

ofImage* ImageList::getActiveImage(){
	return this->activeImage;
}

string ImageList::getActiveImageName(){
	return this->activeImageName;
}

bool ImageList::setActiveImage(string imageName){
	if(imageName == ""){
		this->activeImage = NULL;
		this->activeImageName = "";
		return true;
	}
	else if(this->images.find(imageName) != this->images.end()){
		this->activeImage = this->images[imageName];
		this->activeImageName = imageName;
		return true;
	}
	return false;
}
