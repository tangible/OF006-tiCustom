/*
 *  ToggleButton.h
 *  openFrameworks
 *
 *  Created by Pat Long on 27/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_TOGGLE_BUTTON
#define _OFX_GUISHISHA_TOGGLE_BUTTON

#include "BasicButton.h"

#define BUTTON_STYLE_TOGGLE_SOLID	0
#define BUTTON_STYLE_TOGGLE_CROSS	1

class ToggleButton : public BasicButton{
	protected:
		bool toggled;
		int toggleStyle;
	
		virtual void onPress(string cursorID);
		virtual void onDrag(string cursorID);
		virtual void onRelease(string cursorID);
		
	public:
		ToggleButton();
		~ToggleButton();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
		virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
		virtual void drawAsCircle(float x, float y, float w, float h, bool borders);
	
		virtual void setToggleStyle(int toggleStyle);
	
		virtual bool toggle();
		virtual bool toggleOn();
		virtual bool toggleOff();
	
		virtual int getGuiDisplayState();
	
		virtual bool getBool(string selector=DEFAULT_SELECTOR);
		virtual void setBool(bool value=false);
};

#endif
