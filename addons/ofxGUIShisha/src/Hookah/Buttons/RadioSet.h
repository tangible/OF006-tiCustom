/*
 *  RadioSet.h
 *  openFrameworks
 *
 *  Created by Pat Long on 27/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_RADIO_SET
#define _OFX_GUISHISHA_RADIO_SET

#include "ShishaContainer.h"
#include "ToggleButton.h"

#define DEFAULT_RADIO_LABEL "Radio #"

class RadioSet : public ShishaContainer{
protected:
	class RadioButton : public ToggleButton{
		protected:
			RadioSet* set;
			bool selected;
			int value;
		
			virtual void onDrag(string cursorID);
		
		public:
			RadioButton(RadioSet* set);
			~RadioButton();
			virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
		
			virtual void setElementID(int elementID);
		
			virtual int getValue();
		
			virtual bool toggle();
			virtual bool toggleOn();
			virtual bool toggleOff();
	};
	
	vector<RadioButton*> buttons;
	RadioButton* selected;
	int buttonShape, buttonStyle, buttonToggleStyle;
	
public:
	RadioSet();
	~RadioSet();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);

	virtual void setButtonShape(int buttonShape);
	virtual void setButtonStyle(int buttonStyle);
	virtual void setButtonToggleStyle(int buttonToggleStyle);
	
	virtual RadioButton* addButton(float w, float h, string label=DEFAULT_RADIO_LABEL, string name="", bool select=false);
	virtual void setSelected(RadioButton* button=NULL);
	virtual int getSelectedIndex();
	virtual int getSelectedValue();
	
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual string getString(string selector=DEFAULT_SELECTOR);
};

#endif
