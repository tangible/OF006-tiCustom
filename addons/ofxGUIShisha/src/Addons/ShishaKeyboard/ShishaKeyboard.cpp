/*
 *  ShishaKeyboard.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 16/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "ShishaKeyboard.h"

ShishaKeyboard::ShishaKeyboard(){
	this->imageBase = "";
}

ShishaKeyboard::~ShishaKeyboard(){
}

void ShishaKeyboard::init(float x, float y, float width, float height){
	ShishaImageMap* imageMap = new ShishaImageMap();
	imageMap->init(0, 0, 0, 0);
	imageMap->setName("ImageMap");
	imageMap->loadImageMap(this->imageBase+"tangible_keyboard.png");
	imageMap->setShouldClearMapPixels(true);
	
	width = imageMap->getWidth();
	height = imageMap->getHeight();
	ShishaContainer::init(x, y, width, height);
	this->setName("ShishaKeyboard");
	
	uppercase.loadImage( this->imageBase+"uppercase_white.png");
	undercase.loadImage( this->imageBase+"undercase_white.png");
	smallChars.loadImage( this->imageBase+"smalls_white.png");

	myText = "";
	anyKeyPressed = false;
	enterSubmit = false;
	shiftVal = false;
	capsVal = false;
	symbolVal = false;
	symbolToggleFlag = false;
	symbolKeyDown = false;
	shiftKeyPressed = false;
	
	for (int i =0; i < 50; i++)
	{
		keyNames[i] = "xxx";
		keysHeld[i] = false;
		keysTyped[i] = false;
		keyShift[i] = "XXX";
	}
	
	keyNames[0] = "q"; keyShift[0] = "Q"; keySymbol[0] = "1";
	keyNames[1] = "w"; keyShift[1] = "W"; keySymbol[1] = "2";
	keyNames[2] = "e"; keyShift[2] = "E"; keySymbol[2] = "3";
	keyNames[3] = "r"; keyShift[3] = "R"; keySymbol[3] = "4";
	keyNames[4] = "t"; keyShift[4] = "T"; keySymbol[4] = "5";
	keyNames[5] = "y"; keyShift[5] = "Y"; keySymbol[5] = "6";
	keyNames[6] = "u"; keyShift[6] = "U"; keySymbol[6] = "7";
	keyNames[7] = "i"; keyShift[7] = "I"; keySymbol[7] = "8";
	keyNames[8] = "o"; keyShift[8] = "O"; keySymbol[8] = "9";
	keyNames[9] = "p"; keyShift[9] = "P"; keySymbol[9] = "0";
	
	keyNames[10] = "a"; keyShift[10] = "A"; keySymbol[10] = "!";
	keyNames[11] = "s"; keyShift[11] = "S"; keySymbol[11] = "@";
	keyNames[12] = "d"; keyShift[12] = "D"; keySymbol[12] = "&";
	keyNames[13] = "f"; keyShift[13] = "F"; keySymbol[13] = "'";
	keyNames[14] = "g"; keyShift[14] = "G"; keySymbol[14] = "-";
	keyNames[15] = "h"; keyShift[15] = "H"; keySymbol[15] = "?";
	keyNames[16] = "j"; keyShift[16] = "J"; keySymbol[16] = ".";
	keyNames[17] = "k"; keyShift[17] = "K"; keySymbol[17] = ",";
	keyNames[18] = "l"; keyShift[18] = "L"; keySymbol[18] = "$";
	
	keyNames[19] = "z"; keyShift[19] = "Z"; keySymbol[19] = "*";
	keyNames[20] = "x"; keyShift[20] = "X"; keySymbol[20] = "#";
	keyNames[21] = "c"; keyShift[21] = "C"; keySymbol[21] = ":";
	keyNames[22] = "v"; keyShift[22] = "V"; keySymbol[22] = "/";
	keyNames[23] = "b"; keyShift[23] = "B"; keySymbol[23] = "(";
	keyNames[24] = "n"; keyShift[24] = "N"; keySymbol[24] = ")";
	keyNames[25] = "m"; keyShift[25] = "M"; keySymbol[25] = ";";
	
	keyNames[26] = " "; keyShift[26] = " "; keySymbol[26] = " ";
	
	
	keyNames[45] = "symbolsKey";
	keyNames[46] = "shiftKey";
	keyNames[47] = "Rtn";
	keyNames[48] = "capsKey";
	keyNames[49] = "Bksp";
	
	BasicButton* newButton;
	
	//-------------------------------------------------------------1st-Row-----------
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("q");
	imageMap->mapElement(newButton, 59, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("w");
	imageMap->mapElement(newButton, 109, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("e");
	imageMap->mapElement(newButton, 159, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("r");
	imageMap->mapElement(newButton, 208, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("t");
	imageMap->mapElement(newButton, 258, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("y");
	imageMap->mapElement(newButton, 308, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("u");
	imageMap->mapElement(newButton, 357, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("i");
	imageMap->mapElement(newButton, 407, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("o");
	imageMap->mapElement(newButton, 457, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("p");
	imageMap->mapElement(newButton, 506, 9, 44, 44);
	//-------------------------------------------------------------2nd-Row-----------
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("a");
	imageMap->mapElement(newButton, 59, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("s");
	imageMap->mapElement(newButton, 109, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("d");
	imageMap->mapElement(newButton,  159, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("f");
	imageMap->mapElement(newButton,208, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("g");
	imageMap->mapElement(newButton, 258, 58, 44, 44);
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("h");
	imageMap->mapElement(newButton, 308, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("j");
	imageMap->mapElement(newButton, 357, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("k");
	imageMap->mapElement(newButton, 407, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("l");
	imageMap->mapElement(newButton, 457, 58, 44, 44);
	//-------------------------------------------------------------3rd
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("z");
	imageMap->mapElement(newButton, 59, 107, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("x");
	imageMap->mapElement(newButton, 109, 107, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("c");
	imageMap->mapElement(newButton, 159, 107, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("v");
	imageMap->mapElement(newButton, 208, 107, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("b");
	imageMap->mapElement(newButton, 357, 107, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("n");
	imageMap->mapElement(newButton, 407, 107, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("m");
	imageMap->mapElement(newButton, 457, 107, 44, 44);
	
	
	//-------------------------------------------------------------special keys-----
	
	newButton = new ToggleButton();
	newButton->init();
	newButton->setName("shiftKey");
	imageMap->mapElement(newButton, 10, 107, 44, 44);
	
	newButton = new ToggleButton();
	newButton->init();
	newButton->setName("capsKey");
	imageMap->mapElement(newButton, 10, 58, 44, 44);
	
	newButton = new ToggleButton();
	newButton->init();
	newButton->setName("symbolsKey");
	imageMap->mapElement(newButton, 10, 9, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("Bksp");
	imageMap->mapElement(newButton, 506, 58, 44, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName(" ");
	imageMap->mapElement(newButton, 258, 107, 100, 44);
	
	newButton = new BasicButton();
	newButton->init();
	newButton->setName("Rtn");
	imageMap->mapElement(newButton, 506,  107, 44, 44);
	
	this->addElement(imageMap);
}

string ShishaKeyboard::getText(){
	return this->myText;
}

void ShishaKeyboard::setText(string text){
	this->myText = text;
}

bool ShishaKeyboard::getBool(string selector){
	if(this->selectSelf(selector)){
		if(keysHeld[47] && !enterSubmit){
			enterSubmit = true;
			return true;
		}
		return false;
	}
	else if(selector == "any"){
		return this->anyKeyPressed;
	}
	return ShishaContainer::getBool(selector);
}

int ShishaKeyboard::getInt(string selector){
	if(this->selectSelf(selector) || selector == "popChar"){
		if(this->keysHeld[47])
			return OF_KEY_RETURN;
		
		if(this->keysHeld[49])
			return OF_KEY_BACKSPACE;
		
		string text = this->getText();
		if(text.length() > 0){
			int theChar = text.at(0);
			if(selector == "popChar")
				this->setText(text.substr(1));
			return theChar;
		}
	}
	return SELECTOR_UNIDENTIFIED_INT; // maybe should be: ShishaContainer::getInt(selector)
}

string ShishaKeyboard::getString(string selector){
	if(this->selectSelf(selector))
		return this->getText();
	return SELECTOR_UNIDENTIFIED_STRING; // maybe should be: ShishaContainer::getString(selector)
}

void ShishaKeyboard::setString(string value){
	this->setText(value);
}

void ShishaKeyboard::setImageBase(string imageBase){
	this->imageBase = imageBase;
}

void ShishaKeyboard::draw(){
	ShishaContainer::draw();
	
	if ( capsVal == true || shiftVal == true)
	{ uppercase.draw( this->getX(), this->getY() );  }
    else
    { undercase.draw( this->getX(), this->getY() ); }
	
    if ( symbolVal) ofSetColor(0, 255, 255);
	smallChars.draw( this->getX(), this->getY() );
    ofSetColor(255, 255, 255);
	
	//ofSetColor(255, 255, 0);
	//ofDrawBitmapString(ofToString(ofGetFrameRate(), 0), ofGetScreenWidth()-400, 200);
	// font1Small.drawString( outputString, 460, 55);
	
//	ofSetColor(255, 255, 255);
}

void ShishaKeyboard::update(){
	ShishaContainer::update();
	
	anyKeyPressed = false;
	
    //----------------------------Check for Keys-----------------------
    bool addLetter = false;
    string nextLetter = "XXX";
	
    for (int i =0; i < 45; i++) //Cycle through the keys, see if they are pressed and add letters to the text field
    {
        string insertName = "ImageMap." + keyNames[i] + ".value" ;
		
		if(keysTyped[i]){
			this->setBoolValue(insertName, true);
		}
		
        bool trigger = (this->getBool(insertName));
        if (trigger == true) // && keysHeld[i] == false)
        {
			addLetter = true;
			nextLetter = keyNames[i];
			keysHeld[i] = true;
			//check for Caps Lock or shift Key
			if (shiftVal == true || capsVal == true)
			{
				nextLetter = keyShift[i];
			}
			if ( symbolVal == true)
			{
				nextLetter = keySymbol[i];
				symbolVal = false;
			}
        }
        else if ( trigger == false)
        {
			keysHeld[i] = false;
        }
    }
    if ( addLetter == true)
    {
		anyKeyPressed = true;
		myText += nextLetter;
		if(!this->shiftKeyPressed){
			shiftVal = false;
			this->setBoolValue("ImageMap.shiftKey.value", false);
		}
    }
	
    //------------------------------Bksp-Key---
    bool trigger = this->getBool("ImageMap.Bksp.value");
    if (trigger == true) // && keysHeld[49] == false)
    {
		if (myText.size() >0)
		{ myText.erase( myText.size()-1);   }
		else { myText = ""; }
		keysHeld[49] = true;
		anyKeyPressed = true;
    }
    else if ( trigger == false)
    {  keysHeld[49] = false;  }
	
    //------------------------------shift-Key / caps Lock / extra Symbols key---
/**    if( this->getBool("ImageMap.shiftKey.value") )
    {
		shiftVal = true;
    }*/
	/**    if( this->getBool("ImageMap.symbolsKey.value") )
	 {
	 symbolVal = true;
	 }*/
	
	shiftVal = this->getBool("ImageMap.shiftKey.value");
    capsVal = this->getBool("ImageMap.capsKey.value");
	symbolVal = this->getBool("ImageMap.symbolsKey.value");
	
    //------------------------------Return-Key---
    trigger = this->getBool("ImageMap.Rtn.value");
    if (trigger == true) // &&  keysHeld[47] == false)
    {
		stringstream logBuilder;
		logBuilder << "the text string is: " << myText << ":";
		ofLog(OF_LOG_VERBOSE, logBuilder.str());
		logBuilder.str("");
		myText = "";
		keysHeld[47] = true;
		anyKeyPressed = true;
    }
    else if ( trigger == false)
    {
		keysHeld[47] = false;
		enterSubmit = false;
	}
}

/**
 keyNames[0] = "q"; keyShift[0] = "Q"; keySymbol[0] = "1";
 keyNames[1] = "w"; keyShift[1] = "W"; keySymbol[1] = "2";
 keyNames[2] = "e"; keyShift[2] = "E"; keySymbol[2] = "3";
 keyNames[3] = "r"; keyShift[3] = "R"; keySymbol[3] = "4";
 keyNames[4] = "t"; keyShift[4] = "T"; keySymbol[4] = "5";
 keyNames[5] = "y"; keyShift[5] = "Y"; keySymbol[5] = "6";
 keyNames[6] = "u"; keyShift[6] = "U"; keySymbol[6] = "7";
 keyNames[7] = "i"; keyShift[7] = "I"; keySymbol[7] = "8";
 keyNames[8] = "o"; keyShift[8] = "O"; keySymbol[8] = "9";
 keyNames[9] = "p"; keyShift[9] = "P"; keySymbol[9] = "0";
 
 keyNames[10] = "a"; keyShift[10] = "A"; keySymbol[10] = "!";
 keyNames[11] = "s"; keyShift[11] = "S"; keySymbol[11] = "@";
 keyNames[12] = "d"; keyShift[12] = "D"; keySymbol[12] = "&";
 keyNames[13] = "f"; keyShift[13] = "F"; keySymbol[13] = "'";
 keyNames[14] = "g"; keyShift[14] = "G"; keySymbol[14] = "-";
 keyNames[15] = "h"; keyShift[15] = "H"; keySymbol[15] = "?";
 keyNames[16] = "j"; keyShift[16] = "J"; keySymbol[16] = ".";
 keyNames[17] = "k"; keyShift[17] = "K"; keySymbol[17] = ",";
 keyNames[18] = "l"; keyShift[18] = "L"; keySymbol[18] = "$";
 
 keyNames[19] = "z"; keyShift[19] = "Z"; keySymbol[19] = "*";
 keyNames[20] = "x"; keyShift[20] = "X"; keySymbol[20] = "#";
 keyNames[21] = "c"; keyShift[21] = "C"; keySymbol[21] = ":";
 keyNames[22] = "v"; keyShift[22] = "V"; keySymbol[22] = "/";
 keyNames[23] = "b"; keyShift[23] = "B"; keySymbol[23] = "(";
 keyNames[24] = "n"; keyShift[24] = "N"; keySymbol[24] = ")";
 keyNames[25] = "m"; keyShift[25] = "M"; keySymbol[25] = ";";
 
 keyNames[26] = " "; keyShift[26] = " "; keySymbol[26] = " ";
 
 
 keyNames[45] = "symbolsKey";
 keyNames[46] = "shiftKey";
 keyNames[47] = "Rtn";
 keyNames[48] = "capsKey";
 keyNames[49] = "Bksp";

*/

int ShishaKeyboard::getKeyIndex(int key){
	if(key == 'q' || key == 'Q' || key == '1')
		return 0;
	else if(key == 'w' || key == 'W' || key == '2')
		return 1;
	else if(key == 'e' || key == 'E' || key == '3')
		return 2;
	else if(key == 'r' || key == 'R' || key == '4')
		return 3;
	else if(key == 't' || key == 'T' || key == '5')
		return 4;
	else if(key == 'y' || key == 'Y' || key == '6')
		return 5;
	else if(key == 'u' || key == 'U' || key == '7')
		return 6;
	else if(key == 'i' || key == 'I' || key == '8')
		return 7;
	else if(key == 'o' || key == 'O' || key == '9')
		return 8;
	else if(key == 'p' || key == 'P' || key == '0')
		return 9;
	
	else if(key == 'a' || key == 'A' || key == '!')
		return 10;
	else if(key == 's' || key == 'S' || key == '@')
		return 11;
	else if(key == 'd' || key == 'D' || key == '&')
		return 12;
	else if(key == 'f' || key == 'F' || key == '\'')
		return 13;
	else if(key == 'g' || key == 'G' || key == '-')
		return 14;
	else if(key == 'h' || key == 'H' || key == '?')
		return 15;
	else if(key == 'j' || key == 'J' || key == '.')
		return 16;
	else if(key == 'k' || key == 'K' || key == ',')
		return 17;
	else if(key == 'l' || key == 'L' || key == '$')
		return 18;
	
	else if(key == 'z' || key == 'Z' || key == '*')
		return 19;
	else if(key == 'x' || key == 'X' || key == '#')
		return 20;
	else if(key == 'c' || key == 'C' || key == ':')
		return 21;
	else if(key == 'v' || key == 'V' || key == '/')
		return 22;
	else if(key == 'b' || key == 'B' || key == '(')
		return 23;
	else if(key == 'n' || key == 'N' || key == ')')
		return 24;
	else if(key == 'm' || key == 'M' || key == ';')
		return 25;
	
	else if(key == ' ')
		return 26;
	
	else if(key == '`' || key == '~') // symbols
		return 45;
	else if(key == 13) // return
		return 47;
	else if(key == 127) // backspace
		return 49;
	
	/**
	 handle them in the update() ...
	keyNames[46] = "shiftKey";
	keyNames[48] = "capsKey";
	 */
	
	return -1;
}

string ShishaKeyboard::getKeyName(int key){
	int keyIdx = this->getKeyIndex(key);
	if(keyIdx >= 0)
		return this->keyNames[keyIdx];
	return "";
}

bool ShishaKeyboard::isCaptial(int key){
	return (key >= 'A' && key <= 'Z');
}

bool ShishaKeyboard::isSymbol(int key){
	return ((key >= '0' && key <= '9')
	   || key == '!' || key == '@' || key == '&' || key == '\'' || key == '-' || key == '?' || key == '.' || key == ',' || key == '$'
	   || key == '*' || key == '#' || key == ':' || key == '/' || key == '(' || key == ')' || key == ';');
}

bool ShishaKeyboard::checkKeyPressed(int key){	
	if(this->isCaptial(key) && !shiftVal){
		this->setBoolValue("ImageMap.shiftKey.value", true);
		this->shiftVal = true;
		this->shiftKeyPressed = true;
	}
	if(this->isSymbol(key) && !symbolVal){
		this->setBoolValue("ImageMap.symbolsKey.value", true);
		this->symbolVal = true;
	}
	
	int keyIdx = this->getKeyIndex(key);
	if(keyIdx >= 0){
		keysTyped[keyIdx] = true;
		string keyName = this->getKeyName(key);
		if(keyName != ""){
			string selector = "ImageMap."+keyName+".value";
			if(keyName == "symbolsKey"){
				if(!symbolKeyDown){
					symbolKeyDown = true;
					if(!this->getBool(selector))
						this->setBoolValue(selector, true);
					else
						symbolToggleFlag = true;
				}
			}
			else
				this->setBoolValue(selector, true);
		}
	}
}

bool ShishaKeyboard::checkKeyReleased(int key){
	if(this->shiftKeyPressed){ //this->isCaptial(key)){
		this->setBoolValue("ImageMap.shiftKey.value", false);
		this->shiftVal = false;
		this->shiftKeyPressed = false;
	}
	if(this->isSymbol(key) && symbolVal){
		this->setBoolValue("ImageMap.symbolsKey.value", false);
		this->symbolVal = false;
	}
	
	int keyIdx = this->getKeyIndex(key);
	if(keyIdx >= 0){
		keysTyped[keyIdx] = false;
		
		string keyName = this->getKeyName(key);
		if(keyName != ""){
			string selector = "ImageMap."+keyName+".value";
			if(keyName == "symbolsKey"){
				if(this->symbolToggleFlag){
					this->setBoolValue(selector, false);
					this->symbolToggleFlag = false;
				}
				this->symbolKeyDown = false;
			}
			else
				this->setBoolValue(selector, false);
		}
	}
}
