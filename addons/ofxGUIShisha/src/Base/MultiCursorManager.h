/*
 *  MultiCursorManager.h
 *  openFrameworks
 *
 *  Created by Pat Long on 30/03/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_CURSOR_MANAGER
#define _OFX_GUISHISHA_CURSOR_MANAGER

#include "ofMain.h"
#include "ofxTI_Utils.h"

#define MOUSE_ID "mouse"

// a cursor base class for multi-touch cursors
class MultiCursor{
protected:
	string cursorID;
	int cursorIntID, cursorPort;
	int x, y, clickX, clickY, lastClickX, lastClickY, originX, originY, releaseX, releaseY;
	int dx, dy, releaseDx, releaseDy;
	float distanceMoved, angleMoved, releaseSpeed, releaseAngle, width, height;
	bool busy, clickedOn, draggedOn, releasedOn;
	
	void init(string cursorID);
	void refreshMovementVectors();
	void refreshReleaseVectors();
	
public:
	MultiCursor(string cursorID);
	~MultiCursor();
	
	virtual void draw();
	
	void setBusy(bool busy);
	virtual void setClickedOn(bool clickedOn);
	virtual void setDraggedOn(bool draggedOn);
	virtual void setReleasedOn(bool releasedOn);
	virtual void setReleaseLocation(int x, int y, bool releasedOn);
	virtual void setLastClick(int x, int y, bool reset=false);
	virtual void setPosition(int x, int y);
	virtual void setSize(float width, float height);
	
	bool isBusy();	
	bool isClickedOn();
	bool isDraggedOn();
	bool wasDraggedOn();
	bool isReleasedOn();
	
	string getCursorID();
	int getCursorIntID();
	int getCursorPort();
	int getX();
	int getY();
	int getOriginX();
	int getOriginY();
	int getMoveX();
	int getMoveY();
	float getMoveAngle();
	float getMoveDistance();
	float getOriginAngle();
	float getOriginDistance();
	int getReleaseX();
	int getReleaseY();
	int getReleaseMoveX();
	int getReleaseMoveY();
	float getReleaseAngle();
	float getReleaseSpeed();
	float getWidth();
	float getHeight();
	
	virtual void output();
	
};

// manages a set of cursors for a multi-touchable object
class MultiCursorManager{
protected:
	map<string, MultiCursor*> cursors;
	
	virtual MultiCursor* getCursor(string cursorID, bool makeNew=true);
	
public:
	MultiCursorManager();
	~MultiCursorManager();
	virtual void deleteCursors();
	
	virtual void draw();
	
	virtual void set(string cursorID, int x, int y, float w=1.0, float h=1.0, int button=-1, bool dragging=false);
	virtual void unset(string cursorID);
	
	void setBusy(string cursorID, bool busy);
	void setReleaseLocation(string cursorID, int x, int y, bool releasedOn);
	
	bool hasCursor(string cursorID);
	bool isBusy(string cursorID);
	bool isClickedOn(string cursorID);
	bool isDraggedOn(string cursorID);
	bool wasDraggedOn(string cursorID);
	bool isReleasedOn(string cursorID);
	
	string getClickedAndReleased();
	int getCount();
	map<string, MultiCursor*>* getCursors();
	int getX(string cursorID);
	int getY(string cursorID);
	int getOriginX(string cursorID);
	int getOriginY(string cursorID);
	int getMoveX(string cursorID);
	int getMoveY(string cursorID);
	float getMoveAngle(string cursorID);
	float getMoveDistance(string cursorID);
	float getOriginAngle(string cursorID);
	float getOriginDistance(string cursorID);
	int getReleaseX(string cursorID);
	int getReleaseY(string cursorID);
	int getReleaseMoveX(string cursorID);
	int getReleaseMoveY(string cursorID);
	float getReleaseAngle(string cursorID);
	float getReleaseSpeed(string cursorID);
	
	virtual void outputCursors();
};

#endif
