/*
 *  ShishaContainer.h
 *  openFrameworks
 *
 *  Created by Pat Long on 27/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHACONTAINER
#define _OFX_GUISHISHA_SHISHACONTAINER

#include "ShishaElement.h"

#define DEFAULT_HORIZONTAL_SPACING	5.0
#define DEFAULT_VERTICAL_SPACING	5.0

#define SHISHA_FLOW_MODE_HORIZONTAL	0
#define SHISHA_FLOW_MODE_VERTICAL	1

class ShishaContainer : public ShishaElement{
	protected:
		map<int,ShishaElement*> children;
	
		float cOffsetX, cOffsetY, cBiggestOffset, horizontalSpacing, verticalSpacing;
		int flowMode;
		bool forceChildCursorCheck;
	
		virtual void drawChildren();
		virtual void updateChildren();
		virtual bool checkChildrenCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkChildrenCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);

		virtual bool checkChildrenKeyPressed(int key);
		virtual bool checkChildrenKeyReleased(int key);
	
		virtual void incrementOffsets(ShishaElement* lastElement, bool forceBreak=false);
		virtual void resetHorizontalOffsets();
		virtual void resetVerticalOffsets();
	
		bool parseSubSelector(string selector, ShishaElement* element, string& subSelector);
	
		virtual void onMove(float xMove, float yMove);
		
	public:
		ShishaContainer();
		~ShishaContainer();
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
		
		virtual void draw();
		virtual void draw(float x, float y);
//		virtual void draw(float x, float y, float w, float h);
		virtual void draw(float x, float y, float w, float h, bool borders);
		virtual void update();
		
		virtual void clearCursors();
	
		virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
		virtual bool checkCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);

		virtual bool checkKeyPressed(int key);
		virtual bool checkKeyReleased(int key);
	
		virtual bool prepareForMotion();
	
		virtual void setFlowMode(int flowMode);
		virtual void setHorizontalSpacing(float horizontalSpacing);
		virtual void setVerticalSpacing(float verticalSpacing);
		virtual void setTheme(ShishaTheme* theme=NULL, ShishaTheme* doNotDelete=NULL, bool themeInherited=false);
		virtual void setForceChildCursorCheck(bool forceChildCursorCheck);
	
		virtual ShishaElement* addElement(ShishaElement* element, bool updateLocation=true, bool inheritTheme=true);
	
		virtual ShishaElement* getElement(string selector=DEFAULT_SELECTOR, string& subSelector="");
		virtual bool getBool(string selector=DEFAULT_SELECTOR);
		virtual int getInt(string selector=DEFAULT_SELECTOR);
		virtual float getFloat(string selector=DEFAULT_SELECTOR);
		virtual string getString(string selector=DEFAULT_SELECTOR);
	
		ShishaElement* selectElement(string selector="");

		virtual ShishaElement* setBoolValue(string selector=DEFAULT_SELECTOR, bool value=false);
		virtual ShishaElement* setIntValue(string selector=DEFAULT_SELECTOR, int value=0);
		virtual ShishaElement* setFloatValue(string selector=DEFAULT_SELECTOR, float value=0.0);
		virtual ShishaElement* setStringValue(string selector=DEFAULT_SELECTOR, string value="");
};

#endif
