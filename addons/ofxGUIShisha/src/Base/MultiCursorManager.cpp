/*
 *  MultiCursorManager.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 30/03/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "MultiCursorManager.h"

MultiCursorManager::MultiCursorManager(){
}

MultiCursorManager::~MultiCursorManager(){
	this->deleteCursors();
}

void MultiCursorManager::deleteCursors(){
	for(map<string,MultiCursor*>::iterator it=this->cursors.begin(); it != this->cursors.end(); it++)
		delete (*it).second;
	this->cursors.clear();
}

void MultiCursorManager::draw(){
	for(map<string, MultiCursor*>::iterator it=this->cursors.begin() ; it != this->cursors.end(); it++)
		(*it).second->draw();
}

int MultiCursorManager::getCount(){
	return this->cursors.size();
}

map<string, MultiCursor*>* MultiCursorManager::getCursors(){
	return &this->cursors;
}

MultiCursor* MultiCursorManager::getCursor(string cursorID, bool makeNew){
	if(this->cursors.find(cursorID) == this->cursors.end()){
		if(!makeNew)
			return NULL;
		this->cursors[cursorID] = new MultiCursor(cursorID);
	}
	return this->cursors[cursorID];
}

void MultiCursorManager::set(string cursorID, int x, int y, float w, float h, int button, bool dragging){
	MultiCursor* cursor = this->getCursor(cursorID);
	if(cursor != NULL){
		cursor->setPosition(x, y);
		cursor->setSize(w, h);
		if(button >= 0){
			if(!dragging)
				cursor->setClickedOn(true);
			
			if(dragging)
				cursor->setDraggedOn(true);
			
			cursor->setLastClick(x, y, !dragging);
		}
	}
}

void MultiCursorManager::unset(string cursorID){
	map<string, MultiCursor*>::iterator cursorIT = this->cursors.find(cursorID);
	if(cursorIT != this->cursors.end()){
		delete (*cursorIT).second;
		this->cursors.erase(cursorIT);
	}
}

void MultiCursorManager::setBusy(string cursorID, bool busy){
	MultiCursor* cursor = this->getCursor(cursorID);
	if(cursor != NULL)
		cursor->setBusy(busy);
}

void MultiCursorManager::setReleaseLocation(string cursorID, int x, int y, bool releasedOn){
	MultiCursor* cursor = this->getCursor(cursorID, false);
	if(cursor != NULL)
		cursor->setReleaseLocation(x, y, releasedOn);
}


bool MultiCursorManager::hasCursor(string cursorID){
	return (this->cursors.find(cursorID) != this->cursors.end());
}

bool MultiCursorManager::isBusy(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->isBusy();
	return false;
}

bool MultiCursorManager::isClickedOn(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->isClickedOn();
	return false;
}

bool MultiCursorManager::isDraggedOn(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->isDraggedOn();
	return false;
}

bool MultiCursorManager::wasDraggedOn(string cursorID){
	if(this->hasCursor(cursorID)){
		return this->cursors[cursorID]->wasDraggedOn();
	}
	return false;
}

bool MultiCursorManager::isReleasedOn(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->isReleasedOn();
	return false;
}

string MultiCursorManager::getClickedAndReleased(){
	for(map<string, MultiCursor*>::iterator it=this->cursors.begin() ; it != this->cursors.end(); it++){
//		cout << "\t" << (*it).second->getCursorID() << ":" << ((*it).second->isClickedOn()?"clickedOn":"clickedOff") << ":" << ((*it).second->isReleasedOn()?"releasedOn":"releasedOff") << ":" << endl;
		if((*it).second->isClickedOn() && (*it).second->isReleasedOn())
			return (*it).second->getCursorID();
	}
	return "";
}

int MultiCursorManager::getX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getX();
	return 0;
}

int MultiCursorManager::getY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getY();
	return 0;
}

int MultiCursorManager::getOriginX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getOriginX();
	return 0;
}

int MultiCursorManager::getOriginY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getOriginY();
	return 0;
}

int MultiCursorManager::getMoveX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveX();
	return 0;
}

int MultiCursorManager::getMoveY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveY();
	return 0;
}

float MultiCursorManager::getMoveAngle(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveAngle();
	return 0.0;
}

float MultiCursorManager::getMoveDistance(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getMoveDistance();
	return 0.0;
}

float MultiCursorManager::getOriginAngle(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getOriginAngle();
	return 0.0;
}

float MultiCursorManager::getOriginDistance(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getOriginDistance();
	return 0.0;
}

int MultiCursorManager::getReleaseX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getReleaseX();
	return 0;
}

int MultiCursorManager::getReleaseY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getReleaseY();
	return 0;
}

int MultiCursorManager::getReleaseMoveX(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getReleaseMoveX();
	return 0;
}

int MultiCursorManager::getReleaseMoveY(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getReleaseMoveY();
	return 0;
}

float MultiCursorManager::getReleaseAngle(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getReleaseAngle();
	return 0.0;
}

float MultiCursorManager::getReleaseSpeed(string cursorID){
	if(this->hasCursor(cursorID))
		return this->cursors[cursorID]->getReleaseSpeed();
	return 0.0;
}

void MultiCursorManager::outputCursors(){
	for(map<string, MultiCursor*>::iterator it=this->cursors.begin() ; it != this->cursors.end(); it++){
		cout << "\t";
		(*it).second->output();
	}
}


MultiCursor::MultiCursor(string cursorID){
	this->init(cursorID);
}

MultiCursor::~MultiCursor(){
}

void MultiCursor::init(string cursorID){
	this->cursorID = cursorID;
	this->cursorIntID = 0;
	this->cursorPort = 0;
	if(this->cursorID != "0" && this->cursorID != MOUSE_ID){
		int pos = this->cursorID.find(':');
		if(pos != string::npos){
			this->cursorIntID = atoi(this->cursorID.substr(pos+1).c_str());
			this->cursorPort = atoi(this->cursorID.substr(0, pos).c_str());
		}
	}
	
	this->x = this->y = this->clickX = this->clickY = this->lastClickX = this->lastClickY = this->originX = this->originY = this->releaseX = this->releaseY = -1;
	this->dx = this->dy = this->releaseDx = this->releaseDy = 0;
	this->width = this->height = 1.0;
	this->distanceMoved = this->angleMoved = this->releaseSpeed = this->releaseAngle = 0.0;
	this->busy = false;
	this->clickedOn = this->draggedOn = this->releasedOn = false;
}

void MultiCursor::refreshMovementVectors(){
	this->dx = this->clickX - this->lastClickX;
	this->dy = this->clickY - this->lastClickY;
	this->distanceMoved = tiDistance(this->dx, this->dy);
	this->angleMoved = tiAngle(this->dx, this->dy);
}

void MultiCursor::refreshReleaseVectors(){
	this->releaseSpeed = tiDistance(this->releaseDx, this->releaseDy);
	this->releaseAngle = tiAngle(this->releaseDx, this->releaseDy);
}

void MultiCursor::draw(){
	float cursorRadius = 10;
	ofLine(this->x, this->y-this->height/2.0, this->x, this->y+this->height/2.0);
	ofLine(this->x-this->width/2.0, this->y, this->x+this->width/2.0, this->y);
}

void MultiCursor::setBusy(bool busy){
	this->busy = busy;
}

void MultiCursor::setClickedOn(bool clickedOn){
	this->clickedOn = clickedOn;
}

void MultiCursor::setDraggedOn(bool draggedOn){
	this->draggedOn = draggedOn;
}

void MultiCursor::setReleasedOn(bool releasedOn){
	this->releasedOn = releasedOn;
}

void MultiCursor::setReleaseLocation(int x, int y, bool releasedOn){
	this->setReleasedOn(releasedOn);
	this->releaseX = x;
	this->releaseY = y;
	this->releaseDx = this->releaseX - this->lastClickX;
	this->releaseDy = this->releaseY - this->lastClickY;
	this->refreshReleaseVectors();
}

void MultiCursor::setLastClick(int x, int y, bool reset){
	if(!reset && this->clickX == -1 && this->clickY == -1)
		reset = true;
	if(!reset){
		this->lastClickX = this->clickX;
		this->lastClickY = this->clickY;
	}
	this->clickX = x;
	this->clickY = y;
	if(reset){
		this->originX = this->clickX;
		this->originY = this->clickY;
		this->lastClickX = this->clickX;
		this->lastClickY = this->clickY;
	}
	this->refreshMovementVectors();
}

void MultiCursor::setPosition(int x, int y){
	this->x = x;
	this->y = y;
}

void MultiCursor::setSize(float width, float height){
	this->width = width;
	this->height = height;
}

bool MultiCursor::isBusy(){
	return this->busy;
}

bool MultiCursor::isClickedOn(){
	return this->clickedOn;
}

bool MultiCursor::isDraggedOn(){
	return this->draggedOn;
}

bool MultiCursor::wasDraggedOn(){
	bool result = this->isDraggedOn();
	this->draggedOn = false;
	return result;
}

bool MultiCursor::isReleasedOn(){
	return this->releasedOn;
}

string MultiCursor::getCursorID(){
	return this->cursorID;
}

int MultiCursor::getCursorIntID(){
	return this->cursorIntID;
}

int MultiCursor::getCursorPort(){
	return this->cursorPort;
}

int MultiCursor::getX(){
	return this->x;
}

int MultiCursor::getY(){
	return this->y;
}

int MultiCursor::getOriginX(){
	return this->originX;
}

int MultiCursor::getOriginY(){
	return this->originY;
}

int MultiCursor::getMoveX(){
	return this->dx;
}

int MultiCursor::getMoveY(){
	return this->dy;
}

float MultiCursor::getMoveAngle(){
	return this->angleMoved;
}

float MultiCursor::getMoveDistance(){
	return this->distanceMoved;
}

float MultiCursor::getOriginAngle(){
	float dx = this->x - this->originX;
	float dy = this->y - this->originY;
	return tiAngle(dx, dy);
}

float MultiCursor::getOriginDistance(){
	float dx = this->x - this->originX;
	float dy = this->y - this->originY;
	return tiDistance(dx, dy);
}

int MultiCursor::getReleaseX(){
	return this->releaseX;
}

int MultiCursor::getReleaseY(){
	return this->releaseY;
}


int MultiCursor::getReleaseMoveX(){
	return this->releaseDx;
}

int MultiCursor::getReleaseMoveY(){
	return this->releaseDy;
}

float MultiCursor::getReleaseAngle(){
	return this->releaseAngle;
}

float MultiCursor::getReleaseSpeed(){
	return this->releaseSpeed;
}

float MultiCursor::getWidth(){
	return this->width;
}

float MultiCursor::getHeight(){
	return this->height;
}

void MultiCursor::output(){
	cout << this->cursorID << ":" << this->getX() << "," << this->getY() << ":" << endl;
}
