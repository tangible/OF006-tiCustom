/*
 *  ShishaEvent.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 12/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "ShishaEvent.h"

ShishaEvent::ShishaEvent(string message){
	this->setMessage(message);
}

ShishaEvent::~ShishaEvent(){
}

string ShishaEvent::getMessage(){
	return this->message;
}

void ShishaEvent::setMessage(string message){
	this->message = message;
}
