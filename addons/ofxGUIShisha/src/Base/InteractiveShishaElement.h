/*
 *  InteractiveShishaElement.h
 *  openFrameworks
 *
 *  Created by Pat Long on 01/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_INTERACTIVE_SHISHA_ELEMENT
#define _OFX_GUISHISHA_INTERACTIVE_SHISHA_ELEMENT

#include "ShishaElement.h"

#define SHISHA_DRAG_MODE_NONE		0	// no dragging
#define SHISHA_DRAG_MODE_ELASTIC	1	// drag, but snap back
#define SHISHA_DRAG_MODE_ALLOW		2	// drag, and stay

#define DEFAULT_DRAG_MODE		SHISHA_DRAG_MODE_NONE
#define DEFAULT_ELASTIC_SNAP	25

// this is a version of ShishaElement that allows dragging, scaling, and rotating in a multi-touch environment
class InteractiveShishaElement : public ShishaElement{
	protected:
		int dragMode, elasticSnap;
		bool allowDragX, allowDragY, isMoving;
		float originalX, originalY, moveToX, moveToY, lastX, lastY, dx, dy;
	
		bool snapCheck();
		void snapTo(float x, float y, int snapTime=0);
		void snapBack(int snapTime=0);
		bool move();
		void endMove();
	
		virtual void updatePosition(int x, int y);
	
	public:
		InteractiveShishaElement(){};
		~InteractiveShishaElement(){};
		virtual void init(int x=0, int y=0, int width=DEFAULT_GUI_ELEMENT_WIDTH, int height=DEFAULT_GUI_ELEMENT_HEIGHT);
	
		virtual bool prepareForMotion();
	
		void setDragMode(int dragMode, int elasticSnap=DEFAULT_ELASTIC_SNAP, bool allowDragX=true, bool allowDragY=true);
		void setElasticSnap(int elasticSnap);
		virtual void setPosition(float x, float y);
	
//		virtual void draw();
//		virtual void draw(float x, float y, float w, float h, bool borders);
		virtual void update();
	
		virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
//		virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
};

#endif
