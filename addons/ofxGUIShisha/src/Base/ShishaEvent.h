/*
 *  ShishaEvent.h
 *  openFrameworks
 *
 *  Created by Pat Long on 12/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHA_EVENT
#define _OFX_GUISHISHA_SHISHA_EVENT

#include "ofMain.h"

class ShishaEvent{
protected:
	string message;
	
public:
	ShishaEvent(string message="");
	~ShishaEvent();
	
	virtual string getMessage();
	virtual void setMessage(string message="");
};

#endif
