/*
 *  ShishaElement.h
 *  openFrameworks
 *
 *  Created by Pat Long on 24/03/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHA_ELEMENT
#define _OFX_GUISHISHA_SHISHA_ELEMENT

#include "ofMain.h"
#include "ofxColours.h"
#include "ofxVectorMath.h"

#include "ElementTypes.h"
#include "MultiCursorManager.h"

class ShishaTheme;

#define DEFAULT_GUI_ELEMENT_WIDTH 25
#define DEFAULT_GUI_ELEMENT_HEIGHT 25

#define GUI_ELEMENT_STATE_MOUSEOUT 0
#define GUI_ELEMENT_STATE_MOUSEOVER 1
#define GUI_ELEMENT_STATE_MOUSEDOWN 2
#define GUI_ELEMENT_STATE_MOUSEDRAG 3

#define GUI_ELEMENT_DISPLAY_STATE_INACTIVE			0
#define GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER	1
#define GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER		2
#define GUI_ELEMENT_DISPLAY_STATE_ACTIVE			3

#define GUI_ELEMENT_SHAPE_RECTANGLE 0
#define GUI_ELEMENT_SHAPE_CIRCLE 1

#define DEFAULT_SELECTOR "value"
#define SELECTOR_UNIDENTIFIED_BOOL		false
#define SELECTOR_UNIDENTIFIED_INT		-1
#define SELECTOR_UNIDENTIFIED_FLOAT		-1.0
#define SELECTOR_UNIDENTIFIED_STRING	"unknown"

class ShishaElement : public ofBaseDraws, ofBaseUpdates {
protected:
	int elementID, elementType, guiShape, guiState, moveFrameCurrent, moveFrameTotal;
	float x, y, width, height, dx, dy, dw, dh, rotation, rotationX, rotationY;
	string name, label;
	bool drawBorders, drawCursors, cropBounds;
	MultiCursorManager cursors;
	ShishaTheme* theme;
	bool themeInherited;
	ShishaElement* parent;
	ofRectangle cropRect;
	
	virtual void clearMovement();
	virtual bool isHovered(string cursorID, float x, float y);
	
	virtual void drawAsRectangle(float x, float y, float w, float h, bool borders);
	virtual void drawAsCircle(float x, float y, float w, float h, bool borders);
	
	virtual bool selectSelf(string selector);
	
	virtual void setCursor(string cursorID, int x, int y, float w=1.0, float h=1.0, int button=-1, bool dragging=false);
	virtual void unsetCursor(string cursorID);
	
/**	virtual void onHover(string cursorID);
	virtual void onDrag(string cursorID);
	virtual void onPress(string cursorID);
	virtual void onRelease(string cursorID);
	virtual void onMove(float xMove, float yMove);
	virtual void onPosition(float x, float y);
	virtual void onResize(float width, float height);
	virtual void onRotate(float angle);
	virtual void onScale(float xScale, float yScale);*/
	
	// cursor events
	virtual void onHover(string cursorID);
	virtual void onDehover(string cursorID);
	
	virtual void onDrag(string cursorID);
	virtual void onDragOn(string cursorID);
	virtual void onDragOff(string cursorID);
	
	virtual void onPress(string cursorID);
	virtual void onPressOff(string cursorID);
	virtual void onRelease(string cursorID);
	
	// element position/size events
	virtual void onMove(float xMove, float yMove);
	virtual void onPosition(float x, float y, float oldX, float oldY);
	virtual void onResize(float width, float height);
	virtual void onRotate(float angle);
	virtual void onScale(float xScale, float yScale);
	
public:
	ShishaElement();
	~ShishaElement();
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual bool prepareForMotion();

	virtual void rotate(float angle);
	virtual void setTheme(ShishaTheme* theme=NULL, ShishaTheme* doNotDelete=NULL, bool themeInherited=false);
	virtual void setElementID(int elementID);
	virtual	void setParent(ShishaElement* parent);
	virtual void setName(string name);
	virtual void setLabel(string label);
	virtual void setCropBounds(bool cropBounds, float x=-1.0, float y=-1.0, float width=-1.0, float height=-1.0);
	virtual void setGUIShape(int guiShape);
	virtual void setGUIState(int guiState);
	virtual void setLocation(float x, float y, float width=-1.0, float height=-1.0, int moveFrames=0);
	virtual void setPosition(float x, float y);
	virtual void setDimensions(float width, float height);
	virtual void setWidth(float width);
	virtual void setHeight(float height);
	void setRotationCenter(float rotationX, float rotationY);
	
	virtual void clearCursors();
	virtual bool hasCursors();
	virtual map<string, MultiCursor*>* getCursors();
	int getElementID();
	int getElementType();
	string getName();
	string getLabel();
	int getGuiState();
	virtual int getGuiDisplayState();
	ShishaTheme* getTheme();
	float getX();
	float getY();
	float getWidth();
	float getHeight();

	virtual ShishaElement* getElement(string selector=DEFAULT_SELECTOR, string& subSelector="");
	virtual bool getBool(string selector=DEFAULT_SELECTOR);
	virtual int getInt(string selector=DEFAULT_SELECTOR);
	virtual float getFloat(string selector=DEFAULT_SELECTOR);
	virtual string getString(string selector=DEFAULT_SELECTOR);
	
	virtual ShishaElement* setBoolValue(string selector=DEFAULT_SELECTOR, bool value=false);
	virtual ShishaElement* setIntValue(string selector=DEFAULT_SELECTOR, int value=0);
	virtual ShishaElement* setFloatValue(string selector=DEFAULT_SELECTOR, float value=0.0);
	virtual ShishaElement* setStringValue(string selector=DEFAULT_SELECTOR, string value="");

	virtual void setBool(bool value=false);
	virtual void setInt(int value=0);
	virtual void setFloat(float value=0.0);
	virtual void setString(string value="");
	
	virtual void draw();
	virtual void draw(float x, float y);
	virtual void draw(float x, float y, float w, float h);
	virtual void draw(float x, float y, float w, float h, bool borders);
	virtual void update();
	
	void translateCoords(float& x, float& y);
	void translateMouseCoords(int& x, int& y);
	
	virtual bool checkCursorHover(int x, int y, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorDrag(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorPress(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	virtual bool checkCursorRelease(int x, int y, int button, string cursorID, float w=1.0, float h=1.0);
	
	virtual bool checkKeyPressed(int key);
	virtual bool checkKeyReleased(int key);
};

#include "ShishaTheme.h"

#endif
