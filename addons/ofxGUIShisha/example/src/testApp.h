#ifndef _TEST_APP
#define _TEST_APP

#include "ofMain.h"
#include "ofxTuio.h"
#include "ofxGUIShisha.h"

#define MOUSE_CURSOR_ID "mouse"

class testApp : public ofBaseApp{
	private:
		ofRectangle windowBounds;
		myTuioClient* tuio;
		ShishaPanel myShisha;

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void tuioAdded(ofxTuioCursor & tuioCursor);
		void tuioRemoved(ofxTuioCursor & tuioCursor);
		void tuioUpdated(ofxTuioCursor & tuioCursor);
		void windowResized(int w, int h);

};

#endif
