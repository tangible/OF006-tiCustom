/*
 *  TI_Theme.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 12/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "TI_Theme.h"

TI_Theme::TI_Theme(){
}

TI_Theme::~TI_Theme(){
}

void TI_Theme::initTheme(){
	this->themeName = "Tangible Theme";
	this->themeColours[SHISHA_COLOUR_DEFAULT] =		new ColourRGBA(255, 255, 255, 255); // should never change this one
	this->themeColours[SHISHA_COLOUR_BACKGROUND] =	new ColourRGBA(0, 0, 0, 0);
	this->themeColours[SHISHA_COLOUR_BORDER] =		new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEOVER] = new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEDOWN] = new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_BORDER_MOUSEDRAG] = new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_FOREGROUND_1] =	new ColourRGBA(0, 255, 0);
	this->themeColours[SHISHA_COLOUR_FOREGROUND_2] =	new ColourRGBA(255, 255, 255);
	this->themeColours[SHISHA_COLOUR_CURSORS] =		new ColourRGBA(255, 255, 255);
	
	this->font.loadFont("fonts/MDL30BasCE.otf", 7);
	
	this->containerBorders = false;
	this->panelBorders = false;
	this->buttonBorders = true;
	this->imageBorders = false;
	this->textOutputBorders = false;
	this->textInputBorders = true;
	this->initted = true;
}

void TI_Theme::drawText(string text, float x, float y, bool center){
	if(this->font.bLoadedOk){
		if(center){
//			x -= this->font.stringWidth(text)/2.0;
			x -= this->getTextWidth(text)/2.0;
//			y += this->font.stringHeight(text)/4.0;
			y += this->getTextHeight(text)/4.0;
		}
		this->font.drawString(text, x, y);
	}
	else
		ShishaTheme::drawText(text, x, y, center);
}

float TI_Theme::getTextWidth(string text){
	int result = 0;
	if(this->font.bLoadedOk){
		// we gotta do some funky stuff if it starts or ends with a space... stringWidth doesn't calculate properly otherwise
		string tempChecker = text;
		bool spaceEnder = false;
		bool spaceStarter = false;
		if(tempChecker != "" && tempChecker.at(0) == ' '){
			tempChecker = '.' + tempChecker;
			spaceStarter = true;
		}
		if(tempChecker != "" && tempChecker.at(tempChecker.length()-1) == ' '){
			tempChecker += '.';
			spaceEnder = true;
		}
		result = this->font.stringWidth(tempChecker);
		if(spaceStarter)
			result -= (this->font.stringWidth(".") - 1);
		if(spaceEnder)
			result -= (this->font.stringWidth(".") - 1);
	}
	else
		result = text.length() * 8.2;
	return result;
}

float TI_Theme::getTextHeight(string text){
	if(this->font.bLoadedOk)
		return this->font.stringHeight(text+"Ay"); // we add a capital and a lowercase hanging letter for maximum height
	return 0;
}
