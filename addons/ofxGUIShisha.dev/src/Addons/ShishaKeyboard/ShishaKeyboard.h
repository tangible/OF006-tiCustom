/*
 *  ShishaKeyboard.h
 *  openFrameworks
 *
 *  Created by Pat Long on 16/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_SHISHA_KEYBOARD
#define _OFX_SHISHA_KEYBOARD

#include "ofxGUIShisha.h"
//#include "ShishaContainer.h"

// adaptation of Dennis Rosenfeld's ShishaKeyboard ... this one can be added to an existing GUIShisha GUI
class ShishaKeyboard : public ShishaContainer{
	protected:
		string      myText;
		bool        anyKeyPressed, specialKeyPressed, pressedOn;
		
		string      keyNames[50];
		string      keyShift[50];
		string      keySymbol[50];
		bool        keysHeld[50];
		bool		keysTyped[50];
		
		bool		noDoubleType;
	
		bool		enterSubmit;
		bool        shiftVal, shiftKeyPressed;
		bool        capsVal;
		bool        symbolVal, symbolToggleFlag, symbolKeyDown;
		
		string imageBase;
		ofImage     undercase;
		ofImage     smallChars;
		ofImage     uppercase;
	
		ShishaElement* inputElement;
		vector<BasicButton*> buttons;
	
		int getShiftKeyIndex(int key);
		int getKeyIndex(int key);
		string getKeyName(int key);
	
		bool isCaptial(int key);
		bool isSymbol(int key);
	
		void updateInputElement();
	
		virtual void onPress(string cursorID);

	public:
		ShishaKeyboard();
		~ShishaKeyboard();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
	
		virtual void draw();
		virtual void update();
	
		string getText();
		void setText(string text);
	
		virtual bool getBool(string selector=DEFAULT_SELECTOR);
		virtual int getInt(string selector=DEFAULT_SELECTOR);
	
		virtual string getString(string selector=DEFAULT_SELECTOR);
		virtual void setString(string value="");
	
		virtual void setElement(ShishaElement* element=NULL);
	
		void setImageBase(string imageBase="");
		void setButtonHoldDelay(int buttonHoldDelay);
		void setButtonPressTriggerDelay(int buttonTriggerDelay);
	
		virtual bool checkKeyPressed(int key);
		virtual bool checkKeyReleased(int key);
	
};

#endif
