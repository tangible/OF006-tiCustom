/*
 *  ShishaElement.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 24/03/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "ShishaElement.h"

ShishaElement::ShishaElement(){
	this->theme = NULL;
	this->initted = false;
}

ShishaElement::~ShishaElement(){
	this->clearCursors();
	gsCursors.dropElementFromAll(this);
	if(!this->themeInherited && this->theme != NULL && this->theme->checkDestroyed()){
		delete this->theme;
		this->theme = NULL;
	}
}

void ShishaElement::init(float x, float y, float width, float height, int elementID){
	this->initted = false;
	this->clearMovement();
	this->setLocation(x, y, width, height);
	this->setCropBounds(false);
	this->setDrawOffset(0.0, 0.0);
	this->setGUIShape(GUI_ELEMENT_SHAPE_RECTANGLE);
	this->setGUIState(GUI_ELEMENT_STATE_MOUSEOUT);
	this->setGUIStyle(GUI_ELEMENT_STYLE_DEFAULT);
	this->label = "";
	this->setName("Shisha Element");
	this->elementID = elementID;
	this->elementType = SHISHA_TYPE_BASE;
	this->theme = NULL;
	this->themeInherited = false;
	this->parent = NULL;
	
	this->rotation = 0.0;
	this->setRotationCenter(this->getX() + this->getWidth()/2.0, this->getY() + this->getHeight()/2.0);
	
	this->drawBorders = false;
#ifdef DRAW_BORDERS
	this->drawBorders = true;
#endif
	this->drawBackground = true;
	
	this->drawCursors = false;
#ifdef DRAW_CURSORS
	this->drawCursors = true;
#endif
	this->initted = true;
}

void ShishaElement::clearCursors(){
	map<string, MultiCursor*>* myCursors = this->cursors.getCursors();
	vector<string> cursorIDs;
	for(map<string, MultiCursor*>::iterator cIt = myCursors->begin(); cIt != myCursors->end(); cIt++)
		cursorIDs.push_back((*cIt).first);
	for(int i=0; i < cursorIDs.size(); i++){
		this->onRelease(cursorIDs[i]);
		this->unsetCursor(cursorIDs[i]);
	}
}

void ShishaElement::outputCursors(bool verbose){
	cout << this->getName() << ":cursors:" << this->cursors.getCount() << endl;
	if(verbose)
		this->cursors.outputCursors();
}

void ShishaElement::clearMovement(){
	this->dx = this->dy = this->dw = this->dh = 0.0;
	this->moveFrameCurrent = this->moveFrameTotal = 0;
}

bool ShishaElement::isHovered(string cursorID, float x, float y){
	int tX = x;
	int tY = y;
	if(this->parent != NULL)
		this->parent->translateMouseCoords(tX, tY);
	return (tX >= this->x && tX <= this->x+this->width && tY >= this->y && tY <= this->y+this->height);
//	return (x >= this->x && x <= this->x+this->width && y >= this->y && y <= this->y+this->height);
}

bool ShishaElement::hasCursors(){
	return (this->cursors.getCount() > 0);
}

map<string, MultiCursor*>* ShishaElement::getCursors(){
	return this->cursors.getCursors();
}

void ShishaElement::translateCoords(float& x, float& y){
	if(this->parent != NULL && this->parent != this)
		this->parent->translateCoords(x, y);
	if(this->rotation == 0.0)
		return;
	ofxVec2f mouse(x,y);
	mouse -= ofxVec2f(this->rotationX, this->rotationY);
	mouse.rotate(-this->rotation);
	mouse += ofxVec2f(this->rotationX, this->rotationY);	
	x = mouse.x;
	y = mouse.y;
}

void ShishaElement::translateMouseCoords(int& x, int& y){
	float tmpX = (float)x;
	float tmpY = (float)y;
	this->translateCoords(tmpX, tmpY);
	x = (int)tmpX;
	y = (int)tmpY;
	
	/**
	if(this->parent != NULL && this->parent != this)
		this->parent->translateMouseCoords(x, y);
	if(this->rotation == 0.0)
		return;
	float cX = this->getX() + this->getWidth()/2.0;
	float cY = this->getY() + this->getHeight()/2.0;
	ofxVec2f mouse(x,y);
	mouse -= ofxVec2f(cX, cY);
	mouse.rotate(-this->rotation);
	mouse += ofxVec2f(cX, cY);	
	x = mouse.x;
	y = mouse.y;
	 */
}

bool ShishaElement::prepareForMotion(){
	return true;
}

void ShishaElement::rotate(float angle){
	this->rotation += angle;
	if(this->rotation > 360.0)
		this->rotation = tiNormalizeAngle(this->rotation);
	this->onRotate(angle);
}

void ShishaElement::setTheme(ShishaTheme* theme, ShishaTheme* doNotDelete, bool themeInherited){
	if(theme != NULL && theme == this->theme)
		return;
	if(this->theme != NULL && theme != this->theme && (doNotDelete == NULL || this->theme != doNotDelete)){
		delete this->theme;
		this->theme = NULL;
	}
	if(theme == NULL){
		theme = new ShishaTheme();
	}
	theme->checkInitted(); // make sure it's initialized
	this->theme = theme;
	this->themeInherited = themeInherited;
}

void ShishaElement::setElementID(int elementID){
	this->elementID = elementID;
}

void ShishaElement::setParent(ShishaElement* parent){
	this->parent = parent;
}

void ShishaElement::setName(string name){
	this->name = name;
}

void ShishaElement::setLabel(string label){
	this->label = label;
}

void ShishaElement::setCropBounds(bool cropBounds, float x, float y, float width, float height){
	this->cropBounds = cropBounds;
	if(x == -1.0)
		x = this->x;
	if(y == -1.0)
		y = this->y;
	if(width == -1.0)
		width = this->width;
	if(height == -1.0)
		height = this->height;
	this->cropRect.x = x;
	this->cropRect.y = y;
	this->cropRect.width = width;
	this->cropRect.height = height;
}

void ShishaElement::setDrawOffset(float x, float y){
	this->drawOffset.x = x;
	this->drawOffset.y = y;
}

void ShishaElement::setDrawBackground(bool drawBackground){
	this->drawBackground = drawBackground;
}

void ShishaElement::setDrawBorders(bool drawBorders){
	this->drawBorders = drawBorders;
}

void ShishaElement::setGUIShape(int guiShape){
	this->guiShape = guiShape;
}

void ShishaElement::setGUIState(int guiState){
	this->guiState = guiState;
}

void ShishaElement::setGUIStyle(int guiStyle){
	this->guiStyle = guiStyle;
}

void ShishaElement::setLocation(float x, float y, float width, float height, int moveFrames){
	if(moveFrames <= 0){
		this->setPosition(x, y);
		
		if(width != -1.0 && height != -1.0)
			this->setDimensions(width, height);
		
		this->moveFrameCurrent = 0;
		this->moveFrameTotal = 0;
	}
	else{
		this->dx = (x-this->x)/(float)moveFrames;
		this->dy = (y-this->y)/(float)moveFrames;

		if(width > 0 && height > 0){
			this->dw = (width-this->width)/(float)moveFrames;
			this->dh = (height-this->height)/(float)moveFrames;
		}
		this->moveFrameCurrent = 0;
		this->moveFrameTotal = moveFrames;
	}
}

void ShishaElement::setPosition(float x, float y){
	float xOffset = x - this->x;
	float yOffset = y - this->y;
	float oldX = (this->initted?this->x:-1.0);
	float oldY = (this->initted?this->y:-1.0);
	this->x = x;
	this->y = y;
	if(this->initted)
		this->onMove(xOffset, yOffset);
	this->onPosition(x, y, oldX, oldY);
}

void ShishaElement::setDimensions(float width, float height){
	float oldWidth = this->width;
	float oldHeight = this->height;
	
	if(width >= 0)
		this->width = width;
	if(height >= 0)
		this->height = height;
	
	if(oldWidth <= 0)
		oldWidth = this->width;
	if(oldHeight <= 0)
		oldHeight = this->height;

	this->onResize(width, height);
	if(this->initted && oldWidth != 0.0 && oldHeight != 0.0)
		this->onScale(this->width/oldWidth, this->height/oldHeight);
}

void ShishaElement::setWidth(float width){
//	this->width = width;
	this->setDimensions(width, this->getHeight());
}

void ShishaElement::setHeight(float height){
//	this->height = height;
	this->setDimensions(this->getWidth(), height);
}

void ShishaElement::setRotationCenter(float rotationX, float rotationY){
	this->rotationX = rotationX;
	this->rotationY = rotationY;
}

int ShishaElement::getElementID(){
	return this->elementID;
}

int ShishaElement::getElementType(){
	return this->elementType;
}

string ShishaElement::getName(){
	return this->name;
}

string ShishaElement::getLabel(){
	return this->label;
}

int ShishaElement::getGuiState(){
	return this->guiState;
}

int ShishaElement::getGuiStyle(){
	return this->guiStyle;
}

int ShishaElement::getGuiDisplayState(){
	return(this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_ACTIVE);
}

ShishaTheme* ShishaElement::getTheme(){
	if(this->theme == NULL)
		this->setTheme();
	return this->theme;
}

float ShishaElement::getX(){
	return this->x;
}

float ShishaElement::getY(){
	return this->y;
}

float ShishaElement::getWidth(){
	return this->width;
}

float ShishaElement::getHeight(){
	return this->height;
}

ShishaElement* ShishaElement::getParent(){
	return this->parent;
}

bool ShishaElement::selectSelf(string selector){
	return (selector == "" || selector == "value");
}

// virtual wrappers so that extending classes can take control if a cursor is added or not
void ShishaElement::setCursor(string cursorID, int x, int y, float w, float h, int button, bool dragging){
//	cout << "-" << this->getName() << ":set:" << cursorID << ":" << (dragging?"drag":"notDrag") << ":" << endl;	
	this->cursors.set(cursorID, x, y, w, h, button, dragging);

	gsCursors.set(cursorID, x, y, w, h, button, dragging);
	gsCursors.addElement(cursorID, this);
	
	if(button >= 0 && !dragging){
		this->cursors.checkElementClicked(cursorID, this);
		gsCursors.checkElementClicked(cursorID, this);
	}
}

void ShishaElement::unsetCursor(string cursorID){
//	cout << "-" << this->getName() << ":unset:" << cursorID << ":" << endl;
	this->cursors.unset(cursorID);
	
	if(gsCursors.dropElement(cursorID, this) <= 0)
		gsCursors.unset(cursorID);
}

// for event handling...
void ShishaElement::onHover(string cursorID){
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " hovered by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton( ((cursor->getClickX() == -1 || cursor->getClickY() == -1)?0:1) );
		eventObject.setCursorHovered(true);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementHover, eventObject, this);
#endif
}

void ShishaElement::onDehover(string cursorID){
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " dehovered by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton( ((cursor->getClickX() == -1 || cursor->getClickY() == -1)?0:1) );
		eventObject.setCursorHovered(false);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementDehover, eventObject, this);
#endif
}

void ShishaElement::onDrag(string cursorID){
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " dragged by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton(1);
		eventObject.setCursorHovered(true);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementDrag, eventObject, this);
#endif
}

void ShishaElement::onDragOn(string cursorID){
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " dragged on by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton(1);
		eventObject.setCursorHovered(true);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementDragOn, eventObject, this);
#endif
}

void ShishaElement::onDragOff(string cursorID){
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " dragged off by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton(1);
		eventObject.setCursorHovered(false);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementDragOff, eventObject, this);
#endif
}

void ShishaElement::onPress(string cursorID){
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " pressed by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton(1);
		eventObject.setCursorHovered(true);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementPress, eventObject, this);
#endif
}

void ShishaElement::onPressOff(string cursorID){
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " pressed off by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton(1);
		eventObject.setCursorHovered(false);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementPressOff, eventObject, this);
#endif
}

void ShishaElement::onRelease(string cursorID){	
	
#ifdef OFX_GUISHISHA_EVENTS_ENABLED
	stringstream msgBuilder;
	msgBuilder << "element " << this->getName() << " released by " << cursorID;
	ShishaCursorEvent eventObject(this, msgBuilder.str(), cursorID);
	
	MultiCursor* cursor = this->cursors.getCursor(cursorID, false);
	if(cursor != NULL){
		eventObject.setCursorPosition(cursor->getX(), cursor->getY());
		eventObject.setCursorMotion(cursor->getMoveX(), cursor->getMoveY());
		eventObject.setCursorButton(0);
		eventObject.setCursorHovered(true);
	}
	
	msgBuilder.str("");
	ofNotifyEvent(elementRelease, eventObject, this);
#endif
}

void ShishaElement::onMove(float xMove, float yMove){
	this->rotationX += xMove;
	this->rotationY += yMove;
	this->cropRect.x += xMove;
	this->cropRect.y += yMove;
}

void ShishaElement::onPosition(float x, float y, float oldX, float oldY){
}

void ShishaElement::onResize(float width, float height){
}

void ShishaElement::onRotate(float angle){
}

void ShishaElement::onScale(float xScale, float yScale){
}

ShishaElement* ShishaElement::getElement(string selector, string& subSelector){
	if(this->selectSelf(selector))
		return this;
	return NULL;
}

bool ShishaElement::getBool(string selector){
	if(this->selectSelf(selector))
		return (this->getName().length() > 0);
	return SELECTOR_UNIDENTIFIED_BOOL;
}

int ShishaElement::getInt(string selector){
	if(this->selectSelf(selector))
		return (this->getName().length());
	return SELECTOR_UNIDENTIFIED_INT;
}

float ShishaElement::getFloat(string selector){
	if(this->selectSelf(selector))
		return ((float)this->getName().length());
	return SELECTOR_UNIDENTIFIED_FLOAT;
}

string ShishaElement::getString(string selector){
	if(this->selectSelf(selector))
		return this->getName();
	return SELECTOR_UNIDENTIFIED_STRING;
}

ShishaElement* ShishaElement::setBoolValue(string selector, bool value){
	if(this->selectSelf(selector)){
		this->setBool(value);
		return this;
	}
	return NULL;
}

ShishaElement* ShishaElement::setElementValue(string selector, ShishaElement* value){
	if(this->selectSelf(selector)){
		this->setElement(value);
		return this;
	}
	return NULL;
}

ShishaElement* ShishaElement::setIntValue(string selector, int value){
	if(this->selectSelf(selector)){
		this->setInt(value);
		return this;
	}
	return NULL;
}

ShishaElement* ShishaElement::setFloatValue(string selector, float value){
	if(this->selectSelf(selector)){
		this->setFloat(value);
		return this;
	}
	return NULL;
}

ShishaElement* ShishaElement::setStringValue(string selector, string value){
	if(this->selectSelf(selector)){
		this->setString(value);
		return this;
	}
	return NULL;
}

void ShishaElement::setBool(bool value){
	// we have no value to set, so do nothing
}

void ShishaElement::setElement(ShishaElement* element){
	// we have no value to set, so do nothing
}

void ShishaElement::setInt(int value){
	// we have no value to set, so do nothing
}

void ShishaElement::setFloat(float value){
	// we have no value to set, so do nothing
}

void ShishaElement::setString(string value){
	// we have no value to set, so do nothing
}

void ShishaElement::rectViewCrop(ofRectangle rectCrop){
	glViewport(rectCrop.x, ofGetHeight()-rectCrop.y-rectCrop.height, rectCrop.width-1, rectCrop.height-1);
	
	// all the matrix setup is copied from the ofGraphics.cpp::void ofSetupScreen() method.
	float halfFov, theTan, screenFov, aspect;
	screenFov 		= 60.0f;
	float eyeX 		= (float)rectCrop.width / 2.0;
	float eyeY 		= (float)rectCrop.height / 2.0;
	halfFov 		= PI * screenFov / 360.0;
	theTan 			= tanf(halfFov);
	float dist 		= eyeY / theTan;
	float nearDist 	= dist / 10.0;	// near / far clip plane
	float farDist 	= dist * 10.0;
	aspect 			= (float)rectCrop.width/(float)rectCrop.height;
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(screenFov, aspect, nearDist, farDist);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyeX, eyeY, dist, eyeX, eyeY, 0.0, 0.0, 1.0, 0.0);
	
	glClear(GL_DEPTH_BUFFER_BIT);
	
	glScalef(1, -1, 1);           // invert Y axis so increasing Y goes down.
	glTranslatef(-rectCrop.x+this->drawOffset.x, -rectCrop.y-rectCrop.height+this->drawOffset.y, 0);       // shift origin up to upper-left corner.
}

void ShishaElement::rectViewUncrop(){
	glFlush();
	glViewport(0, 0, ofGetWidth(), ofGetHeight());
	ofSetupScreen();
}

void ShishaElement::draw(){
	this->draw(this->x, this->y, this->width, this->height);
}

void ShishaElement::draw(float x,float y){
	this->draw(x, y, this->width, this->height);
}

void ShishaElement::draw(float x, float y, float w, float h){
	// if we be croppin dem borders, create us a viewport to do it in
	if(this->cropBounds)
		this->rectViewCrop(this->cropRect);
	
	if(this->rotation != 0.0){
		ofPushMatrix();
		ofTranslate(x+w/2.0, y+h/2.0, 0);
		ofRotateZ(this->rotation);
		ofTranslate((x+w/2.0)*-1.0, (y+h/2.0)*-1.0, 0);
	}
	
	// draw ourselves
	this->draw(x, y, w, h, this->drawBorders);
	
	if(this->rotation != 0.0){
		ofPopMatrix();
	}
	
	// reset viewport back to main screen
	if(this->cropBounds)
		this->rectViewUncrop();
}

void ShishaElement::draw(float x, float y, float w, float h, bool borders){
	switch(this->guiShape){
		case GUI_ELEMENT_SHAPE_RECTANGLE:
			this->drawAsRectangle(x, y, w, h, borders);
			break;
		case GUI_ELEMENT_SHAPE_CIRCLE:
			this->drawAsCircle(x, y, w, h, borders);
			break;
	}

	if(this->drawCursors){
		this->getTheme()->setColour(SHISHA_COLOUR_CURSORS, this);
		this->cursors.draw();
	}
	
	this->getTheme()->setColour(); // reset to white
}

void ShishaElement::drawAsRectangle(float x, float y, float w, float h, bool borders){
	if(drawBackground){
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofRect(x, y, w, h);
	}
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
	}
}

void ShishaElement::drawAsCircle(float x, float y, float w, float h, bool borders){
	float radius = w/2.0;
	
	if(drawBackground){
		this->getTheme()->setColour(SHISHA_COLOUR_BACKGROUND, this);
		ofFill();
		ofCircle(x+radius, y+radius, radius);
	}
	
	if(borders){
		this->getTheme()->setColour(SHISHA_COLOUR_BORDER, this);	
		ofNoFill();
		ofCircle(x+radius, y+radius, radius);
		ofFill();
	}
}

void ShishaElement::update(){
	if(this->moveFrameTotal > 0){
		if(this->dx != 0 || this->dy != 0)
			this->setPosition(this->x + this->dx, this->y + this->dy);
		this->width += this->dw;
		this->height += this->dh;
		this->moveFrameCurrent++;
		
		if(this->cropBounds){
			this->cropRect.x = this->x;
			this->cropRect.y = this->y;
			this->cropRect.width = this->width;
			this->cropRect.height = this->height;
		}
		
		if(this->moveFrameCurrent == this->moveFrameTotal)
			this->clearMovement();
	}
}

bool ShishaElement::checkCursorHover(int x, int y, string cursorID, float w, float h){
	if(this->isHovered(cursorID, x, y)){
		this->setGUIState(GUI_ELEMENT_STATE_MOUSEOVER);
		this->setCursor(cursorID, x, y, w, h);
		this->onHover(cursorID);
		return true;
	}
	else if(this->cursors.hasCursor(cursorID)){
		this->unsetCursor(cursorID);
		if(this->cursors.getCount() < 1)
			this->setGUIState(GUI_ELEMENT_STATE_MOUSEOUT);
		this->onDehover(cursorID);
	}
	return false;
}

bool ShishaElement::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	if(button >= 0){
		bool wasOn = this->cursors.hasCursor(cursorID); //this->isHovered(cursorID, gsCursors.getLastX(cursorID), gsCursors.getLastY(cursorID));
		bool isOn = this->isHovered(cursorID, x, y);
		if(isOn){
			this->setGUIState(GUI_ELEMENT_STATE_MOUSEDRAG);
			this->setCursor(cursorID, x, y, w, h, button, true);
		}
		if(wasOn && isOn)
			this->onDrag(cursorID);
		else if(!wasOn && isOn)
			this->onDragOn(cursorID);
		else if(wasOn && !isOn){
			this->unsetCursor(cursorID);
			if(this->cursors.getCount() < 1)
				this->setGUIState(GUI_ELEMENT_STATE_MOUSEOUT);
			this->onDragOff(cursorID);
		}
		else if(!wasOn && !isOn){
			return false;
		}
		
		return true;
	}
	return false;
}

bool ShishaElement::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
	if(button >= 0){
		if(this->isHovered(cursorID, x, y)){
			this->setGUIState(GUI_ELEMENT_STATE_MOUSEDOWN);
			this->setCursor(cursorID, x, y, w, h, button);
			this->onPress(cursorID);
			return true;
		}
		else{
			this->onPressOff(cursorID);
			return false;
		}
	}
	return false;
}

bool ShishaElement::checkCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	bool wasOn = this->cursors.hasCursor(cursorID); //this->isHovered(cursorID, gsCursors.getLastX(cursorID), gsCursors.getLastY(cursorID));
	if(wasOn)
		this->onRelease(cursorID);
	this->unsetCursor(cursorID);
	if(cursorID == MOUSE_ID && this->isHovered(cursorID, x, y)){
		this->setCursor(cursorID, x, y, w, h);
		if(this->cursors.getCount() == 1)
			this->setGUIState(GUI_ELEMENT_STATE_MOUSEOVER);
	}
	if(this->cursors.getCount() < 1)
		this->setGUIState(GUI_ELEMENT_STATE_MOUSEOUT);
//	cout << this->getName() << ":" << cursorID << ":" << (wasOn?"wasOn":"wasntOn") << ":" << endl;
	// this is where the if(wasOn) this->onRelease(cursorID); used to be
	return (this->isHovered(cursorID, x, y));
}

bool ShishaElement::checkKeyPressed(int key){
	return false;
}

bool ShishaElement::checkKeyReleased(int key){
	return false;
}
