/*
 *  ShishaEvent.h
 *  openFrameworks
 *
 *  Created by Pat Long on 12/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHA_EVENT
#define _OFX_GUISHISHA_SHISHA_EVENT

#define OFX_GUISHISHA_EVENTS_ENABLED

#include "ofMain.h"

class ShishaElement;

class ShishaEvent{
protected:
	ShishaElement* element;
	string message;
	
public:
	ShishaEvent(ShishaElement* element=NULL, string message=""){
		this->setElement(element);
		this->setMessage(message);
	};
	
	~ShishaEvent(){
	};
	
	virtual ShishaElement* getElement(){
		return this->element;
	};
	
	virtual string getMessage(){
		return this->message;
	};
	
	virtual void setElement(ShishaElement* element=NULL){
		this->element = element;
	};
	
	virtual void setMessage(string message=""){
		this->message = message;
	};
};

#include "ShishaElement.h"

#endif
