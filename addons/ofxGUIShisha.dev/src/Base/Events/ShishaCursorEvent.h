/*
 *  ShishaCursorEvent.h
 *  openFrameworks
 *
 *  Created by Pat Long on 17/08/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_SHISHA_CURSOR_EVENT
#define _OFX_GUISHISHA_SHISHA_CURSOR_EVENT

#include "../ShishaEvent.h"

class ShishaCursorEvent : public ShishaEvent{
	protected:
		string cursorID;
		bool hovered;
		float x, y, dx, dy;
		int button;
		
	public:
		ShishaCursorEvent(ShishaElement* element=NULL, string message="", string cursorID=""):ShishaEvent(element, message){
			this->setCursorID(cursorID);
			this->setCursorPosition();
			this->setCursorMotion();
			this->setCursorButton();
			this->setCursorHovered();
		};
	
		~ShishaCursorEvent(){
		};
	
		virtual int getCursorButton(){
			return this->button;
		};
	
		virtual bool getCursorHovered(){
			return this->hovered;
		};
		
		virtual string getCursorID(){
			return this->cursorID;
		};
	
		virtual float getCursorMotionX(){
			return this->dx;
		};
		
		virtual float getCursorMotionY(){
			return this->dy;
		};
	
		virtual float getCursorPositionX(){
			return this->x;
		};
		
		virtual float getCursorPositionY(){
			return this->y;
		};
	
		virtual void setCursorButton(int button=0){
			this->button = button;
		};
		
		virtual void setCursorHovered(bool hovered=false){
			this->hovered = hovered;
		};
	
		virtual void setCursorID(string cursorID=""){
			this->cursorID = cursorID;
		};
		
		virtual void setCursorMotion(float dx=0.0, float dy=0.0){
			this->dx = dx;
			this->dy = dy;
		};
	
		virtual void setCursorPosition(float x=-1.0, float y=-1.0){
			this->x = x;
			this->y = y;
		};
		

		


		
};

#endif
