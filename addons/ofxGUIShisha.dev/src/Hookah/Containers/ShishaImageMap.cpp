/*
 *  ShishaImageMap.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 18/06/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ShishaImageMap.h"

ShishaImageMap::ShishaImageMap(){
}

ShishaImageMap::~ShishaImageMap(){
	for(int i=0; i < this->mapElements.size(); i++)
		delete this->mapElements[i];
}

void ShishaImageMap::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->setName("ShishaImageMap");
	this->setHorizontalSpacing(0.0);
	this->setVerticalSpacing(0.0);
	this->shouldClearMapPixels = false;
	this->shouldDrawInactiveBG = false;
	this->drawBackground = false;
	this->cOffsetX = 0.0;
	this->cOffsetY = 0.0;
	this->cBiggestOffset = 0.0;
}

ShishaImageMap::ImageMapElement* ShishaImageMap::getMapElement(ShishaElement* element){
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->element == element)
			return (*it);
	}
	return NULL;
}

bool ShishaImageMap::mapHasPixel(int x, int y){
	if(!this->shouldClearMapPixels)
		return true;
	
	bool check = true;
	
	// localize co-ordinates
	x -= this->x;
	y -= this->y;
	
	//retrieve the pixel
	unsigned char* pixel = this->mapInactive.getPixels(NULL, x, y, 1, 1);
	if(pixel[3] == 0)
		check = false;
	delete pixel;
	pixel = NULL;
	return check;
}

bool ShishaImageMap::loadImageMap(string filename, int stateCount){
	if(stateCount <= 0)
		return false;
	bool result = this->imageMap.loadImage(filename);
	this->imageMap.setImageType(OF_IMAGE_COLOR_ALPHA);
	
	float stateWidth = this->imageMap.width;
	float stateHeight = this->imageMap.height / stateCount;
	
	this->setDimensions(stateWidth, stateHeight);

	int cState = 0;
	unsigned char* pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapInactive.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	cState++;

	pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapInactiveHover.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	cState++;
	
	pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapActiveHover.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	cState++;
	
	pixels = new unsigned char[(int)(stateWidth*stateHeight*4)];
	this->mapActive.setFromPixels(this->imageMap.getPixels(pixels, 0, cState*stateHeight, stateWidth, stateHeight), stateWidth, stateHeight, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	cState++;
	
	return result;
}

void ShishaImageMap::clearArea(float x, float y, float w, float h){
	ColourRGBA clear;
	clear.setRGBA(255, 0, 255, 0); // we will clear them pixels like we mean it
	this->mapInactive.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
	this->mapInactiveHover.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
	this->mapActiveHover.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
	this->mapActive.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);
}

ShishaImageMap::ImageMapElement* ShishaImageMap::mapElement(ShishaElement* element, float x, float y, float w, float h, bool containerAdd, bool drawElement){
	ImageMapElement* newMapElement;
	vector<ofImage*> mapImages; // = new vector<ofImage*>(); // using a pointer because we don't want it to go out of scope
	ColourRGBA clear;
	clear.setRGBA(56, 56, 56, 0); // we will seriously cut out the pixels that get sliced already - destructive eh
	bool shouldClearPixels = this->shouldClearMapPixels;

	ofImage* mapImage;
	unsigned char* pixels;
	
	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapInactive.getPixels(pixels, (int)x, (int)y, (int)w, (int)h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapInactive.clearPixels((int)x, (int)y, (int)w, (int)h, clear.r, clear.g, clear.b, clear.a);

	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapInactiveHover.getPixels(pixels, (int)x, (int)y, (int)w, (int)h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapInactiveHover.clearPixels((int)x, (int)y, (int)w, (int)h, clear.r, clear.g, clear.b, clear.a);

	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapActiveHover.getPixels(pixels, x, y, w, h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapActiveHover.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);

	mapImage = new ofImage();
	pixels = new unsigned char[(int)(w*h*4)];
	mapImage->setFromPixels(this->mapActive.getPixels(pixels, x, y, w, h), w, h, OF_IMAGE_COLOR_ALPHA);
	delete [] pixels;
	pixels = NULL;
	mapImages.push_back(mapImage);
	if(shouldClearPixels)
		this->mapActive.clearPixels(x, y, w, h, clear.r, clear.g, clear.b, clear.a);

	newMapElement = new ImageMapElement(element, mapImages, drawElement);
	newMapElement->setMapLocation(x, y, w, h);
	
	this->mapElements.push_back(newMapElement);
	if(containerAdd)
		ShishaContainer::addElement(element, false, false);
	element->setLocation(this->x+x, this->y+y, w, h);

	return newMapElement;
}

void ShishaImageMap::disableHorizontalArea(float y, float h, ImageMapElement* element){
	float localY = y - this->getY();
	if(localY >= 0 && (localY + h) <= this->getHeight()){
		mapInactive.clearPixels(0, localY, mapInactive.getWidth(), h, 0, 0, 0, 0);
		float newHeight = this->getHeight();
		for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
			ImageMapElement* cElement = *it;
			float cLocalY = cElement->getY()-this->getY();
			if(cElement == element){	
				cElement->setVisibility(false);
				newHeight -= cElement->getHeight();
			}
			else if(cLocalY >= localY+h){
				// for elements below the area, we shall move them up
				float newY = cElement->getY()-h;
				if(newY < this->getY()) newY = this->getY();
				cElement->setPosition(cElement->getX(), newY);
			}
		}
		this->setHeight(newHeight);
	}
}

void ShishaImageMap::enableHorizontalArea(float y, float h, ImageMapElement* element){
	float localY = y - this->getY();
	if(localY >= 0){
		float newHeight = this->getHeight();
		for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
			ImageMapElement* cElement = *it;
			float cLocalY = cElement->getY()-this->getY();
			if(cElement == element){
				cElement->setVisibility(true);
				newHeight += cElement->getHeight();
			}
			else if(cLocalY >= localY){
				// for elements below the area, we shall move them down
				float newY = cElement->getY()+h;
				cElement->setPosition(cElement->getX(), newY);
			}
		}
		this->setHeight(newHeight);
	}
}

bool ShishaImageMap::toggleElementVisibility(ShishaElement* element, bool alterHorizontalSpace, bool alterVerticalSpace){
	bool result = false;
	ImageMapElement* mapElement = this->getMapElement(element);
	if(mapElement != NULL){
		if(alterHorizontalSpace){
			if(mapElement->isVisible())
				this->disableHorizontalArea(mapElement->getY(), mapElement->getHeight(), mapElement);
			else
				this->enableHorizontalArea(mapElement->getY(), mapElement->getHeight(), mapElement);
		}
		if(alterVerticalSpace){
			// TODO: implement the disableVerticalArea and enableVerticalArea methods
		}
		if(!alterVerticalSpace && !alterHorizontalSpace)
			result = mapElement->toggleVisibility();
		else
			result = mapElement->isVisible();

	}
	return result;
}

void ShishaImageMap::setShouldClearMapPixels(bool shouldClearMapPixels){
	this->shouldClearMapPixels = shouldClearMapPixels;
}

void ShishaImageMap::setShouldDrawInactiveBG(bool shouldDrawInactiveBG){
	this->shouldDrawInactiveBG = shouldDrawInactiveBG;
}

void ShishaImageMap::draw(){
	ShishaContainer::draw();
}

void ShishaImageMap::draw(float x, float y){
	ShishaContainer::draw(x, y);
}

void ShishaImageMap::draw(float x, float y, float w, float h, bool borders){
	if(this->shouldDrawInactiveBG)
		this->mapInactive.draw(x, y);

	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++)
		(*it)->draw();
	
//	this->mapInactiveHover.draw(x, y);
//	this->mapActiveHover.draw(x, y);
//	this->mapActive.draw(x, y);
}

void ShishaImageMap::clearCursors(){
	ShishaContainer::clearCursors();
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		(*it)->element->clearCursors();
	}
}

void ShishaImageMap::onMove(float xMove, float yMove){
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		float newX = (*it)->getX() + xMove;
		float newY = (*it)->getY() + yMove;
		(*it)->setPosition(newX, newY);
	}
}

bool ShishaImageMap::checkCursorHover(int x, int y, string cursorID, float w, float h){
	bool check = ShishaContainer::checkCursorHover(x, y, cursorID, w, h);
	return ((this->mapHasPixel(x, y) && this->isHovered(cursorID, x, y))?true:check);
}

bool ShishaImageMap::checkCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool check = ShishaContainer::checkCursorDrag(x, y, button, cursorID, w, h);
	return ((this->mapHasPixel(x, y) && this->isHovered(cursorID, x, y))?true:check);
}

bool ShishaImageMap::checkCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool check = ShishaContainer::checkCursorPress(x, y, button, cursorID, w, h);
	return ((this->mapHasPixel(x, y) && this->isHovered(cursorID, x, y))?true:check);
}

bool ShishaImageMap::checkChildrenCursorHover(int x, int y, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorHover(x, y, cursorID, w, h))
			check = true;
	}
	return check;
}

void ShishaImageMap::updateChildren(){
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible){
			(*it)->element->update();
		}
	}
}

bool ShishaImageMap::checkChildrenCursorDrag(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorDrag(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaImageMap::checkChildrenCursorPress(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorPress(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}

bool ShishaImageMap::checkChildrenCursorRelease(int x, int y, int button, string cursorID, float w, float h){
	bool check = false;
	for(vector<ImageMapElement*>::iterator it=this->mapElements.begin(); it != this->mapElements.end(); it++){
		if((*it)->visible && (*it)->element->checkCursorRelease(x, y, button, cursorID, w, h))
			check = true;
	}
	return check;
}


ShishaImageMap::ImageMapElement::ImageMapElement(ShishaElement* element, vector<ofImage*> stateImages, bool drawElement){
	this->element = element;
	this->stateImages = stateImages;
	this->setMapLocation(element->getX(), element->getY(), element->getWidth(), element->getHeight());
	this->visible = true;
	this->drawElement = drawElement;
}

ShishaImageMap::ImageMapElement::~ImageMapElement(){
	for(int i=0; i < this->stateImages.size(); i++){
		delete this->stateImages[i];
		this->stateImages[i] = NULL;
	}
	this->stateImages.clear();
}
				   
bool ShishaImageMap::ImageMapElement::isVisible(){
	return this->visible;
}

float ShishaImageMap::ImageMapElement::getWidth(){
	return ((this->element != NULL)?this->element->getWidth():this->mapLocation.width);
}

float ShishaImageMap::ImageMapElement::getHeight(){
	return ((this->element != NULL)?this->element->getHeight():this->mapLocation.height);
}

float ShishaImageMap::ImageMapElement::getX(){
	return ((this->element != NULL)?this->element->getX():this->mapLocation.x);
}

float ShishaImageMap::ImageMapElement::getY(){
	return ((this->element != NULL)?this->element->getY():this->mapLocation.y);
}

void ShishaImageMap::ImageMapElement::setPosition(float x, float y){
	this->setMapLocation(x, y, this->mapLocation.width, this->mapLocation.height);
	if(this->element != NULL){
		this->element->setPosition(x, y);
	}
}

void ShishaImageMap::ImageMapElement::setMapLocation(float x, float y, float w, float h){
	this->mapLocation.x = x;
	this->mapLocation.y = y;
	this->mapLocation.width = w;
	this->mapLocation.height = h;
}

void ShishaImageMap::ImageMapElement::setVisibility(bool visibility){
	this->visible = visibility;
}

bool ShishaImageMap::ImageMapElement::toggleVisibility(){
	this->setVisibility(!this->visible);
	return this->visible;
}

int ShishaImageMap::ImageMapElement::getGuiState(){
	return this->element->getGuiDisplayState();
}

void ShishaImageMap::ImageMapElement::draw(){
	if(this->element != NULL && this->visible){
		int guiState = this->getGuiState();
		if(guiState < this->stateImages.size()){
//			cout << "drawing:" << guiState << ":" << this->element->getX() << "," << this->element->getY() << ":" << endl;
			bool croppedView = false;
			if(this->element->getElementType() == SHISHA_TYPE_HOOKAH_BAR || this->element->getElementType() == SHISHA_TYPE_HOOKAH_SLIDER_BAR){
				float percent = ((BasicBar*)this->element)->getValueAsPercent();
				
				// TODO: build the one for vertical bars... maybe one for circular ones?
				float cropWidth = (float)this->element->getWidth()*percent;
				ofRectangle crop;
				crop.x = this->element->getX()+cropWidth;
				crop.y = this->element->getY();
				crop.width = this->element->getWidth()-cropWidth;
				crop.height = this->element->getHeight();
				
//				this->element->rectViewCrop(crop);
				this->stateImages[(this->element->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_INACTIVE)]->draw(this->element->getX(), this->element->getY(), this->element->getWidth(), this->element->getHeight());
//				this->element->rectViewUncrop();
				
				crop.x = this->element->getX();
				crop.y = this->element->getY();
				crop.width = cropWidth;
				crop.height = this->element->getHeight();
				this->element->rectViewCrop(crop);
				croppedView = true;
			}
			
			this->stateImages[guiState]->draw(this->element->getX(), this->element->getY()); //, this->element->getWidth(), this->element->getHeight());
			
			if(croppedView)
				this->element->rectViewUncrop();
			
			if(this->drawElement)
				this->element->draw();
		//		this->element->draw(this->element->getX(), this->element->getY(), this->element->getWidth(), this->element->getHeight());
		}
	}
}

