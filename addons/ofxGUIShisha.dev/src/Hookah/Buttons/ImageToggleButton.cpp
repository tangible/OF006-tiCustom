/*
 *  ImageToggleButton.cpp
 *  graffitiwall 7.5
 *
 *  Created by Eli Smiles on 11-08-10.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */

#include "ImageToggleButton.h"

ImageToggleButton::ImageToggleButton(){
	this->imageMap = NULL;
}

ImageToggleButton::~ImageToggleButton(){
	if(this->imageMap != NULL){
		delete this->imageMap;
		this->imageMap = NULL;
	}
}

void ImageToggleButton::init(float x, float y, float width, float height, int elementID){
	ToggleButton::init(x, y, width, height, elementID);
	this->imageMap = NULL;
}

void ImageToggleButton::draw(float x, float y, float w, float h){
	if(this->imageMap != NULL){
		this->imageMap->draw(x, y);
		
/**		ofSetColor(255, 255, 255);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
 */
	}
	else{
		ToggleButton::draw(x, y, w, h);
	}
}

bool ImageToggleButton::loadImageMap(string mapName){
	bool result = false;
	if(this->imageMap != NULL){
		delete this->imageMap;
		this->imageMap = NULL;
	}
	if(mapName != ""){
		this->imageMap = new ShishaImageMap();
		this->imageMap->init(this->getX(), this->getY(), this->getWidth(), this->getHeight(), 1);
		if(this->imageMap->loadImageMap(mapName)){
			this->setDimensions(this->imageMap->getWidth(), this->imageMap->getHeight());
			this->imageMap->mapElement(this, 0, 0, this->getWidth(), this->getHeight(), false);
			result = true;
		}
		else{
			delete this->imageMap;
			this->imageMap = NULL;
		}
	}
	return result;
}
