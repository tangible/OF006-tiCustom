/*
 *  ImageButton.h
 *  graffitiwall 7.4
 *
 *  Created by Eli Smiles on 11-04-25.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_IMAGE_BUTTON
#define _OFX_GUISHISHA_IMAGE_BUTTON

#include "BasicButton.h"
#include "ShishaImageMap.h"

class ImageButton : public BasicButton{
	protected:
		ShishaImageMap* imageMap;
		
	public:
		ImageButton();
		~ImageButton();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
		virtual void draw(float x, float y, float w, float h);
	
		bool loadImageMap(string mapName);
	
		
};

#endif
