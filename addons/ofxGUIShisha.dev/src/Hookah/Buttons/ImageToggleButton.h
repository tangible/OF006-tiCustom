/*
 *  ImageToggleButton.h
 *  graffitiwall 7.5
 *
 *  Created by Eli Smiles on 11-08-10.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_IMAGE_TOGGLE_BUTTON
#define _OFX_GUISHISHA_IMAGE_TOGGLE_BUTTON

#include "ToggleButton.h"
#include "ShishaImageMap.h"

class ImageToggleButton : public ToggleButton{
protected:
	ShishaImageMap* imageMap;
	
public:
	ImageToggleButton();
	~ImageToggleButton();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	virtual void draw(float x, float y, float w, float h);
	
	bool loadImageMap(string mapName);
};

#endif
