/*
 *  ToggleButton.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 27/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ToggleButton.h"

ToggleButton::ToggleButton(){
}

ToggleButton::~ToggleButton(){
}

void ToggleButton::onTriggerOn(){
	BasicButton::onTriggerOn();
	this->toggle();
}

void ToggleButton::onToggleOn(){
}

void ToggleButton::onToggleOff(){
}

void ToggleButton::init(float x, float y, float width, float height, int elementID){
	BasicButton::init(x, y, width, height, elementID);
	this->setName("ToggleButton");
	this->setLabel("Toggle");
	this->elementType = SHISHA_TYPE_HOOKAH_TOGGLE_BUTTON;
	this->toggleStyle = BUTTON_STYLE_TOGGLE_SOLID;
	this->toggled = false;
	this->pressTriggerDelay = DEFAULT_PRESS_TOGGLE_DELAY;
}

void ToggleButton::drawAsRectangle(float x, float y, float w, float h, bool borders){
	BasicButton::drawAsRectangle(x, y, w, h, borders);
	if(this->toggled){
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
		float offsetScale = 5.0;
		float offset = h/offsetScale;
		
		if(this->getGuiStyle() != BUTTON_STYLE_LABEL_OVERLAY){
			if(this->getGuiStyle() == BUTTON_STYLE_LABEL_LEFT)
				x += w - h;
		}
	
		if(this->toggleStyle == BUTTON_STYLE_TOGGLE_SOLID){
			if(this->getGuiStyle() == BUTTON_STYLE_LABEL_LEFT)
				ofRect(x+offset, y+offset, h-offset*2.0, h-offset*2.0);
			else if(this->getGuiStyle() == BUTTON_STYLE_LABEL_RIGHT)
				ofRect(x+offset, y+offset, h-offset*2.0, h-offset*2.0);
			else // OVERLAY
				ofRect(x+offset, y+offset, w-offset*2.0, h-offset*2.0);
		}
		else{ // X
			if(this->getGuiStyle() == BUTTON_STYLE_LABEL_LEFT){
				ofLine(x+offset, y+offset, x+h-offset, y+h-offset);
				ofLine(x+h-offset, y+offset, x+offset, y+h-offset);
			}
			else if(this->getGuiStyle() == BUTTON_STYLE_LABEL_RIGHT){
				ofLine(x+offset, y+offset, x+h-offset, y+h-offset);
				ofLine(x+h-offset, y+offset, x+offset, y+h-offset);
			}
			else{ // OVERLAY
				ofLine(x+offset, y+offset, x+w-offset, y+h-offset);
				ofLine(x+w-offset, y+offset, x+offset, y+h-offset);
			}			
		}
	}
}

void ToggleButton::drawAsCircle(float x, float y, float w, float h, bool borders){
	BasicButton::drawAsCircle(x, y, w, h, borders);
	if(this->toggled){
		this->getTheme()->setColour(SHISHA_COLOUR_FOREGROUND_2, this);
		float offsetScale = 4.0;
		float offset = w/offsetScale;
		float radius = w/2.0 - offset;

		if(this->getGuiStyle() != BUTTON_STYLE_LABEL_OVERLAY){
			offset = h/offsetScale;
			radius = h/2.0 - offset;
			if(this->getGuiStyle() == BUTTON_STYLE_LABEL_LEFT)
				x += w - h;
		}

		if(this->toggleStyle == BUTTON_STYLE_TOGGLE_SOLID){
			ofCircle(x+offset+radius, y+offset+radius, radius);
		}
		else{
			x += radius;
			y += radius;
			ofLine(x+radius-offset, y+radius-offset, x+radius+offset, y+radius+offset);
			ofLine(x+radius+offset, y+radius-offset, x+radius-offset, y+radius+offset);
		}
	}
}

void ToggleButton::setToggleStyle(int toggleStyle){
	this->toggleStyle = toggleStyle;
}

bool ToggleButton::toggle(){
	if(this->toggled)
		return this->toggleOff();
	else
		return this->toggleOn();
}

bool ToggleButton::toggleOn(){
	this->toggled = true;
	this->onToggleOn();
	return this->toggled;
}

bool ToggleButton::toggleOff(){
	this->toggled = false;
	this->onToggleOff();
	return this->toggled;
}

int ToggleButton::getGuiDisplayState(){
	if(this->getBool())
		return(this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_ACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_ACTIVE);
	else
		return(this->hasCursors()?GUI_ELEMENT_DISPLAY_STATE_INACTIVE_HOVER:GUI_ELEMENT_DISPLAY_STATE_INACTIVE);
}

bool ToggleButton::getBool(string selector){
	if(this->selectSelf(selector))
		return this->toggled;
	else if(selector == "active"){
		return this->isActive();
	}
	return false;
}

void ToggleButton::setBool(bool value){
	if(value)
		this->toggleOn();
	else
		this->toggleOff();
}
