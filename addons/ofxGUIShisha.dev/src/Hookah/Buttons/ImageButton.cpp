/*
 *  ImageButton.cpp
 *  graffitiwall 7.4
 *
 *  Created by Eli Smiles on 11-04-25.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */

#include "ImageButton.h"

ImageButton::ImageButton(){
	this->imageMap = NULL;
}

ImageButton::~ImageButton(){
	if(this->imageMap != NULL){
		delete this->imageMap;
		this->imageMap = NULL;
	}
}

void ImageButton::init(float x, float y, float width, float height, int elementID){
	BasicButton::init(x, y, width, height, elementID);
	this->imageMap = NULL;
}

void ImageButton::draw(float x, float y, float w, float h){
	if(this->imageMap != NULL){
		this->imageMap->draw(x, y);
		
/**		ofSetColor(255, 255, 255);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
 */
	}
	else{
		BasicButton::draw(x, y, w, h);
	}
}

bool ImageButton::loadImageMap(string mapName){
	bool result = false;
	if(this->imageMap != NULL){
		delete this->imageMap;
		this->imageMap = NULL;
	}
	if(mapName != ""){
		this->imageMap = new ShishaImageMap();
		this->imageMap->init(this->getX(), this->getY(), this->getWidth(), this->getHeight(), 1);
		if(this->imageMap->loadImageMap(mapName)){
			this->setDimensions(this->imageMap->getWidth(), this->imageMap->getHeight());
			this->imageMap->mapElement(this, 0, 0, this->getWidth(), this->getHeight(), false);
			result = true;
		}
		else{
			delete this->imageMap;
			this->imageMap = NULL;
		}
	}
	return result;
}
