/*
 *  RadioSet.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 27/04/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "RadioSet.h"

RadioSet::RadioButton::RadioButton(RadioSet* set){
	this->set = set;
}

RadioSet::RadioButton::~RadioButton(){
}

void RadioSet::RadioButton::init(float x, float y, float width, float height, int elementID){
	ToggleButton::init(x, y, width, height, elementID);
	this->setName("RadioButton");
	this->setLabel("Radio");
	this->elementType = SHISHA_TYPE_HOOKAH_RADIO_BUTTON;
	this->selected = false;
	this->value = this->elementID;
	this->holdTriggerEnabled = false;
}

void RadioSet::RadioButton::onToggleOn(){
	this->selected = true;
	if(this->set != NULL)
		this->set->setSelected(this);
}

void RadioSet::RadioButton::onToggleOff(){
	this->selected = false;
}

bool RadioSet::RadioButton::toggle(){
	if(!this->selected)
		ToggleButton::toggle();
}

void RadioSet::RadioButton::setElementID(int elementID){
	ShishaElement::setElementID(elementID);
	this->value = elementID;
}

int RadioSet::RadioButton::getValue(){
	return this->value;
}

RadioSet::RadioSet(){
}

RadioSet::~RadioSet(){
}

void RadioSet::init(float x, float y, float width, float height, int elementID){
	ShishaContainer::init(x, y, width, height, elementID);
	this->elementType = SHISHA_TYPE_HOOKAH_RADIO_SET;
	this->buttonShape = GUI_ELEMENT_SHAPE_RECTANGLE;
	this->setButtonStyle(BUTTON_STYLE_LABEL_RIGHT);
	this->buttonToggleStyle = BUTTON_STYLE_TOGGLE_SOLID;
	this->setName("RadioSet");
	this->setLabel("Radios");
	this->selected = NULL;
	this->setFlowMode(SHISHA_FLOW_MODE_VERTICAL);
}

int RadioSet::getSelectedIndex(){
	if(this->selected != NULL){
		for(int i=0; i < this->children.size(); i++){
			if(this->children[i] == this->selected)
				return i;
		}
	}
	return -1;
}

int RadioSet::getSelectedValue(){
	if(this->selected == NULL)
		return -1;
	return this->selected->getValue();
}

int RadioSet::getInt(string selector){
	if(this->selectSelf(selector)){
		if(this->selected != NULL)
			return this->getSelectedIndex();
		return -1;
	}
	return ShishaContainer::getInt(selector);
}

string RadioSet::getString(string selector){
	if(this->selectSelf(selector)){
		if(this->selected != NULL)
			return this->selected->getString();
		return "unselected";
	}
	return ShishaContainer::getString(selector);
}

void RadioSet::setButtonShape(int buttonShape){
	this->buttonShape = buttonShape;
}

void RadioSet::setButtonStyle(int buttonStyle){
	this->buttonStyle = buttonStyle;
}

void RadioSet::setButtonToggleStyle(int buttonToggleStyle){
	this->buttonToggleStyle = buttonToggleStyle;
}

RadioSet::RadioButton* RadioSet::addButton(float w, float h, string label, string name, bool select){
	RadioSet::RadioButton* newElement = new RadioSet::RadioButton(this);
	newElement->init(this->x, this->y, w, h);
	((RadioSet::RadioButton*)newElement)->setGUIShape(this->buttonShape);
	((RadioSet::RadioButton*)newElement)->setGUIStyle(this->buttonStyle);
	((RadioSet::RadioButton*)newElement)->setToggleStyle(this->buttonToggleStyle);
	if(select || this->selected == NULL)
		this->setSelected(newElement);
	ShishaContainer::addElement(newElement);
	if(label == DEFAULT_RADIO_LABEL)
		label = "Radio " + ofToString(newElement->getElementID());
	newElement->setLabel(label);
	if(name == "")
		name = label;
	newElement->setName(name);
	return newElement;
}

void RadioSet::setSelected(RadioButton* button){
	if(button == this->selected)
		return;
	if(this->selected != NULL)
		this->selected->toggleOff();
	this->selected = button;
	if(this->selected != NULL && !this->selected->getBool())
		this->selected->toggleOn();
}

void RadioSet::setSelectedIndex(int index){
	if(index >= 0 && index < this->children.size())
		this->setSelected((RadioSet::RadioButton*)this->children[index]);
}
