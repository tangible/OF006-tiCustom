/*
 *  ImageList.h
 *  openFrameworks
 *
 *  Created by Pat Long on 10/03/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_GUISHISHA_IMAGE_LIST
#define _OFX_GUISHISHA_IMAGE_LIST

#include "ofxDirList.h"

#include "ShishaElement.h"

class ImageList : public ShishaElement{
	protected:
		map<string,ofImage*> images;
		map<string,ofImage*>::iterator active;
	
		bool allowEmptyActive;
	
		int autoChangeDelay, lastAutoChange;
		
	public:
		ImageList();
		~ImageList();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
		virtual void draw();
		virtual void draw(float x, float y);
		virtual void draw(float x, float y, float w, float h);
		virtual void draw(float x, float y, float w, float h, bool borders);
	
		virtual void update();
	
		virtual void setTheme(ShishaTheme* theme=NULL, ShishaTheme* doNotDelete=NULL, bool themeInherited=false);
	
		virtual int addImage(string imageFileName, string imageName="", bool activate=false);
		virtual int addImage(ofImage* image, string imageName="", bool activate=false);
		virtual int loadDirectory(string imageDir);
	
		ofImage* getActiveImage();
		string getActiveImageName();
	
		vector<string> getImageNames();
	
		void setAllowEmptyActive(bool allowEmptyActive=true);
		void setAutoChangeDelay(int autoChangeDelay=-1);
		int setNextActiveImage();
		int setPreviousActiveImage();
		bool setActiveImage(int imageIndex=-1);
		bool setActiveImage(string imageName="");
};

#endif
