/*
 *  ImageBar.cpp
 *  graffitiwall 7.5
 *
 *  Created by Eli Smiles on 11-08-10.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */

#include "ImageBar.h"

ImageBar::ImageBar(){
	this->barBackground = NULL;
	this->barSlider = NULL;
}

ImageBar::~ImageBar(){
	if(this->barBackground != NULL){
		delete this->barBackground;
		this->barBackground = NULL;
	}
	
	if(this->barSlider != NULL){
		delete this->barSlider;
		this->barSlider = NULL;
	}
}

void ImageBar::init(float x, float y, float width, float height, int elementID){
	BasicBar::init(x, y, width, height, elementID);
	this->barBackground = NULL;
	this->barSlider = NULL;
}

void ImageBar::draw(float x, float y, float w, float h, bool borders){
	if(this->barBackground != NULL && this->barSlider != NULL){
/**		float cX = x;
		while(cX < x+w){
			this->barBackground->draw(cX, y);
			cX += this->barBackground->getWidth();
		}
 */
		this->barBackground->draw(x, y);
		
		if(this->slider != NULL){
			this->barSlider->draw(this->slider->getX(), this->slider->getY());
		}
		
/**		ofSetColor(255, 255, 255);
		ofNoFill();
		ofRect(x, y, w, h);
		ofFill();
 */
	}
	else{
		BasicBar::draw(x, y, w, h, borders);
	}
}

bool ImageBar::loadImageMap(string bgName, string sliderName){
	bool result = false;

	if(bgName != ""){
		if(this->barBackground != NULL){
			delete this->barBackground;
			this->barBackground = NULL;
		}
		
		this->barBackground = new ofImage();
		if(this->barBackground->loadImage(bgName)){
			this->setDimensions(this->barBackground->getWidth(), this->barBackground->getHeight());
			result = true;
		}
		else{
			delete this->barBackground;
			this->barBackground = NULL;
		}
	}
	
	if(sliderName != ""){
		if(this->barSlider != NULL){
			delete this->barSlider;
			this->barSlider = NULL;
		}
		
		this->barSlider = new ShishaImageMap();
		this->barSlider->init();
		if(this->barSlider->loadImageMap(sliderName)){
			if(this->slider != NULL){
				this->slider->setDimensions(this->barSlider->getWidth(), this->barSlider->getHeight());
				this->barSlider->setPosition(this->slider->getX(), this->slider->getY());
				this->barSlider->mapElement(this->slider, 0, 0, this->barSlider->getWidth(), this->barSlider->getHeight(), false);
				
			}
			result = true;
		}
		else{
			delete this->barSlider;
			this->barSlider = NULL;
		}
	}
	
	return result;
}
