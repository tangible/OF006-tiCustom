/*
 *  ofxThreadedImageSaver
 *
 *  Saves images on a separate thread.
 *
 *  Original code by memo.
 *  Source: http://www.openframeworks.cc/forum/viewtopic.php?f=14&t=1687
 *  
 *  Created by Pat Long (plong0) on 09/06/10.
 *  Copyright 2010 MindFire Media. All rights reserved.
 *
 */
#ifndef _OFX_THREADED_IMAGE_SAVER
#define _OFX_THREADED_IMAGE_SAVER

#include "ofMain.h"
#include "ofxThread.h"

class ofxThreadedImageSaver : public ofxThread, public ofImage {
public:
	string threaedFileName;
	
	void threadedFunction() {
		if(lock()) {
			saveImage(threaedFileName);
			unlock();
		} else {
			printf("ofxThreadedImageSaver - cannot save %s cos I'm locked", threaedFileName.c_str());
		}
		stopThread();
	}
	
	void saveThreaded(string threaedFileName) {
		this->threaedFileName = threaedFileName;
		startThread(false, false);   // blocking, verbose
	}
};

#endif
