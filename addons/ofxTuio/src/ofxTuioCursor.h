/*
	TUIO C++ Library for OpenFrameworks
    http://www.openframeworks.cc
	
	Copyright (c) 2008 by Matthias Dörfelt based on the Classes by Martin Kaltenbrunner
	which can be found at http://reactivision.sourceforge.net/
	
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _TUIO_CURSOR
#define _TUIO_CURSOR

#include "ofxTuioContainer.h"

//--------------------------------------------------------
class ofxTuioCursor : public ofxTuioContainer{
	
public:
	// PL: 03/31/2009 - added int _port property - useful for apps that use more than one tuio client (ie. to uniquely identify across ports)
	ofxTuioCursor(int _port, long _sid, int _fid, float _xpos, float _ypos) : ofxTuioContainer(_port, _sid,  _xpos,  _ypos){
		fingerId = _fid;
		xpos = _xpos;
		ypos = _ypos;
	};
	
	ofxTuioCursor(ofxTuioCursor * _tuioCursor) : ofxTuioContainer(_tuioCursor){
		fingerId = _tuioCursor->getFingerId();
	};
	
	~ofxTuioCursor(){};
	
	int getFingerId(){
		return fingerId;
	};
	
	// PL: 03/31/2009 - added string getCursorID() to return a composite ID of port and fingerID (ie. to uniquely identify a finger if more than one tuio client is in use)
	string getCursorID(){
		stringstream builder;
		builder << this->getPort() << ":" << this->getFingerId();
		return builder.str();
	};
	
protected:
	int fingerId;
};

#endif	
