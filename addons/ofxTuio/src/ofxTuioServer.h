/*
 *  ofxTuioServer.h
 *  openFrameworks
 *
 *  This is a basic TUIO Server.  It includes 2D Cursor tracking only.  Fiducial tracking may be added in the future.
 *
 *  Created by plong0 on 27/01/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _TUIO_SERVER
#define _TUIO_SERVER

#include "ofMain.h"

#include "ofxOsc.h"
#include "ofxOscSender.h"
#include "ofxTuioCursor.h"

#define DEFAULT_HOST "localhost"
#define DEFAULT_PORT 3333
#define DEFAULT_SOURCENAME "openFrameworks"

class myTuioServer{
	protected:
		ofxOscSender* sender;
		bool isOpen;
		int port;
		string host, sourceName;
		list<ofxTuioCursor*> cursorList;
		int windowWidth, windowHeight;
	
		void init(string sourceName=DEFAULT_SOURCENAME);
		ofxTuioCursor* getCursor(long sessionID);
	
	public:
		myTuioServer();
		myTuioServer(string sourceName);
		~myTuioServer();
	
		void start(string host=DEFAULT_HOST, int port=DEFAULT_PORT);
		void stop();
	
		void setCursor(long sessionID, float xPos, float yPos);
		void removeCursor(int sessionID);
		void resetCursors();
		void sendCursors();
};

#endif
