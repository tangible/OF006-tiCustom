/*
 *  ofxTuioServer.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 27/01/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxTuioServer.h"

myTuioServer::myTuioServer(){
	this->init();
}

myTuioServer::myTuioServer(string sourceName){
	this->init(sourceName);
}

myTuioServer::~myTuioServer(){
	delete this->sender;
	list<ofxTuioCursor*>::iterator tit;
	for (tit=this->cursorList.begin(); tit != this->cursorList.end(); tit++){
		ofxTuioCursor *cCursor = (*tit);
		delete cCursor;
	}
}

void myTuioServer::init(string sourceName){
	this->sender = new ofxOscSender();
	this->isOpen = false;
	this->sourceName = sourceName;
	this->windowWidth = ofGetWidth();
	this->windowHeight = ofGetHeight();
}

ofxTuioCursor* myTuioServer::getCursor(long sessionID){
	list<ofxTuioCursor*>::iterator tit;
	for (tit=this->cursorList.begin(); tit != this->cursorList.end(); tit++){
		ofxTuioCursor *cCursor = (*tit);
		if(cCursor->getSessionId() == sessionID){
			return cCursor;
		}
	}
	return NULL;
}

void myTuioServer::start(string host, int port){
	this->port = port;
	this->host = host;
	this->sender->setup(host.c_str(), port);
	this->isOpen = true;
}

void myTuioServer::stop(){
	this->resetCursors();
	this->sendCursors();
	this->isOpen = false;
}

void myTuioServer::setCursor(long sessionID, float xPos, float yPos){
	ofxTuioCursor* cursor = this->getCursor(sessionID);
	if(cursor != NULL){
		ofxTuioPoint cPosition = cursor->getPosition();
		float dx = xPos - cPosition.getX();
		float dy = yPos - cPosition.getY();
		cursor->update (xPos, yPos, dx, dy, 0.0);
	}
	else{
		cursor = new ofxTuioCursor(this->port, sessionID, sessionID, xPos, yPos);
		this->cursorList.push_back(cursor);
	}
}

void myTuioServer::removeCursor(int sessionID){
	list<ofxTuioCursor*>::iterator tit, help;
	for (tit=this->cursorList.begin(); tit != this->cursorList.end(); ){
		ofxTuioCursor *cCursor = (*tit);
		help = tit;
		tit++;
		if(cCursor->getSessionId() == sessionID){
			delete cCursor;
			this->cursorList.erase(help);
		}
	}
}

void myTuioServer::resetCursors(){
	list<ofxTuioCursor*>::iterator tit, help;
	for (tit=this->cursorList.begin(); tit != this->cursorList.end(); ){
		ofxTuioCursor *cCursor = (*tit);
		help = tit;
		tit++;
		delete cCursor;
		this->cursorList.erase(help);
	}
}

void myTuioServer::sendCursors(){
	if(!this->isOpen){
		return;
	}
	
	string address = "/tuio/2Dcur";

	ofxOscMessage cMessage;
	cMessage.setAddress(address);
	cMessage.addStringArg("source");
	cMessage.addStringArg(this->sourceName.c_str());
	this->sender->sendMessage(cMessage);
	cMessage.clear();
	
	list<ofxTuioCursor*>::iterator tit;
	
	// alive message (which points are alive)
	cMessage.setAddress(address);
	cMessage.addStringArg("alive");
	for (tit=cursorList.begin(); tit != cursorList.end(); tit++){
		ofxTuioCursor *cCursor = (*tit);
		cMessage.addIntArg(cCursor->getSessionId());
	}
	this->sender->sendMessage(cMessage);
	cMessage.clear();
	
	// set points (co-ordinates for each point)
	for (tit=cursorList.begin(); tit != cursorList.end(); tit++){
		ofxTuioCursor *cCursor = (*tit);
		ofxTuioPoint cPosition = cCursor->getPosition();
		cMessage.setAddress(address);
		cMessage.addStringArg("set");
		cMessage.addIntArg(cCursor->getSessionId());
		cMessage.addFloatArg(cPosition.getX()); // (scale to 0..1)
		cMessage.addFloatArg(cPosition.getY()); // (scale to 0..1)
		cMessage.addFloatArg(cCursor->getMotionAccel());
		cMessage.addFloatArg(0.0);
		cMessage.addFloatArg(0.0);
		this->sender->sendMessage(cMessage);
		cMessage.clear();
	}

	// finish this sequence
	cMessage.setAddress(address);
	cMessage.addStringArg("fseq");
	cMessage.addIntArg(-1);
	this->sender->sendMessage(cMessage);
	cMessage.clear();
}
