/*
 *  ofxTI_HubGui_ElementTypes.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-06-06.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB_GUI_ELEMENT_TYPES
#define _OFX_TI_HUB_GUI_ELEMENT_TYPES

#define SHISHA_TYPE_HUB_GUI_SERVICEFORM		200
#define SHISHA_TYPE_HUB_GUI_TEXTINPUT		201

#endif
