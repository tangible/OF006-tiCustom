/*
 *  ofxTI_HubGui_TextInput.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-06-06.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB_GUI_TEXTINPUT
#define _OFX_TI_HUB_GUI_TEXTINPUT

#include "ofxGUIShisha.h"

#include "ofxTI_HubGui_ElementTypes.h"

enum ValidationType{VALIDATION_NONE, VALIDATION_NOT_EMPTY, VALIDATION_EMAIL};

class ofxTI_HubGui_TextInput : public TextInput{
protected:
	bool firstClick, firstType;
	string defaultValue;
	
	ofImage defaultValueImage;
	bool hasDefaultValueImage;
	
	ValidationType validationType;
	
	virtual int calculateMaxTextLength();
	
	virtual void onPress(string cursorID);
	
public:
	ofxTI_HubGui_TextInput();
	~ofxTI_HubGui_TextInput();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	
	virtual void draw(float x, float y, float w, float h, bool borders);
	
	virtual bool isValid();
	virtual void reset();
	virtual void setDefaultValue(string defaultValue="");
	virtual void setDefaultValueImage(string filename="");
	virtual string getString(string selector=DEFAULT_SELECTOR);
	virtual void setString(string value="");
	void setValidationType(ValidationType validationType=VALIDATION_NONE);
	
};

#endif
