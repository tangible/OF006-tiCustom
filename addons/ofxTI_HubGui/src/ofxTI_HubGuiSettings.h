/*
 *  ofxTI_HubGuiSettings.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-06-08.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB_GUI_SETTINGS
#define _OFX_TI_HUB_GUI_SETTINGS

//#include "ofxGUIShisha.h"
#include "ofxTI_HubGui.h"
#include "ofxXmlSettings.h"

class ofxTI_HubGuiSettings{
protected:
	struct ServiceFieldSettings{
		string name;
		bool enabled;
		string validation;
		string defaultValue;
	};
	struct ServiceSettings{
		string name;
		bool enabled;
		vector<ServiceFieldSettings> fields;
	};
	
	ofxTI_HubGui* hubGui;
	
	map<int,string> animationModes;
	
	string xmlFilename;
	
	string imageBase;
	
	int tabAnimationMode;
	int tabAnimationSpeed;
	
	ofPoint formPosition;
	
	vector<ServiceSettings> services;
	
	void initAnimationModes();
	
	ServiceSettings getServiceSettings(string serviceName);
	ServiceFieldSettings getServiceFieldSettings(string serviceName, string fieldName);
	
	void initHubService(ServiceSettings settings);
	void killHubService(ServiceSettings settings);
	
	ofxTI_HubGui_ServiceForm* getHubServiceForm(string serviceName);
	
	void applySettingsToHubGui();
	
	
	void setHubFieldEnabled(string serviceName, string fieldName, bool enabled);
	void setHubFieldValidation(string serviceName, string fieldName, string validation);
	void setHubFieldDefaultValue(string serviceName, string fieldName, string defaultValue);
	
	
public:
	ofxTI_HubGuiSettings();
	~ofxTI_HubGuiSettings();
	
	void setHubGui(ofxTI_HubGui* hubGui=NULL);
	
	void loadFromXML(string xmlFilename);
	void saveToXML(string xmlFilename="");
	
	string animationModeToString(int animationMode);
	int animationModeToInt(string animationMode);
	
	ValidationType fieldValidationToValidationType(string validation);
	
	string getImageBase();
	
	int getTabAnimationMode();
	int getTabAnimationSpeed();
	
	vector<string> getServiceNames();
	bool hasService(string serviceName);
	bool isServiceEnabled(string serviceName);
	
	vector<string> getServiceFieldNames(string serviceName);
	bool hasServiceField(string serviceName, string fieldName);
	bool isServiceFieldEnabled(string serviceName, string fieldName);
	string getServiceFieldValidation(string serviceName, string fieldName);
	string getServiceFieldDefaultValue(string serviceName, string fieldName);
	
	
	void setImageBase(string imageBase);
	
	void setTabAnimationMode(int tabAnimationMode);
	void setTabAnimationSpeed(int tabAnimationSpeed);
	
	bool setServiceEnabled(string serviceName, bool enabled);
	bool toggleServiceEnabled(string serviceName);
	
	bool toggleServiceFieldEnabled(string serviceName, string fieldName);
	void setServiceFieldEnabled(string serviceName, string fieldName, bool enabled);
	void setServiceFieldValidation(string serviceName, string fieldName, string validation);
	void setServiceFieldDefaultValue(string serviceName, string fieldName, string defaultValue);
};

#endif
