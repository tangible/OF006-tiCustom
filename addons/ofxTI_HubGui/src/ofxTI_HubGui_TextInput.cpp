/*
 *  ofxTI_HubGui_TextInput.cpp
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-06-06.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxTI_HubGui_TextInput.h"


ofxTI_HubGui_TextInput::ofxTI_HubGui_TextInput(){
}

ofxTI_HubGui_TextInput::~ofxTI_HubGui_TextInput(){
}

void ofxTI_HubGui_TextInput::init(float x, float y, float width, float height, int elementID){
	TextInput::init(x, y, width, height, elementID);
	this->setLabel("");
	this->setPadding(3, 3, 2);
	this->setUserCursorHeight(21, 4);
	this->maxLineCount = 1;
	this->maxTextLength = -1;
	this->firstClick = true;
	this->firstType = true;
	this->defaultValue = "";
	this->hasDefaultValueImage = false;
	this->elementType = SHISHA_TYPE_HUB_GUI_TEXTINPUT;
	this->setValidationType();
}

void ofxTI_HubGui_TextInput::draw(float x, float y, float w, float h, bool borders){
	if(this->hasDefaultValueImage && this->defaultValue != "" && this->textValue == this->defaultValue && this->firstType)
		this->defaultValueImage.draw(x+this->getPaddingL(), y+this->getPaddingT());//(this->getHeight()-this->defaultValueImage.getHeight())/2.0);
	else
		TextInput::draw(x, y, w, h, false);
}

int ofxTI_HubGui_TextInput::calculateMaxTextLength(){
	TextInput::calculateMaxTextLength();
	float textAreaHeight = this->getHeight()-this->getVPadding();
	float textAreaWidth = this->getMaxLineWidth();
	this->maxLineCount = textAreaHeight / this->getTextHeight("yY");
	this->maxTextLength = this->maxLineCount * (textAreaWidth / this->getTextWidth(" "));
	return this->maxTextLength;
}

void ofxTI_HubGui_TextInput::onPress(string cursorID){
	TextInput::onPress(cursorID);
	if(this->firstClick){
		this->setString("");
		this->setCursorIndex(0);
		this->firstClick = false;
	}
}

bool ofxTI_HubGui_TextInput::isValid(){
	bool result = true;
	
	if(this->validationType == VALIDATION_NONE){
	}
	else if(this->validationType == VALIDATION_NOT_EMPTY){
		if(this->getString().length() <= 0)
			result = false;
	}
	else if(this->validationType == VALIDATION_EMAIL){
		string checkValue = this->getString();
		result = false; // we reset to true if it passes
		if(checkValue.length() > 0){
			vector<string> emSplit = split(checkValue, '@');
			if(emSplit.size() == 2){
				if(emSplit[0].length() > 0 && emSplit[1].length() >= 4){
					vector<string> hostSplit = split(emSplit[1], '.');
					if(hostSplit.size() >= 2){
						int domainLength = hostSplit[hostSplit.size()-1].length();
						if(domainLength >= 2 && domainLength <= 4)
							result = true;
					}
				}
			}
		}
		
	}
	
	return result;
}

void ofxTI_HubGui_TextInput::reset(){
	this->firstClick = true;
	this->setString(this->defaultValue);
	this->firstType = true;
}

void ofxTI_HubGui_TextInput::setDefaultValue(string defaultValue){
	this->defaultValue = defaultValue;
//	this->setString(this->defaultValue);
	this->reset();
}

void ofxTI_HubGui_TextInput::setDefaultValueImage(string filename){
	this->hasDefaultValueImage = false;
	if(filename != "")
		this->hasDefaultValueImage = this->defaultValueImage.loadImage(filename);
}

string ofxTI_HubGui_TextInput::getString(string selector){
	if(this->selectSelf(selector)){
		if(this->textValue == this->defaultValue && this->firstType)
			return "";
		return this->textValue;
	}
	return ShishaElement::getString(selector);
}

void ofxTI_HubGui_TextInput::setString(string value){
	TextInput::setString(value);
	this->firstType = false;
}

void ofxTI_HubGui_TextInput::setValidationType(ValidationType validationType){
	this->validationType = validationType;
}
