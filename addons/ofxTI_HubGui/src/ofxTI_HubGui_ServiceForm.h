/*
 *  ofxTI_HubGui_ServiceForm.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-05-25.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB_GUI_SERVICEFORM
#define _OFX_TI_HUB_GUI_SERVICEFORM

#include "ofxTI_Hub.h"
#include "ofxTI_HubGui_ElementTypes.h"
#include "ofxTI_HubGui_TextInput.h"

#include "ofxGUIShisha.h"

class ofxTI_HubGui_ServiceForm : public ShishaTabContainer{
protected:
	class FormInterface : public ShishaContainer{
	protected:
		ofImage formBgTop, formBg, formBgBottom;
		
	public:
		FormInterface();
		~FormInterface();
		
		virtual void initInterfaceElements(string interfacePath);
		
		virtual void draw(float x, float y, float w, float h, bool borders);
		
		virtual bool formValuesValid();
		virtual map<string,string> getFormValues();
		virtual void resetFormValues(ShishaElement* doNotReset=NULL);
		
		ShishaElement* selectElement(string selector="");
		
	};
	
	ofxTI_Hub* hub;
	map<string,ofImage*> sendImagesNamed;
	vector<ofImage*> sendImages;
	
	string imageBase;
	
	string serviceName;
	ofImage serviceIcon;
	
	ShishaKeyboard* mtKeyboard;
	ShishaElement* kbElement;
	bool manageKeyboard;
	ofPoint kbOffset;
	
	FormInterface* form;
	ColourRGBA* inputColour;
	ofTrueTypeFont* inputFont;
	BasicButton* submitButton;
	ShishaImageMap* submitMap;
	BasicButton* cancelButton;
	ShishaImageMap* cancelMap;
	
	map<string,bool> enabledFormFields;
	map<string,string> formFieldsDefaults;
	
	map<ofxTI_HubStatus, ofImage*> stateImages;
	
	ofxTI_HubStatus formState;
	
	void updateMTKeyboard();
	
	string getCustomFieldImageName(string namePattern, string fieldName);
	ShishaImageMap* loadElementImageMap(ShishaElement* element);
	
	void loadStateImages();
	void unloadStateImages();
	
	void layoutChildren();
	
public:
	ofxTI_HubGui_ServiceForm(string serviceName="");
	~ofxTI_HubGui_ServiceForm();
	
	virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT, int elementID=-1);
	virtual void buildFormInterface(float x=0, float y=0);
	
	virtual void draw(float x, float y, float w, float h, bool borders);
	virtual void update();
	
//	virtual void doShow(bool force=false);
	
	virtual void doSubmit();
	virtual void doCancel();
	
	ofImage getServiceIcon();
	
	void setHub(ofxTI_Hub* hub=NULL);
	bool isFieldEnabled(string formField);
	void enableFormField(string formField, bool enable=true);
	void setFormFieldDefault(string formField, string defaultValue="");
	void addSendImage(ofImage* sendImage, string name="");

	void setTextInputValidation(string fieldName, ValidationType validationType=VALIDATION_NONE);
	virtual void setFormState(ofxTI_HubStatus formState);
	void setImageBase(string imageBase="");
	void setMultiTouchKeyboard(ShishaKeyboard* mtKeyboard=NULL);
	void setServiceName(string serviceName="");
	
};


#endif
