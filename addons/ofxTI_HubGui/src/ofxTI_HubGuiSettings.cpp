/*
 *  ofxTI_HubGuiSettings.cpp
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-06-08.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxTI_HubGuiSettings.h"

ofxTI_HubGuiSettings::ofxTI_HubGuiSettings(){
	this->setHubGui();
	
	this->imageBase = "";
	this->tabAnimationMode = DEFAULT_TABCONTAINER_ANIMATE_MODE;
	this->tabAnimationSpeed = DEFAULT_TABCONTAINER_ANIMATE_SPEED;
	
	this->initAnimationModes();
}

ofxTI_HubGuiSettings::~ofxTI_HubGuiSettings(){
}

void ofxTI_HubGuiSettings::initAnimationModes(){
	animationModes[TABCONTAINER_ANIMATE_MODE_NONE] = "none";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_TOP] = "slide_top";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOM] = "slide_bottom";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_LEFT] = "slide_left";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_RIGHT] = "slide_right";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_TOPLEFT] = "slide_top_left";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_TOPRIGHT] = "slide_top_right";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMLEFT] = "slide_bottom_left";
	animationModes[TABCONTAINER_ANIMATE_MODE_SLIDE_BOTTOMRIGHT] = "slide_bottom_right";
}

string ofxTI_HubGuiSettings::animationModeToString(int animationMode){
	if(animationModes.count(animationMode) > 0)
		return animationModes[animationMode];
	return "none";
}

int ofxTI_HubGuiSettings::animationModeToInt(string animationMode){
	for(map<int,string>::iterator amIt=animationModes.begin(); amIt != animationModes.end(); amIt++){
		if(amIt->second == animationMode)
			return amIt->first;
	}
	return DEFAULT_TABCONTAINER_ANIMATE_MODE;
}

ValidationType ofxTI_HubGuiSettings::fieldValidationToValidationType(string validation){
	ValidationType result = VALIDATION_NONE;
	if(validation == "none") result = VALIDATION_NONE;
	else if( validation == "not_empty") result = VALIDATION_NOT_EMPTY;
	else if(validation == "email") result = VALIDATION_EMAIL;
	return result;
}

void ofxTI_HubGuiSettings::setHubGui(ofxTI_HubGui* hubGui){
	this->hubGui = hubGui;
	this->applySettingsToHubGui();
}

void ofxTI_HubGuiSettings::applySettingsToHubGui(){
	if(this->hubGui != NULL){
		this->hubGui->setImageBase(this->imageBase);
		this->hubGui->setTabAnimationMode(this->tabAnimationMode, true);
		this->hubGui->setTabAnimationSpeed(this->tabAnimationSpeed, true);
		
		for(int i=0; i < services.size(); i++){
			if(services[i].enabled)
				this->initHubService(services[i]);
			else
				this->killHubService(services[i]);
		}
	}
}

void ofxTI_HubGuiSettings::initHubService(ServiceSettings settings){
	if(settings.enabled){
		if(this->hubGui != NULL){			
			ofxTI_HubGui_ServiceForm* newForm = new ofxTI_HubGui_ServiceForm(settings.name);
			this->hubGui->initServiceFormSettings(newForm);
			
			newForm->init(this->formPosition.x, this->formPosition.y);
			
			for(int i=0; i < settings.fields.size(); i++)
				newForm->enableFormField(settings.fields[i].name, settings.fields[i].enabled);
			
			for(int i=0; i < settings.fields.size(); i++){
				newForm->setTextInputValidation(settings.fields[i].name, this->fieldValidationToValidationType(settings.fields[i].validation));
				newForm->setFormFieldDefault(settings.fields[i].name, settings.fields[i].defaultValue);
			}
			
			this->hubGui->addContainer(newForm);
		}
	}
	else{
		this->killHubService(settings);
	}
}

void ofxTI_HubGuiSettings::killHubService(ServiceSettings settings){
	if(!settings.enabled){
		if(this->hubGui != NULL)
			this->hubGui->removeService(settings.name);
	}
}

ofxTI_HubGui_ServiceForm* ofxTI_HubGuiSettings::getHubServiceForm(string serviceName){
	ofxTI_HubGui_ServiceForm* result = NULL;
	if(this->hubGui != NULL)
		result = this->hubGui->getServiceForm(serviceName);
	return result;
}

void ofxTI_HubGuiSettings::loadFromXML(string xmlFilename){
	if(xmlFilename == "") return;
	
	this->imageBase = "";
	this->tabAnimationMode = DEFAULT_TABCONTAINER_ANIMATE_MODE;
	this->tabAnimationSpeed = DEFAULT_TABCONTAINER_ANIMATE_SPEED;
	this->formPosition.x = this->formPosition.y = 0;
	this->services.clear();

	ofxXmlSettings xml;
	if(xml.loadFile(xmlFilename)){
		if(xml.tagExists("Hub:interface")){
			xml.pushTag("Hub");
			xml.pushTag("interface");
			
			this->setImageBase(xml.getValue("image_base", ""));
			
			// tabs settings
			string tabAnimationMode = xml.getValue("tabs:animation_mode", "");
			if(tabAnimationMode != "")
				this->setTabAnimationMode(this->animationModeToInt(tabAnimationMode));
			else
				this->setTabAnimationMode(DEFAULT_TABCONTAINER_ANIMATE_MODE);
			
			this->setTabAnimationSpeed(xml.getValue("tabs:animation_speed", DEFAULT_TABCONTAINER_ANIMATE_SPEED));
			
			this->formPosition.x = xml.getValue("form_position:x", 0);
			this->formPosition.y = xml.getValue("form_position:y", 0);
			
			// services
			if(xml.tagExists("services")){
				xml.pushTag("services");
				
				int serviceIndex = 0;
				while(xml.tagExists("service", serviceIndex)){
					if(xml.attributeExists("service", "name", serviceIndex)){
						string serviceName = xml.getAttribute("service", "name", "", serviceIndex);
						if(serviceName != ""){
							xml.pushTag("service", serviceIndex);
							
							ServiceSettings serviceSettings;
							serviceSettings.name = serviceName;
							
							serviceSettings.enabled = (xml.getValue("enabled", "true") == "true");

							int fieldIndex = 0;							
							while(xml.tagExists("field", fieldIndex)){
								if(xml.attributeExists("field", "name", fieldIndex)){
									string fieldName = xml.getAttribute("field", "name", "", fieldIndex);
									if(fieldName != ""){
										xml.pushTag("field", fieldIndex);
										
										ServiceFieldSettings fieldSettings;
										
										fieldSettings.name = fieldName;
										
										fieldSettings.enabled = (xml.getValue("enabled", "true") == "true");
										fieldSettings.validation = xml.getValue("validation", "none");
										fieldSettings.defaultValue = xml.getValue("default_value", "");
										
										serviceSettings.fields.push_back(fieldSettings);
										
										xml.popTag(); // field
									}
								}
								
								fieldIndex++;
							};
							
							this->services.push_back(serviceSettings);
							
							xml.popTag(); // service
						}
					}
					serviceIndex++;
				}
				
				xml.popTag(); // services
			}
			
			xml.popTag(); // interface
			xml.popTag(); // Hub
		}
		this->xmlFilename = xmlFilename;
	}
	
	this->applySettingsToHubGui();
	
	/**
	cout << "settings loaded:" << endl;
	cout << "\timageBase: " << this->imageBase << endl;
	cout << endl;
	cout << "\ttabAnimationMode: " << this->tabAnimationMode << endl;
	cout << "\ttabAnimationSpeed: " << this->tabAnimationSpeed << endl;
	cout << endl;
	cout << "\tservices: " << this->services.size() << endl;
	for(int i=0; i < this->services.size(); i++){
		ServiceSettings service = this->services[i];
		cout << "\tservice #" << i << endl;
		cout << "\t\tname: " << service.name << endl;
		cout << "\t\tenabled: " << (service.enabled?"true":"false") << endl;
		cout << "\t\tfields: " << service.fields.size() << endl;
		for(int j=0; j < service.fields.size(); j++){
			ServiceFieldSettings field = service.fields[j];
			cout << "\t\tfield #" << j << endl;
			cout << "\t\t\tname: " << field.name << endl;
			cout << "\t\t\tenabled: " << (field.enabled?"true":"false") << endl;
			cout << "\t\t\tvalidation: " << field.validation << endl;
			cout << "\t\t\tdefaultValue: " << field.defaultValue << endl;
		}
	}
	 */
}

void ofxTI_HubGuiSettings::saveToXML(string xmlFilename){
	if(xmlFilename == "") xmlFilename = this->xmlFilename;
	
	ofxXmlSettings xml;
	xml.loadFile(xmlFilename);
	
	if(!xml.tagExists("Hub"))
		xml.addTag("Hub");
	xml.pushTag("Hub");
	
	if(!xml.tagExists("interface"))
		xml.addTag("interface");
	xml.pushTag("interface");
	
	
	xml.setValue("image_base", this->imageBase);
	
	if(!xml.tagExists("tabs"))
		xml.addTag("tabs");
	xml.pushTag("tabs");
	
	xml.setValue("animation_mode", animationModeToString(this->tabAnimationMode));
	xml.setValue("animation_speed", this->tabAnimationSpeed);
	
	xml.popTag(); // tabs
	
	if(!xml.tagExists("services"))
		xml.addTag("services");
	xml.pushTag("services");
	
	for(int i=0; i < this->services.size(); i++){
		int serviceIndex = 0;
		bool foundService = false;
		while(xml.tagExists("service", serviceIndex)){
			if(xml.getAttribute("service", "name", "", serviceIndex) == this->services[i].name){
				foundService = true;
				break;
			}
			serviceIndex++;
		}
		
		if(!foundService){
			serviceIndex = xml.addTag("service");
			xml.setAttribute("service", "name", this->services[i].name, serviceIndex);
		}
		xml.pushTag("service", serviceIndex);
		
		xml.setValue("enabled", (this->services[i].enabled?"true":"false"));
		
		for(int j=0; j < this->services[i].fields.size(); j++){
			int fieldIndex = 0;
			bool foundField = false;
			while(xml.tagExists("field", fieldIndex)){
				if(xml.getAttribute("field", "name", "", fieldIndex) == this->services[i].fields[j].name){
					foundField = true;
					break;
				}
				fieldIndex++;
			}
			
			if(!foundField){
				fieldIndex = xml.addTag("field");
				xml.setAttribute("field", "name", this->services[i].fields[j].name, fieldIndex);
			}
			xml.pushTag("field", fieldIndex);
			
			xml.setValue("enabled", (this->services[i].fields[j].enabled?"true":"false"));
			xml.setValue("validation", this->services[i].fields[j].validation);
			xml.setValue("default_value", this->services[i].fields[j].defaultValue);
			
			xml.popTag(); // field
		}
		
		xml.popTag(); // service
	}
	
	
	xml.popTag(); // services
	
	xml.popTag(); // interface
	xml.popTag(); // Hub
	
	xml.saveFile(xmlFilename);
}

string ofxTI_HubGuiSettings::getImageBase(){
	return this->imageBase;
}

int ofxTI_HubGuiSettings::getTabAnimationMode(){
	return this->tabAnimationMode;
}

int ofxTI_HubGuiSettings::getTabAnimationSpeed(){
	return this->tabAnimationSpeed;
}

ofxTI_HubGuiSettings::ServiceSettings ofxTI_HubGuiSettings::getServiceSettings(string serviceName){
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName)
			return this->services[i];
	}
	ServiceSettings empty;
	return empty;
}

ofxTI_HubGuiSettings::ServiceFieldSettings ofxTI_HubGuiSettings::getServiceFieldSettings(string serviceName, string fieldName){
	ServiceSettings serviceSettings = this->getServiceSettings(serviceName);
	if(serviceSettings.name != ""){
		for(int i=0; i < serviceSettings.fields.size(); i++){
			if(serviceSettings.fields[i].name == fieldName)
				return serviceSettings.fields[i];
		}
	}
	ServiceFieldSettings empty;
	return empty;
}

vector<string> ofxTI_HubGuiSettings::getServiceNames(){
	vector<string> result;
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name != "")
			result.push_back(this->services[i].name);
	}
	return result;
}

bool ofxTI_HubGuiSettings::hasService(string serviceName){
	ServiceSettings settings = this->getServiceSettings(serviceName);
	return (settings.name != "");
}

bool ofxTI_HubGuiSettings::isServiceEnabled(string serviceName){
	ServiceSettings settings = this->getServiceSettings(serviceName);
	if(settings.name != "")
		return settings.enabled;
	return false;
}

vector<string> ofxTI_HubGuiSettings::getServiceFieldNames(string serviceName){
	vector<string> result;
	ServiceSettings serviceSettings = this->getServiceSettings(serviceName);
	for(int i=0; i < serviceSettings.fields.size(); i++){
		if(serviceSettings.fields[i].name != "")
			result.push_back(serviceSettings.fields[i].name);
	}
	return result;
}

bool ofxTI_HubGuiSettings::hasServiceField(string serviceName, string fieldName){
	ServiceFieldSettings settings = this->getServiceFieldSettings(serviceName, fieldName);
	return (settings.name != "");
}

bool ofxTI_HubGuiSettings::isServiceFieldEnabled(string serviceName, string fieldName){
	ServiceFieldSettings settings = this->getServiceFieldSettings(serviceName, fieldName);
	if(settings.name != "")
		return settings.enabled;
	return false;
}

string ofxTI_HubGuiSettings::getServiceFieldValidation(string serviceName, string fieldName){
	ServiceFieldSettings settings = this->getServiceFieldSettings(serviceName, fieldName);
	if(settings.name != "")
		return settings.validation;
	return "";
}

string ofxTI_HubGuiSettings::getServiceFieldDefaultValue(string serviceName, string fieldName){
	ServiceFieldSettings settings = this->getServiceFieldSettings(serviceName, fieldName);
	if(settings.name != "")
		return settings.defaultValue;
	return "";
}

void ofxTI_HubGuiSettings::setImageBase(string imageBase){
	this->imageBase = imageBase;
	if(this->hubGui != NULL)
		this->hubGui->setImageBase(this->imageBase);
}

void ofxTI_HubGuiSettings::setTabAnimationMode(int tabAnimationMode){
	this->tabAnimationMode = tabAnimationMode;
	if(this->hubGui != NULL)
		this->hubGui->setTabAnimationMode(this->tabAnimationMode, true);
}

void ofxTI_HubGuiSettings::setTabAnimationSpeed(int tabAnimationSpeed){
	this->tabAnimationSpeed = tabAnimationSpeed;
	if(this->hubGui != NULL)
		this->hubGui->setTabAnimationSpeed(this->tabAnimationSpeed, true);
}

bool ofxTI_HubGuiSettings::setServiceEnabled(string serviceName, bool enabled){
	bool result = false;
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName){
			this->services[i].enabled = enabled;
			
			if(enabled)
				this->initHubService(this->getServiceSettings(serviceName));
			else
				this->killHubService(this->getServiceSettings(serviceName));
			
			result = enabled;
		}
	}
	
	return result;
}

bool ofxTI_HubGuiSettings::toggleServiceEnabled(string serviceName){
	bool result = false;
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName){
			this->services[i].enabled = !this->services[i].enabled;
			result = this->services[i].enabled;
		}
	}
	if(result)
		this->initHubService(this->getServiceSettings(serviceName));
	else
		this->killHubService(this->getServiceSettings(serviceName));

	return result;
}

void ofxTI_HubGuiSettings::setHubFieldEnabled(string serviceName, string fieldName, bool enabled){
	ofxTI_HubGui_ServiceForm* serviceForm = this->getHubServiceForm(serviceName);
	if(serviceForm != NULL){
		serviceForm->enableFormField(fieldName, enabled);
		
		ServiceFieldSettings settings = this->getServiceFieldSettings(serviceName, fieldName);
		if(settings.name != ""){
			serviceForm->setTextInputValidation(fieldName, fieldValidationToValidationType(settings.validation));
			serviceForm->setFormFieldDefault(fieldName, settings.defaultValue);
		}
	}
}

void ofxTI_HubGuiSettings::setHubFieldValidation(string serviceName, string fieldName, string validation){
	ofxTI_HubGui_ServiceForm* serviceForm = this->getHubServiceForm(serviceName);
	if(serviceForm != NULL){
		serviceForm->setTextInputValidation(fieldName, fieldValidationToValidationType(validation));
	}
}

void ofxTI_HubGuiSettings::setHubFieldDefaultValue(string serviceName, string fieldName, string defaultValue){
	ofxTI_HubGui_ServiceForm* serviceForm = this->getHubServiceForm(serviceName);
	if(serviceForm != NULL){
		serviceForm->setFormFieldDefault(fieldName, defaultValue);
	}
}

bool ofxTI_HubGuiSettings::toggleServiceFieldEnabled(string serviceName, string fieldName){
	bool result = false;
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName){
			for(int j=0; j < this->services[i].fields.size(); j++){
				if(this->services[i].fields[j].name == fieldName){
					this->services[i].fields[j].enabled = !this->services[i].fields[j].enabled;
					result = this->services[i].fields[j].enabled;
					this->setHubFieldEnabled(serviceName, fieldName, result);
				}
			}
		}
	}
	return result;
}

void ofxTI_HubGuiSettings::setServiceFieldEnabled(string serviceName, string fieldName, bool enabled){
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName){
			for(int j=0; j < this->services[i].fields.size(); j++){
				if(this->services[i].fields[j].name == fieldName){
					this->services[i].fields[j].enabled = enabled;
					this->setHubFieldEnabled(serviceName, fieldName, enabled);
				}
			}
		}
	}
}

void ofxTI_HubGuiSettings::setServiceFieldValidation(string serviceName, string fieldName, string validation){
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName){
			for(int j=0; j < this->services[i].fields.size(); j++){
				if(this->services[i].fields[j].name == fieldName){
					this->services[i].fields[j].validation = validation;
					this->setHubFieldValidation(serviceName, fieldName, validation);
				}
			}
		}
	}
}

void ofxTI_HubGuiSettings::setServiceFieldDefaultValue(string serviceName, string fieldName, string defaultValue){
	for(int i=0; i < this->services.size(); i++){
		if(this->services[i].name == serviceName){
			for(int j=0; j < this->services[i].fields.size(); j++){
				if(this->services[i].fields[j].name == fieldName){
					this->services[i].fields[j].defaultValue = defaultValue;
					this->setHubFieldDefaultValue(serviceName, fieldName, defaultValue);
				}
			}
		}
	}
}
