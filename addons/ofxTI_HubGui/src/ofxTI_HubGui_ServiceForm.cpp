/*
 *  ofxTI_HubGui_ServiceForm.cpp
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-05-25.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxTI_HubGui_ServiceForm.h"

ofxTI_HubGui_ServiceForm::ofxTI_HubGui_ServiceForm(string serviceName){
	this->hub = NULL;
	this->form = NULL;
	this->mtKeyboard = NULL;
	this->kbElement = NULL;
	this->inputColour = NULL;
	this->inputFont = NULL;
	this->setImageBase();
	this->setMultiTouchKeyboard();
	this->setServiceName(serviceName);
}

ofxTI_HubGui_ServiceForm::~ofxTI_HubGui_ServiceForm(){
	if(this->mtKeyboard != NULL)
		this->dropElement(this->mtKeyboard, false);
	if(this->manageKeyboard){
		delete this->mtKeyboard;
		this->mtKeyboard = NULL;
	}
	
	if(this->inputColour != NULL){
		delete this->inputColour;
		this->inputColour = NULL;
	}
	
	if(this->inputFont != NULL){
		delete this->inputFont;
		this->inputFont = NULL;
	}
	
	this->unloadStateImages();
}

void ofxTI_HubGui_ServiceForm::init(float x, float y, float width, float height, int elementID){	
	ShishaTabContainer::init(x, y, width, height, this->serviceName, elementID);
	this->setName("Hub:"+this->serviceName);
	this->elementType = SHISHA_TYPE_HUB_GUI_SERVICEFORM;
	
	this->formState = STATUS_IDLE;
	this->loadStateImages();
	
	if(this->serviceName != ""){
		stringstream iconName;
		iconName << this->imageBase << "icons/icon_" << this->serviceName << ".png";
		this->serviceIcon.loadImage(iconName.str());
	}
	
	if(this->mtKeyboard == NULL){
		this->mtKeyboard = new ShishaKeyboard();
		this->mtKeyboard->setImageBase(this->imageBase+"keyboard");
		this->mtKeyboard->init();
		this->manageKeyboard = true;
	}
	
	
	if(this->mtKeyboard != NULL)
		this->addElement(this->mtKeyboard, false);
	
	
	this->submitButton = new BasicButton();
	this->submitButton->init(0, 0, 97, 33);
	this->submitButton->setName("submit");
	this->submitButton->setLabel("Submit");	
	this->submitMap = this->loadElementImageMap(this->submitButton);
	if(this->submitMap != NULL){
		this->submitMap->setName("SubmitMap");
		this->addElement(this->submitMap, false);
	}
	else{
		this->addElement(this->submitButton, false);
	}
	
	this->cancelButton = new BasicButton();
	this->cancelButton->init(0, 0, 97, 33);
	this->cancelButton->setName("cancel");
	this->cancelButton->setLabel("Cancel");
	this->cancelMap = this->loadElementImageMap(this->cancelButton);
	if(this->cancelMap != NULL){
		this->cancelMap->setName("CancelMap");
		this->addElement(this->cancelMap, false);
	}
	else{
		this->addElement(this->cancelButton, false);
	}
	
	this->buildFormInterface(x, y);
}

void ofxTI_HubGui_ServiceForm::layoutChildren(){
	if(this->form == NULL)
		return;
	
	float cX = this->getX();
	float cY = this->form->getY() + this->form->getHeight();
	float maxX = this->form->getX()+this->form->getWidth();
	float maxY = this->form->getY()+this->form->getHeight();	
	
	kbOffset.x = -6.0;
	kbOffset.y = (cY-this->getY())+1.0;
	
	if(this->mtKeyboard != NULL){
		cY += this->mtKeyboard->getHeight()+2.0;
		
		if((this->getX()+kbOffset.x+this->mtKeyboard->getWidth()) > maxX)
			maxX = (this->getX()+kbOffset.x+this->mtKeyboard->getWidth());
		if((this->getY()+kbOffset.y+this->mtKeyboard->getHeight()) > maxY)
			maxY = (this->getY()+kbOffset.y+this->mtKeyboard->getHeight());
	}
	
	cX = this->getX()+this->form->getWidth();
	
	if(this->submitButton != NULL){
		cX -= this->submitButton->getWidth();

		if(this->submitMap != NULL)
			this->submitMap->setPosition(cX, cY);
		else
			this->submitButton->setPosition(cX, cY);
		
		if((this->submitButton->getX()+this->submitButton->getWidth()) > maxX)
			maxX = (this->submitButton->getX()+this->submitButton->getWidth());
		if((this->submitButton->getY()+this->submitButton->getHeight()) > maxY)
			maxY = (this->submitButton->getY()+this->submitButton->getHeight());
	}
	
	if(this->cancelButton != NULL){
		cX -= this->cancelButton->getWidth();
		
		if(this->cancelMap != NULL)
			this->cancelMap->setPosition(cX, cY);
		else
			this->cancelButton->setPosition(cX, cY);
		
		if((this->cancelButton->getX()+this->cancelButton->getWidth()) > maxX)
			maxX = (this->cancelButton->getX()+this->cancelButton->getWidth());
		if((this->cancelButton->getY()+this->cancelButton->getHeight()) > maxY)
			maxY = (this->cancelButton->getY()+this->cancelButton->getHeight());
	}
	
	
	width = maxX - x;
	height = maxY - y;
	this->setDimensions(width, height);
}

void ofxTI_HubGui_ServiceForm::buildFormInterface(float x, float y){
	if(this->form != NULL){
		this->dropElement(this->form, true);
		this->form = NULL;
	}
	
	float cX = x + 10.0;
	float cY = y + 10.0 + this->serviceIcon.getHeight() + 8.0;
	
	ShishaImageMap* mappedElement;
	ofxTI_HubGui_TextInput* formInput;
	if(this->inputColour == NULL)
		this->inputColour = new ColourRGBA(255.0, 255.0, 255.0);
	
	if(this->inputFont == NULL){
		this->inputFont = new ofTrueTypeFont();
		this->inputFont->loadFont(this->imageBase+"fonts/Geogtq-Md.otf", 10.0);
		if(!this->inputFont->bLoadedOk){
			delete this->inputFont;
			this->inputFont = NULL;
		}
	}
	
	string defaultImagePattern = this->imageBase+"form/form%s_field%f_default.png";
	
	this->form = new FormInterface();
	this->form->init(x, y, 544, this->serviceIcon.getHeight()+10.0);
	
	if(this->isFieldEnabled("from")){
		formInput = new ofxTI_HubGui_TextInput();
		formInput->init(0, 0, 260, 40);
		formInput->setName("from");
		formInput->setLabel("");
		formInput->setPadding(9, 7, 9, 5);
		formInput->setUserCursorHeight(20.0, -2.0);
		formInput->setCustomColour(this->inputColour, false);
		formInput->setCustomFont(this->inputFont, false);
		formInput->setDefaultValue("From");
		formInput->setDefaultValueImage(getCustomFieldImageName(defaultImagePattern, "from"));
		mappedElement = this->loadElementImageMap(formInput);
		if(mappedElement != NULL){
			mappedElement->setPosition(cX, cY);
			this->form->addElement(mappedElement, false);
		}
		else
			formInput->setPosition(cX, cY);
		this->form->addElement(formInput, false);
		
		if(!this->isFieldEnabled("to")){
			cX = x + 10.0;
			cY += formInput->getHeight() + 2.0;
		}
		else{
			cX += formInput->getWidth() + 4.0;
		}
	}
	
	if(this->isFieldEnabled("to")){
		formInput = new ofxTI_HubGui_TextInput();
		formInput->init(0, 0, 260, 40);
		formInput->setName("to");
		formInput->setLabel("");
		formInput->setPadding(9, 7, 9, 5);
		formInput->setUserCursorHeight(20.0, -2.0);
		formInput->setCustomColour(this->inputColour, false);
		formInput->setCustomFont(this->inputFont, false);
		formInput->setDefaultValue("To");
		formInput->setDefaultValueImage(getCustomFieldImageName(defaultImagePattern, "to"));
		mappedElement = this->loadElementImageMap(formInput);
		if(mappedElement != NULL){
			mappedElement->setPosition(cX, cY);
			this->form->addElement(mappedElement, false);
		}
		else
			formInput->setPosition(cX, cY);
		this->form->addElement(formInput, false);
		
		cX = x + 10.0;
		cY += formInput->getHeight() + 2.0;
	}
	
	if(this->isFieldEnabled("message")){
		formInput = new ofxTI_HubGui_TextInput();
		formInput->init(0, 0, 524, 56);
		formInput->setName("message");
		formInput->setLabel("");
		formInput->setPadding(9, 7, 9, 5);
		formInput->setUserCursorHeight(20.0, -2.0);
		formInput->setCustomColour(this->inputColour, false);
		formInput->setCustomFont(this->inputFont, false);
		formInput->setLineHeightMultiplier(1.2);
		formInput->setDefaultValue("Message (140 characters maximum)");
		formInput->setDefaultValueImage(getCustomFieldImageName(defaultImagePattern, "message"));
		mappedElement = this->loadElementImageMap(formInput);
		if(mappedElement != NULL){
			mappedElement->setPosition(cX, cY);
			this->form->addElement(mappedElement, false);
		}
		else
			formInput->setPosition(cX, cY);
		formInput->setMaxTextLength(140);
		this->form->addElement(formInput, false);
		
		cY += formInput->getHeight();
	}
	
	cY += 10.0;
	
	if((cY-this->form->getY()) > this->form->getHeight())
		this->form->setDimensions(this->form->getWidth(), (cY-this->form->getY()));
	this->form->initInterfaceElements(this->imageBase);
	
	this->addElement(this->form, false);
	
	this->layoutChildren();
}

void ofxTI_HubGui_ServiceForm::draw(float x, float y, float w, float h, bool borders){
/**	ofNoFill();
	ofSetColor(0, 255, 255);
	ofRect(x, y, w, h-2);
	ofSetColor(255, 255, 255);
	ofFill();
 */
	
	if(this->mtKeyboard != NULL){
		if(this->mtKeyboard->getX() != x+kbOffset.x || this->mtKeyboard->getY() != y+kbOffset.y)
			this->mtKeyboard->setPosition(x+kbOffset.x, y+kbOffset.y);
	}

	this->drawChildren();
	
	float iconX = x + 12.0;
	float iconY = y + 12.0;
	
	this->serviceIcon.draw(iconX, iconY);
	
	ofImage* stateImage = this->stateImages[this->formState];
	if(stateImage != NULL){
		stateImage->draw(iconX + this->serviceIcon.getWidth() + 8.0, iconY + (this->serviceIcon.getHeight()-stateImage->getHeight())/2.0);
	}
}

void ofxTI_HubGui_ServiceForm::update(){
	ShishaTabContainer::update();
	this->updateMTKeyboard();
	if(this->submitButton->getBool())
		this->doSubmit();
	if(this->cancelButton->getBool())
		this->doCancel();
	
	if(this->hub != NULL){
		if(this->formState == STATUS_SENDING && this->hub->getStatus() != STATUS_IDLE && this->formState != this->hub->getStatus()){
			this->setFormState(this->hub->getStatus());
		}
		
/**		if(this->formState != this->hub->getStatus()){			
			if(this->formState != STATUS_ERROR && this->formState != STATUS_SUCCESS){
				this->setFormState(this->hub->getStatus());
			}
		}*/
	}
}

void ofxTI_HubGui_ServiceForm::doSubmit(){
	if(this->form != NULL){
		if(this->hub != NULL && this->hub->getStatus() != STATUS_SENDING){
			if(this->form->formValuesValid()){
				ofxTI_HubEntry* newEntry = this->hub->getNewEntry();	
				map<string,string> values = this->form->getFormValues();
				for(map<string,string>::iterator vIt = values.begin(); vIt != values.end(); vIt++){
					string theValue = vIt->second;
					if(theValue == "" && this->formFieldsDefaults.count(vIt->first) > 0)
						theValue = this->formFieldsDefaults[vIt->first];
					newEntry->addTextNode(theValue, vIt->first);
				}
				for(map<string,ofImage*>::iterator imgIt = sendImagesNamed.begin(); imgIt != sendImagesNamed.end(); imgIt++){
					newEntry->addImageNode(*(imgIt->second), imgIt->first);
				}
				for(vector<ofImage*>::iterator imgIt = sendImages.begin(); imgIt != sendImages.end(); imgIt++){
					newEntry->addImageNode(*(*imgIt));
				}
				newEntry->addService(this->serviceName);
//				this->setFormState(STATUS_IDLE);
				this->hub->setStatus(STATUS_IDLE);
				this->setFormState(STATUS_SENDING);
				this->hub->postEntry(newEntry);
			}
			else{
				this->setFormState(STATUS_ERROR);
				this->hub->setStatus(STATUS_ERROR);
			}
		}
	}
}

void ofxTI_HubGui_ServiceForm::doCancel(){
	if(this->form != NULL){
		this->form->resetFormValues();
	}
	if(this->hub != NULL && this->hub->getStatus() != STATUS_SENDING){
		this->hub->setStatus(STATUS_IDLE);
		this->setFormState(STATUS_IDLE);
	}
	this->doHide();
}

string ofxTI_HubGui_ServiceForm::getCustomFieldImageName(string namePattern, string fieldName){
	string name = stringReplace(stringReplace(namePattern, "%s", "_"+this->serviceName), "%f", "_"+fieldName);
	ofImage loadCheck;
	if(loadCheck.loadImage(name))
		return name;
	else{
		name = stringReplace(stringReplace(namePattern, "%f", "_"+fieldName), "%s", "");
		if(loadCheck.loadImage(name))
			return name;
		else{
			name = stringReplace(stringReplace(namePattern, "%s", "_"+this->serviceName), "%f", "");
			if(loadCheck.loadImage(name))
				return name;
			else{
				name = stringReplace(stringReplace(namePattern, "%s", ""), "%f", "");
				if(loadCheck.loadImage(name))
					return name;
			}
		}
	}
	return "";
}

ShishaImageMap* ofxTI_HubGui_ServiceForm::loadElementImageMap(ShishaElement* element){
	ShishaImageMap* result = NULL;
	
	if(element != NULL){
		stringstream nameBuilder;
		
		nameBuilder << this->imageBase << "form/form%s_";
		if(element->getElementType() == SHISHA_TYPE_HOOKAH_BUTTON)
			nameBuilder << "button";
		else
			nameBuilder << "field";
		nameBuilder << "%f.png";
		
		string imageName = this->getCustomFieldImageName(nameBuilder.str(), element->getName());
		
		result = new ShishaImageMap();
		result->init(0, 0, 0, 0);
		if(imageName == "" || !result->loadImageMap(imageName)){
			delete result;
			result = NULL;
		}
		
/**		string genericBase = nameBuilder.str();
		if(!result->loadImageMap(genericBase+"_"+element->getName()+".png")){
			if(!result->loadImageMap(genericBase+".png")){
				delete result;
				result = NULL;
			}
		}
 */
		
		if(result != NULL){
			result->setShouldDrawInactiveBG(false);
			result->mapElement(element, 0, 0, result->getWidth(), result->getHeight(), false, false);
		}
	}
		
	return result;
}

void ofxTI_HubGui_ServiceForm::loadStateImages(){
	string statusImageBase = this->imageBase+"status_messages/";
	ofImage* stateImage;
	
	stateImage = NULL;
	stateImages[STATUS_IDLE] = stateImage;
	
	stateImage = new ofImage();	
	if(!(stateImage->loadImage(statusImageBase+"status_processing.png"))){
		delete stateImage;
		stateImage = NULL;
	}
	stateImages[STATUS_SENDING] = stateImage;
	
	stateImage = new ofImage();
	if(!stateImage->loadImage(statusImageBase+"status_success.png")){
		delete stateImage;
		stateImage = NULL;
	}
	stateImages[STATUS_SUCCESS] = stateImage;
	
	stateImage = new ofImage();
	if(!stateImage->loadImage(statusImageBase+"status_error.png")){
		delete stateImage;
		stateImage = NULL;
	}
	stateImages[STATUS_ERROR] = stateImage;	
}

void ofxTI_HubGui_ServiceForm::unloadStateImages(){
	for(map<ofxTI_HubStatus,ofImage*>::iterator siIt=stateImages.begin(); siIt != stateImages.end(); siIt++){
		ofImage* cImage = siIt->second;
		if(cImage != NULL){
			delete cImage;
			cImage = NULL;
		}
		stateImages[siIt->first] = cImage;
	}
	stateImages.clear();
}

void ofxTI_HubGui_ServiceForm::updateMTKeyboard(){
	if(this->form != NULL && this->mtKeyboard != NULL){
		ShishaElement* element = this->form->selectElement("activeInput");
		if(element != this->kbElement){
			// so if we have an active kbElement that is being deactivated before anything is typed, reset it back to default text
			if(this->kbElement != NULL){
				if(this->kbElement->getString() == ""){
					if(this->kbElement->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT)
						((ofxTI_HubGui_TextInput*)this->kbElement)->reset();
				}
			}
			if(element != NULL && this->hub != NULL && this->hub->getStatus() != STATUS_SENDING){
				if(this->hub->getStatus() == STATUS_SUCCESS){
					this->form->resetFormValues();
					element->checkCursorPress(element->getX(), element->getY(), 0, MOUSE_ID);
/**					if(element->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT){
						element->setString("");
						((ofxTI_HubGui_TextInput*)element)->setCursorIndex(0);
					}*/
				}
				this->hub->setStatus(STATUS_IDLE);
				this->setFormState(STATUS_IDLE);
			}
			this->mtKeyboard->setElement(element);
			this->kbElement = element;
		}
	}
}

ofImage ofxTI_HubGui_ServiceForm::getServiceIcon(){
	return this->serviceIcon;
}

void ofxTI_HubGui_ServiceForm::setHub(ofxTI_Hub* hub){
	this->hub = hub;
}

bool ofxTI_HubGui_ServiceForm::isFieldEnabled(string formField){
	if(this->enabledFormFields.count(formField) > 0)
		return this->enabledFormFields[formField];
	return false;
}

void ofxTI_HubGui_ServiceForm::enableFormField(string formField, bool enable){
	if(enable != this->isFieldEnabled(formField)){
		this->enabledFormFields[formField] = enable;
		this->buildFormInterface(this->getX(), this->getY());
	}
}

void ofxTI_HubGui_ServiceForm::setFormFieldDefault(string formField, string defaultValue){
	this->formFieldsDefaults[formField] = defaultValue;
}

void ofxTI_HubGui_ServiceForm::addSendImage(ofImage* sendImage, string name){
	if(name != "")
		this->sendImagesNamed[name] = sendImage;
	else
		this->sendImages.push_back(sendImage);
}

void ofxTI_HubGui_ServiceForm::setTextInputValidation(string fieldName, ValidationType validationType){
	string subSelector="";
	if(this->form != NULL){
		ShishaElement* element = this->form->getElement(fieldName, subSelector);
		if(element != NULL && element->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT){
			((ofxTI_HubGui_TextInput*)element)->setValidationType(validationType);
		}
	}
}

void ofxTI_HubGui_ServiceForm::setFormState(ofxTI_HubStatus formState){
	this->formState = formState;
}

void ofxTI_HubGui_ServiceForm::setImageBase(string imageBase){
	if(imageBase != "" && imageBase.at(imageBase.length()-1) != '/')
		imageBase += "/";
	this->imageBase = imageBase;
}

void ofxTI_HubGui_ServiceForm::setMultiTouchKeyboard(ShishaKeyboard* mtKeyboard){
	this->mtKeyboard = mtKeyboard;
	this->manageKeyboard = false;
}

void ofxTI_HubGui_ServiceForm::setServiceName(string serviceName){
	this->serviceName = serviceName;
}


ofxTI_HubGui_ServiceForm::FormInterface::FormInterface(){
}

ofxTI_HubGui_ServiceForm::FormInterface::~FormInterface(){
}


void ofxTI_HubGui_ServiceForm::FormInterface::initInterfaceElements(string interfacePath){
	stringstream elementName;
	elementName << interfacePath << "form/interface_bg_top.png";
	this->formBgTop.loadImage(elementName.str());
	elementName.str("");
	
	elementName << interfacePath << "form/interface_bg.png";
	this->formBg.loadImage(elementName.str());
	elementName.str("");
	
	elementName << interfacePath << "form/interface_bg_bottom.png";
	this->formBgBottom.loadImage(elementName.str());
	elementName.str("");
	
	float width = this->formBg.getWidth();
	float height = this->formBgTop.getHeight()+this->formBgBottom.getHeight();
	if(this->height > height)
		height = this->height;
	this->setDimensions(width, height);
}

void ofxTI_HubGui_ServiceForm::FormInterface::draw(float x, float y, float w, float h, bool borders){	
	float cY = 0.0;
	float bgHeight = h - (this->formBgTop.getHeight()+this->formBgBottom.getHeight());
	
	this->formBgTop.draw(x, y+cY, this->formBgTop.getWidth(), this->formBgTop.getHeight());
	cY += this->formBgTop.getHeight();
	
	this->formBg.draw(x, y+cY, this->formBg.getWidth(), bgHeight);
	cY += bgHeight;
	
	this->formBgBottom.draw(x, y+cY, this->formBgBottom.getWidth(), this->formBgBottom.getHeight());
	
	this->drawChildren();
	
/**	ofNoFill();
	ofSetColor(255, 255, 0);
	ofRect(x, y, w, h);
	ofSetColor(255, 255, 255);
	ofFill();
 */
}

ShishaElement* ofxTI_HubGui_ServiceForm::FormInterface::selectElement(string selector){
	if(selector == "activeInput"){
		for(map<int,ShishaElement*>::iterator it=this->children.begin() ; it != this->children.end(); it++){
			ShishaElement* cElement = (*it).second;
			if(cElement != NULL && cElement->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT && cElement->getBool())
				return cElement;
		}
		return NULL;
	}
	return ShishaContainer::selectElement(selector);
}

bool ofxTI_HubGui_ServiceForm::FormInterface::formValuesValid(){
	for(map<int,ShishaElement*>::iterator chIt = children.begin(); chIt != children.end(); chIt++){
		ShishaElement* cElement = chIt->second;
		if(cElement != NULL && cElement->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT){
			if(!((ofxTI_HubGui_TextInput*)cElement)->isValid())
				return false;
		}
	}
	return true;
}

map<string,string> ofxTI_HubGui_ServiceForm::FormInterface::getFormValues(){
	map<string,string> result;
	for(map<int,ShishaElement*>::iterator chIt = children.begin(); chIt != children.end(); chIt++){
		ShishaElement* cElement = chIt->second;
		if(cElement != NULL && cElement->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT){
			result[cElement->getName()] = cElement->getString();
		}
	}
	return result;
}

void ofxTI_HubGui_ServiceForm::FormInterface::resetFormValues(ShishaElement* doNotReset){
	for(map<int,ShishaElement*>::iterator chIt = children.begin(); chIt != children.end(); chIt++){
		ShishaElement* cElement = chIt->second;
		if(cElement != NULL && cElement->getElementType() == SHISHA_TYPE_HUB_GUI_TEXTINPUT && cElement != doNotReset)
			((ofxTI_HubGui_TextInput*)cElement)->reset();
	}	
}
