/*
 *  ofxTI_HubGui.cpp
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-05-25.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxTI_HubGui.h"

ofxTI_HubGui::ofxTI_HubGui(){
	this->hub = NULL;
	this->imageBase = "";
	this->mtKeyboard = NULL;
	this->manageKeyboard = false;
}

ofxTI_HubGui::~ofxTI_HubGui(){
	if(this->manageKeyboard && this->mtKeyboard != NULL){
		delete this->mtKeyboard;
		this->mtKeyboard = NULL;
	}
}

void ofxTI_HubGui::layoutChildren(){
	float width = 0.0;
	float height = 0.0;
	float cX = this->getX();
	float cY = this->getY();
	for(map<int,ShishaElement*>::iterator chIt = children.begin(); chIt != children.end(); chIt++){
		ShishaElement* child = chIt->second;
		child->setPosition(cX, cY);
		cY += child->getHeight();
		if(child->getWidth() > width)
			width = child->getWidth();
		if((cY-this->getY()) > height)
			height = cY-this->getY();
	}
	this->setDimensions(width, height);
}

void ofxTI_HubGui::addContainer(ShishaTabContainer* container, bool inheritAnimation, bool manageTab){
	ShishaElement* containerTab = container->getContainerTab();
	
	bool hasTabImage = false;
	ofxTI_HubGuiServiceTab* tabMap = NULL;
	if(containerTab != NULL){
		tabMap = new ofxTI_HubGuiServiceTab();
		tabMap->init();
		tabMap->setName(container->getName()+":TabMap");
		if(tabMap->loadImageMap(this->imageBase+"Service_Tab.png")){
			tabMap->mapElement(containerTab, 0, 0, tabMap->getWidth(), tabMap->getHeight());
			if(container->getElementType() == SHISHA_TYPE_HUB_GUI_SERVICEFORM)
				tabMap->setServiceIcon(((ofxTI_HubGui_ServiceForm*)container)->getServiceIcon());
			hasTabImage = true;
		}
		else{
			delete tabMap;
			tabMap = NULL;
		}
	}
	
	ShishaTabManager::addContainer(container, inheritAnimation, (!hasTabImage && manageTab));
	if(tabMap != NULL){
		tabMap->setElementID(container->getElementID());
		ShishaContainer::addElement(tabMap, true, true);
	}

	this->layoutChildren();
}

void ofxTI_HubGui::removeService(string serviceName){
	if(serviceName != ""){
		
		ShishaTabContainer* tabContainer = this->getServiceContainer(serviceName);
		if(tabContainer != NULL)
			this->removeContainer(tabContainer);
		
		ShishaElement* tabMap = this->getServiceTabMap(serviceName);
		if(tabMap != NULL)
			this->dropElement(tabMap, true);
		
/**		string targetName = "Hub:"+serviceName;
		for(int i=0; i < this->containers.size(); i++){
			if(this->containers[i]->getName() == targetName){
				this->removeContainer(this->containers[i]);
				break;
			}
		}
		targetName += ":TabMap";
		for(map<int,ShishaElement*>::iterator cIt = this->children.begin(); cIt != this->children.end(); cIt++){
			ShishaElement* cElement = cIt->second;
			if(cElement->getName() == targetName){
				this->dropElement(cElement, true);
				break;
			}
		}
 */
		
		this->layoutChildren();
	}
}

ShishaTabContainer* ofxTI_HubGui::getServiceContainer(string serviceName){
	if(serviceName != ""){
		string targetName = "Hub:"+serviceName;
		for(int i=0; i < this->containers.size(); i++){
			if(this->containers[i]->getName() == targetName){
				return this->containers[i];
			}
		}
	}
	return NULL;
}

ShishaElement* ofxTI_HubGui::getServiceTabMap(string serviceName){
	if(serviceName != ""){
		string targetName = "Hub:"+serviceName+":TabMap";
		for(map<int,ShishaElement*>::iterator cIt = this->children.begin(); cIt != this->children.end(); cIt++){
			ShishaElement* cElement = cIt->second;
			if(cElement->getName() == targetName){
				return cElement;
			}
		}
	}
	return NULL;
}

ofxTI_HubGui_ServiceForm* ofxTI_HubGui::getServiceForm(string serviceName){
	ofxTI_HubGui_ServiceForm* result = NULL;
	if(serviceName != ""){
		string targetName = "Hub:"+serviceName;
		for(int i=0; i < this->containers.size(); i++){
			if(this->containers[i]->getName() == targetName){
				result = (ofxTI_HubGui_ServiceForm*)this->containers[i];
				break;
			}
		}
	}
	return result;
}

void ofxTI_HubGui::initServiceFormSettings(ofxTI_HubGui_ServiceForm* serviceForm){
	if(serviceForm != NULL){
		serviceForm->setHub(this->hub);
		serviceForm->setImageBase(this->imageBase);
		serviceForm->setMultiTouchKeyboard(this->mtKeyboard);
		
		for(map<string,ofImage*>::iterator imgIt = sendImagesNamed.begin(); imgIt != sendImagesNamed.end(); imgIt++){
			serviceForm->addSendImage(imgIt->second, imgIt->first);
		}
		for(vector<ofImage*>::iterator imgIt = sendImages.begin(); imgIt != sendImages.end(); imgIt++){
			serviceForm->addSendImage(*imgIt);
		}
	}
}

void ofxTI_HubGui::setHub(ofxTI_Hub* hub){
	this->hub = hub;
}

void ofxTI_HubGui::addSendImage(ofImage* sendImage, string name){
	if(name != "")
		this->sendImagesNamed[name] = sendImage;
	else
		this->sendImages.push_back(sendImage);
}

void ofxTI_HubGui::setImageBase(string imageBase){
	if(imageBase != "" && imageBase.at(imageBase.length()-1) != '/')
		imageBase += "/";
	this->imageBase = imageBase;
}

void ofxTI_HubGui::setMultiTouchKeyboard(ShishaKeyboard* mtKeyboard, bool manageKeyboard){
	this->mtKeyboard = mtKeyboard;
	this->manageKeyboard = manageKeyboard;
}

ofxTI_HubGui::ofxTI_HubGuiServiceTab::ofxTI_HubGuiServiceTab(){
}

ofxTI_HubGui::ofxTI_HubGuiServiceTab::~ofxTI_HubGuiServiceTab(){
}

void ofxTI_HubGui::ofxTI_HubGuiServiceTab::draw(float x, float y, float w, float h, bool borders){
	ShishaImageMap::draw(x, y, w, h, borders);
	
	if(this->serviceIcon.getWidth() > 0 && this->serviceIcon.getHeight() > 0){
		float iconRatio = this->serviceIcon.getWidth() / this->serviceIcon.getHeight();
		float iconWidth = w * 0.6;
		float iconHeight = iconWidth / iconRatio;
		
		this->serviceIcon.draw(x+(w-iconWidth)/2.0, y+(h-iconHeight)/2.0, iconWidth, iconHeight);
	}
}

void ofxTI_HubGui::ofxTI_HubGuiServiceTab::setServiceIcon(ofImage serviceIcon){
	this->serviceIcon = serviceIcon;
}

