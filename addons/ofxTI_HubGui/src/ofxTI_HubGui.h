/*
 *  ofxTI_HubGui.h
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-05-25.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_TI_HUB_GUI
#define _OFX_TI_HUB_GUI

#include "ofxGUIShisha.h"
#include "ofxTI_Hub.h"

#include "ofxTI_HubGui_ServiceForm.h"

class ofxTI_HubGui : public ShishaTabManager{
protected:
	class ofxTI_HubGuiServiceTab : public ShishaImageMap{
		protected:
			ofImage serviceIcon;
		
		public:
			ofxTI_HubGuiServiceTab();
			~ofxTI_HubGuiServiceTab();
		
			virtual void draw(float x, float y, float w, float h, bool borders);
		
			void setServiceIcon(ofImage serviceIcon);
	};
	
	ofxTI_Hub* hub;
	string imageBase;
	ShishaKeyboard* mtKeyboard;
	bool manageKeyboard;
	
	map<string,ofImage*> sendImagesNamed;
	vector<ofImage*> sendImages;
	
	virtual void layoutChildren();
	
public:
	ofxTI_HubGui();
	~ofxTI_HubGui();
	
	virtual void addContainer(ShishaTabContainer* container, bool inheritAnimation=true, bool manageTab=true);
	virtual void removeService(string serviceName);
	ShishaTabContainer* getServiceContainer(string serviceName);
	ShishaElement* getServiceTabMap(string serviceName);
	ofxTI_HubGui_ServiceForm* getServiceForm(string serviceName);
	
	void initServiceFormSettings(ofxTI_HubGui_ServiceForm* serviceForm);
	
	void setHub(ofxTI_Hub* hub=NULL);
	
	void addSendImage(ofImage* sendImage, string name="");
	void setImageBase(string imageBase="");
	void setFormOffset(float x, float y);
	void setMultiTouchKeyboard(ShishaKeyboard* mtKeyboard=NULL, bool manageKeyboard=false);
};


#endif
