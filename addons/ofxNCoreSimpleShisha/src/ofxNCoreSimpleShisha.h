/*
 *  ofxNCoreSimpleShisha.h
 *  openFrameworks
 *
 *  Created by Pat Long on 19/02/10.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef OFX_NCORE_SIMPLE_SHISHA_H
#define OFX_NCORE_SIMPLE_SHISHA_H

#include "ofxNCore.h"
#include "ofxGUIShisha.h"

class ofxNCoreSimpleShisha : public ShishaImageMap{
	protected:
		ofxNCoreVision* nCore;
		ofTrueTypeFont resFont;
		bool bCalibrating;
		bool bSavingSettings;

		void buildInterface();
	
		void drawFingerOutlines();
		void startNCoreCalibration();
		
	public:
		ofxNCoreSimpleShisha();
		~ofxNCoreSimpleShisha();
	
		virtual void init(float x=0, float y=0, float width=DEFAULT_GUI_ELEMENT_WIDTH, float height=DEFAULT_GUI_ELEMENT_HEIGHT);
	
		virtual bool loadImageMap(string filename, int stateCount=4);
		virtual void draw();
		virtual void draw(float x, float y, float w, float h, bool borders);
		virtual void update();
	
		bool doCalibration();
		void setDoCalibration(bool bCalibrating);
	
		void setNCore(ofxNCoreVision* nCore);
	
		virtual bool checkKeyPressed(int key);
		virtual bool checkKeyReleased(int key);
	
};

#endif
