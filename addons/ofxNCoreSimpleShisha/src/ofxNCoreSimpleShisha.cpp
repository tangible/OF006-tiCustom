/*
 *  ofxNCoreSimpleShisha.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 19/02/10.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */

#include "ofxNCoreSimpleShisha.h"

ofxNCoreSimpleShisha::ofxNCoreSimpleShisha(){
	this->nCore = NULL;
	this->bCalibrating = false;
	this->bSavingSettings = false;
}

ofxNCoreSimpleShisha::~ofxNCoreSimpleShisha(){
}

void ofxNCoreSimpleShisha::init(float x, float y, float width, float height){
	ShishaImageMap::init(x, y, width, height);
	this->setName("NCoreSimpleShisha");
	this->shouldDrawInactiveBG = true;
}

bool ofxNCoreSimpleShisha::loadImageMap(string filename, int stateCount){
	bool result = ShishaImageMap::loadImageMap(filename, stateCount);
	if(result)
		this->buildInterface();
	return result;
}

void ofxNCoreSimpleShisha::buildInterface(){;
	resFont.loadFont("../resources/fonts/HirukoRegular.otf", 11);
	
	BasicButton* newButton;
	newButton = new BasicButton();
	newButton->init(117, 421, 33, 33);
	newButton->setName("CalibrateButton");
	this->mapElement(newButton, 117, 421, 33, 33);
	
	newButton = new BasicButton();
	newButton->init(117, 421, 33, 33);
	newButton->setName("SaveSettingsButton");
	this->mapElement(newButton, 270, 421, 33, 33);
	
}

void ofxNCoreSimpleShisha::draw(){
	ShishaImageMap::draw();
}

void ofxNCoreSimpleShisha::drawFingerOutlines(){
	if(this->nCore != NULL && this->nCore->contourFinder.nBlobs > 0){
		ofSetColor(0xCCFFCC);
		for (int i=0; i < this->nCore->contourFinder.nBlobs; i++)
		{
			float xpos = this->nCore->contourFinder.blobs[i].centroid.x * (MAIN_WINDOW_WIDTH/this->nCore->camWidth);
			float ypos = this->nCore->contourFinder.blobs[i].centroid.y * (MAIN_WINDOW_HEIGHT/this->nCore->camHeight);
			
			char idStr[1024];
			sprintf(idStr, "id: %i", this->nCore->contourFinder.blobs[i].id);
			this->nCore->verdana.drawString(idStr, 493-20 + xpos, 146+15 + ypos + this->nCore->contourFinder.blobs[i].boundingRect.height/2);
		}
		ofSetColor(255, 255, 255);
	}
}

void ofxNCoreSimpleShisha::draw(float x, float y, float w, float h, bool borders){
	ShishaImageMap::draw(x, y, w, h, borders);
	if(this->nCore != NULL && this->nCore->filter != NULL){
		this->nCore->filter->grayImg.draw(x+133, y+146, 320, 240);
		this->nCore->filter->grayDiff.draw(x+493, y+146, 320, 240);
		this->drawFingerOutlines();
		
		if(this->resFont.bLoadedOk){
			stringstream resBuilder;
			
			resBuilder << this->nCore->camWidth << "x" << this->nCore->camHeight;
			this->resFont.drawString(resBuilder.str(), 620+8, 430+11);
		}
	}
}

void ofxNCoreSimpleShisha::update(){
	ShishaImageMap::update();
	if(!this->bCalibrating && this->getBool("CalibrateButton")){
		this->bCalibrating = true;
		this->startNCoreCalibration();
	}
	else if(this->bCalibrating && !this->getBool("CalibrateButton")){
		this->bCalibrating = false;
	}
	
	if(!this->bSavingSettings && this->getBool("SaveSettingsButton")){
		this->bSavingSettings = true;
		if(this->nCore != NULL)
			this->nCore->saveSettings();
	}
	else if(this->bSavingSettings && !this->getBool("SaveSettingsButton")){
		this->bSavingSettings = false;
	}
}

bool ofxNCoreSimpleShisha::doCalibration(){
	return this->bCalibrating;
}

void ofxNCoreSimpleShisha::setDoCalibration(bool bCalibrating){
	this->bCalibrating = bCalibrating;
	if(!bCalibrating)
		this->setBoolValue("CalibrateButton", false);
}

void ofxNCoreSimpleShisha::startNCoreCalibration(){
	if(this->nCore != NULL){
		this->nCore->showConfiguration = true;
		this->nCore->bShowInterface = false;
		//Enter/Exit Calibration
		this->nCore->bCalibration = true;
		this->nCore->calib.calibrating = true;
		this->nCore->tracker.isCalibrating = true;
		this->nCore->bFullscreen = true;
		ofSetFullscreen(true);
	}
}

bool ofxNCoreSimpleShisha::checkKeyPressed(int key){
	if(key == 'c' || key == 'C'){
		this->setBoolValue("CalibrateButton", true);
		return true;
	}
	else if(key == 's' || key == 'S'){
		this->setBoolValue("SaveSettingsButton", true);
		return true;
	}
	return false;
}

bool ofxNCoreSimpleShisha::checkKeyReleased(int key){
	if(key == 'c' || key == 'C'){
		this->setBoolValue("CalibrateButton", false);
		return true;
	}
	else if(key == 's' || key == 'S'){
		this->setBoolValue("SaveSettingsButton", false);
		return true;
	}
	else if(key == 'b' || key == 'B'){
		if(this->nCore != NULL && this->nCore->filter != NULL)
			this->nCore->filter->bLearnBakground = true;
		return true;
	}
	return false;
}

void ofxNCoreSimpleShisha::setNCore(ofxNCoreVision* nCore){
	this->nCore = nCore;
}
