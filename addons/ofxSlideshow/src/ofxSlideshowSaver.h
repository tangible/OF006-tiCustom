/*
 *  ofxSlideshowSaver.h
 *  graffitiwall 7.5
 *
 *  Created by Eli Smiles on 11-08-17.
 *  Copyright 2011 tangibleInteraction. All rights reserved.
 *
 */
#ifndef _OFX_SLIDESHOW_SAVER
#define _OFX_SLIDESHOW_SAVER

#include "ofMain.h"
#include "ofxThread.h"
#include "ofxQtVideoSaver.h"

class ofxSlideshow;
class ofxSlideshowFrame;

class ofxSlideshowSaver : public ofxThread{
protected:
	ofxSlideshow* slideshow;
	string movieName;
	float savePercent;
	bool saveComplete;
	
	float* backgroundColour;
	ofImage* backgroundImage;
	ofImage* overlayImage;
	
	float frameLength;
	
	ofxQtVideoSaver movieSaver;
	
	unsigned char* getRenderedPixels(ofImage* forFrame, int w, int h);
	
public:
	ofxSlideshowSaver(ofxSlideshow* slideshow);
	~ofxSlideshowSaver();
	
	virtual void threadedFunction();
	
	float getSavePercent();
	
	bool isFinished();
	
	string saveSlideshow(string movieName);
	
	void setBackgroundColour(float* backgroundColour=NULL);
	void setBackgroundImage(ofImage* backgroundImage=NULL);
	void setFrameLength(float frameLength=(1.0/30.0));
	void setOverlayImage(ofImage* overlayImage=NULL);
	

	
};

#include "ofxSlideshow.h"
#include "ofxSlideshowFrame.h"

#endif
