/*
 *  ofxSlideshow.cpp
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-08-03.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxSlideshow.h"

ofxSlideshow::ofxSlideshow(){
	activeFrameIndex = -1;
	lastSwitch = -1;
	state = SLIDESHOW_STOPPED;
	
	setBaseSavePath();
	setBufferSize();
	setDirection();
	setFrameDelay();
	setImageType();
	setLoopingEnabled();
	setMaxFrameDimensions();
	setMaxLoadPerFrame();
	
	movieSaver = NULL;
}

ofxSlideshow::~ofxSlideshow(){
	clearFrames();
	if(movieSaver != NULL){
		delete movieSaver;
		movieSaver = NULL;
	}
}

void ofxSlideshow::outputBuffer(){
	for(int i=0; i < frames.size(); i++){
		if(i == activeFrameIndex)
			cout << (frames[i]->isLoaded()?"*":"?");
		else if(shouldBuffer(i))
			cout << (frames[i]->isLoaded()?"+":"-");
		else
			cout << (frames[i]->isLoaded()?"/":"|");
		
		cout << "[" << i << "]=" << frames[i]->getImageName() << endl;
	}
	cout << endl;
}


void ofxSlideshow::update(){
	if(state == SLIDESHOW_PLAYING){
		int cTime = ofGetElapsedTimeMillis();
		if(lastSwitch == -1 || (cTime - lastSwitch) >= frameDelay){
			if(direction == SLIDESHOW_FORWARD)
				nextFrame();
			else if(direction == SLIDESHOW_REVERSE)
				prevFrame();
		}
	}
	
/**	if(ofGetFrameNum() % 60 == 0){
		cout << "UPDATING [frame #" << ofGetFrameNum() << "]" << endl;
		outputBuffer();
	}*/


	loadedThisFrame = 0;
	
	// TODO: build different buffering modes: forwards, backwards, centered (current mode is "centered")
	
	// we update the frames starting with the active and moving away from it
	// so frames closest to the active frame get updated first
	for(int i=0; i <= ceil(frames.size()/2); i++){
		int offset = (direction == SLIDESHOW_FORWARD)?i:-i;
		updateFrame(this->activeFrameIndex+offset);
		if(i > 0 && (frames.size() % 2 != 0 || i != ceil(frames.size()/2)))
			updateFrame(this->activeFrameIndex-offset);
	}
	
	if(movieSaver != NULL){
		if(movieSaver->isFinished()){
			delete movieSaver;
			movieSaver = NULL;
		}
	}
}

void ofxSlideshow::updateFrame(int frameIndex){	
	if(loopingEnabled){
		if(frameIndex < 0)
			frameIndex = frames.size() + frameIndex;
		else if(frameIndex >= frames.size())
			frameIndex = frameIndex - frames.size();
	}
	if(frameIndex >= 0 && frameIndex < frames.size()){
		//if(ofGetFrameNum() % 60 == 0) cout << "update:" << frameIndex << ":";
		
		if(frames[frameIndex]->getIndex() != frameIndex)
			frames[frameIndex]->setIndex(frameIndex);
		
		if(shouldBuffer(frameIndex)){
			if(!frames[frameIndex]->isLoaded() && frames[frameIndex]->canLoad() && loadedThisFrame < maxLoadPerFrame){
				frames[frameIndex]->load();
				loadedThisFrame++;
			//	if(ofGetFrameNum() % 60 == 0) cout << "loaded";
			}
		}
		else if(frames[frameIndex]->isLoaded() && loadedThisFrame < maxLoadPerFrame){
			frames[frameIndex]->unload();
			loadedThisFrame++;
		//	if(ofGetFrameNum() % 60 == 0) cout << "unloaded";
		}
		
		if(loadedThisFrame < maxLoadPerFrame){
			if(frames[frameIndex]->hasChanged()){
				frames[frameIndex]->saveImage();
				loadedThisFrame++;
			}
			else{
				string currentName = frames[frameIndex]->getImageName();
				string targetName = getNewImageName(frames[frameIndex], false);
				if(currentName != targetName){
					if(frames[frameIndex]->renameImage(targetName))
						loadedThisFrame++;
				}
			}
		}
//		if(ofGetFrameNum() % 60 == 0) cout << endl;
	}
}

void ofxSlideshow::clearFrames(){
	for(int i=0; i < frames.size(); i++){
		delete frames[i];
		frames[i] = NULL;
	}
	frames.clear();
}

int ofxSlideshow::getActiveFrameIndex(){
	return this->activeFrameIndex;
}

string ofxSlideshow::getBaseSavePath(){
	return this->baseSavePath;
}

int ofxSlideshow::getBufferSize(){
	return bufferSize;
}

ofxSlideshowFrame* ofxSlideshow::getFrame(int index){
	if(index == -1) index = activeFrameIndex;
	if(index >= 0 && index < frames.size())
		return frames[index];
	return NULL;
}

int ofxSlideshow::getBufferStartIndex(){
	return (activeFrameIndex - bufferSize/2) + (direction == SLIDESHOW_FORWARD && (bufferSize % 2 == 0)?1:0);
}

int ofxSlideshow::getBufferEndIndex(){
	return (activeFrameIndex + bufferSize/2) - (direction == SLIDESHOW_REVERSE && (bufferSize % 2 == 0)?1:0);;
}

int ofxSlideshow::getFrameCount(){
	return frames.size();
}

bool ofxSlideshow::getLoopingEnabled(){
	return loopingEnabled;
}

string ofxSlideshow::getNewImageName(ofxSlideshowFrame* forFrame, bool forceUnique){
//	int forIndex = (forFrame != NULL)?forFrame->getIndex():frames.size();
	
	stringstream nameBuilder;
	nameBuilder << baseSavePath << ((baseSavePath != "" && baseSavePath[baseSavePath.length()-1] != '/')?"/":"");
	nameBuilder << ((forFrame != NULL)?forFrame->getIndexAsString():"00000");
	
	string baseName = nameBuilder.str();
	nameBuilder << "." << imageType;
	
	if(forceUnique){
		char indexNum[4];
		int tries = 0;
		while(tiFileExists(nameBuilder.str()) && tries < 1000){
			sprintf(indexNum, "%03d", tries++);
			nameBuilder.str("");
			nameBuilder << baseName << "." << indexNum << "." << imageType;
		}
	}
	
	return nameBuilder.str();
}

float ofxSlideshow::getScaledFrameDimensions(float& width, float& height){
	float result = 1.0;
	if(width > 0 && height > 0){
		if(maxWidth != -1.0 && width > maxWidth){
			result = maxWidth / width;
			width = width * result;
			height = height * result;
		}
		
		if(maxHeight != -1.0 && height > maxHeight){
			result = maxHeight / height;
			width = width * result;
			height = height * result;
		}
	}
	return result;
}

bool ofxSlideshow::isPlaying(){
	return (state == SLIDESHOW_PLAYING);
}

bool ofxSlideshow::isPaused(){
	return (state == SLIDESHOW_PAUSED);
}

bool ofxSlideshow::isStopped(){
	return (state == SLIDESHOW_STOPPED);
}

bool ofxSlideshow::shouldBuffer(int frameIndex){
	int bufferStart = getBufferStartIndex();
	int bufferEnd = getBufferEndIndex();
	bool result = (frameIndex >= bufferStart && frameIndex <= bufferEnd);
	if(loopingEnabled){
		if(bufferStart < 0 && frameIndex >= (frames.size()+bufferStart) && frameIndex < frames.size())
			result = true;
		else if(bufferEnd >= frames.size() && frameIndex >= 0 && frameIndex <= (bufferEnd-frames.size()))
			result = true;
	}
	
	return result;
}

int ofxSlideshow::addFrame(string imageName, int index){
//	cout << "add frame..." << this->frames.size() << endl;
	
	if(index == -1)
		index = activeFrameIndex;
	
	ofxSlideshowFrame* newFrame = new ofxSlideshowFrame(imageName);
	newFrame->setSlideshow(this);
	
	if(index >= frames.size() || index < 0){
		this->frames.push_back(newFrame);
		index = this->frames.size()-1;
	}
	else
		this->frames.insert(this->frames.begin()+index, newFrame);
	
	if(activeFrameIndex < 0)
		activeFrameIndex = index;
	
	return this->frames.size();
}

int ofxSlideshow::loadSlideshow(string directory){
	int result = 0;
	if(directory != ""){
		ofxDirList dir;
		if(this->imageType != "")
			dir.allowExt(imageType);
		
		int numFound = dir.listDir(directory);
		for(int i=0; i < numFound; i++)			
			this->addFrame(dir.getPath(i));
		result = numFound;
		setActiveFrame(0);
		
		setBaseSavePath(directory);
	}
	return result;
}

void ofxSlideshow::resetSlideshow(){
	clearFrames();
	activeFrameIndex = -1;
	lastSwitch = -1;
	state = SLIDESHOW_STOPPED;
}

bool ofxSlideshow::saveSlideshow(string movieName, float* backgroundColour, ofImage* backgroundImage, ofImage* overlayImage){
	bool result = false;
	if(movieName != ""){
		movieSaver = new ofxSlideshowSaver(this);
		movieSaver->setFrameLength( (float)frameDelay / 1000.0f  );
		movieSaver->setBackgroundColour(backgroundColour);
		movieSaver->setBackgroundImage(backgroundImage);
		movieSaver->setOverlayImage(overlayImage);
		movieSaver->saveSlideshow(movieName);
	}
	return result;
}

bool ofxSlideshow::isSaving(){
	return (movieSaver != NULL);
}

float ofxSlideshow::getSavePercent(){
	if(movieSaver != NULL)
		return movieSaver->getSavePercent();
	return 0.0;
}

void ofxSlideshow::play(){
	state = SLIDESHOW_PLAYING;
}

void ofxSlideshow::pause(){
	state = SLIDESHOW_PAUSED;
}

void ofxSlideshow::stop(){
	state = SLIDESHOW_STOPPED;
}

int ofxSlideshow::nextFrame(){
	if(activeFrameIndex < frames.size()-1)
		setActiveFrame(activeFrameIndex + 1);
	else if(loopingEnabled)
		setActiveFrame(0);
}

int ofxSlideshow::prevFrame(){
	if(activeFrameIndex > 0)
		setActiveFrame(activeFrameIndex - 1);
	else if(loopingEnabled)
		setActiveFrame(frames.size()-1);
}

int ofxSlideshow::setActiveFrame(int frameIndex, bool forceLoad){
	if(frameIndex >= 0 && frameIndex < frames.size()){
		activeFrameIndex = frameIndex;
		if(forceLoad){
			ofxSlideshowFrame* cFrame = frames[activeFrameIndex];
			if(cFrame != NULL){
				cFrame->load();
				loadedThisFrame++;
			}
		}
		lastSwitch = ofGetElapsedTimeMillis();
	}
}

void ofxSlideshow::setBaseSavePath(string baseSavePath){
	this->baseSavePath = baseSavePath;
}

void ofxSlideshow::setBufferSize(int bufferSize){
	this->bufferSize = bufferSize;
}

void ofxSlideshow::setDirection(SlideshowDirection direction){
	this->direction = direction;
}

void ofxSlideshow::setFrameDelay(int frameDelay){
	this->frameDelay = frameDelay;
}

void ofxSlideshow::setImageType(string imageType){
	this->imageType = imageType;
}

void ofxSlideshow::setLoopingEnabled(bool loopingEnabled){
	this->loopingEnabled = loopingEnabled;
}

void ofxSlideshow::setMaxFrameDimensions(float maxWidth, float maxHeight){
	this->maxWidth = maxWidth;
	this->maxHeight = maxHeight;
}

void ofxSlideshow::setMaxLoadPerFrame(int maxLoadPerFrame){
	this->maxLoadPerFrame = maxLoadPerFrame;
}
