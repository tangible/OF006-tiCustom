/*
 *  ofxSlideshowFrame.cpp
 *  openFrameworks
 *
 *  Created by Eli Smiles on 11-08-05.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxSlideshowFrame.h"

ofxSlideshowFrame::ofxSlideshowFrame(string imageName){
	this->image = NULL;
	changed = false;
	this->setSlideshow();
	this->setImageName(imageName);
	this->setIndex();
}

ofxSlideshowFrame::~ofxSlideshowFrame(){
	if(this->image != NULL){
		delete this->image;
		this->image = NULL;
	}
}

void ofxSlideshowFrame::draw(){
	if(this->image != NULL){
		this->image->draw(0, 0);
	}
}

ofImage* ofxSlideshowFrame::getImage(){
	return this->image;
}

string ofxSlideshowFrame::getImageName(){
	return this->imageName;
}

int ofxSlideshowFrame::getIndex(){
	return this->index;
}

string ofxSlideshowFrame::getIndexAsString(){
	char indexNum[6];
	sprintf(indexNum, "%05d", index);
	return string(indexNum);
}

bool ofxSlideshowFrame::hasChanged(){
	return this->changed;
}

bool ofxSlideshowFrame::canLoad(){
	return (this->imageName != "");
}

bool ofxSlideshowFrame::isLoaded(){
	return (this->image != NULL);
}

bool ofxSlideshowFrame::load(){
	bool result = false;
	if(imageName != ""){
		ofImage* image = new ofImage();
		if(image->loadImage(imageName)){
			if(this->image != NULL){
				delete this->image;
				this->image = NULL;
			}
			this->image = image;
			result = true;
		}
		else{
			delete image;
			image = NULL;
		}
	}
	return result;
}

void ofxSlideshowFrame::unload(){
	if(this->image != NULL){
		if(this->image->getFileName() == "" || changed)
			this->imageName = this->saveImage();
		
		delete this->image;
		this->image = NULL;
	}
}

bool ofxSlideshowFrame::renameImage(string newName){
	bool result = false;
	if(this->image != NULL){
		if(!tiFileExists(newName)){
			string oldName = this->imageName;
			if(saveImage(newName)){
				if(tiFileExists(oldName))
					tiRemoveFile(oldName);
				result = true;
			}
		}
	}
	return result;
}

bool ofxSlideshowFrame::saveImage(string saveAs){
	bool result = false;
	if(saveAs != "" && this->image != NULL){
		bool resized = false;
/**		float origWidth = this->image->getWidth();
		float origHeight = this->image->getHeight();
		if(slideshow != NULL){
			float width = origWidth;
			float height = origHeight;
			this->savedScale = slideshow->getScaledFrameDimensions(width, height);
			if(width != origWidth || height != origHeight){
				this->image->resize(width, height);
				resized = true;
			}
		}*/
		
		imageSaver.setFromPixels(this->image->getPixels(), this->image->getWidth(), this->image->getHeight(), this->image->type);
		imageSaver.saveThreaded(saveAs);
		
//		if(this->image->saveImage(saveAs)){
			this->imageName = saveAs;
			result = true;
//		}
		
/**		if(resized){
			this->image->resize(origWidth, origHeight);
		}*/
	}
	return result;
}

string ofxSlideshowFrame::saveImage(){
	string saveAs = this->imageName;
	if(saveAs == ""){
		if(this->slideshow != NULL)
			saveAs = this->slideshow->getNewImageName(this);
		else{
			stringstream nameBuild;
			nameBuild << "frame_" << ((index >= 0)?ofToString(index, 0):"") << ".png";
			saveAs = nameBuild.str();
			nameBuild.str("");
		}
	}
	saveImage(saveAs);
	changed = false;
	return this->imageName;
}

void ofxSlideshowFrame::setFromPixels(unsigned char* pixels, int w, int h, int imageType){
	if(image == NULL) image = new ofImage();
	image->setFromPixels(pixels, w, h, imageType);
	changed = true;
}

bool ofxSlideshowFrame::setImageName(string imageName){
	this->imageName = imageName;
}

void ofxSlideshowFrame::setIndex(int index){
	this->index = index;
}

void ofxSlideshowFrame::setSlideshow(ofxSlideshow* slideshow){
	this->slideshow = slideshow;
}
