#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	// set the KeyLok values from the values defined in the included CLIENT.h
	keyLok.setValidateCodes(ValidateCode1, ValidateCode2, ValidateCode3);
	keyLok.setClientIDCodes(ClientIDCode1, ClientIDCode2);
	keyLok.setReadCodes(ReadCode1, ReadCode2, ReadCode3);
	keyLok.setWriteCodes(WriteCode1, WriteCode2, WriteCode3);
	
	cout << "Checking for Key-Lok..." << endl;
	if(keyLok.isConnected())
		cout << "Key-Lok is connected!" << endl;
	else{
		cout << "Key-Lok is not connected!" << endl;
		cout << "Program will now terminate..." << endl;
		cout << "Good bye." << endl;
		abort();
	}
}

//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){

}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

