/*
 *  KeyLokThreadedTasks.cpp
 *  emptyExample
 *
 *  Created by Eli Smiles on 01/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "KeyLokThreadedTasks.h"

KeyLokThreadedTask::KeyLokThreadedTask(KeyLokTaskCodes taskCode){
	this->taskCode = taskCode;
	this->setArguments();
	this->setResult(0);
}

KeyLokThreadedTask::~KeyLokThreadedTask(){
}

KeyLokTaskCodes KeyLokThreadedTask::getTaskCode(){
	return this->taskCode;
}

int KeyLokThreadedTask::getArg1(){
	return (int)this->arg1;
}

int KeyLokThreadedTask::getArg2(){
	return (int)this->arg2;
}

int KeyLokThreadedTask::getArg3(){
	return (int)this->arg3;
}

int KeyLokThreadedTask::getResult(){
	return this->result;
}

string KeyLokThreadedTask::getMessage(){
	return this->message;
}

void KeyLokThreadedTask::setArguments(unsigned arg1, unsigned arg2, unsigned arg3){
	this->arg1 = arg1;
	this->arg2 = arg2;
	this->arg3 = arg3;
}

void KeyLokThreadedTask::setMessage(string message){
	this->message = message;
}

void KeyLokThreadedTask::setResult(int result, string message){
	this->result = result;
	this->setMessage(message);
}

void KeyLokThreadedTask::setResult(bool result, string message){
	this->setResult(result?1:0, message);
}


KeyLokThreadedTasks::KeyLokThreadedTasks(KeyLok* keyLok){
	this->keyLok = keyLok;
}

KeyLokThreadedTasks::~KeyLokThreadedTasks(){
}

void KeyLokThreadedTasks::setKeyLok(KeyLok* keyLok){
	this->keyLok = keyLok;
}

void KeyLokThreadedTasks::queueTask(KeyLokTaskCodes taskCode, unsigned arg1, unsigned arg2, unsigned arg3){
	KeyLokThreadedTask* newTask = new KeyLokThreadedTask(taskCode);
	newTask->setArguments(arg1, arg2, arg3);
	this->taskQueue.push_back(newTask);
}

bool KeyLokThreadedTasks::hasTasks(){
	return (this->taskQueue.size() > 0);
}

void KeyLokThreadedTasks::doNextTask(){
	KeyLokThreadedTask* task = this->taskQueue.front();
	this->taskQueue.pop_front();
	if(task != NULL){
		if(this->keyLok != NULL){
			bool triggerEvent = false;
			bool bResult;
			int nResult;
			string validationModeStr;
			string validationResultStr;
			ofxDateTime dateTime;
			stringstream messageBuilder;
			
			cout << "do task:" << keyLok->getTaskCodeString((KeyLokTaskCodes)task->getTaskCode()) << endl;
			
			KeyLokThreadedTask taskResult(task->getTaskCode());
			taskResult.setArguments(task->getArg1(), task->getArg2(), task->getArg3());
			
			switch(task->getTaskCode()){
				case KL_CONNECT:
					bResult = this->keyLok->isConnected(true);
					if(bResult)
						messageBuilder << "Key-Lok is connected";
					else
						messageBuilder << "Key-Lok is not connected";
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_DISCONNECT:
					bResult = this->keyLok->disconnect();
					if(bResult)
						messageBuilder << "Key-Lok successfully disconnected";
					else
						messageBuilder << "Key-Lok is not connected";
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_READ_AUTH:
					bResult = this->keyLok->hasReadAuthorization();
					if(bResult)
						messageBuilder << "Key-Lok has read authorization";
					else
						messageBuilder << "Key-Lok does not have read authorization";
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_GET_SN:
					nResult = this->keyLok->getSerialNumber();
					messageBuilder << "Key-Lok Serial Number: " << nResult;
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_GET_VAR:
					nResult = this->keyLok->getVarWord(task->getArg1());
					messageBuilder << "Key-Lok Read Variable #" << task->getArg1() << " = " << nResult;
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_WRITE_AUTH:
					bResult = this->keyLok->hasWriteAuthorization();
					if(bResult)
						messageBuilder << "Key-Lok has write authorization";
					else
						messageBuilder << "Key-Lok does not have write authorization";
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_WRITE_VAR:
					bResult = this->keyLok->setVarWord(task->getArg1(), task->getArg2());
					if(bResult)
						messageBuilder << "Key-Lok set Variable #" << task->getArg1() << " = " << task->getArg2();
					else
						messageBuilder << "Key-Lok could not set Variable #" << task->getArg1();
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_DEC_VAR:
					nResult = this->keyLok->decrementMemory(task->getArg1());
					if(nResult >= 0)
						messageBuilder << "Key-Lok variable #" << task->getArg1() << " has " << nResult << " run" << ((nResult != 1)?"s":"") << " remaining";
					else
						messageBuilder << "Key-Lok variable #" << task->getArg1() << " has expired";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_GET_EXP_DATE:
					nResult = this->keyLok->getExpirationDate();
					if(nResult > 0){
						dateTime.setFromEpoch(nResult);
						messageBuilder << "Key-Lok expiry date is set to: " << dateTime.getDateString();
					}
					else
						messageBuilder << "Key-Lok expiry date is disabled";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_CHECK_EXP_DATE:
					nResult = this->keyLok->checkExpirationDate();
					if(nResult >= 0)
						messageBuilder << "Key-Lok expires in approximately " << nResult << " day" << ((nResult != 1)?"s":"");
					else if(nResult == -2)
						messageBuilder << "Key-Lok expiry date is disabled";
					else
						messageBuilder << "Key-Lok expiry date has been reached";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_SET_EXP_DATE:
					bResult = this->keyLok->setExpirationDate(task->getArg1(), task->getArg2(), task->getArg3());
					if(bResult){
						if(task->getArg1() <= 0 && task->getArg2() <= 0 && task->getArg3() <= 0)
							messageBuilder << "Key-Lok expiry date is now disabled";
						else{
							dateTime.setDate(task->getArg2(), task->getArg1(), task->getArg3());
							messageBuilder << "Key-Lok expiry date updated to: " << dateTime.getDateString();
						}
							
					}
					else
						messageBuilder << "Key-Lok expiry date could not be updated";
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					
					break;
					
				case KL_SET_RUN_COUNT:
					bResult = this->keyLok->setRunCount(task->getArg1());
					if(task->getArg1() != -1){
						if(bResult)
							messageBuilder << "Key-Lok updated run count to " << task->getArg1();
						else
							messageBuilder << "Key-Lok could not updated run count to " << task->getArg1();
					}
					else{
						if(bResult)
							messageBuilder << "Key-Lok disabled run count";
						else
							messageBuilder << "Key-Lok could not disable run count";
					}
					taskResult.setResult(bResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_GET_RUN_COUNT:
					nResult = this->keyLok->getRunCount();
					if(nResult != -1)
						messageBuilder << "Key-Lok run count is set to " << nResult;
					else
						messageBuilder << "Key-Lok run count is disabled";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_DEC_RUN_COUNT:
					nResult = this->keyLok->decrementRunCount();
					if(nResult >= 0)
						messageBuilder << "Key-Lok run count has " << nResult << " run" << ((nResult != 1)?"s":"") << " remaining";
					else if(nResult == -2)
						messageBuilder << "Key-Lok run count is disabled";
					else
						messageBuilder << "Key-Lok run count has expired";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_CHECK_RUN_COUNT:
					nResult = this->keyLok->checkRunCount((task->getArg1() != 0));
					if(nResult >= 0)
						messageBuilder << "Key-Lok run count has " << nResult << " run" << ((nResult != 1)?"s":"") << " remaining";
					else if(nResult == -2)
						messageBuilder << "Key-Lok run count is disabled";
					else
						messageBuilder << "Key-Lok run count has expired";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
					
				case KL_IS_VALID:
					nResult = this->keyLok->isValid((KeyLokValidationMode)task->getArg1(), (task->getArg2() != 0));
					validationModeStr = keyLok->getValidationModeString((KeyLokValidationMode)task->getArg1());
					validationResultStr = keyLok->getValidationResultString((KeyLokValidationResults)nResult);
					if(nResult != VALID)
						messageBuilder << "Key-Lok " << validationModeStr << " failed because " << validationResultStr;
					else
						messageBuilder << "Key-Lok " << validationModeStr << " passed";
					taskResult.setResult(nResult, messageBuilder.str());
					messageBuilder.str("");
					triggerEvent = true;
					break;
			}
			
			if(triggerEvent){
				ofNotifyEvent(keyLokTaskComplete, taskResult, this);
			}
		}
		
		delete task;
		task = NULL;
	}
}

void KeyLokThreadedTasks::start(){
	this->startThread(false, false);
}

void KeyLokThreadedTasks::stop(){
	if(this->isThreadRunning())
		this->stopThread();
}

void KeyLokThreadedTasks::threadedFunction(){
	while(this->isThreadRunning()){
		stringstream logBuilder;
		
		while(this->hasTasks()){
			this->doNextTask();
		}
	}
}
