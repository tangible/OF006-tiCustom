/*
 *  ofxTI_KeyLok.h
 *
 *	An adddon for interfacing with the KeyLok dongles.
 *
 *  Created by Pat Long (plong0) on 08/04/09.
 *  Copyright 2009 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _OFX_TI_KEYLOK
#define _OFX_TI_KEYLOK

#include "KeyLok.h"

#ifndef KEYLOK_DISABLE_THREADS
#include "KeyLokThreadedTasks.h"
#include "KeyLokThreadedValidator.h"
#endif

#endif
