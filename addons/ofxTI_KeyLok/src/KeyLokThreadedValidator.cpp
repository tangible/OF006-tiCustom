/*
 *  KeyLokThreadedValidator.cpp
 *  emptyExample
 *
 *  Created by Eli Smiles on 25/09/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "KeyLokThreadedValidator.h"

KeyLokThreadedValidator::KeyLokThreadedValidator(KeyLok* keyLok){
	this->keyLok = keyLok;
	this->valid = false;
	this->safeToRemove = true;
	this->lastCheck = -1;
	this->setSafeToRemoveSpan();
	this->setValidationMode();
}

KeyLokThreadedValidator::~KeyLokThreadedValidator(){
}

void KeyLokThreadedValidator::start(bool timedCheck){
	this->timedCheck = timedCheck;
	this->startThread(false, false);
}

void KeyLokThreadedValidator::stop(){
	if(this->isThreadRunning())
		this->stopThread();
}

bool KeyLokThreadedValidator::doCheckNow(){
	if(this->keyLok != NULL){
		cout << "checking..." << endl;
		this->safeToRemove = false;
		KeyLokValidationResults results = this->keyLok->isValid(this->validationMode, false);
		if(results == VALID)
			this->valid = true;
		else
			this->valid = false;
		cout << "done checking..." << endl;
		this->safeToRemove = true;
	}
	return this->valid;
}

void KeyLokThreadedValidator::threadedFunction(){
	while(this->isThreadRunning()){
		int cTime = ofGetElapsedTimeMillis();
		if(this->lastCheck == -1 || (cTime - this->lastCheck) >= this->checkDelay){
			stringstream logBuilder;
			
			logBuilder << "ofxTI_KeyLok: ThreadedValidator: checking...";
			ofLog(OF_LOG_NOTICE, logBuilder.str());
			logBuilder.str("");
			
			
			if(this->keyLok != NULL){
				this->doCheckNow();
			}
			
			logBuilder << "ofxTI_KeyLok: ThreadedValidator: results: " << (this->isValid()?"valid":"not valid") << "! (" << (ofGetElapsedTimeMillis()-cTime) << "ms)";
			ofLog(OF_LOG_NOTICE, logBuilder.str());
			logBuilder.str("");

			this->lastCheck = cTime;
			
			if(!this->timedCheck)
				this->stop();
		}
	}
}

int KeyLokThreadedValidator::getTimeUntilCheck(int cTime){
	if(this->lastCheck == -1) return 0;
	
	if(cTime < 0)
		cTime = ofGetElapsedTimeMillis();
	
	return ((cTime - this->lastCheck) - this->checkDelay) * -1;
}

bool KeyLokThreadedValidator::isSafeToRemove(){
	return this->safeToRemove;
}

bool KeyLokThreadedValidator::isValid(){
	return this->valid;
}

void KeyLokThreadedValidator::setCheckDelay(int checkDelay){
	this->checkDelay = checkDelay;
	this->lastCheck = -1;
}

void KeyLokThreadedValidator::setSafeToRemoveSpan(int safeToRemoveSpan){
	this->setCheckDelay(KEYLOK_AVERAGE_CHECK_TIME + safeToRemoveSpan);
}

void KeyLokThreadedValidator::setValidationMode(KeyLokValidationMode validationMode){
	this->validationMode = validationMode;
}
