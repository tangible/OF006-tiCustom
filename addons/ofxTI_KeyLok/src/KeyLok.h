/*
 *  KeyLok.h
 *
 *  Created by Pat Long (plong0) on 08/04/09.
 *  Copyright 2009 Tangible Interaction Inc. All rights reserved.
 *
 */
#ifndef _TI_KEYLOK
#define _TI_KEYLOK

#include "ofMain.h"
#include "ofxDateTime.h"

#include "KEYERROR.H"    // KEY-LOK error codes
#include "KEYCOMMANDS.H" // KEY-LOK command codes

#define LEASEOK            0xFFFF /* Lease active - not expired */
#define LEASEEXPIRED       0xFFFE /* Clock date Beyond Lease Expiration date */
#define SYSDATESETBACK     0xFFFD /* User has set back his system clock */
#define NOLEASEDATE        0xFFFC /* Lease expiration date address contains 0 */
#define LEASEDATEBAD       0xFFFB /* Invalid lease expiration date */
#define LASTSYSDATECORRUPT 0xFFFA /* Last System Date - contents corrupt */

#define KEYLOK_BASEYEAR					1990
#define KEYLOK_DEFAULT_RUNCOUNT_ADDRESS	0

enum KeyLokTaskCodes { 
	KL_IDLE,
	KL_CONNECT, 
	KL_DISCONNECT,
	KL_READ_AUTH, 
	KL_GET_SN, 
	KL_GET_VAR, 
	KL_WRITE_AUTH, 
	KL_WRITE_VAR, 
	KL_DEC_VAR, 
	KL_GET_EXP_DATE, 
	KL_CHECK_EXP_DATE, 
	KL_SET_EXP_DATE, 
	KL_SET_RUN_COUNT, 
	KL_GET_RUN_COUNT,
	KL_DEC_RUN_COUNT,
	KL_CHECK_RUN_COUNT,
	KL_IS_VALID
};
enum KeyLokValidationMode { VALIDATE_FULL, VALIDATE_CONNECTED, VALIDATE_DATE, VALIDATE_RUN_COUNT };
enum KeyLokValidationResults { VALID, NOT_CONNECTED, INVALID_DATE, INVALID_RUN_COUNT, INVALID_DATE_AND_RUN_COUNT };

extern "C" {
	extern unsigned long KFUNC(unsigned, unsigned, unsigned, unsigned);
}

// wrapper for the KeyLok functions
class KeyLok{
	private:
		unsigned validateCode1, validateCode2, validateCode3, clientIDCode1, clientIDCode2, readCode1, readCode2, readCode3, writeCode1, writeCode2, writeCode3;
		unsigned ReturnValue1, ReturnValue2;
		bool connected, readAuth, writeAuth;
	
		KeyLokTaskCodes activeTask;
	
		unsigned runCountAddress;
	
		void KTASK(unsigned CommandCode, unsigned Argument2, unsigned Argument3, unsigned Argument4);
		unsigned RotateLeft(unsigned Target, int Counts);
	
		unsigned getDummyArgument();
	
	public:
		KeyLok();
		~KeyLok();
	
		bool isConnected(bool getNewConnection=false);
		bool disconnect();

		KeyLokValidationResults isValid(KeyLokValidationMode validationMode=VALIDATE_FULL, bool updateRunCount=true);
	
		int checkExpirationDate();
		int checkRunCount(bool updateRunCount=true);
	
		bool hasReadAuthorization(bool getNewAuth=true);
		bool hasWriteAuthorization(bool getNewAuth=true);
	
		int getExpirationDate();
		int getRunCount();
		int getSerialNumber();
		int getVarWord(int address);
	
		bool isIdle();
	
		KeyLokTaskCodes getActiveTask();
		string getActiveTaskString();
		string getTaskCodeString(KeyLokTaskCodes taskCode);
		string getValidationModeString(KeyLokValidationMode validationMode);
		string getValidationResultString(KeyLokValidationResults resultCode);
	
		int decrementMemory(int address);
		int decrementRunCount();
		bool setExpirationDate(int month=-1, int day=-1, int year=-1);
		bool setRunCount(int runCount=-1);
		bool setVarWord(int address, int value);
	
		void setActiveTask(KeyLokTaskCodes activeTask=KL_IDLE);
		void setRunCountAddress(int runCountAddress=KEYLOK_DEFAULT_RUNCOUNT_ADDRESS);
		void setValidateCode(int validateCode, int codeNum=1);
		void setValidateCodes(int validateCode1, int validateCode2, int validateCode3);
		void setClientIDCode(int clientIDCode, int codeNum=1);
		void setClientIDCodes(int clientIDCode1, int clientIDCode2);
		void setReadCode(int readCode, int codeNum=1);
		void setReadCodes(int readCode1, int readCode2, int readCode3);
		void setWriteCode(int writeCode, int codeNum=1);
		void setWriteCodes(int writeCode1, int writeCode2, int writeCode3);
	
};

#endif
