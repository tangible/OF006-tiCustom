/*
 *  KeyLokThreadedTasks.h
 *  emptyExample
 *
 *  Created by Eli Smiles on 01/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef KEYLOK_DISABLE_THREADS
#ifndef _TI_KEYLOK_THREADED_TASKS
#define _TI_KEYLOK_THREADED_TASKS

#include "KeyLok.h"

#include "ofxDateTime.h"
#include "ofxThread.h"

#include <deque>
using namespace std;

class KeyLokThreadedTask{
protected:
	KeyLokTaskCodes taskCode;
	unsigned arg1, arg2, arg3;

	int result;
	string message;
	
public:
	KeyLokThreadedTask(KeyLokTaskCodes taskCode);
	~KeyLokThreadedTask();
	
	KeyLokTaskCodes getTaskCode();
	int getArg1();
	int getArg2();
	int getArg3();
	
	int getResult();
	string getMessage();
	
	void setArguments(unsigned arg1=0, unsigned arg2=0, unsigned arg3=0);
	void setMessage(string message="");
	void setResult(int result, string message="");
	void setResult(bool result, string message="");
};

class KeyLokThreadedTasks : public ofxThread{
protected:
	KeyLok* keyLok;
	deque<KeyLokThreadedTask*> taskQueue;
	
	bool hasTasks();
	void doNextTask();
	
public:
	KeyLokThreadedTasks(KeyLok* keyLok=NULL);
	~KeyLokThreadedTasks();
	
	void setKeyLok(KeyLok* keyLok=NULL);
	
	void queueTask(KeyLokTaskCodes taskCode, unsigned arg1=0, unsigned arg2=0, unsigned arg3=0);
	
	void start();
	void stop();
	
	virtual void threadedFunction();
	
	ofEvent<KeyLokThreadedTask> keyLokTaskComplete;
};

#endif
#endif
