/*
 *  KeyLok.cpp
 *
 *  Created by Pat Long (plong0) on 08/04/09.
 *  Copyright 2009 Tangible Interaction Inc. All rights reserved.
 *
 */

#include "KeyLok.h"

KeyLok::KeyLok(){
	this->ReturnValue1 = this->ReturnValue2 = 0;
	this->connected = this->readAuth = this->writeAuth = false;
	this->activeTask = KL_IDLE;
	this->setValidateCodes(0, 0, 0);
	this->setClientIDCodes(0, 0);
	this->setReadCodes(0, 0, 0);
	this->setWriteCodes(0, 0, 0);
	this->setRunCountAddress();
}

KeyLok::~KeyLok(){
}

/** This routine makes the security system call. */
void KeyLok::KTASK(unsigned CommandCode, unsigned Argument2, unsigned Argument3, unsigned Argument4){
	unsigned long int ReturnValue;
	ReturnValue = KFUNC(CommandCode, Argument2, Argument3, Argument4);
	this->ReturnValue1 = (unsigned) (ReturnValue & 0XFFFF);
	this->ReturnValue2 = (unsigned) (ReturnValue >> 16);
	
	if(this->ReturnValue1 == (unsigned short)-1)
		this->ReturnValue1 = -1;
	if(this->ReturnValue2 == (unsigned short)-1)
		this->ReturnValue2 = -1;
}

/** This function rotates the bits in the Target left the number of positions
 identified by the argument Counts */
unsigned KeyLok::RotateLeft(unsigned Target, int Counts){
	int i;
	static unsigned LocalTarget, HighBit;
	
	LocalTarget = Target;
	for (i=0; i<Counts; i++)
	{
		HighBit = LocalTarget & 0X8000;
		LocalTarget = (LocalTarget << 1) + (HighBit >> 15);
	}
	LocalTarget = LocalTarget & 0XFFFF; /* For 32 bit integers */
	return (LocalTarget);
}

unsigned KeyLok::getDummyArgument(){
	return (unsigned)ofRandom(0, 9999);
}


bool KeyLok::isConnected(bool getNewConnection){
	bool connected = false;
	
	this->setActiveTask(KL_CONNECT);
	if(!getNewConnection){
		connected = this->connected;
	}
	else{		
		this->KTASK((unsigned)(KLCHECK), this->validateCode1, this->validateCode2, this->validateCode3);
		this->KTASK(RotateLeft(this->ReturnValue1, this->ReturnValue2 & 7) ^ this->readCode3 ^ this->ReturnValue2,
					RotateLeft(this->ReturnValue2, this->ReturnValue1 & 15),
					this->ReturnValue1 ^ this->ReturnValue2, 0);
		
		// NOTE: Higher security can be achieved by using return values in
		//       computations and performing comparison of computed values to
		//       expected values deeper in your code than here.
		if ((this->ReturnValue1 == this->clientIDCode1) && (this->ReturnValue2 == this->clientIDCode2))
			connected = true;
		else
			connected = false;
		this->connected = connected;
	}
	if(this->getActiveTask() == KL_CONNECT)
		this->setActiveTask();

	return connected;
}

bool KeyLok::disconnect(){
	bool result = false;
	this->setActiveTask(KL_DISCONNECT);
	if(this->isConnected()){
		cout << "disconnecting...." << endl;
		this->KTASK(TERMINATE, this->getDummyArgument(), this->getDummyArgument(), this->getDummyArgument());
		this->connected = false;
		result = true;
	}
	if(this->getActiveTask() == KL_DISCONNECT)
		this->setActiveTask();
	return result;
}

int KeyLok::checkExpirationDate(){
	int result = -1;
	this->setActiveTask(KL_CHECK_EXP_DATE);
	if(this->isConnected() && this->hasWriteAuthorization()){
		this->KTASK(CKLEASEDATE, this->getDummyArgument(), this->getDummyArgument(), this->getDummyArgument());
		if(ReturnValue2 == NOLEASEDATE)
			result = -2;
		else if(ReturnValue2 != LEASEEXPIRED 
		   && ReturnValue2 != SYSDATESETBACK   
		   && ReturnValue2 != LEASEDATEBAD 
		   && ReturnValue2 != LASTSYSDATECORRUPT)
			result = (int)ReturnValue2; // approximate number of days until lease expires
	}
	if(this->activeTask == KL_CHECK_EXP_DATE)
		this->setActiveTask();
	
	return result;
}

int KeyLok::checkRunCount(bool updateRunCount){
	this->setActiveTask(KL_CHECK_RUN_COUNT);
	int runCount = this->getRunCount();
	
	if(runCount == -1)
		runCount == -2;
	
	if(runCount > 0 && updateRunCount){
		this->decrementRunCount();
	}
	if(this->activeTask == KL_CHECK_RUN_COUNT)
		this->setActiveTask();

	return runCount;
}

KeyLokValidationResults KeyLok::isValid(KeyLokValidationMode validationMode, bool updateRunCount){
	KeyLokValidationResults result = VALID; 
	
	this->setActiveTask(KL_IS_VALID);
	if(this->isConnected()){
		bool dateValid = true;
		bool runCountValid = true;
		
		if(validationMode == VALIDATE_FULL || validationMode == VALIDATE_DATE){
			int expDate = this->checkExpirationDate();
			dateValid = (expDate == -2 || expDate >= 0);
		}
		
		if(validationMode == VALIDATE_FULL || validationMode == VALIDATE_RUN_COUNT)
			runCountValid = this->checkRunCount(updateRunCount);
		if(!dateValid && !runCountValid)
			result = INVALID_DATE_AND_RUN_COUNT;
		else if(!dateValid)
			result = INVALID_DATE;
		else if(!runCountValid)
			result = INVALID_RUN_COUNT;
	}
	else{
		result = NOT_CONNECTED;
		this->readAuth = this->writeAuth = false;
	}
	if(this->getActiveTask() == KL_IS_VALID)
	   this->setActiveTask();
	
	return result;
}

bool KeyLok::hasReadAuthorization(bool getNewAuth){
	this->setActiveTask(KL_READ_AUTH);
	if(!this->readAuth && getNewAuth){
		if(this->isConnected()){			
			this->KTASK(READAUTH, readCode1, readCode2, readCode3);
			
			/**
			// check that read auth was successful by getting Serial Number twice and making sure values are same			
			this->KTASK(GETSN, this->getDummyArgument(), this->getDummyArgument(), this->getDummyArgument());
			unsigned sn1 = ReturnValue1;
			
			this->KTASK(GETSN, this->getDummyArgument(), this->getDummyArgument(), this->getDummyArgument());
			unsigned sn2 = ReturnValue1;
			
			if(sn1 == sn2)
				this->readAuth = true;
			*/
			this->readAuth = true;
		}
	}
	if(this->getActiveTask() == KL_READ_AUTH)
		this->setActiveTask();
	return this->readAuth;
}

bool KeyLok::hasWriteAuthorization(bool getNewAuth){
	this->setActiveTask(KL_WRITE_AUTH);
	if(!this->writeAuth && getNewAuth){
		if(this->isConnected() && this->hasReadAuthorization(getNewAuth)){
			this->KTASK(WRITEAUTH, writeCode1, writeCode2, writeCode3);
			this->writeAuth = true;
		}
	}
	if(this->getActiveTask() == KL_WRITE_AUTH)
		this->setActiveTask();
	return this->writeAuth;
}

int KeyLok::getExpirationDate(){
	int result = 0;
	this->setActiveTask(KL_GET_EXP_DATE);
	if(this->isConnected() && this->hasReadAuthorization()){
		this->KTASK(GETEXPDATE, this->getDummyArgument(), this->getDummyArgument(), this->getDummyArgument());
		
		unsigned DateRead = ReturnValue1;
		
		if(DateRead > 0){
			int ExpYear = ((DateRead & 0XFE00) / 512) + KEYLOK_BASEYEAR;
			int ExpMonth = (DateRead & 0X1E0) / 32;
			int ExpDay = (DateRead & 0X1F);
			
			ofxDateTime dateTime;
			dateTime.setDate(ExpDay, ExpMonth, ExpYear);
			cout << dateTime.getDateString() << endl;
			result = dateTime.getEpoch();
		}
	}
	if(this->getActiveTask() == KL_GET_EXP_DATE)
		this->setActiveTask();
	return result;
}

int KeyLok::getRunCount(){
	int result = -1;
	this->setActiveTask(KL_GET_RUN_COUNT);
	result = this->getVarWord(this->runCountAddress);
	if(this->getActiveTask() == KL_GET_RUN_COUNT)
		this->setActiveTask();
	return result;
}

int KeyLok::getSerialNumber(){
	int serialNumber = -1;
	this->setActiveTask(KL_GET_SN);
	if(this->isConnected() && this->hasReadAuthorization()){
		this->KTASK(GETSN, this->getDummyArgument(), this->getDummyArgument(), this->getDummyArgument());
		serialNumber = (int)ReturnValue1;
	}
	if(this->getActiveTask() == KL_GET_SN)
		this->setActiveTask();
	return serialNumber;
}

int KeyLok::getVarWord(int address){
	int varWord = -1;
	this->setActiveTask(KL_GET_VAR);
	if(address >= 0 && address <= 55){
		if(this->isConnected() && this->hasReadAuthorization()){
			this->KTASK(GETVARWORD, address, this->getDummyArgument(), this->getDummyArgument());
			varWord = (int)ReturnValue1;
		}
	}
	if(this->getActiveTask() == KL_GET_VAR)
		this->setActiveTask();
	return varWord;
}

bool KeyLok::isIdle(){
	return (this->getActiveTask() == KL_IDLE);
}

KeyLokTaskCodes KeyLok::getActiveTask(){
	return this->activeTask;
}

string KeyLok::getActiveTaskString(){
	return this->getTaskCodeString(this->getActiveTask());
}

string KeyLok::getTaskCodeString(KeyLokTaskCodes taskCode){
	string result = "";
	switch(taskCode){
		case KL_CONNECT:
			result = "connecting";
			break;
			
		case KL_DISCONNECT:
			result = "disconnecting";
			break;
			
		case KL_READ_AUTH:
			result = "checking read auth";
			break;
			
		case KL_GET_SN:
			result = "reading serial number";
			break;
			
		case KL_GET_VAR:
			result = "reading data";
			break;
			
		case KL_WRITE_AUTH:
			result = "checking write auth";
			break;
			
		case KL_WRITE_VAR:
			result = "writing data";
			break;
			
		case KL_DEC_VAR:
			result = "decreasing data";
			break;
			
		case KL_GET_EXP_DATE:
			result = "getting expiry date";
			break;
			
		case KL_CHECK_EXP_DATE:
			result = "checking expiry date";
			break;
			
		case KL_SET_EXP_DATE:
			result = "setting expiry date";
			break;
			
		case KL_SET_RUN_COUNT:
			result = "setting run count";
			break;
			
		case KL_GET_RUN_COUNT:
			result = "getting run count";
			break;
			
		case KL_DEC_RUN_COUNT:
			result = "decreasing run count";
			break;
			
		case KL_CHECK_RUN_COUNT:
			result = "checking run count";
			break;
			
		case KL_IS_VALID:
			result = "validating";
			break;
			
		case KL_IDLE:
		default:
			result = "idle";
			break;
	}
	return result;
	
}

string KeyLok::getValidationModeString(KeyLokValidationMode validationMode){
	string result = "";
	switch(validationMode){
		case VALIDATE_CONNECTED:
			result = "connection check";
			break;
		case VALIDATE_DATE:
			result = "date validation";
			break;
		case VALIDATE_RUN_COUNT:
			result = "run count validation";
			break;
		case VALIDATE_FULL:
		default:
			result = "full validation";
			break;
	}
	return result;
}

string KeyLok::getValidationResultString(KeyLokValidationResults resultCode){
	string result = "";
	switch(resultCode){
		case NOT_CONNECTED:
			result = "Key-Lok is not connected";
			break;
		case INVALID_DATE:
			result = "expiration date has expired";
			break;
		case INVALID_RUN_COUNT:
			result = "run counter has expired";
			break;
		case INVALID_DATE_AND_RUN_COUNT:
			result = "expiration date and run counter have expired";
			break;
		case VALID:
		default:
			result = "device successfully validated";
			break;
	}
	return result;
}

int KeyLok::decrementMemory(int address){
	int result = -1;
	this->setActiveTask(KL_DEC_VAR);
	if(address >= 0 && address <= 55){
		if(this->isConnected() && this->hasWriteAuthorization()){
			this->KTASK(DECREMENTMEM, address, this->getDummyArgument(), this->getDummyArgument());
			int errorCode = (int)ReturnValue2;
			if(errorCode == 0)
				result = (int)ReturnValue1;
		}
	}
	if(this->getActiveTask() == KL_DEC_VAR)
		this->setActiveTask();
	return result;
}

int KeyLok::decrementRunCount(){
	int result = -1;
	this->setActiveTask(KL_DEC_RUN_COUNT);
	result = this->decrementMemory(this->runCountAddress);
	if(this->getActiveTask() == KL_DEC_RUN_COUNT)
		this->setActiveTask();
	return result;
}

bool KeyLok::setExpirationDate(int month, int day, int year){
	bool result = false;
	unsigned setValue = -1;
	this->setActiveTask(KL_SET_EXP_DATE);
	if(month <= 0 && day <= 0 && year <= 0){
		// disable the expiration date
		setValue = 0;
	}
	else{
		stringstream msgBuilder;
		bool goodInput = true;
		if(month < 1 || month > 12){
			msgBuilder << "month must be in range: 1-12.";
			ofLog(OF_LOG_ERROR, msgBuilder.str());
			msgBuilder.str("");
			goodInput = false;
		}
		if(day < 1 || day > 31){
			msgBuilder << "day must be in range: 1-31.";
			ofLog(OF_LOG_ERROR, msgBuilder.str());
			msgBuilder.str("");
			goodInput = false;
		}
		if(year < 2010 || year > 2099){
			msgBuilder << "year must be in range: 2010-2099.";
			ofLog(OF_LOG_ERROR, msgBuilder.str());
			msgBuilder.str("");
			goodInput = false;
		}
		
		if(goodInput){
			//Convert the input expiration date into the proper storage format
			//Date will be stored in YYYYYYYMMMMDDDDD format with YYYYYY being the 
			//number of years after BASEYEAR (e.g. YYYYYYY = 3 for 1993 with base 1990)
			setValue = 512 * (year - KEYLOK_BASEYEAR) + (32 * month) + day;
		}
	}
	if(setValue != -1){
		if(this->isConnected() && this->hasWriteAuthorization()){
			//Write the expiration date into the KEY-LOK
			this->KTASK(SETEXPDATE, this->getDummyArgument(), setValue, this->getDummyArgument());
			result = true;
		}
	}
	if(this->getActiveTask() == KL_SET_EXP_DATE)
		this->setActiveTask();
	return result;
}

bool KeyLok::setRunCount(int runCount){
	bool result = false;
	this->setActiveTask(KL_SET_RUN_COUNT);
	result = this->setVarWord(this->runCountAddress, runCount);
	if(this->getActiveTask() == KL_SET_RUN_COUNT)
		this->setActiveTask();
	return result;
}

bool KeyLok::setVarWord(int address, int value){
	bool result = false;
	this->setActiveTask(KL_WRITE_VAR);
	if(address >= 0 && address <= 55){
		if(this->isConnected() && this->hasWriteAuthorization()){
			this->KTASK(WRITEVARWORD, address, value, this->getDummyArgument());
			result = true;
		}
	}
	if(this->getActiveTask() == KL_WRITE_VAR)
		this->setActiveTask();
	return result;
}

void KeyLok::setActiveTask(KeyLokTaskCodes activeTask){
	if(this->activeTask == KL_IDLE || activeTask == KL_IDLE)
		this->activeTask = activeTask;
}

void KeyLok::setRunCountAddress(int runCountAddress){
	this->runCountAddress = runCountAddress;
}

void KeyLok::setValidateCode(int validateCode, int codeNum){
	if(codeNum == 1)
		this->validateCode1 = validateCode;
	else if(codeNum == 2)
		this->validateCode2 = validateCode;
	else if(codeNum == 3)
		this->validateCode3 = validateCode;
	else
		cout << "Unknown validate code number:" << codeNum << endl;
}

void KeyLok::setValidateCodes(int validateCode1, int validateCode2, int validateCode3){
	this->setValidateCode(validateCode1, 1);
	this->setValidateCode(validateCode2, 2);
	this->setValidateCode(validateCode3, 3);
}

void KeyLok::setClientIDCode(int clientIDCode, int codeNum){
	if(codeNum == 1)
		this->clientIDCode1 = clientIDCode;
	else if(codeNum == 2)
		this->clientIDCode2 = clientIDCode;
	else
		cout << "Unknown clientID code number:" << codeNum << endl;
}

void KeyLok::setClientIDCodes(int clientIDCode1, int clientIDCode2){
	this->setClientIDCode(clientIDCode1, 1);
	this->setClientIDCode(clientIDCode2, 2);
}

void KeyLok::setReadCode(int readCode, int codeNum){
	if(codeNum == 1)
		this->readCode1 = readCode;
	else if(codeNum == 2)
		this->readCode2 = readCode;
	else if(codeNum == 3)
		this->readCode3 = readCode;
	else
		cout << "Unknown read code number:" << codeNum << endl;
}

void KeyLok::setReadCodes(int readCode1, int readCode2, int readCode3){
	this->setReadCode(readCode1, 1);
	this->setReadCode(readCode2, 2);
	this->setReadCode(readCode3, 3);
}

void KeyLok::setWriteCode(int writeCode, int codeNum){
	if(codeNum == 1)
		this->writeCode1 = writeCode;
	else if(codeNum == 2)
		this->writeCode2 = writeCode;
	else if(codeNum == 3)
		this->writeCode3 = writeCode;
	else
		cout << "Unknown write code number:" << codeNum << endl;
}

void KeyLok::setWriteCodes(int writeCode1, int writeCode2, int writeCode3){
	this->setWriteCode(writeCode1, 1);
	this->setWriteCode(writeCode2, 2);
	this->setWriteCode(writeCode3, 3);
}
