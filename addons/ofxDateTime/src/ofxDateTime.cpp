/*
 *  ofxDateTime.cpp
 *
 *  Created by Pat Long (plong0) on 29/09/10.
 *  Copyright 2010 Spiral Sense. All rights reserved.
 *
 */
#include "ofxDateTime.h"

//------------------------------------
ofxDateTime::ofxDateTime(){
	time_t rawtime;
	time ( &rawtime );
	this->dateTime = localtime ( &rawtime );
	this->refreshDateTime();
}

//------------------------------------
ofxDateTime::~ofxDateTime(){
}

//------------------------------------
string ofxDateTime::formatDateTime(string formatStr){
	string result = "";
	if(formatStr != ""){
		size_t bufferSize = formatStr.length()*256;	
		char* buffer = new char[bufferSize];
		strftime (buffer, bufferSize, formatStr.c_str(), this->dateTime);
		result.assign(buffer);
		delete [] buffer;
	}
	else{
		result.assign(asctime(this->dateTime));
	}
	
	return result;
}

//------------------------------------
int ofxDateTime::getDay(){
	return this->dateTime->tm_mday;
}

//------------------------------------
int ofxDateTime::getMonth(){
	return this->dateTime->tm_mon + 1;
}

//------------------------------------
int ofxDateTime::getYear(){
	return this->dateTime->tm_year + 1900;
}

//------------------------------------
int ofxDateTime::getHour(){
	return this->dateTime->tm_hour;
}

//------------------------------------
int ofxDateTime::getMinute(){
	return this->dateTime->tm_min;
}

//------------------------------------
int ofxDateTime::getSecond(){
	return this->dateTime->tm_sec;
}

//------------------------------------
int ofxDateTime::getWeekday(){
	return this->dateTime->tm_wday;
}

//------------------------------------
int ofxDateTime::getEpoch(){
	return mktime(this->dateTime);
}

//------------------------------------
string ofxDateTime::getDateString(bool shortForm){
	return this->formatDateTime((shortForm?"%m/%d/%y":"%B %d, %Y"));
}

//------------------------------------
string ofxDateTime::getDateTimeString(bool shortForm){
	if(shortForm)
		return this->formatDateTime("%m/%d/%y @ %H:%M:%S");
	return this->formatDateTime();
}

//------------------------------------
string ofxDateTime::getMonthString(bool shortForm){
	return this->formatDateTime((shortForm?"%b":"%B"));
}

//------------------------------------
string ofxDateTime::getTimeString(){
	return this->formatDateTime("%H:%M:%S");
}

//------------------------------------
string ofxDateTime::getWeekdayString(bool shortForm){
	return this->formatDateTime((shortForm?"%a":"%A"));
}

//------------------------------------
void ofxDateTime::refreshDateTime(){
	mktime(this->dateTime);
}

//------------------------------------
void ofxDateTime::setFromEpoch(int epoch){
	time_t rawtime = epoch;
	this->dateTime = localtime(&rawtime);
}

//------------------------------------
void ofxDateTime::setDate(int day, int month, int year){
	this->setDay(day);
	this->setMonth(month);
	this->setYear(year);
}

//------------------------------------
void ofxDateTime::setDay(int day){
	if(day < 1) day = 1;
	if(day > 31) day = 31;
	this->dateTime->tm_mday = day;
}

//------------------------------------
void ofxDateTime::setMonth(int month){
	if(month < 1) month = 1;
	if(month > 12) month = 12;
	this->dateTime->tm_mon = month - 1;
}

//------------------------------------
void ofxDateTime::setYear(int year){
	if(year < 1900) year = 1900;
	this->dateTime->tm_year = year - 1900;
}

//------------------------------------
void ofxDateTime::setTime(int hour, int minute, int second){
	this->setHour(hour);
	this->setMinute(minute);
	this->setSecond(second);
}

//------------------------------------
void ofxDateTime::setHour(int hour){
	if(hour < 0) hour = 0;
	if(hour > 23) hour = 23;
	this->dateTime->tm_hour = hour;
}

//------------------------------------
void ofxDateTime::setMinute(int minute){
	if(minute < 0) minute = 0;
	if(minute > 59) minute = 59;
	this->dateTime->tm_min = minute;
}

//------------------------------------
void ofxDateTime::setSecond(int second){
	if(second < 0) second = 0;
	if(second > 61) second = 61; // tm_sec is generally 0-59. Extra range to accommodate for leap seconds in certain systems
	this->dateTime->tm_sec = second;
}
