/*
 *  ofxDateTimeConstants.h
 *  emptyExample
 *
 *  Created by Eli Smiles on 29/09/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_DATE_TIME_CONSTANTS
#define _OFX_DATE_TIME_CONSTANTS

#include <string.h>

const string monthStrings[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
const string monthShortStrings[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

#endif
