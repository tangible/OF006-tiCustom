/*
 *  ofxHttpForm.h
 *  openFrameworks
 *
 *  Created by Pat Long on 16/11/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#ifndef _OFX_HTTP_FORM
#define _OFX_HTTP_FORM

#include "Poco/Net/HTMLForm.h"
using Poco::Net::HTMLForm;


// we are extending the HTMLForm for the simple reason of making sure the content-length header gets sent
class ofxHttpForm : public HTMLForm{
protected:
	void myWriteMultipart(std::ostream& ostr);
	
public:
	ofxHttpForm(const std::string& encoding) : HTMLForm(encoding){};
	~ofxHttpForm(){};
	
	void myWrite(std::ostream& ostr);
	
};

#endif
