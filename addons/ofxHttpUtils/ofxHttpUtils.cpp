/*
    ofxHttpUtils v0.2
    Chris O'Shea, Art, Jesus, CJ

    Modified: 12th November
*/

#include "ofxHttpUtils.h"
#include "ofxHttpEvents.h"

// ----------------------------------------------------------------------
ofxHttpUtils::ofxHttpUtils(){
    timeoutSeconds = 40; // default timeout
    verbose = false;
    start();
}
// ----------------------------------------------------------------------
ofxHttpUtils::~ofxHttpUtils(){
    stop();
}
// ----------------------------------------------------------------------
void ofxHttpUtils::submitForm(ofxHttpForm form){
	doPostForm(form);
}
// ----------------------------------------------------------------------
void ofxHttpUtils::addForm(ofxHttpForm form){
    forms.push_back(form);
    if (isThreadRunning() == false){
        start();
    }
}
// ----------------------------------------------------------------------
void ofxHttpUtils::start() {
     if (isThreadRunning() == false){
        if(verbose) printf("starting http thread\n");
        startThread(true, verbose);
    }
}
// ----------------------------------------------------------------------
void ofxHttpUtils::stop() {
    stopThread();
}
// ----------------------------------------------------------------------
void ofxHttpUtils::threadedFunction(){

    // loop through this process whilst thread running
    while( isThreadRunning() == true )
    	if(forms.size()>0){
			ofxHttpForm form = forms.front();
			if(form.method==OFX_HTTP_POST){
				doPostForm(form);
				if(verbose) printf("ofxHttpUtils: (thread running) form submitted (post): %s\n", form.name.c_str());
			}else{
				string url = generateUrl(form);
				getUrl(url);
				if(verbose) printf("ofxHttpUtils: (thread running) form submitted (get): %s\n", form.name.c_str());
			}
			forms.erase( forms.begin() );
    	}else{
    		stop();
    	}

}

// ----------------------------------------------------------------------
string ofxHttpUtils::generateUrl(ofxHttpForm & form) {
    // url to send to
    string url = form.action;

    // do we have any form fields?
    if(form.formIds.size() > 0){
        url += "?";
        for(unsigned int i=0;i<form.formIds.size();i++){
            url += form.formIds[i] +"="+ form.formValues[i];
            if(i<form.formIds.size()-1)
                url += "&";
        }
    }
    return url;
}

// ----------------------------------------------------------------------
void ofxHttpUtils::doPostForm(ofxHttpForm & form){

    try{
        URI uri( form.action.c_str() );
        std::string path(uri.getPathAndQuery());
        if (path.empty()) path = "/";
		
		int contentLength = 0;

        HTTPClientSession session(uri.getHost(), uri.getPort());
        HTTPRequest req(HTTPRequest::HTTP_POST, path, HTTPMessage::HTTP_1_1);

        session.setTimeout(Poco::Timespan(timeoutSeconds,0));

        // create the form data to send
        HTMLForm pocoForm((form.multipart)?HTMLForm::ENCODING_MULTIPART:HTMLForm::ENCODING_URL);
//        HTMLForm pocoForm(HTMLForm::ENCODING_URL);

        // form values
        for(unsigned int i=0; i<form.formIds.size(); i++){
			bool isFile = form.formFiles[i];
            const std::string name = form.formIds[i].c_str();
            const std::string val = form.formValues[i].c_str();

			if(isFile){
				// need to get filesize of val
				
				PartSource* partSource = new FilePartSource(val);
				pocoForm.addPart(name, partSource);
				contentLength += 69; // for the preset stuff like Content-Disposition: form-data; name=""; filename=""\nContent-Type: 
				contentLength += name.length(); // the name of the field
				contentLength += partSource->filename().length();
				contentLength += partSource->mediaType().length();
				contentLength += tiGetFileSize(val, true);
			}
			else{
				pocoForm.set(name, val);
				contentLength += 41; // for the preset stuff like Content-Disposition: form-data; name=""
				contentLength += name.length(); // the name of the field
				contentLength += val.length(); // the value of the field
			}
        }
		
        pocoForm.prepareSubmit(req);
		
		string contentType = "Content-Type: " + req.getContentType();
		string boundary = pocoForm.boundary();
		contentLength += contentType.length();
		contentLength += boundary.length() * (1+form.formIds.size()); // one boundary for each form element, plus one at the end
		
		req.setContentLength(contentLength);
//		req.set("Content-Length", ofToString(contentLength).c_str());
//		req.set("Custom-Test","test123");
//		req.setChunkedTransferEncoding(false);
		
		ostream& myStream = session.sendRequest(req);
		myStream << "Content-Length:" << contentLength << "\r\n\r\n";
        pocoForm.write(myStream);
		
        HTTPResponse res;
        istream& rs = session.receiveResponse(res);

		ofxHttpResponse response=ofxHttpResponse(res, rs, form.action);

    	ofxHttpEvents.notifyNewResponse(response);


    }catch (Exception& exc){
        ofxHttpEvents.notifyNewError("time out ");
        if(verbose) std::cerr << exc.displayText() << std::endl;
    }

}

// ----------------------------------------------------------------------

//I've taken this function out for now whilst I make everything run ok in a thread
void ofxHttpUtils::getUrl(string url){
   try{
		URI uri(url.c_str());
		std::string path(uri.getPathAndQuery());
		if (path.empty()) path = "/";

		HTTPClientSession session(uri.getHost(), uri.getPort());
		HTTPRequest req(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
		session.sendRequest(req);

		session.setTimeout(Poco::Timespan(timeoutSeconds,0));

		HTTPResponse res;
		istream& rs = session.receiveResponse(res);

		ofxHttpResponse response=ofxHttpResponse(res,rs,url);

		ofxHttpEvents.notifyNewResponse(response);

	}catch (Exception& exc){
		if(verbose) std::cerr << exc.displayText() << std::endl;
		ofxHttpEvents.notifyNewError("time out ");
	}
}

void ofxHttpUtils::addUrl(string url){
	ofxHttpForm form;
	form.action=url;
	form.method=OFX_HTTP_GET;
    form.name=form.action;
	addForm(form);
}

ofxHttpUtils ofxHttpUtil;
ofxHttpEventManager ofxHttpEvents(&ofxHttpUtil);

