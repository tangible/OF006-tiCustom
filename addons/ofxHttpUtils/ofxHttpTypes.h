/*
    ofxHttpUtils v0.2
    Chris O'Shea, Art, Jesus, CJ

    Modified: 12th November
*/

#ifndef OFX_HTTP_TYPES
#define OFX_HTTP_TYPES

#define OFX_HTTP_GET  0
#define OFX_HTTP_POST 1

struct ofxHttpForm{
	vector <string> formIds;
	vector <string> formValues;
	vector <bool>	formFiles;

	// ----------------------------------------------------------------------
	void addFormField(string id, string value, bool isFile=false){
	    formIds.push_back( id );
	    formValues.push_back( value );
		formFiles.push_back( isFile );
	}
	// ----------------------------------------------------------------------
	void clearFormFields(){
	    formIds.clear();
	    formValues.clear();
		formFiles.clear();
	}
	int method;
	string action;
	string name;
	bool multipart;
};

#endif
