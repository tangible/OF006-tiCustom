/*
 *  ofxHttpForm.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 16/11/09.
 *  Copyright 2009 Tangible Interaction. All rights reserved.
 *
 */
#include "ofxHttpForm.h"

void ofxHttpForm::myWrite(std::ostream& ostr){
	if (_encoding == ENCODING_URL)
		writeUrl(ostr);
	else
		myWriteMultipart(ostr);
}

void ofxHttpForm::myWriteMultipart(std::ostream& ostr){
	MultipartWriter writer(ostr, _boundary);
	for (NameValueCollection::ConstIterator it = begin(); it != end(); ++it)
	{
		MessageHeader header;
		std::string disp("form-data; name=\"");
		disp.append(it->first);
		disp.append("\"");
		header.set("Content-Disposition", disp);
		writer.nextPart(header);
		ostr << it->second;
	}	
	for (PartVec::iterator ita = _parts.begin(); ita != _parts.end(); ++ita)
	{
		MessageHeader header;
		std::string disp("file; name=\"");
		disp.append(ita->name);
		disp.append("\"");
		std::string filename = ita->pSource->filename();
		if (!filename.empty())
		{
			disp.append("; filename=\"");
			disp.append(filename);
			disp.append("\"");
		}
		header.set("Content-Disposition", disp);
		header.set("Content-Type", ita->pSource->mediaType());
		writer.nextPart(header);
		StreamCopier::copyStream(ita->pSource->stream(), ostr);
	}
	writer.close();
	_boundary = writer.boundary();
}
