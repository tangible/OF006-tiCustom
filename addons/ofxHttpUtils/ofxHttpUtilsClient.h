/*
 *  ofxHttpUtilsClient.h
 *  openFrameworks
 *
 *  Created by Pat Long on 17/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_HTTP_UTILS_CLIENT
#define _OFX_HTTP_UTILS_CLIENT

#include "Poco/Net/Net.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPSession.h"
#include "Poco/Net/SocketAddress.h"
#include <istream>
#include <ostream>

//using n Poco::Net;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPSession;
using Poco::Net::SocketAddress;

class ofxHttpUtilsClient : public HTTPClientSession{
	
public:
	ofxHttpUtilsClient(){};
	ofxHttpUtilsClient(const std::string& host, Poco::UInt16 port = HTTPSession::HTTP_PORT):HTTPClientSession(host, port){};
	~ofxHttpUtilsClient(){};
	
	virtual std::ostream& sendRequest(HTTPRequest& request);
	
};


#endif
