/*
 *  ofxNetworkVideoComponent.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxNetworkVideoComponent.h"

ofxNetworkVideoComponent::ofxNetworkVideoComponent(){
}

ofxNetworkVideoComponent::~ofxNetworkVideoComponent(){
	this->Close();
}

void ofxNetworkVideoComponent::init(int portNum, int width, int height, int imageType){
	this->portNum = portNum;
	this->width = width;
	this->height = height;
	
	this->setImageType(imageType);
	this->hasNewImage = false;
}

void ofxNetworkVideoComponent::update(){
}

int ofxNetworkVideoComponent::getWidth(){
	return this->width;
}

int ofxNetworkVideoComponent::getHeight(){
	return this->height;
}

int ofxNetworkVideoComponent::getImageType(){
	return this->imageType;
}

unsigned char* ofxNetworkVideoComponent::getPixels(){
	this->hasNewImage = false;
	return this->videoFrame.getPixels();
}

bool ofxNetworkVideoComponent::hasNewFrame(){
	return this->hasNewImage;
}

void ofxNetworkVideoComponent::setImageType(int imageType){
	this->imageType = imageType;
	
	if(imageType == OF_IMAGE_GRAYSCALE)
		this->bpp = 1;
	else if(imageType == OF_IMAGE_COLOR)
		this->bpp = 3;
	else if(imageType == OF_IMAGE_COLOR_ALPHA)
		this->bpp = 4;
	
	this->videoFrame.allocate(this->width, this->height, this->imageType);
}

void ofxNetworkVideoComponent::setNewPixels(unsigned char* pixels, int pixelLength){
	int correctPixelLength = this->width*this->height*this->bpp;
	if(pixelLength == correctPixelLength){
		this->videoFrame.setFromPixels(pixels, this->width, this->height, this->imageType);
		this->hasNewImage = true;
	}
	else{
		stringstream errorBuilder;
		errorBuilder << "ofxNetworkVideoComponent: setting invalid frame with length of " << pixelLength << ". (expected length is " << correctPixelLength << ")";
		ofLog(OF_LOG_ERROR, errorBuilder.str());
		errorBuilder.str("");
	}
}

