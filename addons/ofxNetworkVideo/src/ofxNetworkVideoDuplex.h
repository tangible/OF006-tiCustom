/*
 *  ofxNetworkVideoDuplex.h
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_NETWORK_VIDEO_DUPLEX_H
#define _OFX_NETWORK_VIDEO_DUPLEX_H

#include "ofxNetworkVideoSender.h"
#include "ofxNetworkVideoReceiver.h"
#include "ofxNetworkVideoConstants.h"

class ofxNetworkVideoDuplex{
protected:
	ofxNetworkVideoSender sender;
	ofxNetworkVideoReceiver receiver;
	
	void initSender(string hostName="", int portNum=-1, int width=DEFAULT_NETWORK_VIDEO_FRAME_WIDTH, int height=DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT, int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);
	void initReceiver(int portNum=-1, int width=DEFAULT_NETWORK_VIDEO_FRAME_WIDTH, int height=DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT, int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);
	
public:
	ofxNetworkVideoDuplex();
	~ofxNetworkVideoDuplex();
	
	void init(string hostName="", int sendPort=-1, int receivePort=-1, int width=DEFAULT_NETWORK_VIDEO_FRAME_WIDTH, int height=DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT, int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);
	void setBufferSizes(int bufferSize=DEFAULT_NETWORK_VIDEO_BUFFER_SIZE);
	
	void update();
	
	int getReceivedWidth();
	int getReceivedHeight();
	int getReceivedImageType();
	unsigned char* getReceivedPixels();
	bool hasReceivedNewFrame();
	
	void setNewSenderPixels(unsigned char* pixels, int width, int height, int type);
	void setSenderFrameDelay(int frameDelay=DEFAULT_NETWORK_VIDEO_FRAME_DELAY);
//	int sendVideoFrame(unsigned char* pixels, int width, int height, int type);
	
};

#endif
