/*
 *  ofxNetworkVideoReceiver.h
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_NETWORK_VIDEO_RECEIVER_H
#define _OFX_NETWORK_VIDEO_RECEIVER_H

#include "ofxNetworkVideoComponent.h"

class ofxNetworkVideoReceiver : public ofxNetworkVideoComponent{	
protected:

	
public:
	ofxNetworkVideoReceiver();
	~ofxNetworkVideoReceiver();
	
	virtual void init(int portNum=-1, int width=DEFAULT_NETWORK_VIDEO_FRAME_WIDTH, int height=DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT, int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);	
	
	virtual void update();

	void setBufferSize(int bufferSize=DEFAULT_NETWORK_VIDEO_BUFFER_SIZE);
};

#endif
