/*
 *  ofxNetworkVideoSender.h
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_NETWORK_VIDEO_SENDER_H
#define _OFX_NETWORK_VIDEO_SENDER_H

#include "ofxNetworkVideoComponent.h"

class ofxNetworkVideoSender : public ofxNetworkVideoComponent{	
protected:
	string hostName;
	int frameDelay;
	int lastSendFrame;
	
public:
	ofxNetworkVideoSender();
	~ofxNetworkVideoSender();
	
	virtual void init(string hostName="", int portNum=-1, int width=DEFAULT_NETWORK_VIDEO_FRAME_WIDTH, int height=DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT, int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);
	virtual void update();
	
	bool isReadyToSend();
	
	void setBufferSize(int bufferSize=DEFAULT_NETWORK_VIDEO_BUFFER_SIZE);
	void setFrameDelay(int frameDelay=DEFAULT_NETWORK_VIDEO_FRAME_DELAY);
	
	void setNewPixels(unsigned char* pixels, int width, int height, int type);
	int sendVideoFrame(unsigned char* pixels, int width, int height, int type);
};

#endif
