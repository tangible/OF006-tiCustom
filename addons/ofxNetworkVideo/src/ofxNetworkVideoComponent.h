/*
 *  ofxNetworkVideoComponent.h
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _OFX_NETWORK_VIDEO_COMPONENT_H
#define _OFX_NETWORK_VIDEO_COMPONENT_H

#include "ofMain.h"
#include "ofxNetwork.h"
#include "ofxNetworkVideoConstants.h"

class ofxNetworkVideoComponent : public ofxUDPManager{
protected:
	int portNum;
	int width;
	int height;
	int imageType;
	int bpp;
	
	ofImage videoFrame;
	bool hasNewImage;
	
public:
	ofxNetworkVideoComponent();
	~ofxNetworkVideoComponent();
	
	virtual void init(int portNum=-1, int width=DEFAULT_NETWORK_VIDEO_FRAME_WIDTH, int height=DEFAULT_NETWORK_VIDEO_FRAME_HEIGHT, int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);	
	virtual void update();
	
	int getWidth();
	int getHeight();
	int getImageType();
	unsigned char* getPixels();
	bool hasNewFrame();
	
	void setImageType(int imageType=DEFAULT_NETWORK_VIDEO_IMAGE_TYPE);
	void setNewPixels(unsigned char* pixels, int pixelLength);
};

#endif
