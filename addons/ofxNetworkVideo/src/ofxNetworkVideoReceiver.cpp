/*
 *  ofxNetworkVideoReceiver.cpp
 *  openFrameworks
 *
 *  Created by Pat Long on 11/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ofxNetworkVideoReceiver.h"

ofxNetworkVideoReceiver::ofxNetworkVideoReceiver(){
	this->init();
}

ofxNetworkVideoReceiver::~ofxNetworkVideoReceiver(){
}

void ofxNetworkVideoReceiver::init(int portNum, int width, int height, int imageType){
	ofxNetworkVideoComponent::init(portNum, width, height, imageType);
	
	if(this->portNum > 0){
		this->Create();
		this->Bind(this->portNum);
		this->SetNonBlocking(true);
		this->setBufferSize();
	}
}

void ofxNetworkVideoReceiver::update(){
	char udpMessage[this->GetReceiveBufferSize()];
	int receivedLength = this->Receive(udpMessage, this->GetReceiveBufferSize());
	if(receivedLength > 0){
//		cout << "received bytes:" << receivedLength << ":" << endl;
		unsigned char* receivedBytes = new unsigned char[receivedLength];
		for(int i=0; i < receivedLength; i++)
			receivedBytes[i] = (unsigned char)udpMessage[i];
		this->setNewPixels(receivedBytes, receivedLength);
		delete [] receivedBytes;
	}
}

void ofxNetworkVideoReceiver::setBufferSize(int bufferSize){
	this->SetReceiveBufferSize(bufferSize);
}
