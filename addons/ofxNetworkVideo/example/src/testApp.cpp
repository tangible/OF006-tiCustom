#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
	ofSetLogLevel(OF_LOG_VERBOSE);
	ofSetFrameRate(60);
	ofBackground(0, 0, 0);
	
	int localCameraID = 0;
	int localCameraWidth = 320;
	int localCameraHeight = 240;
	
	string sendHost = "127.0.0.1";
	int sendPort = 11999;
	int receivePort = 11998;
	int netVideoWidth = 160;
	int netVideoHeight = 120;
	int netImageType = OF_IMAGE_COLOR;
	
	ofxXmlSettings settings;
	settings.loadFile("networkVideo_config.xml");
	if(settings.tagExists("LOCAL_VIDEO")){
		settings.pushTag("LOCAL_VIDEO");
		
		localCameraID = settings.getValue("CAMERA_ID", localCameraID);
		localCameraWidth = settings.getValue("CAMERA_WIDTH", localCameraWidth);
		localCameraHeight = settings.getValue("CAMERA_HEIGHT", localCameraHeight);
		
		settings.popTag(); // /LOCAL_VIDEO
	}
	
	if(settings.tagExists("NETWORK_VIDEO")){
		settings.pushTag("NETWORK_VIDEO");
		
		netVideoWidth = settings.getValue("WIDTH", netVideoWidth);
		netVideoHeight = settings.getValue("HEIGHT", netVideoHeight);
		
		string tempValue = "COLOR";
		tempValue = settings.getValue("IMAGE_TYPE", tempValue);
		netImageType = ((tempValue == "GRAY" || tempValue == "GRAYSCALE")?OF_IMAGE_GRAYSCALE:OF_IMAGE_COLOR);
		
		if(settings.tagExists("SEND")){
			settings.pushTag("SEND");
			
			sendHost = settings.getValue("HOST", sendHost);
			sendPort = settings.getValue("PORT", sendPort);
			
			settings.popTag(); // /SEND
		}
		
		if(settings.tagExists("RECEIVE")){
			settings.pushTag("RECEIVE");
			
			receivePort = settings.getValue("PORT", receivePort);
			
			settings.popTag(); // /RECEIVE
		}
		
		settings.popTag(); // /NETWORK_VIDEO
	}
	
	netVideo.init(sendHost, sendPort, receivePort, netVideoWidth, netVideoHeight, netImageType);
	netVideo.setBufferSizes(500000);
	
	localVideo.setDeviceID(3);
	localVideo.initGrabber(320, 240);
	localImage.allocate(320, 240, OF_IMAGE_COLOR);
	remoteImage.allocate(320, 240, OF_IMAGE_COLOR);
}

//--------------------------------------------------------------
void testApp::update(){
	// update local video grabber / image
	localVideo.update();
	if(localVideo.isFrameNew()){
		localImage.setFromPixels(localVideo.getPixels(), localVideo.getWidth(), localVideo.getHeight(), OF_IMAGE_COLOR);
		netVideo.setNewSenderPixels(localImage.getPixels(), localImage.getWidth(), localImage.getHeight(), OF_IMAGE_COLOR);
	}
	
	netVideo.update();
	
	// update remote video image
	if(netVideo.hasReceivedNewFrame())
		remoteImage.setFromPixels(netVideo.getReceivedPixels(), netVideo.getReceivedWidth(), netVideo.getReceivedHeight(), netVideo.getReceivedImageType());
		
}

//--------------------------------------------------------------
void testApp::draw(){
	this->localImage.draw(25, 25, 320, 240);
	this->remoteImage.draw(345, 25, 320, 240);
	ofSetColor(0, 255, 0);
	
	ofDrawBitmapString("Local", 25, 20);
	ofDrawBitmapString("Remote", 345, 20);
	
	ofNoFill();
	ofRect(25, 25, 320, 240);
	ofRect(345, 25, 320, 240);
	ofFill();
	ofSetColor(255, 255, 255);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

