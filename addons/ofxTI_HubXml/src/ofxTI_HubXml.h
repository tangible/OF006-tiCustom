/*
 *  ofxTI_HubXml.h
 *
 *  Created by Pat Long (plong0) on 11-01-26.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#ifndef TI_HUBXML
#define TI_HUBXML

#include "ofMain.h"
#include "ofxDateTime.h"

#define DEFAULT_TI_HUB_IMAGE_EXTENSION "jpg"

class ofxTI_HubXmlEntry;

enum TimeStampMode {TS_HUMAN, TS_EPOCH};

class ofxTI_HubXml {
	protected:
		int installationID;
		string authenticationCode;
		string savePath;
		string defaultImageExtension;
		TimeStampMode tsMode;
	
		virtual string getTimeStamp(TimeStampMode tsMode);
		
	public:
		ofxTI_HubXml(int installationID=0, string savePath="", string defaultImageExtension=DEFAULT_TI_HUB_IMAGE_EXTENSION, TimeStampMode tsMode=TS_HUMAN);
		~ofxTI_HubXml();
	
		string getAuthenticationCode();
		int getInstallationID();
		virtual string getNewEntryName();
		virtual string getNewImageName(string extension="");
		string getSavePath();
	
		void setAuthenticationCode(string authenticationCode="");
		void setInstallationID(int installationID=0);
		void setSavePath(string savePath);
		void setDefaultImageExtension(string defaultImageExtension=DEFAULT_TI_HUB_IMAGE_EXTENSION);
		void setTimeStampMode(TimeStampMode tsMode);
};

#include "ofxTI_HubXmlEntry.h"

#endif
