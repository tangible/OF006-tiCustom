/*
 *  ofxTI_HubXmlEntry.h
 *
 *  Created by Pat Long (plong0) on 11-01-26.
 *  Copyright 2011 Tangible Interaction. All rights reserved.
 *
 */
#ifndef TI_HUBXML_ENTRY
#define TI_HUBXML_ENTRY

#include "ofxTI_HubXml.h"
#include "ofxXmlSettings.h"

class ofxTI_HubXmlEntry{
	protected:
		ofxTI_HubXml* hub;
		ofxXmlSettings xml;
		string xmlFileName;
		vector<string> services;
	
		void pushEntryTag();
		void popEntryTag();
		
	public:
		ofxTI_HubXmlEntry(ofxTI_HubXml* hub, string xmlFileName="");
		~ofxTI_HubXmlEntry();
	
		bool loadXml(string xmlFileName);
		bool saveXml(string xmlFileName="");
	
		bool postToHub();
	
		void addService(string service);
	
		void addTextNode(string value, string name="");
		void addImageNode(string fileName, string name="");
		void addImageNode(ofImage image, string name="");
	
		string getXmlFileName();
	
		void setHub(ofxTI_HubXml* hub);
};

#endif
