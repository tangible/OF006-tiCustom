/*
 *  ColourFader.h
 *  openFrameworks
 *
 *  Created by Pat Long on 19/05/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _COLOUR_FADER_H
#define _COLOUR_FADER_H

#include "ofMain.h"

#include "ColourRGBA.h"

#include <vector>
using namespace std;

class ColourFader{
	private:
		class FaderColourRGBA{
			private:
				ColourRGBA *colour1, *colour2, *increments;
				int totalFrames, currFrame;
			
				void init(ColourRGBA* colour1, ColourRGBA* colour2, int transitionFrames);
				void calculateIncrements();
			
			public:
				FaderColourRGBA(ColourRGBA* colour1, ColourRGBA* colour2, int transitionFrames);
				~FaderColourRGBA();
			
				float getPercent();
				ColourRGBA* getCurrentColour(ColourRGBA* colour);
				float nextFrame();
			
				void reset();
		};
	
		ColourRGBA defaultColour, currentColour;
		vector<FaderColourRGBA*> transitions;
		int currFader;
	
		void init();
		int nextTransition();
		
	public:
		ColourFader();
		~ColourFader();

//		FaderColourRGBA* addColour(ColourRGBA* colour1);
		FaderColourRGBA* addTransition(ColourRGBA* colour1, ColourRGBA* colour2, int transitionFrames);
		void setDefaultColour(float r, float g, float b, float a=255.0);
		void setDefaultColour(ColourRGBA* colour);
	
		ColourRGBA* getCurrentColour();
		ColourRGBA* getNextColour();
		ColourRGBA* getColourAtPercent(float percent);
		ColourRGBA* activateCurrent();
		ColourRGBA* activateCurrentAndAdvance();
};

#endif
