/*
 *  ofFBOTexture.cpp
 *  openFrameworks
 *
 *  Created by Zach Gage on 3/28/08.
 *  Copyright 2008 STFJ.NET. All rights reserved.
 *
 */

#include "ofxFBOTexture.h"

ofFBOTexture::~ofFBOTexture(){
	this->clean();
}

void ofFBOTexture::allocate(int w, int h, bool autoC, bool transparentBG)
{
	this->transparentBG = transparentBG;
	invertY = true;
	
    texData.width = w;
    texData.height = h;

	//-------------ofTexture-------------
	// 	can pass in anything (320x240) (10x5)
	// 	here we make it power of 2 for opengl (512x256), (16x8)

    if (GLEE_ARB_texture_rectangle){
    	texData.tex_w = w;
    	texData.tex_h = h;
    	texData.textureTarget = GL_TEXTURE_RECTANGLE_ARB;   // zach  -- added to get texture target right
    } else {
    	texData.tex_w = ofNextPow2(w);
    	texData.tex_h = ofNextPow2(h);
    }

	if (GLEE_ARB_texture_rectangle){
		texData.tex_t = w;
		texData.tex_u = h;
	} else {
		texData.tex_t = 1.0f;
		texData.tex_u = 1.0f;
	}
	
//	texData.tex_w = w;
//	texData.tex_h = h;
//	texData.textureTarget = GL_TEXTURE_2D;

	// attempt to free the previous bound texture, if we can:
	clean();

	texData.width = w;
	texData.height = h;
	texData.bFlipTexture = true;

	//--FBOTexture-------------------
	autoClear = autoC;
	// Setup our FBO
	glGenFramebuffersEXT(1, &fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

	// Create the render buffer for depth
	glGenRenderbuffersEXT(1, &depthBuffer);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthBuffer);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, texData.tex_w, texData.tex_h);

	// Now setup a texture to render to
	glGenTextures(1, (GLuint *)texData.textureName);   // could be more then one, but for now, just one

	glEnable(texData.textureTarget);

	glBindTexture(texData.textureTarget, (GLuint)texData.textureName[0]);
		glTexImage2D(texData.textureTarget, 0, GL_RGBA, texData.tex_w, texData.tex_h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);  // init to black... 
		glTexParameterf(texData.textureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(texData.textureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(texData.textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(texData.textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// attach it to the FBO so we can render to it

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, texData.textureTarget, (GLuint)texData.textureName[0], 0);


	// Attach the depth render buffer to the FBO as it's depth attachment
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthBuffer);


	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if(status != GL_FRAMEBUFFER_COMPLETE_EXT)
	{
		cout<<"glBufferTexture failed to initialize. Perhaps your graphics card doesnt support the framebuffer extension? If you are running osx prior to system 10.5, that could be the cause"<<endl;
		std::exit(1);
	}
	
	clear();

	glFlush();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);	// Unbind the FBO for now

	glDisable(texData.textureTarget);
	
}


void ofFBOTexture::setupScreenForMe(){

	int w = texData.width;
	int h = texData.height;

	float halfFov, theTan, screenFov, aspect;
	screenFov 		= 60.0f;

	float eyeX 		= (float)w / 2.0;
	float eyeY 		= (float)h / 2.0;
	halfFov 		= PI * screenFov / 360.0;
	theTan 			= tanf(halfFov);
	float dist 		= eyeY / theTan;
	float nearDist 	= dist / 10.0;	// near / far clip plane
	float farDist 	= dist * 10.0;
	aspect 			= (float)w/(float)h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(screenFov, aspect, nearDist, farDist);
	gluLookAt(eyeX, eyeY, dist, eyeX, eyeY, 0.0, 0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glScalef(1, 1*(this->invertY?-1.0:1.0), 1);           // invert Y axis so increasing Y goes down.
  	glTranslatef(0, h*(this->invertY?-1.0:1.0), 0);       // shift origin up to upper-left corner.

    glViewport(0,0,texData.width, texData.height);

}

void ofFBOTexture::setupScreenForThem(){

    int w, h;

	w = glutGet(GLUT_WINDOW_WIDTH);
	h = glutGet(GLUT_WINDOW_HEIGHT);

	float halfFov, theTan, screenFov, aspect;
	screenFov 		= 60.0f;

	float eyeX 		= (float)w / 2.0;
	float eyeY 		= (float)h / 2.0;
	halfFov 		= PI * screenFov / 360.0;
	theTan 			= tanf(halfFov);
	float dist 		= eyeY / theTan;
	float nearDist 	= dist / 10.0;	// near / far clip plane
	float farDist 	= dist * 10.0;
	aspect 			= (float)w/(float)h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(screenFov, aspect, nearDist, farDist);
	gluLookAt(eyeX, eyeY, dist, eyeX, eyeY, 0.0, 0.0, 1.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glScalef(1, -1, 1);           // invert Y axis so increasing Y goes down.
  	glTranslatef(0, -h, 0);       // shift origin up to upper-left corner.


    glViewport(0,0,w, h);
}

void ofFBOTexture::swapIn()
{

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo); // bind the FBO to the screen so we can draw to it

	if(autoClear)
	{
		clear();
	}
	this->swappedIn = true;

	// Save the view port and set it to the size of the texture
}

void ofFBOTexture::swapOut()
{
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0); //unbind the FBO
	this->swappedIn = false;
}

bool ofFBOTexture::isSwappedIn(){
	return this->swappedIn;
}

unsigned char* ofFBOTexture::getPixels(unsigned char* pixels, int x, int y, int w, int h){
	if(w <= 0 || w > this->getWidth())
		w = this->getWidth();
	if(h <= 0 || h > this->getHeight())
		h = this->getHeight();
	if(x > this->getWidth())
		x = 0;
	if(y > this->getHeight())
		y = 0;
	if((x + w) > this->getWidth())
		x -= (x+w-this->getWidth());
	if((y + h) > this->getHeight())
		y -= (y+h-this->getHeight());
	
	bool shouldSwapOut = false;
	if(!this->isSwappedIn()){
		this->swapIn();
		this->setupScreenForMe();
		shouldSwapOut = true;
	}

	if(invertY){
		unsigned char* tmpPix = new unsigned char[w*h*4];
		glReadPixels(x, this->getHeight()-y-h, w, h, GL_RGBA, GL_UNSIGNED_BYTE, tmpPix);
		for(int i=0; i < h; i++)
			memcpy(pixels+(i*w*4), tmpPix+((h-i-1)*w*4), w*4);
		delete [] tmpPix;
		tmpPix = NULL;
	}
	else
		glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	if(shouldSwapOut){
		this->setupScreenForThem();
		this->swapOut();
	}
	
	return pixels;
}

void ofFBOTexture::clean()
{
	// try to free up the texture memory so we don't reallocate
	// http://www.opengl.org/documentation/specs/man_pages/hardcopy/GL/html/gl/deletetextures.html
	if (texData.textureName[0] != 0){
		glDeleteFramebuffersEXT(1, &fbo);
		glDeleteRenderbuffersEXT(1, &depthBuffer);
		glDeleteTextures(1, (GLuint *)texData.textureName);
	}
	texData.width = 0;
	texData.height = 0;
	texData.bFlipTexture = false;
}

void ofFBOTexture::clear()
{
	glClearColor(bgColor[0], bgColor[1], bgColor[2], this->transparentBG?0.0f:bgColor[3]);
//	glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
}

void ofFBOTexture::clear(int x, int y, int w, int h){
	glScissor(x, this->getHeight()-y-h, w, h);
	glEnable(GL_SCISSOR_TEST);
	this->clear();
	glDisable(GL_SCISSOR_TEST);
}

void ofFBOTexture::setClearColor(int r, int g, int b, int a){
	clearColor.r = (float)r / 255.0f;
	clearColor.g = (float)g / 255.0f;
	clearColor.b = (float)b / 255.0f;
	clearColor.a = (float)a / 255.0f;
}

void ofFBOTexture::setInvertY(bool invertY){
	this->invertY = invertY;
}
